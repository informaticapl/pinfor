var timer, seconds = 10;//300;

function getFechaHora() {
    /* Actualiza cada segundo la fecha y hora del sistema*/
    var meses = new Array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
    var dias = new Array("Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado");
    var fecha = new Date();
    var diaSem = dias[fecha.getDay()];
    var diaMes = fecha.getDate();
    var mes = meses[fecha.getMonth()];
    var agno = fecha.getFullYear();
    var hora = fecha.toLocaleTimeString();
    var fecha_hora = diaSem + ", " + diaMes + " de " + mes + " de " + agno + " - " + hora;
    document.getElementById('fecha').value = fecha_hora;

    setTimeout('getFechaHora()', 1000);
}

PrimeFaces.locales['es'] = {
    closeText: 'Cerrar',
    prevText: 'Anterior',
    nextText: 'Siguiente',
    monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
    monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
    dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
    dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
    dayNamesMin: ['D', 'L', 'M', 'X', 'J', 'V', 'S'],
    weekHeader: 'Semana',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: '',
    timeOnlyTitle: 'Indique la hora',
    timeText: 'Tiempo',
    hourText: 'Hora',
    minuteText: 'Minuto',
    secondText: 'Segundo',
    currentText: 'Ahora',
    ampm: false,
    month: 'Mes',
    week: 'Semana',
    day: 'Día',
    allDayText: 'Todo el día'
};

function miembros() {
    $(document).on('click', '.selectedMiembro', function () {
        var selectedItem = $(this).closest('.miembros').find('.ui-orderlist-item.ui-state-highlight');
        if (selectedItem.length > 0) {
            setMiembro([{
                    name: 'selectedIndex',
                    value: selectedItem.closest('ul').find('li').index(selectedItem)
                }]);
        }
    });
}

function closeSessionTimer() {
    if (seconds < 0) {
        document.getElementById("fCloseSession:bnCloseSessionNow").click();
    } else {
        document.getElementById('minutos').value = toMMSS(seconds) + ' minutos';
        seconds--;
        timer = setTimeout("closeSessionTimer()", 1000);
    }
}

function stopCountDown() {
    clearTimeout(timer);
    seconds = 300;
}

function toMMSS(secs) {
    var sec_num = parseInt(secs, 10); // don't forget the second param
    var hours = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    var seconds = sec_num - (hours * 3600) - (minutes * 60);

    if (hours < 10) {
        hours = "0" + hours;
    }
    if (minutes < 10) {
        minutes = "0" + minutes;
    }
    if (seconds < 10) {
        seconds = "0" + seconds;
    }
    var time = minutes + ':' + seconds;
    return time;
}

function socketListener(message, channel, event) {
    var jsonObj = JSON.parse(message);
    var idUnidad = jsonObj.idUnidad;
    var msg = jsonObj.content;
    if (idUnidad === undefined) {
        PF('growl').renderMessage({
            "summary": "AVISO IMPORTANTE",
            "detail": msg,
            "severity": "info"
        });
    } else {
        var currentUnidad = $("#idUnidad").val();
        var isOficina = $("#isOficina").val();
        if (currentUnidad == idUnidad && isOficina) {
            PF('growl').renderMessage({
                "summary": "NOTIFICACIÓN",
                "detail": msg,
                "severity": "info"
            });
        }
    }
}