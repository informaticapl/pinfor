package org.svq.pol.exception;

public class UserNotAllowedException extends Exception {
    private static final long serialVersionUID = -4118881093507517426L;

    public UserNotAllowedException() {
        super("Usuario no autorizado a utilizar esta aplicación");
    }
}
