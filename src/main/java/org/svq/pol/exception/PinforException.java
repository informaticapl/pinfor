package org.svq.pol.exception;

public class PinforException extends Exception {

    private static final long serialVersionUID = 105013499317600882L;

    public PinforException() {

    }

    public PinforException(Exception exception) {
        super(exception);
    }

    public PinforException(String msg) {
        super(msg);
    }
}
