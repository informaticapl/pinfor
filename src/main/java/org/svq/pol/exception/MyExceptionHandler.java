package org.svq.pol.exception;

import java.util.Iterator;
import javax.faces.FacesException;
import javax.faces.context.ExceptionHandler;
import javax.faces.context.ExceptionHandlerWrapper;
import javax.faces.context.FacesContext;
import javax.faces.event.ExceptionQueuedEvent;

/**
 *
 * @author jjcampac
 */
public class MyExceptionHandler extends ExceptionHandlerWrapper {

    private ExceptionHandler exceptionHandler;

    public MyExceptionHandler(ExceptionHandler exceptionHandler) {
        this.exceptionHandler = exceptionHandler;
    }

    @Override
    public void handle() throws FacesException {
        FacesContext facesContext = FacesContext.getCurrentInstance();

        for (Iterator<ExceptionQueuedEvent> iter = getUnhandledExceptionQueuedEvents().iterator(); iter.hasNext();) {
            Throwable exception = iter.next().getContext().getException(); // There it is!

            // Now do your thing with it. This example implementation merely prints the stack trace.
            exception.printStackTrace();

            // You could redirect to an error page (bad practice).
            // Or you could render a full error page (as OmniFaces does).
            // Or you could show a FATAL faces message.
            // Or you could trigger an oncomplete script.
            // etc..
        }

        getWrapped().handle();
    }
    
//    @Override
//    public void handle() throws FacesException {
//        final Iterator<ExceptionQueuedEvent> queue = getUnhandledExceptionQueuedEvents().iterator();
//
//        while (queue.hasNext()) {
//            ExceptionQueuedEvent item = queue.next();
//            ExceptionQueuedEventContext exceptionQueuedEventContext = (ExceptionQueuedEventContext) item.getSource();
//
//            try {
//                Throwable throwable = exceptionQueuedEventContext.getException();
//                System.err.println("Exception: " + throwable.getMessage());
//
//                FacesContext context = FacesContext.getCurrentInstance();
//                Map<String, Object> requestMap = context.getExternalContext().getRequestMap();
//                NavigationHandler nav = context.getApplication().getNavigationHandler();
//
//                requestMap.put("error-message", throwable.getMessage());
//                requestMap.put("error-stack", throwable.getStackTrace());
//                nav.handleNavigation(context, null, "/error");
//                context.renderResponse();
//
//            } finally {
//                queue.remove();
//            }
//        }
//    }

    @Override
    public ExceptionHandler getWrapped() {
        return exceptionHandler;
    }
}
