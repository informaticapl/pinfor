package org.svq.pol.exception;

public class NoItemSelectedException extends Exception {
    private static final long serialVersionUID = 4457498045932796248L;

    public NoItemSelectedException(String msg) {
        super(msg);
    }
}
