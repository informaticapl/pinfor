package org.svq.pol.http;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;
import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;

/**
 * GesateMovil app Created by jjcampac on 4/06/17.
 */
public class SslOkHttpClient {

    private static final String LOGGER = SslOkHttpClient.class.getName();


    public static OkHttpClient getOkHttpClient(String keystoreType, String keystoreFile, String keystorePass, String truststoreType, String truststoreFile, String truststorePass) throws GeneralSecurityException, IOException, IllegalStateException {

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        TrustManager[] trustManager = getTrustManager(truststoreType, truststoreFile, truststorePass);
//        TrustManager[] trustManager = getAllTrustManager();
        SSLContext sslContext = createSSLContext(trustManager, keystoreType, keystoreFile, keystorePass);
        SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

        return new OkHttpClient.Builder()
                .sslSocketFactory(sslSocketFactory, (X509TrustManager) trustManager[0])
                .readTimeout(10, TimeUnit.SECONDS)
                .connectTimeout(10, TimeUnit.SECONDS)
                .addInterceptor(interceptor)
                //                .hostnameVerifier(new HostnameVerifier() {
                //                    @Override
                //                    public boolean verify(String hostname, SSLSession session) {
                //                        return hostname.substring(hostname.indexOf('.') + 1, hostname.length()).equals("sevilla.org");
                //                    }
                //                })
                .build();
    }

    private static SSLContext createSSLContext(TrustManager[] trustManager, String keystoreType, String keystoreFile, String keystorePass) throws KeyStoreException, IOException, NoSuchAlgorithmException, CertificateException, UnrecoverableKeyException, KeyManagementException {
        // Create the key manager
        KeyStore keyStore = KeyStore.getInstance(keystoreType);
        keyStore.load(new FileInputStream(keystoreFile), keystorePass.toCharArray());

        KeyManagerFactory keyManagerFac = KeyManagerFactory.getInstance("SunX509");
        keyManagerFac.init(keyStore, keystorePass.toCharArray());
        KeyManager[] keyManager = keyManagerFac.getKeyManagers();

        // Initialize SSLContext
        SSLContext sslCon = SSLContext.getInstance("TLS");
        sslCon.init(keyManager, trustManager, null);

        return sslCon;
    }

    // Devuelve un TrustManager que permite confiar sólo en los certificados emitidos por las CAs contenidas en truststoreFile
    private static TrustManager[] getTrustManager(String truststoreType, String truststoreFile, String truststorePass) throws KeyStoreException, IOException, NoSuchAlgorithmException, CertificateException {
        // Create the trust manager
        KeyStore trustStore = KeyStore.getInstance(truststoreType);
        trustStore.load(new FileInputStream(truststoreFile), truststorePass.toCharArray());
        TrustManagerFactory trustManagerFac = TrustManagerFactory.getInstance("SunX509");
        trustManagerFac.init(trustStore);
        return trustManagerFac.getTrustManagers();
    }

    // Devuelve un TrustManager que permite confiar en certificados emitidos por cualquier CA
    private static TrustManager[] getAllTrustManager() throws KeyStoreException, NoSuchAlgorithmException {
        TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
        trustManagerFactory.init((KeyStore) null);
        TrustManager[] trustManagers = trustManagerFactory.getTrustManagers();
        if (trustManagers.length != 1 || !(trustManagers[0] instanceof X509TrustManager)) {
            throw new IllegalStateException("Unexpected default trust managers:" + Arrays.toString(trustManagers));
        }
        return trustManagers;
    }

    private static KeyStore newEmptyKeyStore(char[] password) throws GeneralSecurityException {
        try {
            KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
            InputStream in = null; // By convention, 'null' creates an empty key store.
            keyStore.load(in, password);
            return keyStore;
        } catch (IOException e) {
            throw new AssertionError(e);
        }
    }
}
