package org.svq.pol.http;

import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.nio.charset.UnsupportedCharsetException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.Date;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicHeader;
import org.apache.log4j.Logger;
import org.svq.pol.json.GesateRestResponse;
import org.svq.pol.pinfor.bean.MyEntity;
import org.svq.pol.pinfor.utility.Constants;
import org.svq.pol.pinfor.utility.Log;
import org.svq.pol.pinfor.utility.serializer.JsonDateDeserializer;
import org.svq.pol.pinfor.utility.serializer.JsonDoubleDeserializer;

/**
 *
 * @author jjcampac
 * @param <T>
 */
public class HttpCaller<T> implements Serializable {

    private static final Logger LOGGER = Logger.getLogger(HttpCaller.class);

    private static final String KEYSTORE_TYPE, TRUSTSTORE_TYPE;
    private static final String KEYSTORE_FILE, TRUSTSTORE_FILE;
    private static final String KEYSTORE_PASS, TRUSTSTORE_PASS;
    private static final long serialVersionUID = 1860587933683359492L;

    static {
        KEYSTORE_TYPE = Constants.KEYSTORE_GAT_TYPE;
        KEYSTORE_FILE = Constants.KEYSTORE_GAT_PATH + Constants.KEYSTORE_GAT_FILE;
        KEYSTORE_PASS = Constants.KEYSTORE_GAT_PASSWORD;
        TRUSTSTORE_TYPE = Constants.TRUSTSTORE_GAT_TYPE;
        TRUSTSTORE_FILE = Constants.KEYSTORE_GAT_PATH + Constants.TRUSTSTORE_GAT_FILE;
        TRUSTSTORE_PASS = Constants.TRUSTSTORE_GAT_PASSWORD;
    }

    public GesateRestResponse doGet(String url, TypeToken<T> responseType) throws IOException, KeyStoreException, NoSuchAlgorithmException, CertificateException, UnrecoverableKeyException, KeyManagementException {
        GesateRestResponse result;
        HttpsClient httpsClient = new HttpsClient(KEYSTORE_TYPE, KEYSTORE_FILE, KEYSTORE_PASS, TRUSTSTORE_TYPE, TRUSTSTORE_FILE, TRUSTSTORE_PASS);

        url = url.replace(" ", "%20");
        HttpGet httpGet = new HttpGet(url);
        httpGet.setHeader("Content-Type", "application/json; charset=utf-8");
        HttpResponse response = httpsClient.execute(httpGet);
        result = getGesateResponse(response, responseType);

        return result;
    }

    public GesateRestResponse doPost(String url, MyEntity entity, TypeToken<T> responseType) {
        GesateRestResponse result = new GesateRestResponse();
        try {
            HttpsClient httpsClient = new HttpsClient(KEYSTORE_TYPE, KEYSTORE_FILE, KEYSTORE_PASS, TRUSTSTORE_TYPE, TRUSTSTORE_FILE, TRUSTSTORE_PASS);

            url = url.replace(" ", "%20");
            HttpPost httpPost = new HttpPost(url);
            httpPost.setHeader("Content-Type", "application/json; charset=utf-8");
            StringEntity stringEntity = new StringEntity(entity.getJson(), StandardCharsets.UTF_8);
            stringEntity.setContentEncoding(new BasicHeader(StandardCharsets.UTF_8.name(), "application/json"));
            httpPost.setEntity(stringEntity);

            HttpResponse response = httpsClient.execute(httpPost);
            result = getGesateResponse(response, responseType);
        } catch (IOException | IllegalStateException | KeyStoreException | NoSuchAlgorithmException | CertificateException | UnrecoverableKeyException | KeyManagementException | UnsupportedCharsetException ex) {
            result.setStatus(Constants.RESULT_FAIL);
            result.setMessage(ex.getCause().getMessage());
            Log.log(LOGGER, Constants.ERROR, ex.getMessage());
        }
        return result;
    }

    public GesateRestResponse doDelete(String url, TypeToken<T> responseType) {

        GesateRestResponse result = new GesateRestResponse();
        try {
            HttpsClient httpsClient = new HttpsClient(KEYSTORE_TYPE, KEYSTORE_FILE, KEYSTORE_PASS, TRUSTSTORE_TYPE, TRUSTSTORE_FILE, TRUSTSTORE_PASS);
            url = url.replace(" ", "%20");
            HttpDelete httpDelete = new HttpDelete(url);
            httpDelete.setHeader("Content-Type", "application/json; charset=utf-8");

            HttpResponse response = httpsClient.execute(httpDelete);
            result = getGesateResponse(response, responseType);

        } catch (IOException ex) {
            result.setStatus(Constants.RESULT_FAIL);
            result.setMessage(ex.getMessage());
            Log.log(LOGGER, Constants.ERROR, ex.getMessage());
        } catch (KeyStoreException | NoSuchAlgorithmException | CertificateException | UnrecoverableKeyException | KeyManagementException ex) {
            result.setStatus(Constants.RESULT_FAIL);
            result.setMessage(ex.getCause().getMessage());
            Log.log(LOGGER, Constants.ERROR, ex.getMessage());
        }
        return result;
    }

    public GesateRestResponse doPut(String url, MyEntity entity, TypeToken<T> responseType) {
        GesateRestResponse result = new GesateRestResponse();
        try {
            HttpsClient httpsClient = new HttpsClient(KEYSTORE_TYPE, KEYSTORE_FILE, KEYSTORE_PASS, TRUSTSTORE_TYPE, TRUSTSTORE_FILE, TRUSTSTORE_PASS);

            url = url.replace(" ", "%20");
            HttpPut httpPut = new HttpPut(url);
            httpPut.setHeader("Content-Type", "application/json; charset=utf-8");
            StringEntity stringEntity = new StringEntity(entity.getJson(), StandardCharsets.UTF_8);
            stringEntity.setContentEncoding(new BasicHeader(StandardCharsets.UTF_8.name(), "application/json"));
            httpPut.setEntity(stringEntity);

            HttpResponse response = httpsClient.execute(httpPut);
            result = getGesateResponse(response, responseType);
        } catch (IOException ex) {
            result.setStatus(Constants.RESULT_FAIL);
            result.setMessage(ex.getMessage());
            Log.log(LOGGER, Constants.ERROR, ex.getMessage());
        } catch (KeyStoreException | NoSuchAlgorithmException | CertificateException | UnrecoverableKeyException | KeyManagementException | UnsupportedCharsetException ex) {
            result.setStatus(Constants.RESULT_FAIL);
            result.setMessage(ex.getCause().getMessage());
            Log.log(LOGGER, Constants.ERROR, ex.getMessage());
        }
        return result;
    }

    //<editor-fold desc="private methods">
    private GesateRestResponse getGesateResponse(HttpResponse response, TypeToken<T> responseType) throws IOException, IllegalStateException {
        GesateRestResponse result = new GesateRestResponse<>();

        if (response != null) {
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                response.setHeader("Content-Type", "application/json;charset=UTF-8");
                HttpEntity entity = response.getEntity();
                Reader reader = new InputStreamReader(entity.getContent());

                GsonBuilder builder = new GsonBuilder();
//                builder.registerTypeAdapter(Boolean.class, new JsonBooleanDeserializer());
                builder.registerTypeAdapter(Date.class, new JsonDateDeserializer());
                builder.registerTypeAdapter(Double.class, new JsonDoubleDeserializer());
                builder.disableHtmlEscaping();
                result = builder.create().fromJson(reader, responseType.getType());
            } else {
                result.setStatus(response.getStatusLine().getStatusCode());
                result.setMessage(response.getStatusLine().getReasonPhrase());
            }
        }

        return result;
    }
    //</editor-fold>

}
