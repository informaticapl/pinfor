package org.svq.pol.http;

import java.util.HashMap;

/**
 *
 * @author jjcampac
 */
public class Config {

    private static Config instance;
    private final HashMap<String, Object> map;

    private Config() {
        this.map = new HashMap<>();
    }

    public static Config getInstance() {
        if (instance == null) {
            instance = new Config();
        }
        return instance;
    }

    public void setKey(String key, Object value) {
        map.put(key, value);
    }

    public Object getKey(String key) {
        return map.get(key);
    }

    public void eraseConfig() {
        this.map.clear();
    }
}
