package org.svq.pol.http;

import java.io.FileInputStream;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HttpContext;

/**
 *
 * @author jjcampac
 */
public class HttpsClient implements HttpClient {

    private String keystoreType;
    private String keystoreFile;
    private String keystorePass;
    private SSLContext sslContext;
    private HttpClient httpsClient;

//<editor-fold defaultstate="collapsed" desc="constructors">
    HttpsClient() {
    }

    public HttpsClient(String keystoreType, String keystoreFile, String keystorePass, String truststoreType, String truststoreFile, String truststorePass) throws KeyStoreException, IOException, NoSuchAlgorithmException, CertificateException, UnrecoverableKeyException, KeyManagementException {
        this.sslContext = createSSLContext(keystoreType, keystoreFile, keystorePass, truststoreType, truststoreFile, truststorePass);
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="getters & setters">
    public String getKeystoreFile() {
        return keystoreFile;
    }

    public void setKeystoreFile(String keystoreFile) {
        this.keystoreFile = keystoreFile;
    }

    public String getKeystorePass() {
        return keystorePass;
    }

    public void setKeystorePass(String keystorePass) {
        this.keystorePass = keystorePass;
    }

    public String getKeystoreType() {
        return keystoreType;
    }

    public void setKeystoreType(String keystoreType) {
        this.keystoreType = keystoreType;
    }
//</editor-fold>

    
//<editor-fold defaultstate="collapsed" desc="private methods">
    private SSLContext createSSLContext(String keystoreType, String keystoreFile, String keystorePass, String truststoreType, String truststoreFile, String truststorePass) throws KeyStoreException, IOException, NoSuchAlgorithmException, CertificateException, UnrecoverableKeyException, KeyManagementException {
        // Create the key manager
        KeyStore keyStore = KeyStore.getInstance(keystoreType);
        keyStore.load(new FileInputStream(keystoreFile), keystorePass.toCharArray());

        KeyManagerFactory keyManagerFac = KeyManagerFactory.getInstance("SunX509");
        keyManagerFac.init(keyStore, keystorePass.toCharArray());
        KeyManager[] km = keyManagerFac.getKeyManagers();

        // Create the trust manager
        KeyStore trustStore = KeyStore.getInstance(truststoreType);
        trustStore.load(new FileInputStream(truststoreFile), truststorePass.toCharArray());
        TrustManagerFactory trustManagerFac = TrustManagerFactory.getInstance("SunX509");
        trustManagerFac.init(trustStore); 
        TrustManager[] tm = trustManagerFac.getTrustManagers();

        // Initialize SSLContext
        SSLContext sslCon = SSLContext.getInstance("TLS");
        sslCon.init(km, tm, null);

        return sslCon;
    }
//</editor-fold>

    @Override
    public HttpParams getParams() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ClientConnectionManager getConnectionManager() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public HttpResponse execute(HttpUriRequest request) throws IOException, ClientProtocolException {
        httpsClient = HttpClients.custom().setSslcontext(this.sslContext).build();
        return httpsClient.execute(request);
    }

    @Override
    public HttpResponse execute(HttpUriRequest request, HttpContext context) throws IOException, ClientProtocolException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public HttpResponse execute(HttpHost target, HttpRequest request) throws IOException, ClientProtocolException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public HttpResponse execute(HttpHost target, HttpRequest request, HttpContext context) throws IOException, ClientProtocolException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public <T> T execute(HttpUriRequest request, ResponseHandler<? extends T> responseHandler) throws IOException, ClientProtocolException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public <T> T execute(HttpUriRequest request, ResponseHandler<? extends T> responseHandler, HttpContext context) throws IOException, ClientProtocolException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public <T> T execute(HttpHost target, HttpRequest request, ResponseHandler<? extends T> responseHandler) throws IOException, ClientProtocolException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public <T> T execute(HttpHost target, HttpRequest request, ResponseHandler<? extends T> responseHandler, HttpContext context) throws IOException, ClientProtocolException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
