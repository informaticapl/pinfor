package org.svq.pol.json;

import java.io.Serializable;
import java.util.List;
import org.svq.pol.pinfor.utility.Constants;

/**
 *
 * @author jjcampac
 * @param <T>
 */
public class GesateRestResponse<T> implements Serializable {
    private static final long serialVersionUID = -69838807471119041L;

    private int status;
    private String message;
    private String tag;
    private T entity;
    private List<T> entities;

    //<editor-fold desc="constructors">
    public GesateRestResponse() {
        this.status = Constants.RESULT_OK;
    }

    public GesateRestResponse(int status) {
        this.status = status;
    }

    private GesateRestResponse(int status, String message) {
        this(status);
        this.message = message;
    }

    public GesateRestResponse(int status, String message, T entity) {
        this(status, message);
        this.entity = entity;
    }
    //</editor-fold>

    //<editor-fold desc="getters & setters">
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public T getEntity() {
        return entity;
    }

    public void setEntity(T entity) {
        this.entity = entity;
    }

    public List<T> getEntities() {
        return entities;
    }

    public void setEntities(List<T> entities) {
        this.entities = entities;
    }
    //</editor-fold>

}
