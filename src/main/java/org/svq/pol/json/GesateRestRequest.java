package org.svq.pol.json;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import org.apache.log4j.Logger;

/**
 *
 * @author jjcampac
 */
public class GesateRestRequest {

    private static final Logger LOGGER = Logger.getLogger(GesateRestRequest.class);
    private String host;
    private String port;
    private String alias;
    private File keystoreFile;
    private String keystoreType;
    private String keystorePass;


    private SSLContext createSSLContext() throws KeyStoreException, IOException, NoSuchAlgorithmException, CertificateException, UnrecoverableKeyException, KeyManagementException {
        KeyStore keyStore = KeyStore.getInstance(keystoreType);
        keyStore.load(new FileInputStream(keystoreFile), keystorePass.toCharArray());

        // Create the key manager
        KeyManagerFactory keyManagerFac = KeyManagerFactory.getInstance("SunX509");
        keyManagerFac.init(keyStore, keystorePass.toCharArray());
        KeyManager[] keyManager = keyManagerFac.getKeyManagers();

        // Create the trust manager
        TrustManagerFactory trustManagerFac = TrustManagerFactory.getInstance("SunX509");
        trustManagerFac.init(keyStore);
        TrustManager[] trustManager = trustManagerFac.getTrustManagers();

        // Initialize SSLContext
        SSLContext sslContext = SSLContext.getInstance("TLSv1");
        sslContext.init(keyManager, trustManager, null);

        return sslContext;
    }
}
