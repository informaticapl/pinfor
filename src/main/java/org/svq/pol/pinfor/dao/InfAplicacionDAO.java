package org.svq.pol.pinfor.dao;

import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.svq.pol.exception.PinforException;
import org.svq.pol.pinfor.bean.InfAplicacion;
import org.svq.pol.pinfor.utility.HibernateUtil;

/**
 * La clase InfAplicacionDAO es la encargada de efectuar el acceso a la base de
 datos, manipulando los datos de forma conveniente y ocultando toda la capa de
 acceso a datos.
 *
 * @author jjcamacho
 */
final class InfAplicacionDAO {

    private InfAplicacionDAO() {
    }

    /**
     * Devuelve una lista con todas las aplicaciones disponibles
     *
     * @return Lista de aplicaciones
     */
    public static List<InfAplicacion> getAllApplications() {
        List<InfAplicacion> result = null;
        Session session = HibernateUtil.getSession();
        try {
            HibernateUtil.getTransaction();
            Query qry = session.createQuery("from Aplicacion");
            result = qry.list();
        } finally {
            HibernateUtil.closeSession();
        }
        return result;
    }

    /**
     * Dados el alias o el nombre de una aplicacion, la función devuelve una
     * lista de todas aquéllas que cumplen la condición de que las cadenas
     * recibidas como parámetros son parte de los campos correspondientes en la
     * tabla aplicaciones.
     *
     * @param alias String que identifica al alias que se busca (se aplica el
     * operador SQL like)
     * @param nombre String que identifica al nombre de la aplicación que se
     * busca (se aplica el operador SQL <code>like</code>)
     * @return <code>List</code> - Lista de aplicaciones.
     */
    public static List<InfAplicacion> getAplicacion(String alias, String nombre) {
        List<InfAplicacion> result = null;
        Session session = HibernateUtil.getSession();
        try {
            HibernateUtil.getTransaction();
            Criteria crit = session.createCriteria(InfAplicacion.class);
            if (!alias.isEmpty()) {
                crit.add(Restrictions.ilike("alias", '%' + alias + '%'));
            }
            if (!nombre.isEmpty()) {
                crit.add(Restrictions.ilike("nombre", '%' + nombre + '%'));
            }
            result = crit.list();
        } finally {
            HibernateUtil.closeSession();
        }
        return result;
    }

    /**
     * Devuelve la aplicación cuyo id coincide con el id aportado como parámetro
     *
     * @param id Long que identifica al campo id de la aplicación (por tanto,
     * único).
     * @return <code>InfAplicacion</code> - Objeto aplicación.
     */
    public static InfAplicacion getAplicacion(Long id) {
        InfAplicacion result = null;
        Session session = HibernateUtil.getSession();
        try {
            HibernateUtil.getTransaction();
            Criteria crit = session.createCriteria(InfAplicacion.class);
            if (id
                    != null) {
                crit.add(Restrictions.eq("idAplicacion", id));
            }
            result = (InfAplicacion) crit.uniqueResult();
        } finally {
            HibernateUtil.closeSession();
        }
        return result;
    }

    /**
     * Devuelve la aplicación cuyo nombre se corresponde con el aportado como
     * parámetro.
     *
     * @param alias - Alias de la aplicación que se quiere obtener
     * @return InfAplicacion
     */
    public static InfAplicacion getApplicationByAlias(String alias) {
        InfAplicacion result = null;
        Session session = HibernateUtil.getSession();
        try {
            HibernateUtil.getTransaction();
            Criteria crit = session.createCriteria(InfAplicacion.class);
            crit.add(Restrictions.eq("alias", alias));
            result = (InfAplicacion) crit.uniqueResult();
        } finally {
            HibernateUtil.closeSession();
        }
        return result;
    }

    /**
     * Devuelve una lista de objetos de la clase InfAplicacion que se corresponden
 con todos los atributos del objeto recibido como parámetro.
     *
     * @param patronBusqueda Parámetro de la clase Aplicación que contiene el
     * patrón de la búsqueda.
     * @return Lista de objetos de la clase InfAplicacion que corresponden con el
 patrón recibido como parámetro
     */
    public static List<InfAplicacion> search(InfAplicacion patron) {
        List<InfAplicacion> result = null;
        Session session = HibernateUtil.getSession();
        try {
            HibernateUtil.getTransaction();
            String hql = "select app "
                    + "from Aplicacion app "
                    + "where lower(app.nombre) like :nombre "
                    + "  and lower(app.alias) like :alias "
                    + "  and lower(app.descripcion) like :descripcion";
            Query qry = session.createQuery(hql);
            qry.setParameter("nombre", patron.getNombre().isEmpty() ? "%" : patron.getNombre().toLowerCase());
            qry.setParameter("alias", patron.getAlias().isEmpty() ? "%" : patron.getAlias().toLowerCase());
            qry.setParameter("descripcion", patron.getDescripcion().isEmpty() ? "%" : patron.getDescripcion().toLowerCase());
            result = qry.list();
        } finally {
            HibernateUtil.closeSession();
        }
        return result;
    }

    /**
     * Método encargado de hacer persistente el objeto recibido como parámetro.
     *
     * @param aplicacion Objeto de la clase <code>Aplicación</code> que se hará
     * persistente en la base de datos y que previamente no existía.
     * @throws HibernateException Excepción elevada cuando se produce un error
     * en la gestión de Hibernate. Aunque es capturada para devolver la base de
     * datos a su estado anterior (rollback), la excepción es vuelta a lanzar
     * para su posterior tratamiento.
     */
    public static void save(InfAplicacion aplicacion)  {
        Session session = HibernateUtil.getSession();
        Transaction tx = null;
        try {
            tx = HibernateUtil.getTransaction();
            session.saveOrUpdate(aplicacion);
            tx.commit();
        } catch (Exception he) {
            if (tx != null) {
                tx.rollback();
            }
            throw he;
        } finally {
            HibernateUtil.closeSession();
        }
    }

    /**
     * El método se encarga de eliminar de la base de datos el objeto recibido
     * como parámetro.
     *
     * @param aplicacion Objeto de la clase <code>InfAplicacion</code> que dejará
     * de ser persistente.
     * @throws HibernateException Excepción elevada cuando se produce un error
     * en la gestión de Hibernate. Aunque es capturada para devolver la base de
     * datos a su estado anterior (rollback), la excepción es vuelta a lanzar
     * para su posterior tratamiento.
     */
    public static void delete(InfAplicacion aplicacion) throws PinforException {
        Session session = HibernateUtil.getSession();
        Transaction tx = null;
        try {
            tx = HibernateUtil.getTransaction();
            String sql = "from ContenidoPerfil where aplicacion.idAplicacion = :idAplicacion";
            Query qry = session.createQuery(sql);
            qry.setParameter("idAplicacion", aplicacion.getIdAplicacion());
            if (qry.list().isEmpty()) {
                session.delete(aplicacion);
                tx.commit();
            } else {
                throw new PinforException("No puede eliminar la aplicación seleccionada porque hay perfiles que la incluyen.");
            }
        } catch (Exception ex) {
            if (tx != null) {
                tx.rollback();
            }
            throw new PinforException(ex);
        } finally {
            HibernateUtil.closeSession();
        }
    }
}
