package org.svq.pol.pinfor.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Filter;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.svq.pol.exception.NoItemSelectedException;
import org.svq.pol.pinfor.bean.GenLista;
import org.svq.pol.pinfor.bean.GenSessionUser;
import org.svq.pol.pinfor.bean.PerPersona;
import org.svq.pol.pinfor.bean.auxiliar.PatronBusqueda;
import org.svq.pol.pinfor.utility.Constants;
import org.svq.pol.pinfor.utility.HibernateUtil;

/**
 * La clase PerUsuarioDAO es la encargada de efectuar el acceso a la base de
 * datos, manipulando los datos de forma conveniente y ocultando toda la capa de
 * acceso a datos.
 *
 * @author jjcamacho
 */
final class PerUsuarioDAO {

    private static final String[] TIPOS = {"UNI", "SUB", "ADM", "LAB", "MED", "CAT", "GFS", "SFS", "TUR", "CTR", "VPT", "RPT", "UNF"};
    private static List<GenLista> lista;
    private static final String FILTER_SITUACION = "situacionFilter";
    private static final String FILTER_FECHA_APLICACION = "fechaAplicacionFilter";
    private static final String USUARIO = "usuario";
    private static final String PERSONA = "per";

    static {
        try {
            lista = GenListasDAO.getListasGenerales(TIPOS);
        } catch (Exception ignore) {
        }
    }

    private PerUsuarioDAO() {
        super();
    }

    /**
     * Devuelve un objeto PerPersona correspondiente al indicativo y/usuario
     * pasados como parámetros.
     *
     * @param patronBusqueda
     * @param detalle
     * @return
     */
    public static PerPersona getUser(PatronBusqueda patronBusqueda, int detalle) {
        PerPersona result = null;
        Session session = HibernateUtil.getSession();
        try {
            HibernateUtil.getTransaction();
            setFiltrosSituaciones(session.enableFilter(FILTER_SITUACION), lista);

            String sql = "select per.*, pla.*, inf.*, uni.* "
                    + "from per_personal per, per_plantilla pla, inf_datos_informaticos inf, per_situaciones uni, per_situaciones ctr "
                    + "where per.id_personal = pla.id_personal "
                    + "  and per.id_personal = inf.id_personal "
                    + "  and per.id_personal = uni.id_personal "
                    + "  and pla.fecha_aplicacion = (select max(pla2.fecha_aplicacion) "
                    + "                              from per_plantilla pla2 "
                    + "                              where pla2.id_personal = pla.id_personal "
                    + "                                and nls_lower(pla.indicativo) like nls_lower(:indicativo)) "
                    + "  and uni.fecha_aplicacion = (select max(uni2.fecha_aplicacion) "
                    + "                              from per_situaciones uni2 "
                    + "                              where uni2.id_personal = uni.id_personal "
                    + "                                and uni2.id_miembro in (select mie.id_miembro "
                    + "                                                        from gen_miembros mie "
                    + "                                                        where mie.codigo = 'UNI') "
                    + "                                and uni2.fecha_aplicacion<=sysdate)"
                    + "  and ctr.fecha_aplicacion = (select max(ctr2.fecha_aplicacion) "
                    + "                              from per_situaciones ctr2 "
                    + "                              where ctr2.id_personal = ctr.id_personal "
                    + "                                and ctr2.id_miembro in (select mie.id_miembro "
                    + "                                                        from gen_miembros mie "
                    + "                                                        where mie.codigo = 'CTR') "
                    + "                                and ctr2.fecha_aplicacion<=sysdate)"
                    + "  and nls_lower(per.usuario) like nls_lower(:usuario) "
                    + "order by to_number(pla.clave_orden)";

            SQLQuery qry = session.createSQLQuery(sql);
            String usuario = patronBusqueda.getUsuario().isEmpty() ? "%" : patronBusqueda.getUsuario().trim();
            String indicativo = patronBusqueda.getIndicativo().isEmpty() ? "%" : patronBusqueda.getIndicativo().trim();
            qry.setParameter(USUARIO, usuario);
            qry.setParameter("indicativo", indicativo);
            qry.addEntity(PERSONA, PerPersona.class);
            setDetalle(detalle, qry, PERSONA);
            qry.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
            result = (PerPersona) qry.uniqueResult();
        } finally {
            HibernateUtil.closeSession();
        }
        return result;
    }

    /**
     * Este metodo permite la obtención de un objeto <code>PerPersona</code> a
     * partir del nombre de usuario. Puesto que se conoce que, si bien este
     * campo de la tabla no constituye la clave primaria del mismo, es único, el
     * método permite la obtención de uno o ningun elemento.
     *
     * @param userName Nombre de usuario de la persona que se busca.
     * @param detalle
     * @return <code>PerPersona</code> - Si existe una persona con el nombre de
     * usuario dado, se devuelve, en caso contrario, se devuelve null.
     */
    public static PerPersona getByUserName(String userName, int detalle) {
        /*
         * Como los conjuntos que hacen referencia a las Unidades, Situaciones
         * Adtva, Médicas, Turnos, etc. de la clase PerPersona en realidad no son
         * más que entradas de una misma tabla (per_situaciones), es neceario
         * obtener un conjunto con todas listas relativas a cada uno de ellos.
         * Con esto conseguimos definir los filtros que la clase PerPersona define
         * para cada uno de los conjuntos. Es el método setFiltrosSituaciones el
         * que, recibiendo la lista, se dedica a establecer los filtros. Por
         * otra parte, la clase PerPersona define dos filtros, uno para hacer
         * búsquedas restringidas a la fecha presente y otro para las
         * situaciones. Como nos intersa que se obtengan los datos actuales del
         * usuario que se está logando (es posible que tenga entradas pendientes
         * de aplicar), debemos activar el filtro relativo a la fecha. Después,
         * se activa el filtro para las situaciones. Este último tiene numerosos
         * parámetros, tantos como listas de situaciones hay.
         */
        PerPersona result = null;
        Session session = HibernateUtil.getSession();

        try {
            HibernateUtil.getTransaction();
            Criteria crit = session.createCriteria(PerPersona.class);
            crit.add(Restrictions.eq(USUARIO, userName).ignoreCase());

            session.enableFilter(FILTER_FECHA_APLICACION).setParameter("paramFechaAplicacion", new Date());
            setFiltrosSituaciones(session.enableFilter(FILTER_SITUACION), lista);
            result = (PerPersona) crit.uniqueResult();
            // Definimos las colecciones que vamos a obtener...
            setDetalle(detalle, result);
        } catch (HibernateException ex) {
            System.out.println(ex.getMessage());
        } finally {
            HibernateUtil.closeSession();
        }
        return result;
    }

    /**
     * Devuelve un objeto de la clase <code>PerPersona</code> cuyo campo
     * identificador de la tabla coincide con el valor del parámetro recibido.
     *
     * @param id Long identificador del objeto que se pretende objener.
     * @param detalle
     * @return <code>PerPersona</code> - Objeto de la clase
     * <code>PerPersona</code> que se corresponde con la fila de la tabla cuyo
     * <code>id</code> coincide con el parámetro recibido.
     */
    public static PerPersona getById(Long id, int detalle) {
        Session session = HibernateUtil.getSession();
        PerPersona result = null;

        try {
            HibernateUtil.getTransaction();
            setFiltrosSituaciones(session.enableFilter(FILTER_SITUACION), lista);
            Criteria crit = session.createCriteria(PerPersona.class);
            crit.add(Restrictions.eq("idPersona", id));
            result = (PerPersona) crit.uniqueResult();
            // Definimos las colecciones que vamos a obtener...
            setDetalle(detalle, result);
        } finally {
            HibernateUtil.closeSession();
        }
        return result;
    }

    /**
     * Metodo que hace persistente al objeto recibido como parámetro.
     *
     * @param persona Objeto de la clase <code>PerPersona</code> que se quiere
     * hacer persistente. Puesto que se aplica el método <code>update</code>, el
     * objeto debe existir previamente en la base de datos.
     */
    public static void save(PerPersona persona) {
        Session session = HibernateUtil.getSession();
        Transaction tx = null;
        try {
            tx = HibernateUtil.getTransaction();
            session.saveOrUpdate(persona);
            tx.commit();
        } catch (Exception ex) {
            if (tx != null) {
                tx.rollback();
            }
            throw ex;
        } finally {
            HibernateUtil.closeSession();
        }
    }

    public static void remove(PerPersona persona) {
        Session session = HibernateUtil.getSession();
        Transaction tx = null;
        try {
            tx = HibernateUtil.getTransaction();
            // Debe hacerse persistente antes de eliminarla
            session.update(persona);
            session.delete(persona);
            tx.commit();
        } catch (Exception ex) {
            if (tx != null) {
                tx.rollback();
            }
            throw ex;
        } finally {
            HibernateUtil.closeSession();
        }
    }

    /**
     * Refresca los datos del usuario con los contenido en la base de datos. Si
     * éstos han sido modificados por otro usuario desde la última vez que el
     * usuario actual los leyó, se cargarán los nuevos datos.
     *
     * @param sessionUser
     * @param busqueda
     * @throws HibernateException
     * @throws Exception
     */
    public static void reset(GenSessionUser sessionUser, PatronBusqueda busqueda) {
        /*
         * Este método se invoca cuando se quieren cancelar las modificaciones
         * que se han hecho modificaciones en un policía. En vez de mantener un
         * objeto temporal con la versión original, se ha optado por volver a
         * efectuar la consulta del policía en cuestión, con lo que no sólo
         * conseguimos volver a cargar los datos originales, sino que podemos
         * obtener posibles actualzaciones hechas por otros usuarios. Para esta
         * recarga se usa el método refresh de la session, siendo necesario
         * volver a establecer los filtros.
         */
        Session session = HibernateUtil.getSession();
        try {
            HibernateUtil.getTransaction();
            if (busqueda.isVerFuturo()) {
                session.disableFilter(FILTER_FECHA_APLICACION);
            } else {
                session.enableFilter(FILTER_FECHA_APLICACION).setParameter("paramFechaAplicacion", new Date());
            }
            Filter filter = session.enableFilter(FILTER_SITUACION);
            setFiltrosSituaciones(filter, lista);
            session.refresh(sessionUser);
        } finally {
            HibernateUtil.closeSession();
        }
    }

    public static List<PerPersona> search(PatronBusqueda busqueda) throws NoItemSelectedException {
        List<PerPersona> result;
        Session session = HibernateUtil.getSession();
        try {
            HibernateUtil.getTransaction();
            /*
             * Dependiendo de si la búsqueda se efectúa se en el futuro o sólo en el
             * presente, deshabilitamos o habilitamos el filtro
             * 'fechaAplicacionFilter'. Sin embargo, el filtro 'situacionFilter'
             * siempre debe habilitarse, ya que es el que permite que las distintas
             * colecciones de la clase PerPersona (véasae la misma) contengan
             * únicamente los datos referidos por cada una y que en realidad se
             * encuentran todos en la misma tabla per_situaciones. Además, es
             * necesario añadir la fecha de aplicación como condición de cada uno de
             * los conjuntos de datos, de ahí la existencia de dos sentencia sql
             * distintas.
             */
            if (busqueda.isVerFuturo()) {
                session.disableFilter(FILTER_FECHA_APLICACION);
            } else {
                session.enableFilter(FILTER_FECHA_APLICACION).setParameter("paramFechaAplicacion", new Date());
            }
            setFiltrosSituaciones(session.enableFilter(FILTER_SITUACION), lista);

            /*
             * Definimos la consulta SQL estableciendo un parámetro para poder
             * establecer la fecha límite de la consulta.
             */
            String sql = "select per.*, pla.*, uni.*, inf.*, tel.* "
                    + "from per_personal per, per_plantilla pla, per_situaciones uni, inf_datos_informaticos inf, inf_telefonos tel "
                    + "where per.id_personal = pla.id_personal "
                    + "  and per.id_personal = uni.id_personal "
                    + "  and per.id_personal = inf.id_personal (+)"
                    + "  and per.id_personal = tel.id_personal (+)"
                    + "  and nls_lower(per.apellido_1) like nls_lower(:apellido1) "
                    + "  and nls_lower(per.apellido_2) like nls_lower(:apellido2) "
                    + "  and uni.fecha_aplicacion = (select max(uni2.fecha_aplicacion) "
                    + "                              from per_situaciones uni2 "
                    + "                              where uni2.id_personal = uni.id_personal "
                    + "                                and uni2.id_miembro in (select mie.id_miembro "
                    + "                                                        from gen_miembros mie "
                    + "                                                        where mie.codigo = 'UNI') "
                    + "                                 %s) "
                    + "  and ctr.fecha_aplicacion = (select max(ctr2.fecha_aplicacion) "
                    + "                              from per_situaciones ctr2 "
                    + "                              where ctr2.id_personal = ctr.id_personal "
                    + "                                and ctr2.id_miembro in (select mie.id_miembro "
                    + "                                                        from gen_miembros mie "
                    + "                                                        where mie.codigo = 'CTR') "
                    + "  and uni.id_miembro like :unidad "
                    + "  and pla.fecha_aplicacion = (select max(pla2.fecha_aplicacion) "
                    + "                              from per_plantilla pla2 "
                    + "                              where pla2.id_personal = pla.id_personal "
                    + "                                and pla.category like :categoria "
                    + "                                %s) "
                    + "  and pla.activo like :activo "
                    + "  and nls_lower(pla.indicativo) like nls_lower(:indicativo) "
                    + "  and nls_lower(per.usuario) like nls_lower(:usuario)"
                    + "  and rownum <= 25";

            //TODO: Modificar el campo pla.category (prod) de la sql por pla.categoria (desa) cuando se ejecute en datapoldes
            /* Sustituimos sólo el penúltimo parámetro %s con la fecha del sistema
             * si las consultas son relativas al presente. En caso contrario, lo
             * dejamos en blanco. Los otros dos parámetros son sustituidos por '%s'
             * para que sigan existiendo en la cadena y poder ser sustituidos más
             * adelante. */
            sql = String.format(sql, busqueda.isVerFuturo() ? "" : "and uni2.fecha_aplicacion<=sysdate", "%s");
            sql = String.format(sql, busqueda.isVerFuturo() ? "" : "and pla2.fecha_aplicacion<=sysdate");
            SQLQuery qry = session.createSQLQuery(sql);

            qry.addEntity(PERSONA, PerPersona.class);
            setDetalle(Constants.BASICO | Constants.UNIDAD | Constants.PLANTILLA | Constants.INFO | Constants.TELEFONO, qry, PERSONA);
            qry.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
            asignarParametros(qry, busqueda);
            result = qry.list();
        } finally {
            HibernateUtil.closeSession();
        }
        return result;
    }

    /**
     * Devuelve una lista de personas cuyo indicativo comienza por el patrón
     * indicado.
     *
     * @param indicativo Cadena que indica cómo debe comenzar el indicativo de
     * la persona buscada.
     * @param detalle
     * @return
     */
    public static List<PerPersona> searchPersonaByIndicativo(String indicativo, int detalle) {
        List<PerPersona> result = new ArrayList<>();
        Session session = HibernateUtil.getSession();
        try {
            HibernateUtil.getTransaction();
            setFiltrosSituaciones(session.enableFilter("situacionFilter"), lista);

            String sql = "select per.*, pla.* "
                    + "from per_personal per, per_plantilla pla "
                    + "where per.id_personal = pla.id_personal "
                    + "  and pla.fecha_aplicacion = (select max(pla2.fecha_aplicacion) "
                    + "                              from per_plantilla pla2 "
                    + "                              where pla2.id_personal = pla.id_personal "
                    + "                              and nls_lower(pla.indicativo) like nls_lower(:indicativo)) "
                    + "order by to_number(pla.clave_orden)";

            SQLQuery qry = session.createSQLQuery(sql);
            qry.setParameter("indicativo", indicativo.trim().concat("%"));
            qry.addEntity("per", PerPersona.class);
            setDetalle(detalle, qry, "per");
            qry.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
            result = qry.list();
        } finally {
            HibernateUtil.closeSession();
        }
        return result;
    }

    //<editor-fold defaultstate="collapsed" desc="private methods">
    private static void asignarParametros(SQLQuery qry, PatronBusqueda busqueda) throws NoItemSelectedException {
        /*
         * Recorremos todos los parámetros existentes en el SQLQuery recibido
         * como parámetro y lo sustituimos con el valor enviado por el usuario.
         * Debe tenerse en cuenta que si un parámetro existe en el query es
         * porque éste ya ha sido enviado por el usuario. Véase el método
         * 'construirSQL'.
         */
        for (String parameter : qry.getNamedParameters()) {
            switch (parameter) {
                case "indicativo":
                    qry.setParameter("indicativo", busqueda.getIndicativo().isEmpty() ? "%" : busqueda.getIndicativo());
                    break;
                case "activo":
                    qry.setParameter("activo", busqueda.isActivo() ? "T" : "F");
                    break;
                case "apellido1":
                    qry.setParameter("apellido1", busqueda.getApellido1().isEmpty() ? "%" : busqueda.getApellido1());
                    break;
                case "apellido2":
                    qry.setParameter("apellido2", busqueda.getApellido2().isEmpty() ? "%" : busqueda.getApellido2());
                    break;
                case "unidad":
                    qry.setParameter("unidad", busqueda.getUnidad() != 0 ? busqueda.getUnidad() : "%");
                    break;
                case "categoria":
                    qry.setParameter("categoria", busqueda.getCategoria() != 0 ? busqueda.getCategoria() : "%");
                    break;
                case USUARIO:
                    qry.setParameter(USUARIO, busqueda.getUsuario().isEmpty() ? "%" : busqueda.getUsuario());
                    break;
                default:
                    throw new NoItemSelectedException("Error no esperado");
            }
        }
    }

    private static void setFiltrosSituaciones(Filter filter, List<GenLista> lista) {
        for (GenLista lis : lista) {
            try {
                List<Long> ids = new ArrayList<>();
                lis.getMiembros().forEach((mie) -> {
                    ids.add(mie.getIdMiembro());
                });
                switch (lis.getCodigo()) {
                    case "UNI":
                        filter.setParameterList("paramUnidad", ids);
                        break;
                    case "CTR":
                        filter.setParameterList("paramCentroTrabajo", ids);
                        break;
                    case "CAT":
                        filter.setParameterList("paramCategoria", ids);
                        break;
                    default:
                        break;
                }
            } catch (NullPointerException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }

    private static void setDetalle(int detalle, PerPersona persona) {

        if ((detalle & Constants.PERFIL) == Constants.PERFIL) {
            Hibernate.initialize(persona.getPerfiles());
        }

        if ((detalle & Constants.PLANTILLA) == Constants.PLANTILLA) {
            Hibernate.initialize(persona.getPlantillas());
        }

        if ((detalle & Constants.UNIDAD) == Constants.UNIDAD) {
            Hibernate.initialize(persona.getUnidades());
        }

        if ((detalle & Constants.CENTRO_TRABAJO) == Constants.CENTRO_TRABAJO) {
            Hibernate.initialize(persona.getCentrosTrabajo());
        }

    }

    private static void setDetalle(int detalle, SQLQuery query, String alias) {

        if ((detalle & Constants.PERFIL) == Constants.PERFIL) {
            query.addJoin("pfl", alias.concat(".perfiles"));
        }

        if ((detalle & Constants.PLANTILLA) == Constants.PLANTILLA) {
            query.addJoin("pla", alias.concat(".plantillas"));
        }

        if ((detalle & Constants.UNIDAD) == Constants.UNIDAD) {
            query.addJoin("uni", alias.concat(".unidades"));
        }

        if ((detalle & Constants.INFO) == Constants.INFO) {
            query.addJoin("inf", alias.concat(".datosInformaticos"));
        }

        if ((detalle & Constants.TELEFONO) == Constants.TELEFONO) {
            query.addJoin("tel", alias.concat(".telefonos"));
        }
        // Es necesario volver a añadir la clase porque Hibernate toma como entidad raiz la última añadida
        query.addEntity(PerPersona.class);
    }
    //</editor-fold>
}
