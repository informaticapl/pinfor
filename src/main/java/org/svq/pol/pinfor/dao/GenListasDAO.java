package org.svq.pol.pinfor.dao;

import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.svq.pol.exception.PinforException;
import org.svq.pol.pinfor.bean.GenLista;
import org.svq.pol.pinfor.bean.GenMiembro;
import org.svq.pol.pinfor.utility.HibernateUtil;

/**
 * La clase GenListasDAO contiene una serie de metodos auxiliares que extraen de
 * la base de datos los distintos datos necesarios para la construccion de las
 * listas desplegales y de eleccion.
 *
 * @author jjcamacho
 */
final class GenListasDAO {

    private GenListasDAO() {
        super();
    }

    /**
     * Devuelve una lista con todos los elementos tipo {@code GenLista}
     * resultantes de la consulta. Estos elementos son todos aquellos cuyos
     * campos {@code codigo} se encuentra EN la relación que se recibe como
     * parámetro.
     *
     * @param condiciones
     * @return List de objetos {@code GenLista}
     */
    public static List<GenLista> getListasGenerales(String[] condiciones) {
        List<GenLista> result = null;
        Session session = HibernateUtil.getSession();
        try {
            HibernateUtil.getTransaction();
            Criteria crit = session.createCriteria(GenLista.class);
            crit.add(Restrictions.in("codigo", condiciones));
            crit.addOrder(Order.asc("nombre"));
            crit.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
            result = (List<GenLista>) crit.list();
        } catch(HibernateException ex) {
            System.out.println(ex.getMessage());
        } finally {
            HibernateUtil.closeSession();
        }
        return result;
    }

    public static List<GenMiembro> getMiembrosByLista(String lista) {
        List<GenMiembro> result;
        Session session = HibernateUtil.getSession();
        try {
            HibernateUtil.getTransaction();
            Criteria crit = session.createCriteria(GenMiembro.class);
            crit.add(Restrictions.eq("lista.codigo", lista).ignoreCase());
            crit.addOrder(Order.asc("orden"));
            result = crit.list();
        } finally {
            HibernateUtil.closeSession();
        }
        return result;
    }


    public static GenMiembro getById(long id) {
        GenMiembro miembro = null;
        Session session = HibernateUtil.getSession();
        try {
            HibernateUtil.getTransaction();
            Criteria crit = session.createCriteria(GenMiembro.class);
            crit.add(Restrictions.eq("idMiembro", id));
            miembro = (GenMiembro) crit.uniqueResult();
        } finally {
            HibernateUtil.closeSession();
        }
        return miembro;
    }

    public static void save(GenLista lista) {
        Session session = HibernateUtil.getSession();
        Transaction tx = null;
        try {
            tx = HibernateUtil.getTransaction();
            session.saveOrUpdate(lista);
            tx.commit();
        } catch (Exception ex) {
            session.refresh(lista);
            if (tx != null) {
                tx.rollback();
            }
            throw ex;
        } finally {
            HibernateUtil.closeSession();
        }
    }

    public static void delete(GenMiembro miembro) throws PinforException {
        Session session = HibernateUtil.getSession();
        Transaction tx = null;
        try {
            tx = HibernateUtil.getTransaction();
            session.delete(miembro);
            tx.commit();
        } catch (Exception ex) {
            session.refresh(miembro);
            if (tx != null) {
                tx.rollback();
            }
            throw new PinforException(ex);
        } finally {
            HibernateUtil.closeSession();
        }
    }
}
