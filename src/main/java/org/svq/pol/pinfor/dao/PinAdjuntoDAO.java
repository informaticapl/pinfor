package org.svq.pol.pinfor.dao;

import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.svq.pol.pinfor.bean.PinParteAdjunto;
import org.svq.pol.pinfor.utility.HibernateUtil;

/**
 *
 * @author jjcampac
 */
class PinAdjuntoDAO {

    private static final Logger LOGGER = Logger.getLogger(PinAdjuntoDAO.class);

    private PinAdjuntoDAO() {

    }

    
    public static List<PinParteAdjunto> getAllByParte(Long idParte) {
        List<PinParteAdjunto> result = null;
        String sql = "SELECT adj.* FROM pin_adjunto adj, pin_parte par WHERE adj.id_parte = par.id_parte AND par.id_parte = :idParte";
        Session session = HibernateUtil.getSession();

        try {
            HibernateUtil.getTransaction();
            SQLQuery qry = session.createSQLQuery(sql);
            qry.setParameter("idParte", idParte);
            qry.addEntity(PinParteAdjunto.class);
            result = qry.list();
        } finally {
            HibernateUtil.closeSession();
        }
        return result;
    }

    public static void save(PinParteAdjunto adjunto) {
        Session session = HibernateUtil.getSession();
        Transaction tx = null;
        try {
            tx = HibernateUtil.getTransaction();
            session.saveOrUpdate(adjunto);
            tx.commit();
        } catch (Exception ex) {
            session.refresh(adjunto);
            if (tx != null) {
                tx.rollback();
            }
            throw ex;
        } finally {
            HibernateUtil.closeSession();
        }
    }
    
    public static void delete(PinParteAdjunto adjunto) {
        Session session = HibernateUtil.getSession();
        Transaction tx = null;
        try {
            tx = HibernateUtil.getTransaction();
            session.delete(adjunto);
            tx.commit();
        } catch (Exception ex) {
            session.refresh(adjunto);
            if (tx != null) {
                tx.rollback();
            }
            throw ex;
        } finally {
            HibernateUtil.closeSession();
        }
    }
    
    
}
