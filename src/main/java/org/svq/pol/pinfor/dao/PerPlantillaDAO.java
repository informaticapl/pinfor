package org.svq.pol.pinfor.dao;

import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.svq.pol.pinfor.bean.GenMiembro;
import org.svq.pol.pinfor.bean.PerPlantilla;
import org.svq.pol.pinfor.utility.HibernateUtil;

/**
 * La clase ListasDAO contiene una serie de metodos auxiliares que extraen de la
 * base de datos los distintos datos necesarios para la construccion de las
 * listas desplegales y de eleccion.
 *
 * @author jjcamacho
 */
final class PerPlantillaDAO {

    private PerPlantillaDAO() {
        super();
    }
    
    public static List<PerPlantilla> getPlantillaByCategoria(GenMiembro categoria) {
        List<PerPlantilla> result = null;
        Session session = HibernateUtil.getSession();
        try {
            HibernateUtil.getTransaction();
            String hql = "select pla "
                    + "from Plantilla pla "
                    + "where pla.fechaAplicacion = (select max(pla2.fechaAplicacion) from Plantilla pla2 where pla2.persona.idPersona = pla.persona.idPersona) "
                    + "  and pla.activo = 'T' "
                    + "  and pla.categoria.idMiembro = " + categoria.getIdMiembro()
                    + "order by pla.fechaAplicacion asc, pla.claveOrden";
            Query qry = session.createQuery(hql);

            result = qry.list();
        } finally {
            HibernateUtil.closeSession();
        }
        return result;
    }

    public static void saveList(List<PerPlantilla> lista) {
        Session session = HibernateUtil.getSession();
        Transaction tx = null;
        try {
            tx = HibernateUtil.getTransaction();
            for (PerPlantilla plantilla : lista) {
                session.saveOrUpdate(plantilla);
            }
            tx.commit();
        } catch (Exception ex) {
            if (tx != null) {
                tx.rollback();
            }
            throw ex;
        } finally {
            HibernateUtil.closeSession();
        }
    }
}
