package org.svq.pol.pinfor.dao;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import javax.persistence.NoResultException;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.type.StandardBasicTypes;
import org.jfree.util.Log;
import org.svq.pol.exception.PinforException;
import org.svq.pol.pinfor.bean.GenSessionUser;
import org.svq.pol.pinfor.bean.PinActuante;
import org.svq.pol.pinfor.bean.PinDestino;
import org.svq.pol.pinfor.bean.PinLugar;
import org.svq.pol.pinfor.bean.auxiliar.PinLugarAux;
import org.svq.pol.pinfor.bean.auxiliar.PinParteAux;
import org.svq.pol.pinfor.utility.Constants;
import org.svq.pol.pinfor.utility.HibernateUtil;

/**
 * La clase AplicacionDAO es la encargada de efectuar el acceso a la base de
 * datos, manipulando los datos de forma conveniente y ocultando toda la capa de
 * acceso a datos.
 *
 * @author jjcamacho
 */
final class PinParteDAO {

    private PinParteDAO() {
    }

    /**
     * Se obtiene un objeto de la clase PinParteAux a partir de su número, año
     * de registro y tipo.
     *
     * @param numParte
     * @param agno
     * @param tipo
     * @return
     */
    static PinParteAux getParte(long numParte, int agno, String tipo) {
        PinParteAux result = null;

        try {
            String sql = "SELECT pa.* "
                    + " FROM pin_parte pa "
                    + " WHERE pa.numero = %d"
                    + "   AND extract(year from pa.fecha_registro) = %d"
                    + "   AND tipo = '%s'";
            sql = String.format(sql, numParte, agno, tipo);
            Session session = HibernateUtil.getSession();
            HibernateUtil.getTransaction();
            SQLQuery qry = session.createSQLQuery(sql);
            qry.addEntity(PinParteAux.class);
            result = (PinParteAux) qry.uniqueResult();
        } finally {
            HibernateUtil.closeSession();
        }
        return result;
    }

    static int searchCount(PinParteAux patron, Boolean consultaOficina, Date desde, Date hasta, PinActuante actuante, String dni, String persona, Integer estado, Boolean incluidoNovedades, Boolean evaluarFechaImpresion, GenSessionUser currentUser) {
        int result = 0;
        Date hoy = Calendar.getInstance(Constants.LOCALE_SPANISH).getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyyHHmmss");

        try {
            String sql = "SELECT count(distinct parte.id_parte) "
                    + " FROM pin_parte parte WHERE parte.id_parte IN "
                    + " (SELECT distinct p.id_parte"
                    + "  FROM PIN_PARTE p"
                    + "     LEFT JOIN PIN_ACTUANTE a ON a.ID_PARTE = p.ID_PARTE"
                    + "     JOIN PIN_LUGAR l ON l.ID_LUGAR = p.ID_LUGAR"
                    + "     LEFT JOIN PIN_PERSONA pe ON pe.ID_PARTE = p.ID_PARTE"
                    + "     LEFT JOIN PIN_DESTINO d ON d.ID_PARTE = p.ID_PARTE"
                    + "     LEFT JOIN pin_estado e ON e.ID_DESTINO = d.ID_DESTINO"
                    + "    LEFT JOIN pin_miembro m ON m.ID_MIEMBRO = e.ID_MIEMBRO_ESTADO"
                    + "  WHERE (p.NUMERO LIKE '%s' OR p.NUMERO IS null)"
                    + "    AND l.NOMBRE_LUGAR LIKE '%s'"
                    + "    AND p.FECHA_SUCESO BETWEEN to_date('%s','ddmmyyyyhh24miss') AND to_date('%s','ddmmyyyyhh24miss')"
                    + "    %s"
                    + "    AND LOWER(a.INDICATIVO) LIKE '%s'"
                    + "    %s"
                    + "    %s"
                    + "    %s"
                    + "    %s"
                    + "    AND p.ID_MIEMBRO_ASUNTO like '%s'"
                    + "    %s"
                    + "    AND p.TIPO IN (%s)"
                    + "    AND (e.fecha = (SELECT max(e2.fecha) FROM pin_estado e2"
                    + "			LEFT JOIN pin_miembro me ON"
                    + "			   e2.ID_MIEMBRO_ESTADO = me.ID_MIEMBRO"
                    + "			WHERE e2.id_destino = d.id_destino)"
                    + "	   AND m.VALOR %s)"
                    + "    AND p.ANULADO IN (%s))"
                    + " ORDER BY parte.FECHA_SUCESO asc";

            String numeroParte = (patron.getNumeroParte() == null || patron.getNumeroParte() == 0) ? "%" : patron.getNumeroParte().toString();
            String nombreLugar = patron.getLugar().getNombreLugar() == null || patron.getLugar().getNombreLugar().trim().isEmpty() ? "%" : patron.getLugar().getNombreLugar();
            String fechaIni = "01011970000000";
            String fechaFin = sdf.format(hoy);
            if (desde != null) {
                fechaIni = sdf.format(desde);
            }
            if (hasta != null) {
                Calendar cHasta = Calendar.getInstance(Constants.LOCALE_SPANISH);
                cHasta.setTime(hasta);
                cHasta.set(Calendar.HOUR, 23);
                cHasta.set(Calendar.MINUTE, 59);
                cHasta.set(Calendar.SECOND, 59);
                fechaFin = sdf.format(cHasta.getTime());
            }

            String sqlFechaImpresion = Constants.EMPTY_STR;
            if (evaluarFechaImpresion) {
                if (patron.getFechaImpresion() != null) {
                    Calendar cImpresion = Calendar.getInstance(Constants.LOCALE_SPANISH);
                    cImpresion.setTime(patron.getFechaImpresion());
                    sqlFechaImpresion = String.format(" AND p.FECHA_IMPRESION = TO_DATE('%s', 'ddmmyyyyhh24miss')", sdf.format(cImpresion.getTime()));
                } else {
                    if (incluidoNovedades) {
                        sqlFechaImpresion = " AND p.FECHA_IMPRESION is not null";
                    } else {
                        sqlFechaImpresion = " AND p.FECHA_IMPRESION is null";
                    }
                }
            }

            String sqlAmbito = null;
            if (currentUser.getGlobal()) {
                sqlAmbito = Constants.EMPTY_STR;
            } else if (currentUser.getRegional()) {
                sqlAmbito = String.format(" AND (p.ID_MIEMBRO_UNIDAD = %d OR a.INDICATIVO = '%s')", currentUser.getUnidad().getIdMiembro(), currentUser.getIndicativo());
            } else if (currentUser.getLocal()) {
                sqlAmbito = String.format(" AND a.INDICATIVO = '%s'", currentUser.getIndicativo());
            }

            String sqlDni = dni.trim().isEmpty() ? Constants.EMPTY_STR : String.format(" AND LOWER(pe.num_documento) LIKE '%s'", "%" + dni.trim().toLowerCase() + "%");
            String sqlNombrePersona = persona.trim().isEmpty() ? Constants.EMPTY_STR : String.format(" AND LOWER(pe.nombre) LIKE '%s'", "%" + persona.trim().toLowerCase() + "%");
            String sqlTexto = patron.getInforme() == null || patron.getInforme().trim().isEmpty() ? Constants.EMPTY_STR : String.format(" AND LOWER(p.informe) LIKE '%s'", "%" + patron.getInforme().toLowerCase() + "%");

            String sqlIndicativo = actuante.getIndicativo() == null || actuante.getIndicativo().trim().isEmpty() ? "%" : actuante.getIndicativo().toLowerCase();
            String idMiembroAsunto = patron.getAsunto() == null ? "%" : String.valueOf(patron.getAsunto().getIdMiembro());

            String sqlUnidad = "";
            if (patron.getUnidad() != null) {
                sqlUnidad = String.format(" AND p.ID_MIEMBRO_UNIDAD = %d", patron.getUnidad().getIdMiembro());
            }
            String sqlTipo;
            if (patron.getTipo().trim().isEmpty()) {
                sqlTipo = consultaOficina ? "'PI','IF','OM'" : "'PI','IF'";
            } else {
                sqlTipo = "'".concat(patron.getTipo()).concat("'");
            }

            String sqlEstado;
            if (estado == null) {
                sqlEstado = "> -1";
            } else {
                sqlEstado = "= " + estado;
            }
            String anulado = patron.getAnulado() ? "0,1" : "0";

            sql = String.format(sql, numeroParte, nombreLugar, fechaIni, fechaFin, sqlFechaImpresion, sqlIndicativo, sqlAmbito, sqlDni, sqlNombrePersona, sqlTexto, idMiembroAsunto, sqlUnidad, sqlTipo, sqlEstado, anulado);

            Session session = HibernateUtil.getSession();
            HibernateUtil.getTransaction();
            SQLQuery qry = session.createSQLQuery(sql);
            result = ((BigDecimal) qry.uniqueResult()).intValue();
        } finally {
            HibernateUtil.closeSession();
        }
        return result;
    }

    /**
     * Devuelve una lista de objetos de la clase PinParte que se corresponden
     * con todos los atributos del objeto recibido como parámetro.
     *
     * @param patron Parámetro de la clase PinParte que contiene el patrón de la
     * búsqueda.
     * @param desde Fecha desde la que se debe efectuar la búsqueda.
     * @param hasta Fecha hasta la que se tiene que efectuar la búsqueda.
     * @param actuante Objeto de la clase PinActuante que tiene definido el
     * indicativo que tiene que aparecer en el resultado.
     * @return Lista de objetos de la clase PinParte que corresponden con el
     * patrón recibido como parámetro
     */
    static List<PinParteAux> search(PinParteAux patron, Boolean consultaOficina, Date desde, Date hasta, PinActuante actuante, String dni, String persona, Integer estado, Boolean incluidoNovedades, Boolean evaluarFechaImpresion, int initPage, int pageSize, GenSessionUser currentUser) {
        List<PinParteAux> result = null;
        Date hoy = Calendar.getInstance(Constants.LOCALE_SPANISH).getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyyHHmmss");

        try {
            String sql = "SELECT parte.* FROM pin_parte parte WHERE parte.id_parte IN "
                    + " (SELECT distinct p.id_parte"
                    + "  FROM PIN_PARTE p"
                    + "     LEFT JOIN PIN_ACTUANTE a ON a.ID_PARTE = p.ID_PARTE"
                    + "     JOIN PIN_LUGAR l ON l.ID_LUGAR = p.ID_LUGAR"
                    + "     LEFT JOIN PIN_PERSONA pe ON pe.ID_PARTE = p.ID_PARTE"
                    + "     LEFT JOIN PIN_DESTINO d ON d.ID_PARTE = p.ID_PARTE"
                    + "     LEFT JOIN pin_estado e ON e.ID_DESTINO = d.ID_DESTINO"
                    + "     LEFT JOIN pin_miembro m ON m.ID_MIEMBRO = e.ID_MIEMBRO_ESTADO"
                    + "  WHERE (p.NUMERO LIKE '%s' OR p.NUMERO IS null)"
                    + "    AND l.NOMBRE_LUGAR LIKE '%s'"
                    + "    AND p.FECHA_SUCESO BETWEEN to_date('%s','ddmmyyyyhh24miss') AND to_date('%s','ddmmyyyyhh24miss')"
                    + "    %s"
                    + "    AND LOWER(a.INDICATIVO) LIKE '%s'"
                    + "    %s"
                    + "    %s"
                    + "    %s"
                    + "    %s"
                    + "    AND p.ID_MIEMBRO_ASUNTO like '%s'"
                    + "    %s"
                    + "    AND p.TIPO IN (%s)"
                    + "    AND (e.fecha = (SELECT max(e2.fecha) FROM pin_estado e2"
                    + "			LEFT JOIN pin_miembro me ON"
                    + "			   e2.ID_MIEMBRO_ESTADO = me.ID_MIEMBRO"
                    + "			WHERE e2.id_destino = d.id_destino)"
                    + "	   AND m.VALOR %s)"
                    + "    AND p.ANULADO IN (%s))"
                    + " ORDER BY parte.FECHA_SUCESO asc";

            String numeroParte = (patron.getNumeroParte() == null || patron.getNumeroParte() == 0) ? "%" : patron.getNumeroParte().toString();
            String nombreLugar = patron.getLugar().getNombreLugar() == null || patron.getLugar().getNombreLugar().trim().isEmpty() ? "%" : patron.getLugar().getNombreLugar();
            String fechaIni = "01011970000000";
            String fechaFin = sdf.format(hoy);
            if (desde != null) {
                fechaIni = sdf.format(desde);
            }
            if (hasta != null) {
                Calendar cHasta = Calendar.getInstance(Constants.LOCALE_SPANISH);
                cHasta.setTime(hasta);
                cHasta.set(Calendar.HOUR, 23);
                cHasta.set(Calendar.MINUTE, 59);
                cHasta.set(Calendar.SECOND, 59);
                fechaFin = sdf.format(cHasta.getTime());
            }

            String sqlFechaImpresion = Constants.EMPTY_STR;
            if (evaluarFechaImpresion) {
                if (patron.getFechaImpresion() != null) {
                    Calendar cImpresion = Calendar.getInstance(Constants.LOCALE_SPANISH);
                    cImpresion.setTime(patron.getFechaImpresion());
                    sqlFechaImpresion = String.format(" AND p.FECHA_IMPRESION = TO_DATE('%s', 'ddmmyyyyhh24miss')", sdf.format(cImpresion.getTime()));
                } else {
                    if (incluidoNovedades) {
                        sqlFechaImpresion = " AND p.FECHA_IMPRESION is not null";
                    } else {
                        sqlFechaImpresion = " AND p.FECHA_IMPRESION is null";
                    }
                }
            }

            String sqlAmbito = null;
            if (currentUser.getGlobal()) {
                sqlAmbito = Constants.EMPTY_STR;
            } else if (currentUser.getRegional()) {
                sqlAmbito = String.format(" AND (p.ID_MIEMBRO_UNIDAD = %d OR a.INDICATIVO = '%s')", currentUser.getUnidad().getIdMiembro(), currentUser.getIndicativo());
            } else if (currentUser.getLocal()) {
                sqlAmbito = String.format(" AND a.INDICATIVO = '%s'", currentUser.getIndicativo());
            }

            String sqlDni = dni.trim().isEmpty() ? Constants.EMPTY_STR : String.format(" AND LOWER(pe.num_documento) LIKE '%s'", "%" + dni.trim().toLowerCase() + "%");
            String sqlNombrePersona = persona.trim().isEmpty() ? Constants.EMPTY_STR : String.format(" AND LOWER(pe.nombre) LIKE '%s'", "%" + persona.trim().toLowerCase() + "%");
            String sqlTexto = patron.getInforme() == null || patron.getInforme().trim().isEmpty() ? Constants.EMPTY_STR : String.format(" AND LOWER(p.informe) LIKE '%s'", "%" + patron.getInforme().toLowerCase() + "%");

            String sqlIndicativo = actuante.getIndicativo() == null || actuante.getIndicativo().trim().isEmpty() ? "%" : actuante.getIndicativo().toLowerCase();
            String idMiembroAsunto = patron.getAsunto() == null ? "%" : String.valueOf(patron.getAsunto().getIdMiembro());

            String sqlUnidad = "";
            if (patron.getUnidad() != null) {
                sqlUnidad = String.format(" AND p.ID_MIEMBRO_UNIDAD = %d", patron.getUnidad().getIdMiembro());
            }

            String sqlTipo;
            if (patron.getTipo().trim().isEmpty()) {
                sqlTipo = consultaOficina ? "'PI','IF','OM'" : "'PI','IF'";
            } else {
                sqlTipo = "'".concat(patron.getTipo()).concat("'");
            }

            String sqlEstado;
            if (estado == null) {
                sqlEstado = "> -1";
            } else {
                sqlEstado = "= " + estado;
            }

            String anulado = patron.getAnulado() ? "0,1" : "0";

            sql = String.format(sql, numeroParte, nombreLugar, fechaIni, fechaFin, sqlFechaImpresion, sqlIndicativo, sqlAmbito, sqlDni, sqlNombrePersona, sqlTexto, idMiembroAsunto, sqlUnidad, sqlTipo, sqlEstado, anulado);

            Session session = HibernateUtil.getSession();
            HibernateUtil.getTransaction();
            SQLQuery qry = session.createSQLQuery(sql);
            qry.addEntity(PinParteAux.class);
            result = qry.list();
            result.forEach(p -> p.setCurrentUser(currentUser));
        } catch (Exception ex) {
            Log.log(Constants.ERROR, ex.getMessage(), ex);
        } finally {
            HibernateUtil.closeSession();
        }
        return result;
    }

    /**
     * Método encargado de hacer persistente el objeto recibido como parámetro.
     *
     * @param parte Objeto de la clase <code>PinParte</code> que se hará
     * persistente en la base de datos y que previamente no existía.
     * @throws HibernateException Excepción elevada cuando se produce un error
     * en la gestión de Hibernate. Aunque es capturada para devolver la base de
     * datos a su estado anterior (rollback), la excepción es vuelta a lanzar
     * para su posterior tratamiento.
     */
    static void save(PinParteAux parte) throws PinforException {
        Session session = HibernateUtil.getSession();
        Transaction tx = null;
        try {
            tx = HibernateUtil.getTransaction();
            if (parte.getNumeroParte() == null || parte.getNumeroParte() == 0) {
                Calendar calendar = Calendar.getInstance(Constants.LOCALE_SPANISH);
                calendar.setTime(parte.getFechaRegistro());
                parte.setNumeroParte(getSiguienteNumParte(session, tx, calendar.get(Calendar.YEAR), parte.getTipo()));
            }
            session.saveOrUpdate(parte);
//            throw new HibernateException("depurando");
            tx.commit();
        } catch (HibernateException ex) {
            parte.setNumeroParte(0);
            if (tx != null) {
                tx.rollback();
            }
            throw new PinforException(ex);
        } finally {
            HibernateUtil.closeSession();
        }
    }

    /**
     * El método se encarga de eliminar de la base de datos el objeto recibido
     * como parámetro.
     *
     * @param curso Objeto de la clase <code>PinParte</code> que dejará de ser
     * persistente.
     * @throws HibernateException Excepción elevada cuando se produce un error
     * en la gestión de Hibernate. Aunque es capturada para devolver la base de
     * datos a su estado anterior (rollback), la excepción es vuelta a lanzar
     * para su posterior tratamiento.
     */
    static void delete(PinParteAux curso) throws PinforException {
        Session session = HibernateUtil.getSession();
        Transaction tx = null;
        try {
            tx = HibernateUtil.getTransaction();
            String sql = "from parte where idparte = :idparte";
            Query qry = session.createQuery(sql);
            qry.setParameter("idparte", curso.getIdParte());
            session.delete(curso);
            tx.commit();
        } catch (HibernateException ex) {
            if (tx != null) {
                tx.rollback();
            }
            throw new PinforException(ex);
        } finally {
            HibernateUtil.closeSession();
        }
    }

    //<editor-fold defaultstate="collapsed" desc="listados">
    public static List<PinParteAux> listadoPorAsuntos(String asunto, Date fechaDesde, Date fechaHasta, boolean incluirBorradores) {
        SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");

        String sql = "SELECT p.*"
                + " FROM PIN_PARTE p"
                + " JOIN pin_miembro m ON m.id_miembro = p.ID_MIEMBRO_ASUNTO AND lower(m.NOMBRE) LIKE '%s'"
                + " WHERE p.FECHA_SUCESO >= TO_DATE('%s', 'ddMMyyyy')"
                + "   AND p.FECHA_SUCESO <= TO_DATE('%s', 'ddMMyyyy')"
                + "   AND p.ANULADO = 0"
                + "   AND p.ENVIADO_OFICINA IN (%s)"
                + " ORDER BY p.ID_MIEMBRO_ASUNTO ASC, p.FECHA_REGISTRO ASC";
        List<PinParteAux> result = null;
        Session session = HibernateUtil.getSession();
        try {
            HibernateUtil.getTransaction();
            String valor = asunto.toLowerCase().equals("todos") ? "" : asunto.toLowerCase();
            String enOficina = incluirBorradores ? "0,1" : "1";

            sql = String.format(sql, '%' + valor + '%', sdf.format(fechaDesde), sdf.format(fechaHasta), enOficina);
            SQLQuery qry = session.createSQLQuery(sql);
            qry.addEntity(PinParteAux.class);
            result = qry.list();
        } finally {
            HibernateUtil.closeSession();
        }
        return result;
    }

    public static List<PinActuante> listadoPorCategorias(String asunto, Date fechaDesde, Date fechaHasta, boolean incluirBorradores) {
        SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");

        String sql = "SELECT a.*"
                + " FROM PIN_ACTUANTE a"
                + " JOIN PIN_PARTE p ON a.ID_PARTE = p.ID_PARTE"
                + " JOIN GEN_MIEMBROS m ON a.ID_MIEMBRO_CARGO = m.ID_MIEMBRO  AND lower(m.NOMBRE) LIKE '%s'"
                + " WHERE p.FECHA_SUCESO >= TO_DATE('%s', 'ddMMyyyy')"
                + "   AND p.FECHA_SUCESO <= TO_DATE('%s', 'ddMMyyyy')"
                + "   AND p.ANULADO = 0"
                + "   AND p.ENVIADO_OFICINA IN (%s)"
                + " ORDER BY m.ORDEN ASC, lpad(a.INDICATIVO,10) ASC, p.FECHA_REGISTRO ASC";
        List<PinActuante> result = null;
        Session session = HibernateUtil.getSession();
        try {
            HibernateUtil.getTransaction();
            String valor = asunto.toLowerCase().equals("todos") ? "" : asunto.toLowerCase();
            String enOficina = incluirBorradores ? "0,1" : "1";

            sql = String.format(sql, '%' + valor + '%', sdf.format(fechaDesde), sdf.format(fechaHasta), enOficina);
            SQLQuery qry = session.createSQLQuery(sql);
            qry.addEntity(PinActuante.class);
            result = qry.list();
        } finally {
            HibernateUtil.closeSession();
        }
        return result;
    }

    public static List<PinDestino> listadoPorDestinatarios(String asunto, Date fechaDesde, Date fechaHasta, boolean incluirBorradores) {
        SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");

        String sql = "SELECT d.*"
                + " FROM PIN_DESTINO d"
                + " JOIN PIN_PARTE p ON d.ID_PARTE = p.ID_PARTE"
                + " JOIN PIN_MIEMBRO m ON d.ID_MIEMBRO_DESTINO = m.ID_MIEMBRO AND LOWER(m.NOMBRE) LIKE '%s'"
                + " WHERE p.FECHA_SUCESO >= TO_DATE('%s', 'ddMMyyyy')"
                + "   AND p.FECHA_SUCESO <= TO_DATE('%s', 'ddMMyyyy')"
                + "   AND p.ANULADO = 0"
                + "   AND p.ENVIADO_OFICINA IN (%s)"
                + " ORDER BY m.ORDEN ASC, p.FECHA_REGISTRO ASC";
        List<PinDestino> result = null;
        Session session = HibernateUtil.getSession();
        try {
            HibernateUtil.getTransaction();
            String valor = asunto.toLowerCase().equals("todos") ? "" : asunto.toLowerCase();
            String enOficina = incluirBorradores ? "0,1" : "1";

            sql = String.format(sql, '%' + valor + '%', sdf.format(fechaDesde), sdf.format(fechaHasta), enOficina);
            SQLQuery qry = session.createSQLQuery(sql);
            qry.addEntity(PinDestino.class);
            result = qry.list();
        } finally {
            HibernateUtil.closeSession();
        }
        return result;
    }

    public static List<PinParteAux> listadoPorEstadoTramitacion(String subTipoListado, Date fechaDesde, Date fechaHasta) {
        SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");

        String sql = "SELECT p.*"
                + " FROM PIN_PARTE p"
                + " JOIN pin_miembro m ON m.id_miembro = p.ID_MIEMBRO_ASUNTO"
                + " WHERE p.FECHA_SUCESO >= TO_DATE('%s', 'ddMMyyyy')"
                + "   AND p.FECHA_SUCESO <= TO_DATE('%s', 'ddMMyyyy')"
                + "   %s"
                + "   AND p.ANULADO = 0"
                + " ORDER BY p.ENVIADO_OFICINA ASC, p.ARCHIVADO DESC, p.LISTO_TRAMITAR ASC, p.TRAMITADO ASC, p.FECHA_REGISTRO ASC";
        List<PinParteAux> result = null;
        Session session = HibernateUtil.getSession();
        try {
            HibernateUtil.getTransaction();
            String sqlCondicion;
            switch (subTipoListado.toLowerCase()) {
                case "borradores":
                    sqlCondicion = "AND ENVIADO_OFICINA = 0";
                    break;
                case "archivado":
                    sqlCondicion = "AND ARCHIVADO = 1";
                    break;
                case "pendientes envío a estafeta":
                    sqlCondicion = "AND ENVIADO_OFICINA = 1 AND ARCHIVADO = 0 AND LISTO_TRAMITAR = 0";
                    break;
                case "pendientes tramitación por estafeta":
                    sqlCondicion = "AND LISTO_TRAMITAR = 1 AND TRAMITADO = 0";
                    break;
                case "entregados en destino":
                    sqlCondicion = "AND TRAMITADO = 1";
                    break;
                default:
                    sqlCondicion = Constants.EMPTY_STR;
            }
            sql = String.format(sql, sdf.format(fechaDesde), sdf.format(fechaHasta), sqlCondicion);
            SQLQuery qry = session.createSQLQuery(sql);
            qry.addEntity(PinParteAux.class);
            result = qry.list();
        } finally {
            HibernateUtil.closeSession();
        }
        return result;
    }
    //</editor-fold>

    static List<PinLugarAux> getPartesEnRadio(int distancia, double lat, double lng, int agnoInicio, int agnoFin) {
        String coordenadas = "SDO_GEOMETRY(2001, 4326, SDO_POINT_TYPE(%f, %f, NULL), NULL, NULL)";
        coordenadas = String.format(Locale.ENGLISH, coordenadas, lat, lng);
        String sql = "SELECT l.*, SDO_GEOM.SDO_DISTANCE(l.COORDENADAS, %s, 0.005) distancia"
                + " FROM PIN_LUGAR l"
                + "      JOIN PIN_PARTE pa ON pa.id_lugar = l.id_lugar"
                + " JOIN PIN_DESTINO de ON "
                + "	de.ID_PARTE = pa.ID_PARTE AND de.ID_MIEMBRO_DESTINO = pa.ID_MIEMBRO_UNIDAD"
                + " JOIN pin_estado es ON "
                + "	es.ID_DESTINO = de.ID_DESTINO"
                + " JOIN PIN_MIEMBRO mi ON "
                + "	mi.ID_MIEMBRO = es.ID_MIEMBRO_ESTADO AND mi.VALOR != 0"
                + " WHERE extract(year from pa.fecha_registro) between %d and %d"
                + "   AND pa.anulado = 0"
                + "   AND SDO_WITHIN_DISTANCE(l.COORDENADAS, %s, 'distance=%d unit=meter') = 'TRUE'";
        List<PinLugarAux> result = null;
        Session session = HibernateUtil.getSession();
        try {
            HibernateUtil.getTransaction();
            sql = String.format(Locale.ENGLISH, sql, coordenadas, agnoInicio, agnoFin, coordenadas, distancia);
            SQLQuery qry = session.createSQLQuery(sql);
            qry.addEntity(PinLugar.class);
            qry.addScalar("distancia", StandardBasicTypes.FLOAT);
            result = qry.list();
        } finally {
            HibernateUtil.closeSession();
        }
        return result;
    }

    static List<PinLugarAux> getPartesVecinos(int vecinos, double lat, double lng, int agnoInicio, int agnoFin) {
        String coordenadas = "SDO_GEOMETRY(2001, 4326, SDO_POINT_TYPE(%f, %f, NULL), NULL, NULL)";
        coordenadas = String.format(Locale.ENGLISH, coordenadas, lat, lng);
        String sql = "SELECT l.*, SDO_NN_DISTANCE(1) distancia"
                + " FROM PIN_LUGAR l"
                + "      JOIN PIN_PARTE pa ON pa.id_lugar = l.id_lugar"
                + " JOIN PIN_DESTINO de ON "
                + "	de.ID_PARTE = pa.ID_PARTE AND de.ID_MIEMBRO_DESTINO = pa.ID_MIEMBRO_UNIDAD"
                + " JOIN pin_estado es ON "
                + "	es.ID_DESTINO = de.ID_DESTINO"
                + " JOIN PIN_MIEMBRO mi ON "
                + "	mi.ID_MIEMBRO = es.ID_MIEMBRO_ESTADO AND mi.VALOR != 0"
                + " WHERE extract(year from pa.fecha_registro) between %d and %d"
                + "   AND pa.anulado = 0"
                + "   AND SDO_nn(l.COORDENADAS, %s, 'sdo_batch_size=0', 1) = 'TRUE'"
                + "   AND ROWNUM <= %d"
                + " ORDER BY distancia";
        List<PinLugarAux> result = null;
        Session session = HibernateUtil.getSession();
        try {
            HibernateUtil.getTransaction();
            sql = String.format(Locale.ENGLISH, sql, agnoInicio, agnoFin, coordenadas, vecinos);
            SQLQuery qry = session.createSQLQuery(sql);
            qry.addEntity(PinLugar.class);
            qry.addScalar("distancia", StandardBasicTypes.FLOAT);
            result = qry.list();
        } finally {
            HibernateUtil.closeSession();
        }
        return result;
    }

    private static int getSiguienteNumParte(Session session, Transaction tx, int agno, String tipo) throws HibernateException {
        int contador;

        String sqlSelect = "SELECT c.contador FROM pin_contador c WHERE c.agno = :agno AND c.tipo = :tipo FOR UPDATE";
        String sqlInsert = "INSERT INTO pin_contador (contador, agno, tipo) VALUES(:contador,:agno,:tipo)";
        String sqlUpdate = "UPDATE pin_contador SET contador = :contador WHERE agno = :agno  AND tipo = :tipo ";

        Query qry = session.createSQLQuery(sqlSelect);
        qry.setParameter("agno", agno);
        qry.setParameter("tipo", tipo.substring(0, 1));
        try {
            BigDecimal r = (BigDecimal) qry.uniqueResult();
            if (r == null) {
                throw new NoResultException();
            }
            contador = r.intValue() + 1;
            qry = session.createSQLQuery(sqlUpdate);
            qry.setParameter("contador", contador);
            qry.setParameter("agno", agno);
            qry.setParameter("tipo", tipo.substring(0, 1));
            qry.executeUpdate();
        } catch (NoResultException ex) {
            contador = 1;
            qry = session.createSQLQuery(sqlInsert);
            qry.setParameter("contador", contador);
            qry.setParameter("agno", agno);
            qry.setParameter("tipo", tipo.substring(0, 1));
            qry.executeUpdate();
        }

        return contador;
    }

}
