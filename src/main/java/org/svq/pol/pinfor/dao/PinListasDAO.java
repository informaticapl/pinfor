package org.svq.pol.pinfor.dao;

import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.svq.pol.exception.PinforException;
import org.svq.pol.pinfor.bean.PinLista;
import org.svq.pol.pinfor.bean.PinMiembro;
import org.svq.pol.pinfor.utility.HibernateUtil;

/**
 * La clase PinListasDAO contiene una serie de metodos auxiliares que extraen de
 * la base de datos los distintos datos necesarios para la construccion de las
 * listas desplegales y de eleccion.
 *
 * @author jjcamacho
 */
final class PinListasDAO {

    private PinListasDAO() {
        super();
    }

    /**
     * Metodo que devuelve todas las listas de forma especifica para la
     * aplicacion.
     *
     * @return List de objetos {@code GatLista}
     */
    public static List<PinLista> getListasEspecificas(Session session) {
        List<PinLista> result;
        Criteria crit = session.createCriteria(PinLista.class);
        crit.addOrder(Order.asc("descripcion"));
        result = crit.list();
        return result;
    }

    public static List<PinMiembro> getMiembros(String lista, Session session) {
        List<PinMiembro> result;

        Criteria crit = session.createCriteria(PinMiembro.class);
        crit.add(Restrictions.eq("lista.idLista", lista).ignoreCase());
        crit.addOrder(Order.asc("orden"));
        result = crit.list();

        return result;
    }

    public static List<PinMiembro> getMiembrosHijos(String lista, Session session) {
        List<PinMiembro> result;

        Criteria crit = session.createCriteria(PinMiembro.class);
        crit.add(Restrictions.eq("lista.idLista", lista).ignoreCase());
        crit.addOrder(Order.asc("orden"));
        result = crit.list();

        return result;
    }

    public static PinMiembro getMiembroCabeceraDeSublista(String sublista) {
        PinMiembro result;
        Session session = HibernateUtil.getSession();
        try {
            HibernateUtil.getTransaction();
            Criteria crit = session.createCriteria(PinMiembro.class);
            crit.add(Restrictions.eq("sublista.idLista", sublista));
            crit.addOrder(Order.asc("orden"));
            result = (PinMiembro) crit.uniqueResult();
        } finally {
            HibernateUtil.closeSession();
        }
        return result;
    }

    public static PinMiembro getMiembro(String lista, int valor) {
        PinMiembro result;
        Session session = HibernateUtil.getSession();
        try {
            HibernateUtil.getTransaction();
            Criteria crit = session.createCriteria(PinMiembro.class);
            crit.add(Restrictions.eq("lista.idLista", lista));
            crit.add(Restrictions.eq("valor", valor));
            result = (PinMiembro) crit.uniqueResult();
        } finally {
            HibernateUtil.closeSession();
        }
        return result;
    }

    public static List<PinMiembro> getAsuntos(Session session) {
        String sql = "SELECT m.*"
                + " FROM pin_miembro m"
                + " WHERE CODIGO_MIEMBRO IN (SELECT sublista FROM pin_miembro m2 WHERE m2.CODIGO_MIEMBRO = 'ASU')"
                + " ORDER BY m.nombre";
        SQLQuery qry = session.createSQLQuery(sql);
        qry.addEntity(PinMiembro.class);
        return qry.list();
    }

    public static PinMiembro getById(long id) {
        PinMiembro miembro = null;
        Session session = HibernateUtil.getSession();
        try {
            HibernateUtil.getTransaction();
            Criteria crit = session.createCriteria(PinMiembro.class);
            crit.add(Restrictions.eq("idMiembro", id));
            miembro = (PinMiembro) crit.uniqueResult();
        } finally {
            HibernateUtil.closeSession();
        }
        return miembro;
    }

    public static void save(PinLista lista, Session session) {
        session.saveOrUpdate(lista);
    }

    public static void delete(PinMiembro miembro, Session session) throws PinforException {
        session.delete(miembro);
    }

    public static void save(PinMiembro miembro, Session session) {
        session.saveOrUpdate(miembro);
    }
}
