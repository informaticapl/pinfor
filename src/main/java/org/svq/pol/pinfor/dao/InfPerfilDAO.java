package org.svq.pol.pinfor.dao;

import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.svq.pol.pinfor.bean.InfContenidoPerfil;
import org.svq.pol.pinfor.bean.InfPerfil;
import org.svq.pol.pinfor.utility.HibernateUtil;

/**
 * La clase InfPerfilDAO es la encargada de efectuar el acceso a la base de datos,
 manipulando los datos de forma conveniente y ocultando toda la capa de acceso
 a datos.
 *
 * @author jjcamacho
 */
final class InfPerfilDAO {

    private InfPerfilDAO() {
        super();
    }
    
    /**
     * Método encargado de la obtención de una lista de perfiles que cumplen el
     * patrón recibido como parámetro, es decir, cuyo nombre de perfil contiene
     * a la cadena recibida.
     *
     * @param perfil String que identifica parcial o completamente al perfil que
     * se busca.
     * @return <code>List</code> - Lista de objetos de la clase
     * <code>InfPerfil</code>.
     */
    public static List<InfPerfil> getPerfil(String perfil) {
        List<InfPerfil> result = null;
        Session session = HibernateUtil.getSession();
        try {
            HibernateUtil.getTransaction();
            Criteria crit = session.createCriteria(InfPerfil.class);
            if (!perfil.isEmpty()) {
                crit.add(Restrictions.ilike("perfil", '%' + perfil + '%'));
            }
            result = crit.list();
        } finally {
            HibernateUtil.closeSession();
        }
        return result;
    }

    /**
     * El método devuelve un único perfil cuyo campo
     * <code>id</code> coincide con el parámetro recibido.
     *
     * @param id Long que identifica al campo <code>id</code>.
     * @return <code>InfPerfil</code> - Un único objeto de la clase perfil.
     */
    public static InfPerfil getPerfil(Long id) {
        InfPerfil result = null;
        Session session = HibernateUtil.getSession();
        try {
            HibernateUtil.getTransaction();
            session.clear();
            Criteria crit = session.createCriteria(InfPerfil.class);
            if (id != null) {
                crit.add(Restrictions.eq("idPerfil", id));
            }
            result = (InfPerfil) crit.uniqueResult();
        } finally {
            HibernateUtil.closeSession();
        }
        return result;
    }

    public static List<InfPerfil> getAllPerfiles() {
        List<InfPerfil> result = null;
        Session session = HibernateUtil.getSession();
        try {
            HibernateUtil.getTransaction();
            session.clear();
            Criteria crit = session.createCriteria(InfPerfil.class);
            crit.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
            result = (List<InfPerfil>) crit.list();
        } finally {
            HibernateUtil.closeSession();
        }
        return result;
    }

    /**
     * Devuelve una lista de objetos de la clase Aplicacion que se corresponden
     * con todos los atributos del objeto recibido como parámetro.
     *
     * @param patronBusqueda Parámetro de la clase Aplicación que contiene el
     * patrón de la búsqueda.
     * @return Lista de objetos de la clase Aplicacion que corresponden con el
     * patrón recibido como parámetro
     */
    public static List<InfPerfil> search(InfPerfil patron) {
        List<InfPerfil> result = null;
        Session session = HibernateUtil.getSession();
        try {
            HibernateUtil.getTransaction();
            String hql = "select per "
                    + "from Perfil per "
                    + "left join fetch per.contenidoPerfiles "
                    + "where lower(per.perfil) like :perfil "
                    + "  and lower(per.descripcion) like :descripcion ";
            Query qry = session.createQuery(hql);
            qry.setParameter("perfil", patron.getPerfil().isEmpty() ? "%" : patron.getPerfil().toLowerCase());
            qry.setParameter("descripcion", patron.getDescripcion().isEmpty() ? "%" : patron.getDescripcion().toLowerCase());
            qry.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
            result = qry.list();
        } finally {
            HibernateUtil.closeSession();
        }
        return result;
    }

    /**
     * El método hace persistente el nuevo objeto recibido como parámetro.
     *
     * @param perfil Objeto de la clase <code>InfPerfil</code> que se hará
     * persistente en la base de datos. El objeto no debe existir previamente en
     * la tabla.
     * @throws HibernateException Aunque el método captura la excepción que
     * pudiera producirse durante la actuación de Hibernate, la captura sólo se
     * realiza para revertir (rollback) la base de datos a un estado estable
     * anterior, ya que la excepción es vuelta a lanzar para un posterior
     * tratamiento.
     */
    public static void save(InfPerfil perfil) {
        Session session = HibernateUtil.getSession();
        Transaction tx = null;
        try {
            tx = HibernateUtil.getTransaction();
            /* Son dos las consideraciones que deben tenerse en cuenta a la hora de grabar por
             * primera vez un objeto padre (perfil) y sus relaciones (contenido perfil):
             * 1. Hibernate NO maneja la inserción automática de los hijos cuando hay una 
             *    composite-id de por medio, por lo que es necesario grabar el padre, tomar la id
             *    que le ha sido asignada, recorrer el conjunto de hijos y asignar a cada uno de
             *    estos la id en cuesión. 
             * 2. Para que la estrategia 1 tenga efecto es necesario modificar el trigger que 
             *    asigna el id al objeto InfPerfil. La primera vez que grabamos el perfil ya obtenermos
             *    el id, siendo éste el que usamos para establecer la relación de los hijos con 
             *    padre. Sin embargo, cuando se vuelve a hacer la segunda inserción, el trigger se
             *    vuelve a disparar, dando un nuevo id. Ver código del trigger de la tabla INF_PERFIL.
             */
            session.save(perfil);
            Long perfilID = perfil.getIdPerfil();
            for (InfContenidoPerfil cp : perfil.getContenidoPerfiles()) {
                cp.getId().setIdPerfil(perfilID);
            }
            session.save(perfil);
            tx.commit();
        } catch (HibernateException he) {
            if (tx != null) {
                tx.rollback();
            }
            throw he;
        } finally {
            HibernateUtil.closeSession();
        }
    }

    /**
     * El método hace persistente las modificaciones el objeto recibido como
     * parámetro.
     *
     * @param nuevoPerfil Objeto de la clase <code>InfPerfil</code> que servirá
     * para comparar el nuevoPerfil y determinar qué elementos de la colección
     * de <code>InfContenidoPerfil</code> debe eliminarse.
     * @param nuevoPerfil Objeto de la clase <code>InfPerfil</code> que se hará
     * persistente en la base de datos. El objeto debe existir previamente en la
     * BD.
     * @throws HibernateException Aunque el método captura la excepción que
     * pudiera producirse durante la actuación de Hibernate, la captura sólo se
     * realiza para revertir (rollback) la base de datos a un estado estable
     * anterior, ya que la excepción es vuelta a lanzar para un posterior
     * tratamiento.
     */
    public static void update(InfPerfil nuevoPerfil) {
        Session session = HibernateUtil.getSession();
        Transaction tx = null;
        try {
            tx = HibernateUtil.getTransaction();
            for (InfContenidoPerfil cp : nuevoPerfil.getContenidoPerfiles()) {
                if (!nuevoPerfil.getContenidoPerfiles().contains(cp)) {
                    session.delete(cp);
                }
            }
            session.update(nuevoPerfil);
            tx.commit();
        } catch (HibernateException he) {
            if (tx != null) {
                tx.rollback();
            }
            throw he;
        } finally {
            HibernateUtil.closeSession();
        }
    }

    /**
     * El método elimina de la persistencia la fila que se corresponde con el
     * objeto recibido como parámetro.
     *
     * @param perfil Objeto de la clase <code>InfPerfil</code> que será eliminado
     * de la base de datos.
     * @throws HibernateException Aunque el método captura la excepción que
     * pudiera producirse durante la actuación de Hibernate, la captura sólo se
     * realiza para revertir (rollback) la base de datos a un estado estable
     * anterior, ya que la excepción es vuelta a lanzar para un posterior
     * tratamiento.
     */
    public static void delete(InfPerfil perfil) {
        Session session = HibernateUtil.getSession();
        Transaction tx = null;
        try {
            tx = HibernateUtil.getTransaction();
            session.delete(perfil);
            tx.commit();
        } catch (HibernateException he) {
            if (tx != null) {
                tx.rollback();
            }
            throw he;
        } finally {
            HibernateUtil.closeSession();
        }
    }
}
