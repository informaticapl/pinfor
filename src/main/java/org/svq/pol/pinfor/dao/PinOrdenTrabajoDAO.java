package org.svq.pol.pinfor.dao;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;
import javax.persistence.NoResultException;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.svq.pol.exception.PinforException;
import org.svq.pol.pinfor.bean.GenMiembro;
import org.svq.pol.pinfor.bean.GenSessionUser;
import org.svq.pol.pinfor.bean.auxiliar.PinOrdenServicioAux;
import org.svq.pol.pinfor.utility.Constants;
import org.svq.pol.pinfor.utility.HibernateUtil;

/**
 *
 * @author jjcampac
 */
class PinOrdenTrabajoDAO {

    private static final Logger LOGGER = Logger.getLogger(PinOrdenTrabajoDAO.class);

    private PinOrdenTrabajoDAO() {

    }

    static int searchCount(PinOrdenServicioAux patron, Date desde, Date hasta, String estado, GenSessionUser currentUser) {
        int result = 0;
        Date hoy = Calendar.getInstance(Constants.LOCALE_SPANISH).getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyyHHmmss");

        try {
            String sql = "SELECT count(distinct ot.id_orden_trabajo) "
                    + " FROM pin_orden_trabajo ot WHERE ot.id_orden_trabajo IN "
                    + " (SELECT distinct ot2.id_orden_trabajo"
                    + "  FROM PIN_ORDEN_TRABAJO ot2"
                    + "  WHERE (ot2.REFERENCIA_EXTERNA LIKE '%s' OR ot2.REFERENCIA_EXTERNA IS null)"
                    + "    AND (ot2.REFERENCIA_INTERNA LIKE '%s' OR ot2.REFERENCIA_INTERNA IS null)"
                    + "    AND ot2.FECHA_REGISTRO BETWEEN to_date('%s','ddmmyyyyhh24miss') AND to_date('%s','ddmmyyyyhh24miss')"
                    + "    %s"
                    + "    AND ot2.ID_MIEMBRO_ORIGEN like '%s'"
                    + "    %s"
                    + "    %s)"
                    + " ORDER BY ot.FECHA_REGISTRO asc";

            String rfaExterna = (patron.getReferenciaExterna() == null || patron.getReferenciaExterna().isEmpty()) ? "%" : patron.getReferenciaExterna();
            String rfaInterna = patron.getReferenciaInterna() == null || patron.getReferenciaInterna() == 0 ? "%" : String.valueOf(patron.getReferenciaInterna());
            String fechaIni = "01011970000000";
            String fechaFin = sdf.format(hoy);
            if (desde != null) {
                fechaIni = sdf.format(desde);
            }
            if (hasta != null) {
                Calendar cHasta = Calendar.getInstance(Constants.LOCALE_SPANISH);
                cHasta.setTime(hasta);
                cHasta.set(Calendar.HOUR, 23);
                cHasta.set(Calendar.MINUTE, 59);
                cHasta.set(Calendar.SECOND, 59);
                fechaFin = sdf.format(cHasta.getTime());
            }

            String sqlAmbito = null;
            if (currentUser.getGlobal()) {
                sqlAmbito = Constants.EMPTY_STR;
            } else if (currentUser.getRegional()) {
                sqlAmbito = String.format(" AND (ot2.ID_MIEMBRO_UNIDAD = %d)", currentUser.getUnidad().getIdMiembro());
            }

            String idMiembroOrigen = patron.getOrigen() == null ? "%" : String.valueOf(patron.getOrigen().getIdMiembro());

            String sqlUnidad = "";
            if (patron.getUnidad() != null) {
                sqlUnidad = String.format(" AND ot2.ID_MIEMBRO_UNIDAD = %d", patron.getUnidad().getIdMiembro());
            }

            String sqlVigente;
            switch (estado) {
                case "vigente":
                    sqlVigente = "AND SYSDATE BETWEEN ot2.FECHA_DESDE AND ot2.FECHA_HASTA";
                    break;
                case "cumplido":
                    sqlVigente = "AND SYSDATE  NOT BETWEEN ot2.FECHA_DESDE AND ot2.FECHA_HASTA";
                    break;
                default:
                    sqlVigente = Constants.EMPTY_STR;
            }

            sql = String.format(sql, rfaExterna, rfaInterna, fechaIni, fechaFin, sqlAmbito, idMiembroOrigen, sqlUnidad, sqlVigente);

            Session session = HibernateUtil.getSession();
            HibernateUtil.getTransaction();
            SQLQuery qry = session.createSQLQuery(sql);
            result = ((BigDecimal) qry.uniqueResult()).intValue();
        } finally {
            HibernateUtil.closeSession();
        }
        return result;
    }

    /**
     * Devuelve una lista de objetos de la clase PinParte que se corresponden
     * con todos los atributos del objeto recibido como parámetro.
     *
     * @param patron Parámetro de la clase PinParte que contiene el patrón de la
     * búsqueda.
     * @param desde Fecha desde la que se debe efectuar la búsqueda.
     * @param hasta Fecha hasta la que se tiene que efectuar la búsqueda.
     * @param actuante Objeto de la clase PinActuante que tiene definido el
     * indicativo que tiene que aparecer en el resultado.
     * @return Lista de objetos de la clase PinParte que corresponden con el
     * patrón recibido como parámetro
     */
    static List<PinOrdenServicioAux> search(PinOrdenServicioAux patron, Date desde, Date hasta, String estado, GenSessionUser currentUser) {
        List<PinOrdenServicioAux> result = null;
        Date hoy = Calendar.getInstance(Constants.LOCALE_SPANISH).getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyyHHmmss");

        try {
            String sql = "SELECT ot.* FROM pin_orden_trabajo ot "
                    + " WHERE ot.id_orden_trabajo IN (SELECT distinct ot2.id_orden_trabajo "
                    + "                               FROM PIN_ORDEN_TRABAJO ot2"
                    + "                               WHERE (ot2.REFERENCIA_EXTERNA LIKE '%s' OR ot2.REFERENCIA_EXTERNA IS null)"
                    + "                                 AND (ot2.REFERENCIA_INTERNA LIKE '%s' OR ot2.REFERENCIA_INTERNA IS null)"
                    + "                                 AND ot2.FECHA_REGISTRO BETWEEN to_date('%s','ddmmyyyyhh24miss') AND to_date('%s','ddmmyyyyhh24miss')"
                    + "                                 %s"
                    + "                                 AND ot2.ID_MIEMBRO_ORIGEN like '%s'"
                    + "                                 %s"
                    + "                                 %s)"
                    + " ORDER BY ot.FECHA_REGISTRO asc";

            String rfaExterna = (patron.getReferenciaExterna() == null || patron.getReferenciaExterna().isEmpty()) ? "%" : patron.getReferenciaExterna();
            String rfaInterna = patron.getReferenciaInterna() == null || patron.getReferenciaInterna() == 0 ? "%" : String.valueOf(patron.getReferenciaInterna());
            String fechaIni = "01011970000000";
            String fechaFin = sdf.format(hoy);
            if (desde != null) {
                fechaIni = sdf.format(desde);
            }
            if (hasta != null) {
                Calendar cHasta = Calendar.getInstance(Constants.LOCALE_SPANISH);
                cHasta.setTime(hasta);
                cHasta.set(Calendar.HOUR, 23);
                cHasta.set(Calendar.MINUTE, 59);
                cHasta.set(Calendar.SECOND, 59);
                fechaFin = sdf.format(cHasta.getTime());
            }

            String sqlAmbito = null;
            if (currentUser.getGlobal()) {
                sqlAmbito = Constants.EMPTY_STR;
            } else if (currentUser.getRegional()) {
                sqlAmbito = String.format(" AND (ot2.ID_MIEMBRO_UNIDAD = %d)", currentUser.getUnidad().getIdMiembro());
            }

            String idMiembroOrigen = patron.getOrigen() == null ? "%" : String.valueOf(patron.getOrigen().getIdMiembro());

            String sqlUnidad = "";
            if (patron.getUnidad() != null) {
                sqlUnidad = String.format(" AND ot2.ID_MIEMBRO_UNIDAD = %d", patron.getUnidad().getIdMiembro());
            }

            String sqlVigente;
            switch (estado) {
                case "vigente":
                    sqlVigente = "AND SYSDATE BETWEEN ot2.FECHA_DESDE AND ot2.FECHA_HASTA";
                    break;
                case "cumplido":
                    sqlVigente = "AND SYSDATE  NOT BETWEEN ot2.FECHA_DESDE AND ot2.FECHA_HASTA";
                    break;
                default:
                    sqlVigente = Constants.EMPTY_STR;
            }

            sql = String.format(sql, rfaExterna, rfaInterna, fechaIni, fechaFin, sqlAmbito, idMiembroOrigen, sqlUnidad, sqlVigente);

            Session session = HibernateUtil.getSession();
            HibernateUtil.getTransaction();
            SQLQuery qry = session.createSQLQuery(sql);
            qry.addEntity(PinOrdenServicioAux.class);
            result = qry.list();
        } finally {
            HibernateUtil.closeSession();
        }
        return result;
    }

    static PinOrdenServicioAux getById(Long id) {
        PinOrdenServicioAux result = null;
        Session session = HibernateUtil.getSession();
        try {
            HibernateUtil.getTransaction();
            Criteria crit = session.createCriteria(PinOrdenServicioAux.class);
            crit.add(Restrictions.eq("idOrdenTrabajo", id));
            result = (PinOrdenServicioAux) crit.uniqueResult();
        } finally {
            HibernateUtil.closeSession();
        }
        return result;
    }

    static List<PinOrdenServicioAux> getAllByState(Set<Boolean> vigencia, PinOrdenServicioAux selectedOrdenServicio, GenMiembro unidad) {
        List<PinOrdenServicioAux> result = null;
        Session session = HibernateUtil.getSession();
        Date ahora = new Date();
        try {
            HibernateUtil.getTransaction();
            Criteria crit = session.createCriteria(PinOrdenServicioAux.class);
            Criterion critUnidad = Restrictions.eq("unidad", unidad);
            Criterion critOrdenServicio = null;
            Criterion critY = null;
            Criterion critO;

            if (!vigencia.contains(Boolean.FALSE)) {
                Criterion critFechaDesde = Restrictions.le("fechaDesde", ahora);
                Criterion critFechaHasta = Restrictions.ge("fechaHasta", ahora);
                critY = Restrictions.and(critUnidad, critFechaDesde, critFechaHasta);
            }
            if (selectedOrdenServicio != null) {
                critOrdenServicio = Restrictions.eq("idOrdenTrabajo", selectedOrdenServicio.getIdOrdenTrabajo());
            }

            if (critY != null) {
                if (critOrdenServicio != null) {
                    critO = Restrictions.or(critY, critOrdenServicio);
                } else {
                    critO = critY;
                }
            } else {
                if (critOrdenServicio != null) {
                    critO = Restrictions.or(critUnidad, critOrdenServicio);
                } else {
                    critO = critUnidad;
                }
            }

            crit.add(critO);
            crit.addOrder(Order.asc("fechaRegistro"));
            crit.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
            String where = HibernateUtil.getWhereClause(session, crit);
            result = (List<PinOrdenServicioAux>) crit.list();
        } finally {
            HibernateUtil.closeSession();
        }
        return result;
    }

    static void save(PinOrdenServicioAux orden) throws PinforException {
        Session session = HibernateUtil.getSession();
        Transaction tx = null;
        try {
            tx = HibernateUtil.getTransaction();
            if (orden.getReferenciaInterna() == null || orden.getReferenciaInterna() == 0) {
                Calendar calendar = Calendar.getInstance(Constants.LOCALE_SPANISH);
                calendar.setTime(orden.getFechaRegistro());
                orden.setReferenciaInterna(getSiguienteNumOrdenTrabajo(session, tx, calendar.get(Calendar.YEAR), "T"));
            }
            session.saveOrUpdate(orden);
            tx.commit();
        } catch (HibernateException ex) {
            orden.setReferenciaInterna(0);
            if (tx != null) {
                tx.rollback();
            }
            throw new PinforException(ex);
        } finally {
            HibernateUtil.closeSession();
        }
    }

    static void delete(PinOrdenServicioAux orden) {
        Session session = HibernateUtil.getSession();
        Transaction tx = null;
        try {
            tx = HibernateUtil.getTransaction();
            session.delete(orden);
            tx.commit();
        } catch (Exception ex) {
            session.refresh(orden);
            if (tx != null) {
                tx.rollback();
            }
            throw ex;
        } finally {
            HibernateUtil.closeSession();
        }
    }

    static int getSiguienteNumOrdenTrabajo(Session session, Transaction tx, int agno, String tipo) throws HibernateException {
        int contador;

        String sqlSelect = "SELECT c.contador FROM pin_contador c WHERE c.agno = :agno AND c.tipo = :tipo FOR UPDATE";
        String sqlInsert = "INSERT INTO pin_contador (contador, agno, tipo) VALUES(:contador,:agno,:tipo)";
        String sqlUpdate = "UPDATE pin_contador SET contador = :contador WHERE agno = :agno  AND tipo = :tipo ";

        Query qry = session.createSQLQuery(sqlSelect);
        qry.setParameter("agno", agno);
        qry.setParameter("tipo", tipo);
        try {
            BigDecimal r = (BigDecimal) qry.uniqueResult();
            if (r == null) {
                throw new NoResultException();
            }
            contador = r.intValue() + 1;
            qry = session.createSQLQuery(sqlUpdate);
            qry.setParameter("contador", contador);
            qry.setParameter("agno", agno);
            qry.setParameter("tipo", tipo);
            qry.executeUpdate();
        } catch (NoResultException ex) {
            contador = 1;
            qry = session.createSQLQuery(sqlInsert);
            qry.setParameter("contador", contador);
            qry.setParameter("agno", agno);
            qry.setParameter("tipo", tipo);
            qry.executeUpdate();
        }

        return contador;
    }
}
