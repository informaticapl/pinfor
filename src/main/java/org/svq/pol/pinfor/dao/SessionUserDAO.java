package org.svq.pol.pinfor.dao;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.svq.pol.pinfor.bean.GenSessionUser;
import org.svq.pol.pinfor.utility.HibernateUtil;

/**
 *
 * @author jjcamacho
 */
 final class SessionUserDAO {

    private SessionUserDAO() {
        super();
    }
    
    public static boolean sessionExists(GenSessionUser sessionUser) {
        boolean result = false;
        Session session = HibernateUtil.getSession();
        try {
            HibernateUtil.getTransaction();
            Criteria crit = session.createCriteria(GenSessionUser.class);
            crit.add(Restrictions.eq("id.usuario",sessionUser.getId().getUsuario()));
            crit.add(Restrictions.eq("id.ip",sessionUser.getId().getIp()));
            crit.add(Restrictions.eq("ticket", sessionUser.getTicket()));
            GenSessionUser su = (GenSessionUser) crit.uniqueResult();
            result = su != null;
        } finally {
            HibernateUtil.closeSession();
        }

        return result;
    }

    public static void save(GenSessionUser sessionUser) {
        Session session = HibernateUtil.getSession();
        Transaction tx = null;
        try {
            tx = HibernateUtil.getTransaction();
            session.saveOrUpdate(sessionUser);
            tx.commit();
        } catch (Exception ex) {
            if (tx != null) {
                tx.rollback();
            }
            throw ex;
        } finally {
            HibernateUtil.closeSession();
        }
    }
}
