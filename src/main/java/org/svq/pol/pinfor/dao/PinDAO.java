package org.svq.pol.pinfor.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import org.hibernate.*;
import org.hibernate.exception.ConstraintViolationException;
import org.svq.pol.exception.PinforException;
import org.svq.pol.pinfor.bean.GenLista;
import org.svq.pol.pinfor.bean.GenMiembro;
import org.svq.pol.pinfor.bean.GenSessionUser;
import org.svq.pol.pinfor.bean.InfAplicacion;
import org.svq.pol.pinfor.bean.InfPerfil;
import org.svq.pol.pinfor.bean.PerPersona;
import org.svq.pol.pinfor.bean.PinActuante;
import org.svq.pol.pinfor.bean.PinDestino;
import org.svq.pol.pinfor.bean.PinLista;
import org.svq.pol.pinfor.bean.PinMiembro;
import org.svq.pol.pinfor.bean.PinObjeto;
import org.svq.pol.pinfor.bean.auxiliar.PatronBusqueda;
import org.svq.pol.pinfor.bean.auxiliar.PinLugarAux;
import org.svq.pol.pinfor.bean.auxiliar.PinOrdenServicioAux;
import org.svq.pol.pinfor.bean.auxiliar.PinParteAux;
import org.svq.pol.pinfor.utility.HibernateUtil;

/**
 * La clase PerUsuarioDAO es la encargada de efectuar el acceso a la base de
 * datos, manipulando los datos de forma conveniente y ocultando toda la capa de
 * acceso a datos.
 *
 * @author jjcamacho
 */
public final class PinDAO {

    private PinDAO() {
        super();
    }

//<editor-fold defaultstate="collapsed" desc="Listas y miembros">
    public static List<GenMiembro> getMiembrosByLista(String lista) {
        return GenListasDAO.getMiembrosByLista(lista);
    }

    public static PinMiembro getMiembro(String lista, int valor) {
        return PinListasDAO.getMiembro(lista, valor);
    }

    public static PinMiembro getMiembroCabeceraDeSublista(String sublista) {
        return PinListasDAO.getMiembroCabeceraDeSublista(sublista);
    }

    public static List<PinLista> getListasEspecificas() {
        Session session = HibernateUtil.getSession();
        List<PinLista> result = new ArrayList<>();
        try {
            HibernateUtil.getTransaction();
            result = PinListasDAO.getListasEspecificas(session);
        } finally {
            HibernateUtil.closeSession();
        }
        return result;
    }

    public static PinMiembro getMiembroById(long id) {
        return PinListasDAO.getById(id);
    }

    public static List<PinMiembro> getMiembros(String lista) {
        Session session = HibernateUtil.getSession();
        List<PinMiembro> result = new ArrayList<>();
        try {
            HibernateUtil.getTransaction();
            result = PinListasDAO.getMiembros(lista, session);
        } finally {
            HibernateUtil.closeSession();
        }
        return result;
    }

    public static List<GenMiembro> getMiembrosGenericos(String lista) {
        List<GenMiembro> result = new ArrayList<>();
        try {
            result = GenListasDAO.getMiembrosByLista(lista);
        } finally {
            HibernateUtil.closeSession();
        }
        return result;
    }

    public static List<PinMiembro> getAsuntos() {
        Session session = HibernateUtil.getSession();
        List<PinMiembro> result = new ArrayList<>();
        try {
            HibernateUtil.getTransaction();
            result = PinListasDAO.getAsuntos(session);
        } finally {
            HibernateUtil.closeSession();
        }
        return result;
    }

    public static List<PinMiembro> getMiembrosHijos(String lista) {
        Session session = HibernateUtil.getSession();
        List<PinMiembro> result = new ArrayList<>();
        try {
            HibernateUtil.getTransaction();
            result = PinListasDAO.getMiembrosHijos(lista, session);
        } finally {
            HibernateUtil.closeSession();
        }
        return result;
    }

    public static GenMiembro getGenMiembroById(long id) {
        return GenListasDAO.getById(id);
    }

    public static List<GenMiembro> getUnidades() {
        Session session = HibernateUtil.getSession();
        List<GenMiembro> result = new ArrayList<>();
        try {
            HibernateUtil.getTransaction();
            List<GenLista> listas = GenListasDAO.getListasGenerales(new String[]{"UNI"});
            if (!listas.isEmpty()) {
                result = ((GenLista) listas.get(0)).getMiembros();
            }
        } finally {
            HibernateUtil.closeSession();
        }
        return result;
    }

    public static void saveLista(PinLista lista) {
        Session session = HibernateUtil.getSession();
        Transaction tx = HibernateUtil.getTransaction();
        try {
            PinListasDAO.save(lista, session);
            tx.commit();
            session.refresh(lista);
        } catch (TransactionException ex) {
            if (tx != null) {
                tx.rollback();
            }
            throw ex;
        } finally {
            HibernateUtil.closeSession();
        }
    }

    public static void deleteMiembro(PinMiembro miembro) throws PinforException {
        Session session = HibernateUtil.getSession();
        Transaction tx = HibernateUtil.getTransaction();
        try {
            PinListasDAO.delete(miembro, session);
            tx.commit();
        } catch (TransactionException ex) {
            if (tx != null) {
                tx.rollback();
            }
            throw ex;
        } finally {
            HibernateUtil.closeSession();
        }
    }

    public static void saveMiembro(PinMiembro miembro) {
        Session session = HibernateUtil.getSession();
        Transaction tx = HibernateUtil.getTransaction();
        try {
            PinListasDAO.save(miembro, session);
            tx.commit();
        } catch (TransactionException | ConstraintViolationException | TransientObjectException ex) {
            if (tx != null) {
                tx.rollback();
            }
            throw ex;
        } finally {
            HibernateUtil.closeSession();
        }
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Usuarios">
    public static PerPersona getUserById(Long id, int detalle) {
        return PerUsuarioDAO.getById(id, detalle);
    }

    public static PerPersona getUser(PatronBusqueda patronBusqueda, int detalle) {
        return PerUsuarioDAO.getUser(patronBusqueda, detalle);
    }

    public static void saveUser(GenSessionUser sessionUser) {
        SessionUserDAO.save(sessionUser);
    }

    public static boolean sessionExists(GenSessionUser sessionUser) {
        return SessionUserDAO.sessionExists(sessionUser);
    }

    public static PerPersona getByUserName(String userName, int detalle) {
        return PerUsuarioDAO.getByUserName(userName, detalle);
    }

    public static List<PerPersona> searchPersonaByIndicativo(String indicativo, int detalle) {
        return PerUsuarioDAO.searchPersonaByIndicativo(indicativo, detalle);
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Perfiles">
    public static InfPerfil getPerfil(Long id) {
        return InfPerfilDAO.getPerfil(id);
    }

    public static List<InfPerfil> searchPerfil(InfPerfil patron) {
        return InfPerfilDAO.search(patron);
    }

    public static void savePerfil(InfPerfil perfil) {
        InfPerfilDAO.save(perfil);
    }

    public static void updatePerfil(InfPerfil nuevoPerfil) {
        InfPerfilDAO.update(nuevoPerfil);
    }

    public static void deletePerfil(InfPerfil perfil) {
        InfPerfilDAO.delete(perfil);
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Aplicaciones">
    public static List<InfAplicacion> getAllApplications() {
        return InfAplicacionDAO.getAllApplications();
    }

    public static InfAplicacion getAplicacion(Long id) {
        return InfAplicacionDAO.getAplicacion(id);
    }

    public static InfAplicacion getApplicationByAlias(String alias) {
        return InfAplicacionDAO.getApplicationByAlias(alias);
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Partes">
    public static int searchCountPartes(PinParteAux patron, Boolean consultaOficina, Date desde, Date hasta, PinActuante actuante, String dni, String persona, Integer estado, Boolean incluidoNovedades, Boolean evaluarFechaImpresion, GenSessionUser currentUser) {
        return PinParteDAO.searchCount(patron, consultaOficina, desde, hasta, actuante, dni, persona, estado, incluidoNovedades, evaluarFechaImpresion, currentUser);
    }

    public static List<PinParteAux> searchPartes(PinParteAux patron, Boolean consultaOficina, Date desde, Date hasta, PinActuante actuante, String dni, String persona, Integer estado, Boolean incluidoNovedades, Boolean evaluarFechaImpresion, int initPage, int pageSize, GenSessionUser currentUser) {
        return PinParteDAO.search(patron, consultaOficina, desde, hasta, actuante, dni, persona, estado, incluidoNovedades, evaluarFechaImpresion, initPage, pageSize, currentUser);
    }

    public static PinParteAux getParte(long numParte, int agno, String tipo) {
        return PinParteDAO.getParte(numParte, agno, tipo);
    }

    public static List<PinLugarAux> getPartesEnRadio(int distancia, double lat, double lng, int agnoInicio, int agnoFin) {
        return PinParteDAO.getPartesEnRadio(distancia, lat, lng, agnoInicio, agnoFin);
    }

    public static List<PinLugarAux> getPartesVecinos(int vecinos, double lat, double lng, int agnoInicio, int agnoFin) {
        return PinParteDAO.getPartesVecinos(vecinos, lat, lng, agnoInicio, agnoFin);
    }

    public static void saveParte(PinParteAux parte) throws PinforException {
        PinParteDAO.save(parte);
    }

    public static void deleteParte(PinParteAux parte) throws PinforException {
        PinParteDAO.delete(parte);
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Actuantes">
    public static PinActuante getActuanteById(Long id) {
        return PinActuanteDAO.getById(id);
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Órdenes de servicio">
    public static PinOrdenServicioAux getOrdenTrabajoById(Long id) {
        return PinOrdenTrabajoDAO.getById(id);
    }

    public static int searchCountOrdenTrabajo(PinOrdenServicioAux patron, Date desde, Date hasta, String estado, GenSessionUser currentUser) {
        return PinOrdenTrabajoDAO.searchCount(patron, desde, hasta, estado, currentUser);
    }

    public static List<PinOrdenServicioAux> searchOrdenTrabajo(PinOrdenServicioAux patron, Date desde, Date hasta, String estado, int initPage, int pageSize, GenSessionUser currentUser) {
        try {
            return PinOrdenTrabajoDAO.search(patron, desde, hasta, estado, currentUser);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    public static List<PinOrdenServicioAux> getOrdenesTrabajo(Set<Boolean> vigencia, PinOrdenServicioAux selectedOrdenServicio, GenMiembro unidad) {
        return PinOrdenTrabajoDAO.getAllByState(vigencia, selectedOrdenServicio, unidad);
    }

    public static void saveOrdenTrabajo(PinOrdenServicioAux orden) throws PinforException {
        PinOrdenTrabajoDAO.save(orden);
    }

    public static void deleteOrdenTrabajo(PinOrdenServicioAux orden) {
        PinOrdenTrabajoDAO.delete(orden);
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Objetos">
    public static PinObjeto getObjetoById(Long id) {
        return PinObjetoDAO.getById(id);
    }

    public static int searchCountObjetos(PinObjeto patron, Date desde, Date hasta, GenSessionUser currentUser) {
        return PinObjetoDAO.searchCount(patron, desde, hasta, currentUser);
    }

    public static List<PinObjeto> searchObjetos(PinObjeto patron, Date desde, Date hasta, int initPage, int pageSize, GenSessionUser currentUser) {
        return PinObjetoDAO.search(patron, desde, hasta, currentUser);
    }

    public static void saveObjeto(PinObjeto objeto) throws PinforException {
        PinObjetoDAO.save(objeto);
    }
//</editor-fold>    

//<editor-fold defaultstate="collapsed" desc="Listados">
    public static List<PinParteAux> listadoPorAsuntos(String subtipo, Date desde, Date hasta, boolean incluirBorradores) {
        return PinParteDAO.listadoPorAsuntos(subtipo, desde, hasta, incluirBorradores);
    }

    public static List<PinActuante> listadoPorCategorias(String subtipo, Date desde, Date hasta, boolean incluirBorradores) {
        return PinParteDAO.listadoPorCategorias(subtipo, desde, hasta, incluirBorradores);
    }

    public static List<PinDestino> listadoPorDestinatarios(String subtipo, Date desde, Date hasta, boolean incluirBorradores) {
        return PinParteDAO.listadoPorDestinatarios(subtipo, desde, hasta, incluirBorradores);
    }

    public static List<PinParteAux> listadoPorEstadoTramitacion(String subtipo, Date desde, Date hasta) {
        return PinParteDAO.listadoPorEstadoTramitacion(subtipo, desde, hasta);
    }
//</editor-fold>
}
