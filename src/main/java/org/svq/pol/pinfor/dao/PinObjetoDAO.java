package org.svq.pol.pinfor.dao;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.inject.Inject;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.svq.pol.exception.PinforException;
import org.svq.pol.pinfor.bean.GenSessionUser;
import org.svq.pol.pinfor.bean.PinObjeto;
import org.svq.pol.pinfor.utility.Constants;
import org.svq.pol.pinfor.utility.HibernateUtil;
import org.svq.pol.pinfor.utility.Utilities;

/**
 *
 * @author jjcampac
 */
class PinObjetoDAO {

    private static final Logger LOGGER = Logger.getLogger(PinObjetoDAO.class);

    @Inject
    private Utilities utilities;
    
    private PinObjetoDAO() {

    }

    public static int searchCount(PinObjeto patron, Date desde, Date hasta, GenSessionUser currentUser) {
        int result = 0;
        Date hoy = Calendar.getInstance(Constants.LOCALE_SPANISH).getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyyHHmmss");

        try {
            String sql = "SELECT count(distinct ob.id_objeto)"
                    + " FROM pin_objeto ob"
                    + " WHERE"
                    + "	ob.id_objeto IN ("
                    + "			SELECT DISTINCT ob2.ID_OBJETO"
                    + "			FROM PIN_OBJETO ob2"
                    + "			JOIN PIN_PARTE pa2 ON ob2.ID_PARTE = pa2.ID_PARTE"
                    + "			WHERE 1=1" // necesario porque las demás condiciones son condicionales y deben empezar por AND...
                    + "                   %s"
                    + "      		  AND pa2.FECHA_SUCESO BETWEEN to_date('%s','ddmmyyyyhh24miss') AND to_date('%s','ddmmyyyyhh24miss')"
                    + "                   %s"
                    + "                   AND ob2.id_miembro_tipo like '%s'"
                    + "                   %s"
                    + "                   %s"
                    + "                   %s)"
                    + "ORDER BY ob.ID_OBJETO ASC";

           // String numeroParte = (patron.getParte().getNumeroParte() == null || patron.getParte().getNumeroParte() == 0) ? "%" : patron.getParte().getNumeroParte().toString();
            String sqlIdObjeto = patron.getIdObjeto() <= 0 ? Constants.EMPTY_STR : String.format("  AND ob2.ID_OBJETO = %d", patron.getIdObjeto());

            String fechaIni = "01011970000000";
            String fechaFin = sdf.format(hoy);
            if (desde != null) {
                fechaIni = sdf.format(desde);
            }
            if (hasta != null) {
                Calendar cHasta = Calendar.getInstance(Constants.LOCALE_SPANISH);
                cHasta.setTime(hasta);
                cHasta.set(Calendar.HOUR, 23);
                cHasta.set(Calendar.MINUTE, 59);
                cHasta.set(Calendar.SECOND, 59);
                fechaFin = sdf.format(cHasta.getTime());
            }

            String sqlAmbito = null;
            if (currentUser.getGlobal()) {
                sqlAmbito = Constants.EMPTY_STR;
            } else if (currentUser.getRegional()) {
                sqlAmbito = String.format(" AND (pa2.ID_MIEMBRO_UNIDAD = %d)", currentUser.getUnidad().getIdMiembro());
            }

            String idMiembroTipo = patron.getTipo() == null ? "%" : String.valueOf(patron.getTipo().getIdMiembro());

            String sqlIdentificador = "";
            if (patron.getNumObjeto() != null) {
                sqlIdentificador = String.format(" AND ob2.NUM_OBJETO LIKE %s", "'%" + patron.getNumObjeto() + "%'");
            }

            String sqlNota = "";
            if (patron.getNota() != null) {
                sqlNota = String.format(" AND ob2.NOTA LIKE %s", "'%" + patron.getNota() + "%'");
            }

            String sqlUnidad = "";
            if (patron.getParte().getUnidad() != null) {
                sqlUnidad = String.format(" AND pa2.ID_MIEMBRO_UNIDAD = %d", patron.getParte().getUnidad().getIdMiembro());
            }

            sql = String.format(sql, sqlIdObjeto, fechaIni, fechaFin, sqlAmbito, idMiembroTipo, sqlIdentificador, sqlNota, sqlUnidad);

            Session session = HibernateUtil.getSession();
            HibernateUtil.getTransaction();
            SQLQuery qry = session.createSQLQuery(sql);
            result = ((BigDecimal) qry.uniqueResult()).intValue();
        } finally {
            HibernateUtil.closeSession();
        }
        return result;
    }

    /**
     * Devuelve una lista de objetos de la clase PinObjeto que se corresponden
     * con todos los atributos del objeto recibido como parámetro.
     *
     * @param patron Parámetro de la clase PinParte que contiene el patrón de la
     * búsqueda.
     * @param desde Fecha desde la que se debe efectuar la búsqueda.
     * @param hasta Fecha hasta la que se tiene que efectuar la búsqueda.
     */
    public static List<PinObjeto> search(PinObjeto patron, Date desde, Date hasta, GenSessionUser currentUser) {
        List<PinObjeto> result = null;
        Date hoy = Calendar.getInstance(Constants.LOCALE_SPANISH).getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyyHHmmss");

        try {
            String sql = "SELECT ob.*"
                    + " FROM pin_objeto ob"
                    + " WHERE"
                    + "	ob.id_objeto IN ("
                    + "			SELECT DISTINCT ob2.ID_OBJETO"
                    + "			FROM PIN_OBJETO ob2"
                    + "			JOIN PIN_PARTE pa2 ON ob2.ID_PARTE = pa2.ID_PARTE"
                    + "			WHERE 1=1" // necesario porque las demás condiciones son condicionales y deben empezar por AND...
                    + "                   %s"
                    + "      		  AND pa2.FECHA_SUCESO BETWEEN to_date('%s','ddmmyyyyhh24miss') AND to_date('%s','ddmmyyyyhh24miss')"
                    + "                   %s"
                    + "                   AND ob2.id_miembro_tipo like '%s'"
                    + "                   %s"
                    + "                   %s"
                    + "                   %s"
                    + "                   %s)"
                    + "ORDER BY ob.ID_OBJETO ASC";

           // String numeroParte = (patron.getParte().getNumeroParte() == null || patron.getParte().getNumeroParte() == 0) ? "%" : patron.getParte().getNumeroParte().toString();
            String sqlIdObjeto = patron.getIdObjeto() <= 0 ? Constants.EMPTY_STR : String.format("  AND ob2.ID_OBJETO = %d", patron.getIdObjeto());

            String fechaIni = "01011970000000";
            String fechaFin = sdf.format(hoy);
            if (desde != null) {
                fechaIni = sdf.format(desde);
            }
            if (hasta != null) {
                Calendar cHasta = Calendar.getInstance(Constants.LOCALE_SPANISH);
                cHasta.setTime(hasta);
                cHasta.set(Calendar.HOUR, 23);
                cHasta.set(Calendar.MINUTE, 59);
                cHasta.set(Calendar.SECOND, 59);
                fechaFin = sdf.format(cHasta.getTime());
            }

            String sqlAmbito = null;
            if (currentUser.getGlobal()) {
                sqlAmbito = Constants.EMPTY_STR;
            } else if (currentUser.getRegional()) {
                sqlAmbito = String.format(" AND (pa2.ID_MIEMBRO_UNIDAD = %d)", currentUser.getUnidad().getIdMiembro());
            }

            String idMiembroTipo = patron.getTipo() == null ? "%" : String.valueOf(patron.getTipo().getIdMiembro());

            String sqlIdentificador = "";
            if (patron.getNumObjeto() != null) {
                sqlIdentificador = String.format(" AND ob2.NUM_OBJETO LIKE %s", "'%" + patron.getNumObjeto() + "%'");
            }

            String sqlNota = "";
            if (patron.getNota() != null) {
                sqlNota = String.format(" AND ob2.NOTA LIKE %s", "'%" + patron.getNota() + "%'");
            }

            String sqlUnidad = "";
            if (patron.getParte().getUnidad() != null) {
                sqlUnidad = String.format(" AND pa2.ID_MIEMBRO_UNIDAD = %d", patron.getParte().getUnidad().getIdMiembro());
            }

            sql = String.format(sql, sqlIdObjeto, fechaIni, fechaFin, sqlAmbito, idMiembroTipo, sqlIdObjeto, sqlIdentificador, sqlNota, sqlUnidad);

            Session session = HibernateUtil.getSession();
            HibernateUtil.getTransaction();
            SQLQuery qry = session.createSQLQuery(sql);
            qry.addEntity(PinObjeto.class);
            result = qry.list();
        } finally {
            HibernateUtil.closeSession();
        }
        return result;
    }

    public static PinObjeto getById(Long id) {
        PinObjeto result = null;
        Session session = HibernateUtil.getSession();
        try {
            HibernateUtil.getTransaction();
            Criteria crit = session.createCriteria(PinObjeto.class);
            crit.add(Restrictions.eq("idObjeto", id));
            result = (PinObjeto) crit.uniqueResult();
        } finally {
            HibernateUtil.closeSession();
        }
        return result;
    }

    /**
     * Método encargado de hacer persistente el objeto recibido como parámetro.
     *
     * @param objeto Objeto de la clase <code>PinObjeto</code> que se hará
     * persistente en la base de datos y que previamente no existía.
     * @throws HibernateException Excepción elevada cuando se produce un error
     * en la gestión de Hibernate. Aunque es capturada para devolver la base de
     * datos a su estado anterior (rollback), la excepción es vuelta a lanzar
     * para su posterior tratamiento.
     */
    public static void save(PinObjeto objeto) throws PinforException {
        Session session = HibernateUtil.getSession();
        Transaction tx = null;
        try {
            tx = HibernateUtil.getTransaction();

            session.saveOrUpdate(objeto);
            tx.commit();
        } catch (HibernateException ex) {
            if (tx != null) {
                tx.rollback();
            }
            throw new PinforException(ex);
        } finally {
            HibernateUtil.closeSession();
        }
    }
}
