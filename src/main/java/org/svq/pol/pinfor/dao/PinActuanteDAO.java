package org.svq.pol.pinfor.dao;

import org.hibernate.Query;
import org.hibernate.Session;
import org.svq.pol.pinfor.bean.PinActuante;
import org.svq.pol.pinfor.utility.HibernateUtil;

/**
 * La clase AplicacionDAO es la encargada de efectuar el acceso a la base de
 * datos, manipulando los datos de forma conveniente y ocultando toda la capa de
 * acceso a datos.
 *
 * @author jjcamacho
 */

final class PinActuanteDAO {


    private PinActuanteDAO() {
    }

    /**
     * Devuelve una lista con todas las cursos que están por celebrar
     *
     * @return Lista de aplicaciones
     */
    public static PinActuante getById(Long id) {
        PinActuante result = null;
        Session session = HibernateUtil.getSession();
        try {
            HibernateUtil.getTransaction();
            Query qry = session.createQuery("from PinActuante where idActuante = :id");
            qry.setParameter("id", id);
            result = (PinActuante) qry.uniqueResult();
        } finally {
            HibernateUtil.closeSession();
        }
        return result;
    }

}
