package org.svq.pol.pinfor.utility.converter;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import org.apache.commons.validator.routines.RegexValidator;
import org.apache.commons.validator.routines.UrlValidator;

/**
 *
 * @author jjcamacho
 */
@FacesConverter(value = "URLConverter")
public class URLConverter implements Converter {

    private static final String HTTP = "http://";
    private static final String HTTPS = "https://";

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String value) {
        StringBuilder url = new StringBuilder();
        String result;

        /* Como se permiten las URL relativas a http:webpol.ayuntamiento.svq, se comprueba
         * si la URL dada empieza por "/", en cuyo caso el valor devuelto será el mismo
         * que el introducido por el usuario.
         */
        if (value.startsWith("/")) {
            result = value;
        } else {
            /* En caso de que no empiece pro "/", se trata de comprobar si es una URL absoluta, para lo que
             * comprobaremos si empieza con http:// o https://. Si no fura así se le añade http:// y a 
             * continuación se añade el valor para obtener la URL a comprobar. */
            if (!value.startsWith(HTTP) && !value.startsWith(HTTPS)) {
                url.append(HTTP);
            }
            url.append(value);

            // Ahora se usa el validador de URL de Apache Common...
            String[] regexs = new String[] {".{3,}\\.(ayuntamiento)\\.(svq)(:\\d+)?"};
            RegexValidator validator = new RegexValidator(regexs, false);
            UrlValidator urlValidator = new UrlValidator(validator, UrlValidator.ALLOW_LOCAL_URLS);
            if (!urlValidator.isValid(url.toString())) {
                FacesMessage msg = new FacesMessage("Error de conversión de URL.", "Formato de URL no válido.");
                msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                throw new ConverterException(msg);
            }
            result = url.toString();
        }

        return result;
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object value) {
        return value.toString();
    }
}
