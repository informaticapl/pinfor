package org.svq.pol.pinfor.utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import javax.faces.annotation.ApplicationMap;
import javax.faces.annotation.RequestMap;
import javax.faces.annotation.SessionMap;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.application.ViewHandler;
import javax.faces.component.UIViewRoot;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.primefaces.PrimeFaces;
import org.primefaces.model.UploadedFile;
import org.primefaces.model.map.LatLng;
import org.svq.pol.exception.UserNotAllowedException;
import org.svq.pol.pinfor.backing.PinListasBacking;
import org.svq.pol.pinfor.bean.GenLista;
import org.svq.pol.pinfor.bean.GenMiembro;
import org.svq.pol.pinfor.bean.GenSessionUser;
import org.svq.pol.pinfor.bean.InfAplicacion;
import org.svq.pol.pinfor.bean.PerPersona;
import org.svq.pol.pinfor.dao.PinDAO;

/**
 * Clase estática con utilidadesimport org.svq.pol.pinfor.bean.GenLista; import
 * org.svq.pol.pinfor.bean.GenMiembro; varias.
 *
 * @author jjcamacho
 */
public class Utilities implements Serializable {

    private static final long serialVersionUID = 865100450925926193L;
    private static final Logger LOGGER = Logger.getLogger(Utilities.class);
    private int COOKIE_MAX_AGE;

    private Application application;

    @Inject
    private PinListasBacking listasBacking;

    @Inject
    @ApplicationMap
    private Map<String, Object> applicationMap;

    @Inject
    @SessionMap
    private Map<String, Object> sessionMap;

    @Inject
    @RequestMap
    private Map<String, Object> requestMap;

    @Inject
    private FacesContext facesContext;

    @Inject
    private ExternalContext externalContext;

    /**
     * El método devuelve el valor correspondiente a la propiedad
     * <code>property</code> del archivo <code>file</code>
     *
     * @param file Nombre del archivo de propiedades del que se quiere consultar
     * la propiedad <code>property</code>.
     * @param property Nombre de la propiedad a consultar.
     * @return String Valor de la propiedad <code>property</code>
     * @throws FileNotFoundException
     * @throws IOException
     */
    public String getProperty(String file, String property) throws IOException {
        String realPath = getExternalContext().getRealPath("/");
        Properties prop = new Properties();
        String result;
        try ( FileInputStream fis = new FileInputStream(realPath.concat("/WEB-INF/classes/").concat(file))) {
            prop.load(fis);
            result = prop.getProperty(property);
        }
        return result;
    }

    public void addFacesMessage(Logger logger, int tipo, String detalle) {
        Severity severity;
        String resumen;
        switch (tipo) {
            case Constants.FATAL:
                severity = FacesMessage.SEVERITY_FATAL;
                resumen = "ERROR GRAVE";
                break;
            case Constants.ERROR:
                severity = FacesMessage.SEVERITY_ERROR;
                resumen = "ERROR";
                break;
            case Constants.WARN:
                severity = FacesMessage.SEVERITY_WARN;
                resumen = "ATENCIÓN";
                break;
            case Constants.INFO:
                severity = FacesMessage.SEVERITY_INFO;
                resumen = "AVISO";
                break;
            default:
                severity = FacesMessage.SEVERITY_INFO;
                resumen = "AVISO";
                break;
        }
        FacesMessage message = new FacesMessage(severity, resumen, detalle);
        getFacesContext().addMessage(null, message);
        String msg = resumen;
        if (detalle != null) {
            msg = msg.concat(detalle);
        }
        Log.log(logger, tipo, msg);
    }

    public Application getApplication() {
        return facesContext.getApplication();
    }

    public Map<String, Object> getApplicationMap() {
        return applicationMap;
    }

    /**
     * Obtiene el mapa correspondiente a la sesión activa.
     *
     * @return Map<String, Object>
     */
    public Map<String, Object> getSessionMap() {
        return sessionMap;
    }

    /**
     * Obtiene el mapa correspondiente a la petición que se está tratando.
     *
     * @return Map<String, Object>
     */
    public Map<String, Object> getRequestMap() {
        return requestMap;
    }

    /**
     * Obtiene el contexto de la aplicación.
     *
     * @return
     */
    public ExternalContext getExternalContext() {
        return externalContext;
    }

    /**
     * Obtiene el objeto request asociado a la petición http.
     *
     * @return HttpServletRequest
     */
    public HttpServletRequest getRequest() {
        return (HttpServletRequest) externalContext.getRequest();
    }

    /**
     * Obtiene la instancia actual asociada al contexto de Faces.
     *
     * @return FacesContext
     */
    public FacesContext getFacesContext() {
        return facesContext;
    }

    /**
     * El método sirve tanto para establecer un nuevo usuario como para eliminar
     * a uno existente, es decir, para login y logout.
     *
     * @param sessionUser <code>String</code>. Un valor null permite eliminar al
     * usuario del mapa de valores para la sesión, mientras que un objeto que
     * implemente la interfaz <code>Usuario</code> establecerá al mismo como
     * usuario válido de la sesión.
     * @throws org.svq.pol.exception.UserNotAllowedException
     * @throws java.security.NoSuchAlgorithmException
     */
    public void setCurrentUser(GenSessionUser sessionUser) throws UserNotAllowedException, NoSuchAlgorithmException {
        // Primero se invalida la sesión actual
        invalidateSession();

        String url = HibernateUtil.getDbURL();
        int pos = url.lastIndexOf('/') + 1;
        InfAplicacion app = PinDAO.getApplicationByAlias(getRequest().getContextPath().substring(1));

        int detalle = Constants.BASICO | Constants.PERFIL | Constants.UNIDAD | Constants.PLANTILLA | Constants.CENTRO_TRABAJO;
        PerPersona persona = PinDAO.getByUserName(sessionUser.getId().getUsuario(), detalle);
        sessionUser.getId().setIp(getRequest().getRemoteHost());
        sessionUser.setUnidad(persona.getUnidadList().get(0).getMiembro());
        sessionUser.setIndicativo(persona.getPlantillaList().get(0).getIndicativo());
        sessionUser.setIndicativoJa(persona.getPlantillaList().get(0).getIndicativoJa());
        sessionUser.setCategoria(persona.getPlantillaList().get(0).getCategoria());
        sessionUser.setCentroTrabajo(persona.getCentroTrabajoList().get(0).getMiembro());
        sessionUser.setAplicaciones(persona.getPerfilList());
        String ticket = getMD5(sessionUser.getId().getIp() + (new Date()).getTime());
        sessionUser.setTicket(ticket);
        PinDAO.saveUser(sessionUser);

        if (app != null && sessionUser.getAutorizado(app)) {
            // Establecer variables de sesión
            getSessionMap().put("user", sessionUser);
            getSessionMap().put("db", url.substring(pos));
        } else {
            throw new UserNotAllowedException();
        }
    }

    /**
     * Devuelve al usuario actualmente registrado en la sesión.
     *
     * @return GenSessionUser
     */
    public GenSessionUser getCurrentUser() {
        return (GenSessionUser) getSessionMap().get("user");
    }

    /**
     * Elimina al usuario de la sesión activa.
     *
     * @param sessionUser
     * @throws java.io.IOException
     */
    public void cancelUser(GenSessionUser sessionUser) throws IOException {
        String servidor = getRequest().getServerName();
        String puerto = String.valueOf(getRequest().getServerPort());
        externalContext.redirect("http://".concat(servidor).concat(":").concat(puerto).concat("/portal/faces/login.xhtml"));
        invalidateSession();
    }

    public void invalidateSession() {
        try {
            HttpSession session = (HttpSession) externalContext.getSession(false);
            if (session != null) {
                session.invalidate();
            }
        } catch (Exception ex) {
            Log.log(LOGGER, Constants.WARN, ex.getMessage());
        }
    }

    public void setCookies(GenSessionUser sessionUser, boolean delete) {
        try {
            COOKIE_MAX_AGE = Integer.valueOf(getProperty("/default.properties", "cookieMaxAge").trim());

            HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
            Cookie cUser = new Cookie("user", sessionUser.getId().getUsuario());
            Cookie cTicket = new Cookie("ticket", sessionUser.getTicket());
            cUser.setPath("/");
            cTicket.setPath("/");
            if (delete) {
                cUser.setMaxAge(0);
                cTicket.setMaxAge(0);
            } else {
                /* Se pone la caducidad a 24 horas desde este momento.
             * De esta forma, cada vez que el usuario accede a una página, se le prorrogan 
             * las cookies durante 24 horas más, para que no tenga que logarse. Si falta al
             * trabajo más de 24 horas, las cookies habrán caducado, con lo que no existirá ticket y
             * por tanto será redirigido a la página de login.
                 */
                cUser.setMaxAge(COOKIE_MAX_AGE);
                cTicket.setMaxAge(COOKIE_MAX_AGE);
            }
            response.addCookie(cUser);
            response.addCookie(cTicket);
        } catch (IOException ex) {
            Log.log(LOGGER, Constants.FATAL, "Excepcion inicializando LoginBacking: " + ex.getMessage());
        }
    }

    public String getUrlBase() {
        StringBuffer ru = getRequest().getRequestURL();
        return ru.substring(0, ru.indexOf(getRequest().getContextPath()));
    }

    public boolean resourceExists(String url) {
        try {
            HttpURLConnection.setFollowRedirects(false);
            // note : you may also need
            //        HttpURLConnection.setInstanceFollowRedirects(false)
            HttpURLConnection con = (HttpURLConnection) new URL(url).openConnection();
            con.setRequestMethod("HEAD");
            return (con.getResponseCode() == HttpURLConnection.HTTP_OK);
        } catch (IOException ex) {
            Log.log(LOGGER, Constants.WARN, ex.getMessage());
            return false;
        }

    }

    public void executeUI(String command) {
        PrimeFaces context = PrimeFaces.current();
        context.executeScript(command);
    }

    public void refresh() {
        String viewId = getFacesContext().getViewRoot().getViewId();
        ViewHandler handler = getApplication().getViewHandler();
        UIViewRoot root = handler.createView(getFacesContext(), viewId);
        root.setViewId(viewId);
        getFacesContext().setViewRoot(root);
    }

    public GenMiembro getMemberById(long id) {
        boolean found = false;
        GenMiembro result = null;

        Iterator itLista = listasBacking.getListas().iterator();
        while (!found && itLista.hasNext()) {
            GenLista l = (GenLista) itLista.next();
            Iterator itMiembro = l.getMiembros().iterator();
            while (!found && itMiembro.hasNext()) {
                GenMiembro m = (GenMiembro) itMiembro.next();
                if (m.getIdMiembro() == id) {
                    result = m;
                    found = true;
                }
            }
        }
        return result;
    }

    public Serializable getExceptionCause(Exception ex) {
        return ex.getCause() != null ? ex.getCause() : ex.getMessage();
    }

    public String getMD5(String texto) throws NoSuchAlgorithmException {
        return DigestUtils.md5Hex(texto);
    }

    public boolean urlResourceExists(String urlName) {
        boolean result = false;
        try {
            HttpURLConnection.setFollowRedirects(false);
            // note : you may also need
            //        HttpURLConnection.setInstanceFollowRedirects(false)
            HttpURLConnection con = (HttpURLConnection) new URL(urlName).openConnection();
            con.setRequestMethod("HEAD");
            result = (con.getResponseCode() == HttpURLConnection.HTTP_OK);
            return result;
        } catch (IOException ex) {
            Log.log(LOGGER, Constants.WARN, ex.getMessage());
            return result;
        }
    }

    public String getImagesResourcePath() {
        return getExternalContext().getRequestContextPath().concat("/resources/images/");
    }

    public void saveUploadedFile(UploadedFile uploadedFile, String fileName, String fileType, Date fechaRegistro) throws IOException {
        InputStream input;
        OutputStream output;
        Calendar cal = Calendar.getInstance();
        cal.setTime(fechaRegistro);
        int year = cal.get(Calendar.YEAR);
        File path = new File(getProperty("/default.properties", "filesPath") + "/" + year);

        if (!path.exists()) {
            path.mkdir();
        }
        input = uploadedFile.getInputstream();
        output = new FileOutputStream(new File(path, fileName + fileType));
        IOUtils.copy(input, output);
        input.close();
        output.close();
    }

    public void saveUploadedFile(File uploadedFile, String fileName, String fileType, Date fechaRegistro) throws IOException {
        Calendar cal = Calendar.getInstance();
        cal.setTime(fechaRegistro);
        int year = cal.get(Calendar.YEAR);
        File path = new File(getProperty("/default.properties", "filesPath") + "/" + year);

        if (!path.exists()) {
            path.mkdir();
        }

        File target = new File(path.getAbsolutePath(), fileName + fileType);
        if (uploadedFile.renameTo(target)) {
            uploadedFile.delete();
        } else {
            throw new IOException();
        }
    }

    /**
     * Añade un desplazamiento determinado por offsetMeters a las coordenadas
     * recibidas como parámetro.
     *
     * @param latLng
     * @param offsetMeters Desplazamiento expresado en metros
     * @return <code>LatLng</code>
     */
    public LatLng addMeters(LatLng latLng, int offsetMeters) {
        int R = 6378137; //Radio de la Tierra
        double dLat = offsetMeters / R;
        double dLng = offsetMeters / (R * Math.cos(Math.PI * latLng.getLat() / 180));

        double newLat = latLng.getLat() + dLat * 180 / Math.PI;
        double newLng = latLng.getLng() + dLng * 180 / Math.PI;

        return new LatLng(newLat, newLng);
    }
}
