package org.svq.pol.pinfor.utility;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.internal.CriteriaImpl;
import org.hibernate.internal.SessionFactoryImpl;
import org.hibernate.loader.criteria.CriteriaQueryTranslator;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.stat.Statistics;

/**
 * Hibernate Utility class with a convenient method to get Session Factory
 * object.
 *
 * @author jjcamacho
 */
public final class HibernateUtil {

    private static final Logger LOGGER = Logger.getLogger(HibernateUtil.class);

    /**
     * La constante sessionFactory hace referencia a un objeto de la citada
     * clase, mientras que las constantes de la clase ThreadLocal son objetos
     * que referenciarán a las clases Session y Transaction por hilo de
     * ejecución.
     */
    private static ServiceRegistry serviceRegistry;
    private static SessionFactory sessionFactory;
    private static String dbURL;

    static {

        try {
            sessionFactory = configureSessionFactory();
        } catch (Exception ex) {
            Log.log(LOGGER, Constants.ERROR, "Excepcion creando SessionFactory: " + ex.getMessage());
        }
    }

    private HibernateUtil() {
    }

    private static SessionFactory configureSessionFactory() {
        try {
            // Create the SessionFactory from standard (hibernate.cfg.xml) config file.  
            Configuration configuration = new Configuration();
            configuration.configure("hibernate.cfg.xml");
            dbURL = configuration.getProperty("hibernate.connection.datasource");
            serviceRegistry = new StandardServiceRegistryBuilder()
                    .applySettings(configuration.getProperties())
                    .build();
            sessionFactory = configuration.buildSessionFactory(serviceRegistry);
        } catch (HibernateException ex) {
            Log.log(LOGGER, Constants.FATAL, ex.getMessage());
        }
        return sessionFactory;
    }

    /**
     * Obtiene el objeto sessionFactory creado desde el archivo de configuración
     * estándar 'hibernate.cfg.xml'
     *
     * @return SessionFactory
     */
    public static SessionFactory getSessionFactory() {
        if (sessionFactory == null) {
            rebuildSessionFactory();
        }
        return sessionFactory;
    }

    public static void rebuildSessionFactory() {
        try {
            sessionFactory = configureSessionFactory();
        } catch (Exception ex) {
            Log.log(LOGGER, Constants.FATAL, "Excepcion reconstruyendo SessionFactory: " + ex.getMessage());
        }
    }

     public static Session getSession() throws HibernateException {
        Session session = getSessionFactory().getCurrentSession();
        if (session == null || !session.isOpen()) {
            session = (sessionFactory != null) ? sessionFactory.openSession() : null;
        }
        return session;
    }

       
    public static void closeSession() throws HibernateException {
        Session session = getSessionFactory().getCurrentSession();
        if (session != null) {
            session.close();
        }
    }
    
    public static Transaction getTransaction() {
        Session session = getSession();
        Transaction result;
        if (session.getTransaction() != null && session.getTransaction().isActive()) {
            result = session.getTransaction();
        } else {
            result = session.beginTransaction();
        }
        return result;
    }

    public static String getDbURL() {
        return dbURL;
    }

    public static Statistics getStatistics() {
        Statistics stats = HibernateUtil.getSessionFactory().getStatistics();
        stats.setStatisticsEnabled(true);
        return stats;
    }

    public static void showStatistics(Statistics stats) {
        Log.log(LOGGER, Constants.INFO, "Num. de peticion de conexiones: " + stats.getConnectCount());
        Log.log(LOGGER, Constants.INFO, "Num. de sesiones abiertas por el codigo: " + stats.getSessionOpenCount());
        Log.log(LOGGER, Constants.INFO, "Num. de sesiones cerradas por el codigo: " + stats.getSessionCloseCount());
        Log.log(LOGGER, Constants.INFO, "Num. de flushes hechos: " + stats.getFlushCount());
        Log.log(LOGGER, Constants.INFO, "Num. de transacciones (fallidas y exitosas): " + stats.getTransactionCount());
        Log.log(LOGGER, Constants.INFO, "Num. de transacciones exitosas: " + stats.getSuccessfulTransactionCount());
        Log.log(LOGGER, Constants.INFO, "Num. queries ejecutadas: " + stats.getQueryExecutionCount());
        Log.log(LOGGER, Constants.INFO, "Query mas lenta: " + stats.getQueryExecutionMaxTimeQueryString());
        Log.log(LOGGER, Constants.INFO, "Tiempo ejecucion query mas lenta: " + stats.getQueryExecutionMaxTime() + "ms");
        Log.log(LOGGER, Constants.INFO, "Num. de colecciones extraidas de la DB: " + stats.getCollectionFetchCount());
        Log.log(LOGGER, Constants.INFO, "Num. de colecciones cargadas de la DB: " + stats.getCollectionLoadCount());
        Log.log(LOGGER, Constants.INFO, "Num. de colecciones reconstruidas: " + stats.getCollectionRecreateCount());
        Log.log(LOGGER, Constants.INFO, "Num. de colecciones eliminadas: " + stats.getCollectionRemoveCount());
        Log.log(LOGGER, Constants.INFO, "Num. de colecciones actualizadas: " + stats.getCollectionUpdateCount());
        Log.log(LOGGER, Constants.INFO, "Num. de objetos eliminados: " + stats.getEntityDeleteCount());
        Log.log(LOGGER, Constants.INFO, "Num. de objetos extraidos: " + stats.getEntityFetchCount());
        Log.log(LOGGER, Constants.INFO, "Num. de objetos cargados (completamente poblados): " + stats.getEntityLoadCount());
        Log.log(LOGGER, Constants.INFO, "Num. de objetos insertados: " + stats.getEntityInsertCount());
        Log.log(LOGGER, Constants.INFO, "Num. de objetos actualizados: " + stats.getEntityUpdateCount());
        Log.log(LOGGER, Constants.INFO, "Num. de queries obtenidas de la cache: " + stats.getQueryCacheHitCount());
        Log.log(LOGGER, Constants.INFO, "Num. de cache queries no encontradas en la cache: " + stats.getQueryCacheMissCount());
    }

    public static String getWhereClause(Session session, Criteria criteria) {
        return new CriteriaQueryTranslator(
                (SessionFactoryImpl) session.getSessionFactory(),
                (CriteriaImpl) criteria,
                ((CriteriaImpl) criteria).getEntityOrClassName(),
                CriteriaQueryTranslator.ROOT_SQL_ALIAS)
                .getWhereCondition();
    }
}
