/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.svq.pol.pinfor.utility.comparator;

import java.io.Serializable;
import java.util.Comparator;
import org.primefaces.model.TreeNode;
import org.svq.pol.pinfor.bean.PerPersona;

/**
 *
 * @author jjcamacho
 */
public class TreeNodePersonaApellidos implements Comparator<TreeNode>, Serializable {
    private static final long serialVersionUID = -3997042804670140425L;

    @Override
    public int compare(TreeNode o1, TreeNode o2) {
        PerPersona obj1 = (PerPersona) o1.getData();
        PerPersona obj2 = (PerPersona) o2.getData();

        int result;
        result = obj1.getApellido1().compareToIgnoreCase(obj2.getApellido1());
        if(result == 0) {
            result = obj1.getApellido2().compareToIgnoreCase(obj2.getApellido2());
        }
        return result; 
    }
    
}
