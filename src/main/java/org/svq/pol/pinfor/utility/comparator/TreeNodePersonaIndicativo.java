/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.svq.pol.pinfor.utility.comparator;

import java.io.Serializable;
import java.util.Comparator;
import org.primefaces.model.TreeNode;
import org.svq.pol.pinfor.bean.PerPersona;

/**
 *
 * @author jjcamacho
 */
public class TreeNodePersonaIndicativo implements Comparator<TreeNode>, Serializable {
    private static final long serialVersionUID = -7095412281046389083L;

    @Override
    public int compare(TreeNode o1, TreeNode o2) {
        PerPersona obj1 = (PerPersona) o1.getData();
        PerPersona obj2 = (PerPersona) o2.getData();
        int clave1 = Integer.parseInt(obj1.getPlantillaList().get(0).getClaveOrden());
        int clave2 = Integer.parseInt(obj2.getPlantillaList().get(0).getClaveOrden());
        int result;
        if(clave1>clave2){
            result = 1;
        } else {
            if(clave1 == clave2){
                result = 0;
            } else {
                result = -1;
            }
        }
        return result; 
    }
    
}
