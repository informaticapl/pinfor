package org.svq.pol.pinfor.utility.converter;

import java.util.Objects;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import org.svq.pol.pinfor.bean.PinOrdenServicio;
import org.svq.pol.pinfor.dao.PinDAO;

/**
 *
 * @author jjcamacho
 */
@FacesConverter(value = "PinOrdenServicioConverter")
public class PinOrdenServicioConverter implements Converter {

    private Long id;

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String string) {
        Object result = null;
        if (string.trim().equals("")) {
            return result;
        } else {
            try {
                id = Long.parseLong(string);
                result = PinDAO.getOrdenTrabajoById(id);
            } catch (NumberFormatException ex) {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error de conversión de la orden de servicio: " + ex.getMessage(), ""));
            }
        }
        return result;
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object obj) {
        String result = "";
        if (obj != null) {
            result = String.valueOf(((PinOrdenServicio) obj).getIdOrdenTrabajo());
        }
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PinOrdenServicioConverter other = (PinOrdenServicioConverter) obj;
        return Objects.equals(this.id, other.id);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.id);
        return hash;
    }
}
