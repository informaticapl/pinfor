package org.svq.pol.pinfor.utility.converter;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import org.svq.pol.pinfor.bean.InfAplicacion;
import org.svq.pol.pinfor.dao.PinDAO;

/**
 *
 * @author jjcamacho
 */
@FacesConverter(forClass = InfAplicacion.class)
public class InfAplicacionConverter implements Converter {

    private Long id;

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String string) {
        Object result = null;
        if (string.trim().equals("")) {
            return result;
        } else {
            try {
                id = Long.parseLong(string);
                result = PinDAO.getAplicacion(id);
            } catch (NumberFormatException ex) {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error de conversión a la clase Aplicacion: " + ex.getMessage(),""));
            }
        }
        return result;
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object obj) {
        String result = "";
        if (obj != null) {
            result = String.valueOf(((InfAplicacion) obj).getIdAplicacion());
        }
        return result;
    }
}
