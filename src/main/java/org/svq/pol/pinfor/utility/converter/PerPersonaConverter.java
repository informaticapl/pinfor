package org.svq.pol.pinfor.utility.converter;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import org.svq.pol.pinfor.bean.PerPersona;
import org.svq.pol.pinfor.dao.PinDAO;

/**
 *
 * @author jjcamacho
 */
@FacesConverter(forClass = PerPersona.class)
public class PerPersonaConverter implements Converter {

    /* El atributo 'detalle' actúa como parámetro del converter de manera que pueda
     * determinarse desde la propia vista xhtml la cantidad de información de la persona
     * que va a obtenerse. Varios puntos a tener en cuenta:
     * 1. La clase converter debe registrarse en el archivo de configuración faces-config.xml. Es aquí donde se define
     * el nombre y el tipo del parámetro a usar.
     * 2. Debe definirse una nueva librería (taglib.xml) para definir la etiqueta con la que va a llamarse al converter desde la vista.
     * 3. Esta librería taglib debe registrarse por medio del archivo web.xml
     * 4. En la vista hay que incluir la librería en el encabezado como se haría con cualquier otra, asignándole un prefijo.
     * 5. Al usar el converter en la vista, debe darse un valor al parámetro definido. En el caso de este converter, el uso sería:
     *      <j:tagPersonaConverter detalle="12"/>
     */
    private Long id;
    private int detalle;

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String string) {
        Object result = null;
        if (string.trim().equals("")) {
            return result;
        } else {
            try {
                id = Long.parseLong(string);
                result = PinDAO.getUserById(id, detalle);
            } catch (NumberFormatException ex) {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error de conversión de persona: " + ex.getMessage(), ""));
            } 
        }
        return result;
    }

    public int getDetalle() {
        return detalle;
    }

    public void setDetalle(int detalle) {
        this.detalle = detalle;
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object obj) {
        String result = "";
        if (obj != null) {
            result = String.valueOf(((PerPersona) obj).getIdPersona());
        }
        return result;
    }
}
