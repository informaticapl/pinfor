package org.svq.pol.pinfor.utility.serializer;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import java.lang.reflect.Type;

/**
 * GesateMovil app
 * Created by jjcampac on 24/02/2015.
 */
public class JsonBooleanDeserializer implements JsonDeserializer<Boolean> {
    @Override
    public Boolean deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        try {
            Integer value = json.getAsJsonPrimitive().getAsInt();
            return value == 1;
        } catch (ClassCastException e) {
            throw new JsonParseException("Cannot parse json value '" + json.toString() + "'", e);
        }
    }
}
