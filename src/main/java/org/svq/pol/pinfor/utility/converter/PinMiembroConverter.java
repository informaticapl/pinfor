package org.svq.pol.pinfor.utility.converter;

import java.util.Objects;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import org.svq.pol.pinfor.bean.PinMiembro;
import org.svq.pol.pinfor.dao.PinDAO;

/**
 *
 * @author jjcamacho
 */
@FacesConverter(value = "PinMiembroConverter")
public class PinMiembroConverter implements Converter {

    private Long id;

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String string) {
        Object result = null;
        if (string.trim().equals("")) {
            return result;
        } else {
            try {
                id = Long.parseLong(string);
                result = PinDAO.getMiembroById(id);
            } catch (NumberFormatException  ex) {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error de conversión del elemento de la lista: " + ex.getMessage(), ""));
            } 
        }
        return result;
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object obj) {
        String result = "";
        if (obj != null) {
            result = String.valueOf(((PinMiembro) obj).getIdMiembro());
        }
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PinMiembroConverter other = (PinMiembroConverter) obj;
        return Objects.equals(this.id, other.id);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.id);
        return hash;
    }
}
