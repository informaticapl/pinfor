package org.svq.pol.pinfor.utility.comparator;

import java.io.Serializable;
import java.util.Comparator;
import org.svq.pol.pinfor.bean.InfPerfil;

/**
 *
 * @author jjcampac
 */
public class PerfilNombreComparator implements Comparator, Serializable {
    private static final long serialVersionUID = 6837269032976783219L;

    @Override
    public int compare(Object obj1, Object obj2) {
        int cmp = 0;
        InfPerfil perfil1 = null;
        InfPerfil perfil2 = null;
        if(obj1 instanceof InfPerfil) {
            perfil1 = (InfPerfil) obj1;
        }
        if(obj2 instanceof InfPerfil) {
            perfil2 = (InfPerfil) obj2;
        }
        if(perfil1 != null && perfil2 != null) {
            cmp = perfil1.getPerfil().compareTo(perfil2.getPerfil());
        }
        
        return cmp;
    }
    
}
