/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.svq.pol.pinfor.utility.comparator;

import java.io.Serializable;
import java.util.Comparator;
import org.primefaces.model.TreeNode;
import org.svq.pol.pinfor.bean.GenMiembro;

/**
 *
 * @author jjcamacho
 */
public class TreeNodeClaveOrdenComparator implements Comparator<TreeNode>, Serializable {
    private static final long serialVersionUID = 7975838994512623305L;

    @Override
    public int compare(TreeNode node1, TreeNode node2) {
        GenMiembro n1 = (GenMiembro) node1.getData();
        GenMiembro n2 = (GenMiembro) node2.getData();
        return n1.getOrden().compareTo(n2.getOrden());
    }
    
}
