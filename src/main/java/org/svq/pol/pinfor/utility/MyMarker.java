package org.svq.pol.pinfor.utility;

import org.primefaces.model.map.LatLng;
import org.primefaces.model.map.Marker;

/**
 *
 * @author jjcampac
 */
public class MyMarker extends Marker {

    private static final long serialVersionUID = -556906777835680249L;

    public MyMarker(LatLng latlng) {
        super(latlng);
    }

    @Override
    public boolean equals(Object other) {
        boolean result = false;
        if(other instanceof Marker) {
            Marker m = (Marker) other;
            result = this.getLatlng().getLat() == m.getLatlng().getLat() && this.getLatlng().getLng() == m.getLatlng().getLng();
        }
        return result;
    }

    @Override
    public int hashCode() {
        return Double.valueOf(this.getLatlng().getLat()).hashCode() * Double.valueOf(this.getLatlng().getLng()).hashCode();
    }
}
