package org.svq.pol.pinfor.utility.comparator;

import java.io.Serializable;
import java.util.Comparator;
import org.svq.pol.pinfor.bean.GenMiembro;

/**
 *
 * @author jjcampac
 */
public class MiembroNombreComparator implements Comparator, Serializable {
    private static final long serialVersionUID = -66284679093426569L;

    @Override
    public int compare(Object obj1, Object obj2) {
        int cmp = 0;
        GenMiembro miembro1 = null;
        GenMiembro miembro2 = null;
        if(obj1 instanceof GenMiembro) {
            miembro1 = (GenMiembro) obj1;
        }
        if(obj2 instanceof GenMiembro) {
            miembro2 = (GenMiembro) obj2;
        }
        if(miembro1 != null && miembro2 != null) {
            cmp = miembro1.getNombre().compareTo(miembro2.getNombre());
        }
        
        return cmp;
    }
    
}
