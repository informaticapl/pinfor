package org.svq.pol.pinfor.utility.serializer;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import java.lang.reflect.Type;

/**
 * GesateMovil app
 * Created by jjcampac on 24/02/2015.
 */
public class JsonDoubleDeserializer implements JsonDeserializer<Double> {
    @Override
    public Double deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        return Double.valueOf(json.getAsString().replace(",","."));
    }
}
