package org.svq.pol.pinfor.utility;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Locale;
import java.util.Properties;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;

/**
 *
 * @author jjcamacho
 */
public final class Constants {

    private static final Logger LOGGER = Logger.getLogger(Constants.class.getCanonicalName());

    private static final ExternalContext ECONTEXT = FacesContext.getCurrentInstance().getExternalContext();
    public static final Locale LOCALE_SPANISH = new Locale("es_ES");
    public static final String PHOTO_PATH = ECONTEXT.getRequestScheme()
            .concat("://")
            .concat(ECONTEXT.getRequestServerName())
            .concat(":")
            .concat(Integer.toString(ECONTEXT.getRequestServerPort()))
            .concat("/photos/");
    public static final int DEFAULT_BUFFER_SIZE = 10240;
    public static final int FATAL = 0;
    public static final int ERROR = 1;
    public static final int WARN = 2;
    public static final int INFO = 3;
    public static final String EMPTY_STR = "";
    public static final String[] ALLOWED_TYPES = {"jpg", "jpeg", "png", "pdf"};
    public static final String[] ALLOWED_TYPES_ORDENANZA = {"pdf"};
    public static final long ALLOWED_SIZE = 20000000; // 20MB

    // Detalle de los datos de personal a obtener
    public static final int BASICO = 2;
    public static final int EXTENDIDO = 4;
    public static final int PERFIL = 8;
    public static final int PLANTILLA = 16;
    public static final int PROFESIONAL = 32;
    public static final int DOMICILIO = 64;
    public static final int ADMINISTRATIVA = 128;
    public static final int LABORAL = 256;
    public static final int MEDICA = 512;
    public static final int UNIDAD = 1024;
    public static final int GRUPOFS = 2048;
    public static final int TURNO = 4096;
    public static final int VPT = 8192;
    public static final int TELEFONO = 16384;    
    public static final int CENTRO_TRABAJO = 32768;
    public static final int COMPLETO = BASICO | EXTENDIDO | PERFIL | PLANTILLA | PROFESIONAL | DOMICILIO | ADMINISTRATIVA | LABORAL | MEDICA | UNIDAD | GRUPOFS | TURNO | VPT;

    // Archivos adjuntos
    public static String FILES_PATH;
    public static String REPORTS_PATH;
    public static String HELP_PATH;

    // URL del servidor WSPOL
    public static String WEBSERVICE_GAT_URI;
    
    // Varios
    public static String PORTAL_PUERTO;

    // Certificados de firma
    public static String TRUSTSTORE_DGT_PATH;
    public static String KEYSTORE_DGT_PATH;
    public static String KEYSTORE_GAT_PATH;
    public static String KEYSTORE_GAT_FILE;
    public static String KEYSTORE_GAT_TYPE;
    public static String KEYSTORE_GAT_PASSWORD;
    public static String PRIVATE_KEY_GAT_ALIAS;
    public static String PRIVATE_KEY_GAT_PASSWORD;
    public static String TRUSTSTORE_GAT_PATH;
    public static String TRUSTSTORE_GAT_FILE;
    public static String TRUSTSTORE_GAT_TYPE;
    public static String TRUSTSTORE_GAT_PASSWORD;

    // Results
    public static final int RESULT_OK = 200;
    public static final int RESULT_FAIL = 500;
    public static final int INVALID_TICKET = 700;
    public static final int RESULT_LOGIN_NEEDED = 21;
    public static final int RESULT_NO_CERTIFICATE = 23;
    public static final int RESULT_LUGARES = 7;

    // Detalle de los niveles y perfiles de acceso del usuario
    public static final int NONE = 0;
    public static final int USUARIO = 2;
    public static final int SUPERVISOR = 4;
    public static final int ADMINISTRADOR = 8;
    public static final int CONSULTA = 16;
    public static final int ALTA = 32;
    public static final int BAJA = 64;
    public static final int MODIFICACION = 128;
    public static final int LOCAL = 512;
    public static final int REGIONAL = 1024;
    public static final int GLOBAL = 2048;

    // Por defecto, mostramos el mapa centrado en Sevilla (las coordenadas corresponden a la Catedral)
    public static final String DEFAULT_LAT = "37.3754338";
    public static final String DEFAULT_LNG = "-5.9900776";

    // Estados de los partes informativos y órdenes de servicio
    public static final String ARCHIVAR = "1";
    public static final String DESARCHIVAR = "2";
    public static final String REMITIR_REGISTRO_JEFATURA = "3";
    public static final String DEVOLVER_REMITENTE = "4";    
    public static final String REMITIR_UNIDAD_DESTINO = "5";
    public static final String ENVIAR_PORTA_FIRMAS = "6";
    public static final String CONVERTIR_EN_INFORME = "7";

    // Tipos de listados
    public static final String LISTADO_ASUNTOS = "Asuntos";
    public static final String LISTADO_CATEGORIAS = "Categoría de actuantes";
    public static final String LISTADO_DESTINATARIOS = "Destinatarios";
    public static final String LISTADO_ESTADOS_TRAMITACION = "Estados tramitación";

    // Busqueda geografica
    public static final int BUSQUEDA_DISTANCIA = 0;
    public static final int BUSQUEDA_VECINOS = 1;
    public static final String ORIGEN_PARTE = "PI";
    public static final String ORIGEN_INFORME = "IF";
    public static final String ORIGEN_ORDENANZA = "OM";
    public static final String ORIGEN_PUNTO = "OP";
    
    // Destinos
    public static final String DESTINO_UNIDAD = "UNI";
    public static final String DESTINO_JEFATURA = "DJF";
    public static final String DESTINO_AYUNTAMIENTO = "DAY";
    public static final String DESTINO_EXTERNO = "DEX";

    private Constants() {
        // super();
    }

    static {
        try {
            PORTAL_PUERTO = getProperty("/default.properties", "portal.port").trim();
            FILES_PATH = getProperty("/default.properties", "filesPath").trim();
            REPORTS_PATH = getProperty("/default.properties", "reportsPath").trim();
            HELP_PATH = getProperty("/default.properties", "helpPath").trim();
            WEBSERVICE_GAT_URI = getProperty("/https_connection.properties", "server.gat.uri");

            TRUSTSTORE_GAT_PATH = getProperty("/https_connection.properties", "truststore.gat.path").trim();
            TRUSTSTORE_GAT_FILE = getProperty("/https_connection.properties", "truststore.gat.file").trim();
            TRUSTSTORE_GAT_TYPE = getProperty("/https_connection.properties", "truststore.gat.type").trim();
            TRUSTSTORE_GAT_PASSWORD = getProperty("/https_connection.properties", "truststore.gat.password").trim();

            KEYSTORE_GAT_PATH = getProperty("/https_connection.properties", "keystore.gat.path").trim();
            KEYSTORE_GAT_FILE = getProperty("/https_connection.properties", "keystore.gat.file").trim();
            KEYSTORE_GAT_TYPE = getProperty("/https_connection.properties", "keystore.gat.type").trim();
            KEYSTORE_GAT_PASSWORD = getProperty("/https_connection.properties", "keystore.gat.password").trim();

            PRIVATE_KEY_GAT_ALIAS = getProperty("/https_connection.properties", "private_key.gat.alias").trim();
            PRIVATE_KEY_GAT_PASSWORD = getProperty("/https_connection.properties", "private_key.gat.password").trim();
        } catch (IOException ex) {
            Log.log(LOGGER, FATAL, ex.getMessage());
        }
    }

    private static String getProperty(String file, String property) throws IOException {
        String realPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/");
        Properties prop = new Properties();
        String result;
        try (FileInputStream fis = new FileInputStream(realPath.concat("/WEB-INF/classes/").concat(file))) {
            prop.load(fis);
            result = prop.getProperty(property);
        }
        return result;
    }

}
