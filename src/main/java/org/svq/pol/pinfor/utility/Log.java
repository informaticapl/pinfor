package org.svq.pol.pinfor.utility;

import org.apache.log4j.Logger;

/**
 *
 * @author jjcamacho
 */
public final class Log {

    private Log() {
    }

    public static void log(Logger logger, int type, String message) {
        switch (type) {
            case Constants.FATAL:
                logger.fatal(message);
                break;
            case Constants.ERROR:
                logger.error(message);
                break;
            case Constants.WARN:
                logger.warn(message);
                break;
            case Constants.INFO:
                logger.info(message);
                break;
            default:
                logger.debug(message);
                break;
        }
    }
}
