package org.svq.pol.pinfor.utility;

/**
 *
 * @author jjcampac
 */
public class Message {

    private Long idUnidad;
    private String content;

    public Message(Long idUnidad, String content) {
        this.idUnidad = idUnidad;
        this.content = content;
    }

    Long getIdUnidad() {
        return idUnidad;
    }

    void setIdUnidad(Long idUnidad) {
        this.idUnidad = idUnidad;
    }

    String getContent() {
        return content;
    }

    void setContent(String content) {
        this.content = content;
    }
}
