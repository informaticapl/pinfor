package org.svq.pol.pinfor.utility.comparator;

import java.io.Serializable;
import java.util.Comparator;
import org.svq.pol.pinfor.bean.InfAplicacion;

/**
 *
 * @author jjcampac
 */
public class AplicacionNombreComparator implements Comparator, Serializable {
    private static final long serialVersionUID = -6065429562519640664L;

    @Override
    public int compare(Object obj1, Object obj2) {
        int cmp = 0;
        InfAplicacion app1 = null;
        InfAplicacion app2 = null;
        if(obj1 instanceof InfAplicacion) {
            app1 = (InfAplicacion) obj1;
        }
        if(obj2 instanceof InfAplicacion) {
            app2 = (InfAplicacion) obj2;
        }
        if(app1 != null && app2 != null) {
            cmp = app1.getNombre().compareTo(app2.getNombre());
        }
        
        return cmp;
    }
    
}
