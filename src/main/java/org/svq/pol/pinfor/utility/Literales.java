package org.svq.pol.pinfor.utility;

/**
 *
 * @author jjcamacho
 */
public final class Literales {

    private Literales() {
    }
    public static final String FILTRO_FECHA_APLICACION = "fechaAplicacionFilter";
    public static final String PARAM_FECHA_APLICACION = "paramFechaAplicacion";
    public static final String FILTRO_SITUACION = "situacionFilter";
    public static final String FECHA_APLICACION = "fechaAplicacion";
    public static final String UNIFORME = "uniforme";
    public static final String LABORAL = "laboral";
    public static final String MEDICA = "medica";
    public static final String ADMINISTRATIVA = "administrativa";
    public static final String DATO_PROFESIONAL = "datoprofesional";
    public static final String UNIDAD = "unidad";
    public static final String SUBUNIDAD = "subunidad";
    public static final String PERSONAL = "personal";
    public static final String CATEGORIA = "categoria";
    public static final String PLANTILLA = "plantilla";
    public static final String NOMBRAMIENTO = "nombramiento";
    public static final String PERSONA = "persona";
    public static final String CLONE = "clone";
    public static final String PT = "pt";
    public static final String FS = "fs";
    public static final String VPT = "vpt";
    public static final String RPT = "rpt";
    public static final String INCIDENCIA = "incidencia";
    public static final String TELEFONO = "telefono";
    public static final String DOMICILIO = "domicilio";
    public static final String GRUPO_FS = "grupofs";
    public static final String SUBGRUPO_FS = "subgrupofs";
    public static final String CTRABAJO = "ctrabajo";
    public static final String CENTRO_TRABAJO = "centrotrabajo";
    public static final String TURNO = "turno";
    public static final String CAMBIO_FUTURO = "cambiofuturo";
    public static final String LONG = "long";
    public static final String INACTIVO = "inactivo";
    public static final String BAJA = "baja";
    public static final String MED = "MED";
    public static final String EMPTY_STR = "";
    public static final int BATCH_SIZE = 5;
}
