/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.svq.pol.pinfor.utility.comparator;

import java.io.Serializable;
import java.util.Comparator;
import org.primefaces.model.TreeNode;
import org.svq.pol.pinfor.bean.PerPersona;

/**
 *
 * @author jjcamacho
 */
public class TreeNodePersonaEscalafon implements Comparator<TreeNode>, Serializable {
    private static final long serialVersionUID = 3933227893580538554L;

    @Override
    public int compare(TreeNode o1, TreeNode o2) {
        PerPersona obj1 = (PerPersona) o1.getData();
        PerPersona obj2 = (PerPersona) o2.getData();
        
        Long escalafon1 = obj1.getPlantillaList().get(0).getEscalafon();
        long escalafon2 = obj2.getPlantillaList().get(0).getEscalafon();
        int result;
        if (escalafon1 > escalafon2) {
            result = 1;
        } else {
            if (escalafon1 == escalafon2) {
                result = 0;
            } else {
                result = -1;
            }
        }
        return result; 
    }
    
}
