package org.svq.pol.pinfor.utility.converter;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author jjcamacho
 */
@FacesConverter(value = "TimestampConverter")
public class TimestampConverter implements Converter {

    private SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String string) {
        Object result = null;
        
        if (string.trim().equals("")) {
            return result;
        } else {
            try {
                Date date = sdf.parse(string);
                result = new Timestamp(date.getTime());
            } catch (ParseException | NumberFormatException ex) {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error de conversión del elemento de la lista: " + ex.getMessage(), ""));
            }
        }
        return result;
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object obj) {
        String result = "";
        if (obj != null && obj instanceof Date) {
            result = sdf.format(obj);
        }
        return result;
    }
}
