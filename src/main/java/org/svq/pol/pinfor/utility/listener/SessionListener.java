package org.svq.pol.pinfor.utility.listener;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import javax.enterprise.inject.spi.CDI;
import javax.faces.application.NavigationHandler;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.inject.Inject;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import org.svq.pol.exception.UserNotAllowedException;
import org.svq.pol.pinfor.bean.GenSessionUser;
import org.svq.pol.pinfor.dao.PinDAO;
import org.svq.pol.pinfor.utility.Constants;
import org.svq.pol.pinfor.utility.Log;
import org.svq.pol.pinfor.utility.Utilities;

/**
 *
 * @author jjcampac
 */
public class SessionListener implements PhaseListener {

    private static final long serialVersionUID = 86519450935926193L;
    private static final Logger LOGGER = Logger.getLogger(Utilities.class);

    @Inject
    Utilities utilities;

    @Override
    public void afterPhase(PhaseEvent event) {
        String paginaActual = event.getFacesContext().getViewRoot().getViewId();

        // En cada petición se realiza la misma tarea de comprobar si hay un usuario definido en la sesión o no,
        // así como la sesion incluida en el requerimiento, si ya existiera. Además se define cuál es la 
        // URI correspondiente a la página de login.
        final HttpSession session = utilities.getRequest().getSession(false);
        final String loginPage = "login.xhtml";

        // Comprobamos si la petición contiene una cookie con el nombre del usuario...
        Cookie[] cookies = utilities.getRequest().getCookies();
        boolean hasUserCookie = false;
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if ("user".equals(cookie.getName())) {
                    hasUserCookie = true;
                    break;
                }
            }
        }
        
        // Un usuario estará logado si existe una sesión, ésta contiene un usuario y la petición contiene una cookie de usuario.
        // Esto último es importante, porque puede darse el caso de que el usuario tenga una sesión de esta aplicación abierta
        // pero haya hecho logout desde otra aplicación, en cuyo caso debemos dar por cancelada la presente sesión.
        final boolean loggedIn = session != null && session.getAttribute("user") != null && hasUserCookie;

        // Definimos un boolean para saber si la petición corresponde a la página de login
        final boolean loginRequested = utilities.getRequest().getRequestURI().contains(loginPage);

        // Aunque la comparación tiene su equivalente en (!loginRequested && !loggedIn  && !previouslyLoggedIn(utilities.getRequest()))
        // usando esta forma nos aprovechamos de la evaluación perezosa de Java, con lo que evitamos efectuar la comprobación
        // del método previouslyLoggedIn, que es la más pesada, por contener consultas a la DB.
        // Así, si no es un requerimiento de la página de login, el usuario no está logado o lo ha estado previamente (viene
        // de otra aplicación del ecosistema), se redirige a la página de login para que se identifique.
        if (loginRequested || loggedIn  || previouslyLoggedIn(utilities.getRequest())) {
        } else {
            NavigationHandler nh = event.getFacesContext().getApplication().getNavigationHandler();
            nh.handleNavigation(event.getFacesContext(), null, "/login?faces-redirect=true");
        }
    }

    @Override
    public void beforePhase(PhaseEvent event) {
    }

    @Override
    public PhaseId getPhaseId() {
        return PhaseId.RESTORE_VIEW;
    }

    private boolean previouslyLoggedIn(HttpServletRequest request) {

        // El método devolverá cierto o falso, dependiendo de si el usuario que pudiera venir en las cookies (ya que
        // el que debe existir en la sesion es nulo, de lo contrario nunca se hubiera llamado a este método) tiene
        // registrada una sesión válida en la DB.
        // Partimos de un resultado falso.
        boolean result = false;
        utilities = CDI.current().select(Utilities.class).get();

        try {
            // Obtenermos la sesión que pudiera obtener la petición. Si no existiera, NO se crea una.

            // Si existe esa sesión, es decir, no es nula...
            // ...definimos un currentUser con los valores por defecto y el nombre de usuario tomado de las cookies...
            GenSessionUser currentUser = new GenSessionUser(utilities.getProperty("/default.properties", "ldap.domain"));
            currentUser.getId().setIp(request.getRemoteHost());
            Cookie[] cookies = request.getCookies();
            if (cookies != null && cookies.length > 1) {
                for (Cookie c : cookies) {
                    if (c.getName().equals("user")) {
                        currentUser.getId().setUsuario(c.getValue());
                    }
                    if (c.getName().equals("ticket")) {
                        currentUser.setTicket(c.getValue());
                    }
                }
            }

            // Una vez hemos definido al usuario, comprobamos si existe, en cuyo caso lo definimos como el usuario
            // actual de la sesión a través del método Utilities#setCurrentUser, que se encargará de definir otros
            // valores del objeto currentUser, así como la validez del ticket.
            // En cuanto a las cookies, sólo hay dos sitios donde se puedan definir:
            //   1. Durante la llamada del método LoginBacking#login
            //   2. Aquí, tras definir el usuario de sesión a partir de los datos de sesión de la DB.
            if (PinDAO.sessionExists(currentUser)) {
                utilities.setCurrentUser(currentUser);
                utilities.setCookies(currentUser, false);
                result = true;
            }
//            }
        } catch (IOException | NoSuchAlgorithmException | UserNotAllowedException ex) {
            Log.log(LOGGER, Constants.FATAL, ex.getMessage());
        }

        return result;
    }
}
