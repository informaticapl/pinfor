package org.svq.pol.pinfor;

import javax.enterprise.context.ApplicationScoped;
import javax.faces.annotation.FacesConfig;
import static javax.faces.annotation.FacesConfig.Version.JSF_2_3;

/**
 *
 * @author jjcampac
 * Clase vacía requerida a nivel de aplicación para efectuar la activación 
 * efectíva de JSF 2.3 (REVISAR ARCHIVOS BEANS.XML Y FACES-CONFIG.XML)
 */
@ApplicationScoped
@FacesConfig(version = JSF_2_3)
public class PinforApplication {

}
