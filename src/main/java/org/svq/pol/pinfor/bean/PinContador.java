package org.svq.pol.pinfor.bean;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author jjcampac
 */
@Entity
@Table(name = "GAT_CONTADOR")
public class PinContador implements Serializable {

    private static final long serialVersionUID = 1921264993965573854L;
    public static final String PARTE = "P";
    public static final String INFORME = "I";
    public static final String ORDENANZA = "O";
    public static final String ORDEN_TRABAJO = "T";

    @Id
    @SequenceGenerator(name = "seq", sequenceName = "SEQ_PIN")
    @GeneratedValue(generator = "seq")
    @Column(name = "ID_CONTADOR")
    private Long idContador;

    @Column(name = "CONTADOR")
    private Integer contador;

    @Column(name = "AGNO")
    private Integer agno;

    @Column(name = "TIPO")
    private String tipo;

//<editor-fold defaultstate="collapsed" desc="constructors">
    public PinContador() {
    }

    public PinContador(Long idContador) {
        this.idContador = idContador;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc="getters & setters">
    public Long getIdContador() {
        return idContador;
    }

    public void setIdContador(Long idContador) {
        this.idContador = idContador;
    }

    public Integer getContador() {
        return contador;
    }

    public void setContador(Integer contador) {
        this.contador = contador;
    }

    public Integer getAgno() {
        return agno;
    }

    public void setAgno(Integer agno) {
        this.agno = agno;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="equals && hashcode">
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idContador != null ? idContador.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PinContador)) {
            return false;
        }
        PinContador other = (PinContador) object;
        if ((this.idContador == null && other.idContador != null) || (this.idContador != null && !this.idContador.equals(other.idContador))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.svq.pol.gesate.ws.entity.GatContador[ idContador=" + idContador + " ]";
    }

    //</editor-fold>
}
