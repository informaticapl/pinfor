package org.svq.pol.pinfor.bean.auxiliar;

import java.util.Date;
import java.util.List;
import java.util.Map;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortMeta;
import org.primefaces.model.SortOrder;
import org.svq.pol.pinfor.bean.GenSessionUser;
import org.svq.pol.pinfor.dao.PinDAO;

/**
 *
 * @author jjcampac
 */
public class PinOrdenLazyDataModel extends LazyDataModel<PinOrdenServicioAux>  {

    private static final long serialVersionUID = 3471610731661348338L;
    
    private PinOrdenServicioAux patron;
    private Date fechaDesde;
    private Date fechaHasta;
    private String estado;
    private GenSessionUser currentUser;
    private List<PinOrdenServicioAux> result;

    public PinOrdenLazyDataModel() {

    }

    private PinOrdenLazyDataModel(Builder builder) {
        this.patron = builder.patron;
        this.fechaDesde = builder.fechaDesde;
        this.fechaHasta = builder.fechaHasta;
        this.estado = builder.estado;
        this.currentUser = builder.currentUser;

        this.setRowCount(PinDAO.searchCountOrdenTrabajo(patron, fechaDesde, fechaHasta, estado, currentUser));
    }

    @Override
    public List<PinOrdenServicioAux> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
        result = PinDAO.searchOrdenTrabajo(patron, fechaDesde, fechaHasta, estado, first, pageSize, currentUser);
        return result;
    }

    @Override
    public List<PinOrdenServicioAux> load(int first, int pageSize, List<SortMeta> multiSortMeta, Map<String, Object> filters) {
        result = PinDAO.searchOrdenTrabajo(patron, fechaDesde, fechaHasta, estado, first, pageSize, currentUser);
        return result;
    }

    @Override
    public Object getRowKey(PinOrdenServicioAux orden) {
        return orden.getIdOrdenTrabajo();
    }

    @Override
    public PinOrdenServicioAux getRowData(String rowKey) {
        Long rk = Long.valueOf(rowKey);
        PinOrdenServicioAux resultado = null;
        for (PinOrdenServicioAux orden : this) {
            if (orden.getIdOrdenTrabajo() == rk) {
                resultado = orden;
                break;
            }
        }

        return resultado;
    }

    public static class Builder {

        private PinOrdenServicioAux patron;
        private Date fechaDesde;
        private Date fechaHasta;
        private String estado;
        private GenSessionUser currentUser;

        public PinOrdenLazyDataModel build() {
            return new PinOrdenLazyDataModel(this);
        }

        public Builder(PinOrdenServicioAux patron) {
            this.patron = patron;
        }

        public Builder fechaDesde(Date fechaDesde) {
            this.fechaDesde = fechaDesde;
            return this;
        }

        public Builder fechaHasta(Date fechaHasta) {
            this.fechaHasta = fechaHasta;
            return this;
        }

        public Builder estado(String estado) {
            this.estado = estado;
            return this;
        }
        
        public Builder currentUser(GenSessionUser currentUser) {
            this.currentUser = currentUser;
            return this;
        }
    }
}
