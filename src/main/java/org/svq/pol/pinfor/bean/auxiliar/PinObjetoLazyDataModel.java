package org.svq.pol.pinfor.bean.auxiliar;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortMeta;
import org.primefaces.model.SortOrder;
import org.svq.pol.pinfor.bean.GenSessionUser;
import org.svq.pol.pinfor.bean.PinObjeto;
import org.svq.pol.pinfor.dao.PinDAO;

/**
 *
 * @author jjcampac
 */
public class PinObjetoLazyDataModel extends LazyDataModel<PinObjeto>  {

    private static final long serialVersionUID = 3471610731661348338L;

    private PinObjeto patron;
    private Date fechaDesde;
    private Date fechaHasta;
    private GenSessionUser currentUser;
    private List<PinObjeto> result;

    public PinObjetoLazyDataModel() {

    }

    private PinObjetoLazyDataModel(Builder builder) {
        this.patron = builder.patron;
        this.fechaDesde = builder.fechaDesde;
        this.fechaHasta = builder.fechaHasta;
        this.currentUser = builder.currentUser;

        this.setRowCount(PinDAO.searchCountObjetos(patron, fechaDesde, fechaHasta, currentUser));
    }

    @Override
    public List<PinObjeto> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
        result = PinDAO.searchObjetos(patron, fechaDesde, fechaHasta, first, pageSize, currentUser);
        return result;
    }

    @Override
    public List<PinObjeto> load(int first, int pageSize, List<SortMeta> multiSortMeta, Map<String, Object> filters) {
        result = PinDAO.searchObjetos(patron, fechaDesde, fechaHasta, first, pageSize, currentUser);
        return result;
    }

    
    
    @Override
    public Object getRowKey(PinObjeto objeto) {
        return objeto.getIdObjeto();
    }

    @Override
    public PinObjeto getRowData(String rowKey) {
        Long rk = Long.valueOf(rowKey);
        PinObjeto resultado = null;
        for (Iterator<PinObjeto> it = this.iterator(); it.hasNext();) {
            PinObjeto objeto = it.next();
            if (objeto.getIdObjeto()== rk) {
                resultado = objeto;
                break;
            }
        }

        return resultado;
    }


    public static class Builder {

        private PinObjeto patron;
        private Date fechaDesde;
        private Date fechaHasta;
        private GenSessionUser currentUser;

        public PinObjetoLazyDataModel build() {
            return new PinObjetoLazyDataModel(this);
        }

        public Builder(PinObjeto objeto) {
            this.patron = objeto;
        }

        public Builder fechaDesde(Date fechaDesde) {
            this.fechaDesde = fechaDesde;
            return this;
        }

        public Builder fechaHasta(Date fechaHasta) {
            this.fechaHasta = fechaHasta;
            return this;
        }
        
        public Builder currentUser(GenSessionUser currentUser) {
            this.currentUser = currentUser;
            return this;
        }
    }
}
