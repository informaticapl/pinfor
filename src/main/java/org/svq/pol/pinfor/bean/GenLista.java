package org.svq.pol.pinfor.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import javax.persistence.*;

@Entity
@Table(name = "GEN_LISTAS")
public class GenLista implements Serializable, Comparable {
    private static final long serialVersionUID = -4034344213130422341L;

    @Id    
    @Column(name="CODIGO")
    private String codigo; 
    
    private String nombre;
    
    @OneToMany(cascade={CascadeType.ALL},fetch = FetchType.EAGER, mappedBy = "lista", orphanRemoval = true)
    @OrderBy("orden asc")
    private List<GenMiembro> miembros;    
    
    @Version
    private Integer version;

    //<editor-fold defaultstate="collapsed" desc="constructors">
    public GenLista() {
        miembros = new ArrayList<>();
    }

    public GenLista(String codigo, String nombre) {
        this();
        this.codigo = codigo;
        this.nombre = nombre;
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="getters & setters">
    public List<GenMiembro> getMiembros() {
        return miembros;
    }

    public void setMiembros(List<GenMiembro> miembros) {
        this.miembros = miembros;
    }


    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }
    //</editor-fold>
    
    public List<String> getMiembrosList() {
        List<String> resultado = new ArrayList<>();
        Iterator it = (Iterator) miembros.iterator();
        while(it.hasNext()){
            resultado.add(((GenMiembro) it.next()).toString());
        }
    
        return resultado;
    }

    @Override
    public int compareTo(Object obj) {
        GenLista other = (GenLista) obj;
        return this.getNombre().compareTo(other.getNombre());
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + Objects.hashCode(this.nombre);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        return true;
    }
    
    

}
