package org.svq.pol.pinfor.bean;

import java.io.Serializable;
import java.util.BitSet;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.persistence.*;

@Entity
@Table(name = "GEN_MIEMBROS")
public class GenMiembro implements Serializable, Comparable {

    private static final long serialVersionUID = 1673471667016830863L;
    @Id
    @SequenceGenerator(name = "seq", sequenceName = "GEN_SEQ")
    @GeneratedValue(generator = "seq")
    @Column(name = "ID_MIEMBRO")
    private Long idMiembro;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "CODIGO")
    private GenLista lista;

    private String nombre;
    private Integer orden;
    private byte[] incompatibles;

    @OneToOne
    @JoinTable(name = "GEN_UBICACION_CENTRO_TRAB",
            joinColumns = {
                @JoinColumn(name = "ID_MIEMBRO")},
            inverseJoinColumns = {
                @JoinColumn(name = "ID_LUGAR", unique = true)})
    private GenLugar ubicacion;
    
    @ManyToMany(mappedBy = "destinos")
    private Set<PinMiembro> asuntos;

    @Version
    private Integer version;

    //<editor-fold defaultstate="collapsed" desc="constructors">
    public GenMiembro() {
        this.asuntos = new HashSet<>();
    }

    public GenMiembro(String nombre) {
        this();
        this.nombre = nombre;
    }

    public GenMiembro(GenLista lista) {
        this();
        this.idMiembro = 0L;
        this.lista = lista;
        this.nombre = "";
        this.orden = 0;
        this.version = 0;
    }

    public GenMiembro(Long id, GenLista lista, String nombre, byte[] incompatibles) {
        this();
        this.idMiembro = id;
        this.lista = lista;
        this.nombre = nombre;
        this.incompatibles = incompatibles.clone();
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="getters & setters">
    public Long getIdMiembro() {
        return idMiembro;
    }

    public void setIdMiembro(Long idMiembro) {
        this.idMiembro = idMiembro;
    }

    public GenLista getLista() {
        return lista;
    }

    public void setLista(GenLista lista) {
        this.lista = lista;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getOrden() {
        return orden;
    }

    public void setOrden(Integer orden) {
        this.orden = orden;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public BitSet getIncompatibles() {
        return incompatibles != null ? BitSet.valueOf(incompatibles) : new BitSet(2000);
    }

    public void setIncompatibles(BitSet bitset) {
        this.incompatibles = bitset.toByteArray();
    }

    public GenLugar getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(GenLugar ubicacion) {
        this.ubicacion = ubicacion;
    }

    public Set<PinMiembro> getAsuntos() {
        return asuntos;
    }

    public void setAsuntos(Set<PinMiembro> asuntos) {
        this.asuntos = asuntos;
    }
    //</editor-fold>

    @Override
    public int compareTo(Object o) {
        int result = 0;
        if (o instanceof GenMiembro) {
            GenMiembro other = (GenMiembro) o;
            result = this.orden.compareTo(other.orden);
        }
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final GenMiembro other = (GenMiembro) obj;
        return Objects.equals(this.idMiembro, other.idMiembro);
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 53 * hash + (int) (this.idMiembro ^ (this.idMiembro >>> 32));
        return hash;
    }

}
