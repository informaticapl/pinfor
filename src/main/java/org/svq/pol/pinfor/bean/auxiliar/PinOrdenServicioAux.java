package org.svq.pol.pinfor.bean.auxiliar;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Calendar;
import javax.persistence.Entity;
import javax.persistence.Table;
import org.svq.pol.pinfor.bean.GenMiembro;
import org.svq.pol.pinfor.bean.PinOrdenServicio;
import org.svq.pol.pinfor.utility.Constants;

/**
 *
 * @author jjcampac
 */
@Entity
@Table(name = "PIN_ORDEN_TRABAJO")
public class PinOrdenServicioAux extends PinOrdenServicio implements Serializable {

    private static final long serialVersionUID = -910097748325241982L;

    //<editor-fold defaultstate="collapsed" desc="constructors">
    public PinOrdenServicioAux() {
        super();
    }

    public PinOrdenServicioAux(String usuario) {
        super(usuario);
    }

    public PinOrdenServicioAux(GenMiembro unidad, Timestamp fechaRegistro, String usuario) {
        super(unidad, fechaRegistro, usuario);
    }
    //</editor-fold>

    public int sortByReferenciaInterna(String obj1, String obj2) {
        int o1 = Integer.parseInt(obj1.substring(0, obj1.indexOf('-')).replace("/", ""));
        int o2 = Integer.parseInt(obj2.substring(0, obj2.indexOf('-')).replace("/", ""));
        if (o1 < o2) {
            return -1;
        } else if (o1 == o2) {
            return 0;
        } else {
            return 1;
        }
    }

    public String getReferenciaInternaFormateada() {
        String result = "";
        Calendar cal = Calendar.getInstance(Constants.LOCALE_SPANISH);
        try {
            if (this.referenciaInterna != 0) {
                cal.setTime(this.fechaRegistro);
                result = String.format("%d/%d-OS", cal.get(Calendar.YEAR), this.referenciaInterna);
            }
        } catch (NullPointerException ignore) {
        }
        return result;

    }

    public void addParte(PinParteAux parte) {
        parte.setOrdenServicio(this);
        partes.add(parte);
    }

}
