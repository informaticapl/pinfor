package org.svq.pol.pinfor.bean;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.svq.pol.pinfor.bean.auxiliar.PinParteAux;

/**
 *
 * @author jjcampac
 */
@Entity
@Table(name = "PIN_HISTORICO_IMPRESION")
public class PinHistoricoImpresion implements Serializable {

    private static final long serialVersionUID = 159463863389614010L;

    @Id
    @SequenceGenerator(name = "seq", sequenceName = "SEQ_PIN")
    @GeneratedValue(generator = "seq")
    @Column(name = "ID_HISTORICO")
    private Integer idHistorico;

    @JoinColumn(name = "ID_PARTE", referencedColumnName = "ID_PARTE")
    @ManyToOne
    private PinParteAux parte;

    @Column(name = "FECHA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;

    @Column(name = "ELEMENTO")
    private String elemento;

    @Column(name = "USUARIO")
    private String usuario;

    @Version
    @Column(name = "VERSION")
    private Integer version;

    //<editor-fold defaultstate="collapsed" desc="constructors">
    public PinHistoricoImpresion() {
        this.fecha = Calendar.getInstance().getTime();
    }

    public PinHistoricoImpresion(Integer idHistorico) {
        this();
        this.idHistorico = idHistorico;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="getters & setters" >
    public Integer getIdHistorico() {
        return idHistorico;
    }

    public void setIdHistorico(Integer idHistorico) {
        this.idHistorico = idHistorico;
    }

    public PinParteAux getParte() {
        return parte;
    }

    public void setParte(PinParteAux parte) {
        this.parte = parte;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getElemento() {
        return elemento;
    }

    public void setElemento(String elemento) {
        this.elemento = elemento;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }
    //</editor-fold>

    //<editor-fold desc="hashCode, equals & toString">
    @Override
    public int hashCode() {
        HashCodeBuilder hcb = new HashCodeBuilder();
        hcb.append(idHistorico);
        return hcb.toHashCode();
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof PinHistoricoImpresion)) {
            return false;
        }
        PinHistoricoImpresion other = (PinHistoricoImpresion) object;
        EqualsBuilder eb = new EqualsBuilder();
        eb.append(this.idHistorico, other.idHistorico);
        return eb.isEquals();
    }

    @Override
    public String toString() {
        return "org.svq.pol.gesate.ws.entity.GatHistoricoDgt[ idHistorico=" + idHistorico + " ]";
    }
    //</editor-fold>

}
