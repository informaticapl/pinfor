package org.svq.pol.pinfor.bean;

import java.io.Serializable;
import java.util.*;
import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import org.hibernate.annotations.*;

@Entity
@Table(name = "PER_PERSONAL")
@FilterDefs({
    @FilterDef(name = "fechaAplicacionFilter",
            parameters
            = @ParamDef(name = "paramFechaAplicacion", type = "date")),
    @FilterDef(name = "situacionFilter",
            parameters = {
                @ParamDef(name = "paramUnidad", type = "long"),
                @ParamDef(name = "paramCategoria", type = "long"),
                @ParamDef(name = "paramCentroTrabajo", type = "long")
            }
    )
})
public class PerPersona implements Serializable {

    private static final long serialVersionUID = -8272221031069873740L;
    private static final String PERSONA = "persona";

    @Id
    @SequenceGenerator(name = "seq", sequenceName = "PER_SEQ")
    @GeneratedValue(generator = "seq")
    @Column(name = "ID_PERSONAL")
    private long idPersona;

    private String nombre;

    @Column(name = "APELLIDO_1")
    private String apellido1;

    @Column(name = "APELLIDO_2")
    private String apellido2;

    @Column(name = "SEXO")
    private String genero;

    private String dni;

    private String letranif;

    @Column(name = "FECHA_NAC")
    @Temporal(TemporalType.DATE)
    private Date fechaNac;

    @Column(name = "LUGAR_NAC")
    private String lugarNac;

    private String padre;

    private String madre;

    @Column(name = "USR_OPER")
    private String operador;

    @Column(name = "FECHA_MOV")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaMov;

    private String usuario;

    private String vinculo;

    @Version
    private long version;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "INF_PERMISO",
            joinColumns = {
                @JoinColumn(name = "ID_PERSONA")},
            inverseJoinColumns = {
                @JoinColumn(name = "ID_PERFIL")})
    @BatchSize(size = 5)
    private Set<InfPerfil> perfiles;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = PERSONA)
    @BatchSize(size = 5)
    @OrderBy("fechaAplicacion desc")
    @Filter(name = "fechaAplicacionFilter", condition = "Fecha_Aplicacion <= :paramFechaAplicacion")
    private Set<PerPlantilla> plantillas;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = PERSONA, orphanRemoval = true)
    @BatchSize(size = 5)
    @OrderBy("fechaAplicacion desc")
    @Filters({
        @Filter(name = "fechaAplicacionFilter", condition = "Fecha_Aplicacion <= :paramFechaAplicacion"),
        @Filter(name = "situacionFilter", condition = "id_miembro IN (:paramUnidad)")
    })
    private Set<PerSituacion> unidades;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = PERSONA, orphanRemoval = true)
    @BatchSize(size = 5)
    @OrderBy("fechaAplicacion desc")
    @Filters({
        @Filter(name = "fechaAplicacionFilter", condition = "Fecha_Aplicacion <= :paramFechaAplicacion"),
        @Filter(name = "situacionFilter", condition = "id_miembro IN (:paramCentroTrabajo)")
    })
    private Set<PerSituacion> centrosTrabajo;

//<editor-fold defaultstate="collapsed" desc="constructors">
    public PerPersona() {
        perfiles = new HashSet<>(0);
        plantillas = new HashSet<>(0);
        unidades = new HashSet<>(0);
    }

    public PerPersona(long idPersona) {
        this();
        this.idPersona = idPersona;
    }

    public PerPersona(String usuario) {
        this();
        this.usuario = usuario;
    }

    public PerPersona(String operador, Date fechaMov) {
        this();
        this.operador = operador;
        this.fechaMov = (Date) fechaMov.clone();
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="getters & setters">
    public long getIdPersona() {
        return this.idPersona;
    }

    public void setIdPersona(long idPersona) {
        this.idPersona = idPersona;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido1() {
        return this.apellido1;
    }

    public void setApellido1(String apellido1) {
        this.apellido1 = apellido1;
    }

    public String getApellido2() {
        return this.apellido2;
    }

    public void setApellido2(String apellido2) {
        this.apellido2 = apellido2;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public Date getFechaMov() {
        return (Date) fechaMov.clone();
    }

    public void setFechaMov(Date fechaMov) {
        this.fechaMov = (Date) fechaMov.clone();
    }

    public Date getFechaNac() {
        return (Date) fechaNac.clone();
    }

    public void setFechaNac(Date fechaNac) {
        this.fechaNac = (Date) fechaNac.clone();
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getLetranif() {
        return letranif;
    }

    public void setLetranif(String letranif) {
        this.letranif = letranif;
    }

    public String getNif() {
        String result = "";
        if (this.dni != null) {
            result = this.dni.concat("-").concat(this.getLetranif());
        }
        return result;
    }

    public String getLugarNac() {
        return lugarNac;
    }

    public void setLugarNac(String lugarNac) {
        this.lugarNac = lugarNac;
    }

    public String getMadre() {
        return madre;
    }

    public void setMadre(String madre) {
        this.madre = madre;
    }

    public String getOperador() {
        return operador;
    }

    public void setOperador(String operador) {
        this.operador = operador;
    }

    public String getPadre() {
        return padre;
    }

    public void setPadre(String padre) {
        this.padre = padre;
    }

    public String getUsuario() {
        return this.usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getVinculo() {
        return this.vinculo;
    }

    public void setVinculo(String vinculo) {
        this.vinculo = vinculo;
    }

    public Set<InfPerfil> getPerfiles() {
        return this.perfiles;
    }

    public void setPerfiles(Set<InfPerfil> perfiles) {
        this.perfiles = perfiles;
    }

    public long getVersion() {
        return version;
    }

    public void setVersion(long version) {
        this.version = version;
    }

    public Set<PerSituacion> getUnidades() {
        return unidades;
    }

    public void setUnidades(Set<PerSituacion> unidades) {
        this.unidades = unidades;
    }

    public Set<PerSituacion> getCentrosTrabajo() {
        return centrosTrabajo;
    }

    public void setCentrosTrabajo(Set<PerSituacion> centrosTrabajo) {
        this.centrosTrabajo = centrosTrabajo;
    }

    public Set<PerPlantilla> getPlantillas() {
        return plantillas;
    }

    public void setPlantillas(Set<PerPlantilla> plantillas) {
        this.plantillas = plantillas;
    }

//    public String getPhoto() {
//        String result = concat("empty_profile.jpg");
//        if (this.idPersona != 0) {
//            String url = Constants.PHOTO_PATH.concat(String.valueOf(getIdPersona())).concat(".jpg");
//            if (Utilities.resourceExists(url)) {
//                result = url;
//            }
//        }
//        return result;
//    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Conjuntos en forma de listas">
    public List<InfPerfil> getPerfilList() {
        return new ArrayList<>(getPerfiles());
    }

    public List<PerPlantilla> getPlantillaList() {
        List<PerPlantilla> result = new ArrayList<>(getPlantillas());
        Collections.sort(result);
        return result;
    }

    public List<PerSituacion> getUnidadList() {
        List<PerSituacion> result = new ArrayList<>(getUnidades());
        Collections.sort(result);
        return result;
    }

    public List<PerSituacion> getCentroTrabajoList() {
        List<PerSituacion> result = new ArrayList<>(getCentrosTrabajo());
        Collections.sort(result);
        return result;
    }
    //</editor-fold>

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder("");
        if (this.idPersona != 0L) {
            result.append(String.format("%s %s %s", this.nombre, this.apellido1, this.apellido2));
        }
        return result.toString();
    }
}
