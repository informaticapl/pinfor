package org.svq.pol.pinfor.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.*;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(name = "INF_PERFIL")
public class InfPerfil implements Serializable {

    private static final long serialVersionUID = 79746956984690251L;
    
    @Id
    @SequenceGenerator(name = "seq", sequenceName = "INF_SEQ")
    @GeneratedValue(generator = "seq")
    @Column(name = "ID_PERFIL")
    private long idPerfil;
    
    private String perfil;
    private String descripcion;
    
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "perfil", cascade = CascadeType.ALL, orphanRemoval = true)
    @Fetch(FetchMode.JOIN)
    private Set<InfContenidoPerfil> contenidoPerfiles;
    
    @ManyToMany(fetch = FetchType.EAGER, mappedBy = "perfiles")
    private Set<PerPersona> personas;
    
    @Version
    private Integer version;

    //<editor-fold defaultstate="collapsed" desc="constructors">
    public InfPerfil() {
        this.perfil = "";
        this.descripcion = "";
        this.contenidoPerfiles = new HashSet(0);
        this.personas = new HashSet(0);
    }

    public InfPerfil(long idPerfil, String perfil) {
        this();
        this.idPerfil = idPerfil;
        this.perfil = perfil;
    }

    public InfPerfil(long idPerfil, String perfil, String descripcion, Set<InfContenidoPerfil> contenidoPerfiles, Set<PerPersona> personas) {
        this();
        this.idPerfil = idPerfil;
        this.perfil = perfil;
        this.descripcion = descripcion;
        this.contenidoPerfiles = contenidoPerfiles;
        this.personas = personas;
    }

    public InfPerfil(String perfil, String descripcion) {
        this();
        this.perfil = perfil;
        this.descripcion = descripcion;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="getters & setters">
    public long getIdPerfil() {
        return this.idPerfil;
    }

    public void setIdPerfil(long idPerfil) {
        this.idPerfil = idPerfil;
    }

    public String getPerfil() {
        return this.perfil;
    }

    public void setPerfil(String perfil) {
        this.perfil = perfil;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Set<InfContenidoPerfil> getContenidoPerfiles() {
        return this.contenidoPerfiles;
    }

    public void setContenidoPerfiles(Set<InfContenidoPerfil> contenidoPerfiles) {
        this.contenidoPerfiles = contenidoPerfiles;
    }

    public Set<PerPersona> getPersonas() {
        return this.personas;
    }

    public void setPersonas(Set<PerPersona> personas) {
        this.personas = personas;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }
    //</editor-fold>

    public List<InfContenidoPerfil> getContenidoPerfilList() {
        return new ArrayList<>(this.contenidoPerfiles);
    }

    public List<PerPersona> getPersonasList() {
        return new ArrayList<>(this.personas);
    }

    /**
     * Sobreescritura del método
     * <code>equals</code> de manera que dos perfiles serán considerados iguales
     * si sus atributos idPerfil lo son.
     *
     * @param obj
     * @return <code>boolean</code>
     */
    @Override
    public boolean equals(Object obj) {
        boolean res = false;
        if (obj instanceof InfPerfil) {
            InfPerfil p = (InfPerfil) obj;
            if (p.getIdPerfil() == this.getIdPerfil()) {
                res = true;
            }
        }
        return res;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + (int) (this.idPerfil ^ (this.idPerfil >>> 32));
        return hash;
    }
}
