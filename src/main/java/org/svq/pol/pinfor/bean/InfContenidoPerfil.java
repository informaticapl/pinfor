package org.svq.pol.pinfor.bean;

import java.io.Serializable;
import javax.persistence.*;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.svq.pol.pinfor.utility.Constants;

@Entity
@Table(name = "INF_CONTENIDO_PERFIL")
public class InfContenidoPerfil implements Serializable, Comparable {

    private static final long serialVersionUID = 2667057537328519733L;
    @EmbeddedId
    @AttributeOverrides({
        @AttributeOverride(name = "idPerfil", column =
                @Column(name = "ID_PERFIL", nullable = false)),
        @AttributeOverride(name = "idAplicacion", column =
                @Column(name = "ID_APLICACION", nullable = false))})
    private InfContenidoPerfilPK id;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_PERFIL", insertable = false, updatable = false)
    private InfPerfil perfil;
    
    @ManyToOne(fetch = FetchType.EAGER)
    @Fetch(FetchMode.JOIN)
    @JoinColumn(name = "ID_APLICACION", insertable = false, updatable = false)
    private InfAplicacion aplicacion;
    
    @Column(name = "TIPO_ACCESO")
    private Long tipoAcceso;
    
    @Version
    private Integer version;

    //<editor-fold defaultstate="collapsed" desc="constructors">
    public InfContenidoPerfil() {
        // Valores por defecto para el nuevo contenido de perfil
        tipoAcceso = Long.valueOf(Constants.USUARIO | Constants.CONSULTA | Constants.LOCAL);
    }

    public InfContenidoPerfil(InfAplicacion aplicacion) {
        this();
        this.aplicacion = aplicacion;
    }

    public InfContenidoPerfil(InfContenidoPerfilPK id, InfPerfil perfil, InfAplicacion aplicacion) {
        this();
        this.id = id;
        this.perfil = perfil;
        this.aplicacion = aplicacion;
    }

    public InfContenidoPerfil(InfContenidoPerfilPK id, InfPerfil perfil, InfAplicacion aplicacion, Integer version) {
        this();
        this.id = id;
        this.perfil = perfil;
        this.aplicacion = aplicacion;
        this.version = version;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="getters & setters">
    public InfContenidoPerfilPK getId() {
        return this.id;
    }

    public void setId(InfContenidoPerfilPK id) {
        this.id = id;
    }

    public InfPerfil getPerfil() {
        return this.perfil;
    }

    public void setPerfil(InfPerfil perfil) {
        this.perfil = perfil;
    }

    public InfAplicacion getAplicacion() {
        return this.aplicacion;
    }

    public void setAplicacion(InfAplicacion aplicacion) {
        this.aplicacion = aplicacion;
    }

    public int getAmbito() {
        int result = 0;
        if ((tipoAcceso & Constants.GLOBAL) == Constants.GLOBAL) {
            result = Constants.GLOBAL;
        }
        if ((tipoAcceso & Constants.REGIONAL) == Constants.REGIONAL) {
            result = Constants.REGIONAL;
        }
        if ((tipoAcceso & Constants.LOCAL) == Constants.LOCAL) {
            result = Constants.LOCAL;
        }

        return result;
    }

    public void setAmbito(int ambito) {
        // Antes de añadir el nuevo ámbito, es necesario eliminar cualquier posible ámbito definido anteriormente
        this.tipoAcceso &= ~Constants.GLOBAL;
        this.tipoAcceso &= ~Constants.REGIONAL;
        this.tipoAcceso &= ~Constants.LOCAL;
        this.tipoAcceso |= ambito;
    }

    public Boolean getUsuario() {
        return (tipoAcceso & Constants.USUARIO) == Constants.USUARIO;
    }

    public void setUsuario(boolean usuario) {
        this.tipoAcceso = usuario ? (this.tipoAcceso | Constants.USUARIO) : (this.tipoAcceso & ~Constants.USUARIO);
    }

    public Boolean getSupervisor() {
        return (tipoAcceso & Constants.SUPERVISOR) == Constants.SUPERVISOR;
    }

    public void setSupervisor(boolean supervisor) {
        this.tipoAcceso = supervisor ? (this.tipoAcceso | Constants.SUPERVISOR) : (this.tipoAcceso & ~Constants.SUPERVISOR);
    }

    public Boolean getAdministrador() {
        return (tipoAcceso & Constants.ADMINISTRADOR) == Constants.ADMINISTRADOR;
    }

    public void setAdministrador(boolean administrador) {
        this.tipoAcceso = administrador ? (this.tipoAcceso | Constants.ADMINISTRADOR) : (this.tipoAcceso & ~Constants.ADMINISTRADOR);
    }

    public Boolean getConsultas() {
        return (tipoAcceso & Constants.CONSULTA) == Constants.CONSULTA;
    }

    public void setConsultas(boolean consultas) {
        this.tipoAcceso = consultas ? (this.tipoAcceso | Constants.CONSULTA) : (this.tipoAcceso & ~Constants.CONSULTA);
    }

    public Boolean getAltas() {
        return (tipoAcceso & Constants.ALTA) == Constants.ALTA;
    }

    public void setAltas(boolean altas) {
        this.tipoAcceso = altas ? (this.tipoAcceso | Constants.ALTA) : (this.tipoAcceso & ~Constants.ALTA);
    }

    public Boolean getBajas() {
        return (tipoAcceso & Constants.BAJA) == Constants.BAJA;
    }

    public void setBajas(boolean bajas) {
        this.tipoAcceso = bajas ? (this.tipoAcceso | Constants.BAJA) : (this.tipoAcceso & ~Constants.BAJA);
    }

    public Boolean getModificaciones() {
        return (tipoAcceso & Constants.MODIFICACION) == Constants.MODIFICACION;
    }

    public void setModificaciones(boolean modificaciones) {
        this.tipoAcceso = modificaciones ? (this.tipoAcceso | Constants.MODIFICACION) : (this.tipoAcceso & ~Constants.MODIFICACION);
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Long getTipoAcceso() {
        return tipoAcceso;
    }

    public void setTipoAcceso(Long tipoAcceso) {
        this.tipoAcceso = tipoAcceso;
    }
    //</editor-fold>

    @Override
    public boolean equals(Object obj) {
        boolean res = false;
        if (obj != null && obj.getClass() == this.getClass()) {
            InfContenidoPerfil cp = (InfContenidoPerfil) obj;
            if (cp.getId() == this.getId()) {
                res = true;
            }
        }
        return res;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public int compareTo(Object obj) {
        InfContenidoPerfil cp = (InfContenidoPerfil) obj;
        int cmp = this.getAplicacion().compareTo(cp.getAplicacion());
        /*
         * Si ambos objetos son iguales, se devuelve cmp = 0, pero si son
         * distintos, se devuelve cmp=1 (this > obj) si this es 'plantilla' y obj
         * fuera 'unidad' o 'personal'. Si no lo fuera, se continua evaluando.
         */
        if (cmp == 0 && getAmbito() != cp.getAmbito()) {
            switch (getAmbito()) {
                case Constants.GLOBAL:
                    cmp = 1;
                    break;
                case Constants.REGIONAL:
                    cmp = cp.getAmbito() == Constants.LOCAL ? 1 : -1;
                    break;
                default:
                    cmp = -1;
                    break;
            }
        }
        return cmp;
    }
}
