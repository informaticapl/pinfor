package org.svq.pol.pinfor.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.*;
import org.hibernate.annotations.Type;

@Entity
@Table(name = "INF_APLICACION")
public class InfAplicacion implements Serializable, Comparable {
    private static final long serialVersionUID = -8019256231950435995L;

    @Id
    @SequenceGenerator(name = "seq", sequenceName = "INF_SEQ")
    @GeneratedValue(generator = "seq")
    @Column(name = "ID_APLICACION")
    private long idAplicacion;
    
    private String alias;
    private String nombre;
    private String descripcion;
    private String url;
    private String icono;
    
    @Type(type = "boolean")
    private Boolean blank;
    
    @Type(type = "boolean")
    private Boolean mostrar;
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "aplicacion", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<InfContenidoPerfil> contenidoPerfil;
    
    @Version
    private Integer version;

    // <editor-fold defaultstate="collapsed" desc="constructors">
    public InfAplicacion() {
        this.nombre = "";
        this.alias = "";
        this.descripcion = "";
        this.contenidoPerfil = new HashSet(0);
    }

    public InfAplicacion(long idAplicacion, String alias) {
        this();
        this.idAplicacion = idAplicacion;
        this.alias = alias;
    }

    public InfAplicacion(String alias) {
        this();
        this.alias = alias;
    }

    public InfAplicacion(long idAplicacion, String alias, String nombre, String descripcion, String url, String icono, Set<InfContenidoPerfil> contenidoPerfil) {
        this();
        this.idAplicacion = idAplicacion;
        this.alias = alias;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.url = url;
        this.icono = icono;
        this.contenidoPerfil = contenidoPerfil;
    }//</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="getters & setters">
    public long getIdAplicacion() {
        return this.idAplicacion;
    }

    public void setIdAplicacion(long idAplicacion) {
        this.idAplicacion = idAplicacion;
    }

    public String getAlias() {
        return this.alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getIcono() {
        return this.icono;
    }

    public void setIcono(String icono) {
        this.icono = icono;
    }

    public Set<InfContenidoPerfil> getContenidoPerfil() {
        return this.contenidoPerfil;
    }

    public void setContenidoPerfil(Set<InfContenidoPerfil> contenidoPerfil) {
        this.contenidoPerfil = contenidoPerfil;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Boolean getBlank() {
        return blank;
    }

    public void setBlank(boolean blank) {
        this.blank = blank;
    }

    public Boolean getMostrar() {
        return mostrar;
    }

    public void setMostrar(boolean mostrar) {
        this.mostrar = mostrar;
    }
    // </editor-fold>

    public List<InfContenidoPerfil> getContenidoPerfilList() {
        return new ArrayList<>(this.contenidoPerfil);
    }

    /**
     * Sobreescritura del método
     * <code>equals</code> de manera que dos objetos serán considerados si
     * tienen el mismo alias (se da parte del hecho de que en la persistencia no
     * pueden existir dos entradas con el mismo valor para el campo alias.
     *
     * @param obj
     * @return <code>boolean</code>.
     */
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof InfAplicacion)) {
            return false;
        } else {
            InfAplicacion app = (InfAplicacion) obj;
            return alias.equalsIgnoreCase(app.alias);
        }
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 11 * hash + (this.alias != null ? this.alias.hashCode() : 0);
        return hash;
    }

    @Override
    public int compareTo(Object o) {
        int result = 0;
        if (o != null) {
            InfAplicacion app = (InfAplicacion) o;
            result = alias.compareTo(app.alias);
        }
        return result;
    }
}
