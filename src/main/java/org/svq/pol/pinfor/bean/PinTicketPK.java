package org.svq.pol.pinfor.bean;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author jjcampac
 */

@Embeddable
public class PinTicketPK implements Serializable {

    private static final long serialVersionUID = 7440384250905539799L;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "USUARIO")
    private String usuario;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 32)
    @Column(name = "TICKET")
    private String ticket;
    
//<editor-fold defaultstate="collapsed" desc="constructors">

    public PinTicketPK() {
    }

    public PinTicketPK(String usuario, String ticket) {
        this.usuario = usuario;
        this.ticket = ticket;
    }
    
         //</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc="getters & setters">

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getTicket() {
        return ticket;
    }

    public void setTicket(String ticket) {
        this.ticket = ticket;
    }
    
         //</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc="equals && hashcode">

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (usuario != null ? usuario.hashCode() : 0);
        hash += (ticket != null ? ticket.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PinTicketPK)) {
            return false;
        }
        PinTicketPK other = (PinTicketPK) object;
        if ((this.usuario == null && other.usuario != null) || (this.usuario != null && !this.usuario.equals(other.usuario))) {
            return false;
        }
        if ((this.ticket == null && other.ticket != null) || (this.ticket != null && !this.ticket.equals(other.ticket))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.svq.pol.gesate.ws.entity.GatTicketPK[ usuario=" + usuario + ", ticket=" + ticket + " ]";
    }
    
    //</editor-fold>
    
}
