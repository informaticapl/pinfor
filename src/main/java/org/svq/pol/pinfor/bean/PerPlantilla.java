package org.svq.pol.pinfor.bean;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import javax.persistence.*;
import org.hibernate.annotations.Type;

@Entity
@Table(name = "PER_PLANTILLA")
public class PerPlantilla implements Serializable, Comparable {

    private static final long serialVersionUID = 2019876532829658714L;
    
    @Id
    @SequenceGenerator(name = "seq", sequenceName = "PER_SEQ")
    @GeneratedValue(generator = "seq")
    @Column(name = "ID_PLANTILLA")
    private long idPlantilla;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_PERSONAL")
    private PerPersona persona;
    
    @Column(name = "CLAVE_ORDEN")
    private String claveOrden;
    
    @Type(type = "true_false")
    private Boolean activo;
    
    @Column(name = "FECHA_APLICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaAplicacion;

    @OneToOne
    @JoinColumn(name = "CATEGORY", nullable = false)
    private GenMiembro categoria;
    
    private String indicativo;
    
    @Column(name = "INDICATIVO_JA")
    private String indicativoJa;
    
    private Long escalafon;
    
    @Column(name = "FECHA_MOV")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaMov;
    
    @Column(name = "USR_OPER")
    private String operador;
    
    @Version
    private long version;

    //<editor-fold defaultstate="collapsed" desc="constructores">
    public PerPlantilla() {
    }

    public PerPlantilla(long idPlantilla, PerPersona persona) {
        this.idPlantilla = idPlantilla;
        this.persona = persona;
    }

    public PerPlantilla(PerPersona persona, String operador, Date fechaMov) {
        this.persona = persona;
        this.operador = operador;
        this.fechaMov = (Date) fechaMov.clone();
        this.activo = true;
    }

    public PerPlantilla(long idPlantilla, PerPersona persona, String indicativo, String claveOrden, Date fechaMov,  boolean activo, Date fechaAplicacion) {
        this(idPlantilla, persona);
        this.claveOrden = claveOrden;
        this.indicativo = indicativo;
        this.fechaMov = (Date) fechaMov.clone();
        this.activo = activo;
        this.fechaAplicacion = (Date) fechaAplicacion.clone();
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="getters & setters">
    public Boolean getActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public String getClaveOrden() {
        return claveOrden;
    }

    public void setClaveOrden(String claveOrden) {
        this.claveOrden = claveOrden;
    }

    public Long getOrden() {
        return Long.parseLong(this.claveOrden);
    }

    public GenMiembro getCategoria() {
        return categoria;
    }

    public void setCategoria(GenMiembro categoria) {
        this.categoria = categoria;
    }

    public Long getEscalafon() {
        return escalafon;
    }

    public void setEscalafon(Long escalafon) {
        this.escalafon = escalafon;
    }

    public Date getFechaAplicacion() {
        if (fechaAplicacion != null) {
            return (Date) fechaAplicacion.clone();
        } else {
            return null;
        }
    }

    public void setFechaAplicacion(Date fechaAplicacion) {
        this.fechaAplicacion = (Date) fechaAplicacion.clone();
    }

    public Date getFechaMov() {
        if (fechaMov != null) {
            return (Date) fechaMov.clone();
        } else {
            return null;
        }
    }

    public void setFechaMov(Date fechaMov) {
        this.fechaMov = (Date) fechaMov.clone();
    }

    public long getIdPlantilla() {
        return idPlantilla;
    }

    public void setIdPlantilla(long idPlantilla) {
        this.idPlantilla = idPlantilla;
    }

    public String getIndicativo() {
        return indicativo;
    }

    public void setIndicativo(String indicativo) {
        this.indicativo = indicativo;
    }

    public String getIndicativoJa() {
        return indicativoJa;
    }

    public void setIndicativoJa(String indicativoJa) {
        this.indicativoJa = indicativoJa;
    }

    public String getOperador() {
        return operador;
    }

    public void setOperador(String operador) {
        this.operador = operador;
    }

    public PerPersona getPersona() {
        return persona;
    }

    public void setPersona(PerPersona persona) {
        this.persona = persona;
    }

    public long getVersion() {
        return version;
    }

    public void setVersion(long version) {
        this.version = version;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="hash, equals & compareTo">
    @Override
    public int compareTo(Object o) {
        //El orden natural de una colección de plantilla será el orden descendente por fecha de apliación
        PerPlantilla other = (PerPlantilla) o;
        if (this.getFechaAplicacion().compareTo(other.getFechaAplicacion()) == -1) {
            return 1;
        } else if (this.getFechaAplicacion().compareTo(other.getFechaAplicacion()) == 1) {
            return -1;
        } else {
            return 0;
        }
    }
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + Objects.hashCode(this.fechaAplicacion);
        return hash;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PerPlantilla other = (PerPlantilla) obj;
        if (!Objects.equals(this.fechaAplicacion, other.fechaAplicacion)) {
            return false;
        }
        return true;
    }
    //</editor-fold>
}
