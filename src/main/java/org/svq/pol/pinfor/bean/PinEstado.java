package org.svq.pol.pinfor.bean;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 *
 * @author jjcampac
 */
@Entity
@Table(name = "PIN_ESTADO")
public class PinEstado implements Serializable, Comparable<PinEstado>{

    @Id
    @SequenceGenerator(name = "seq", sequenceName = "SEQ_PIN")
    @GeneratedValue(generator = "seq")
    @Column(name = "ID_ESTADO")
    private long idEstado;

    @JoinColumn(name = "ID_DESTINO", referencedColumnName = "ID_DESTINO")
    @ManyToOne
    private PinDestino destino;

    @JoinColumn(name = "ID_MIEMBRO_ESTADO", referencedColumnName = "ID_MIEMBRO")
    @ManyToOne
    private PinMiembro estado;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHA")
    private Date fecha;

    @Column(name = "USR")
    private String usuario;

    @Column(name = "NOTA")
    private String nota;

    @Version
    @Column(name = "VERSION")
    private Integer version;

//<editor-fold defaultstate="collapsed" desc="constructors">
    public PinEstado() {
        this.fecha = new Date();
    }

    public PinEstado(long idEstado) {
        this();
        this.idEstado = idEstado;
    }

    public PinEstado(PinDestino destino, PinMiembro estado, String usuario) {
        this();
        this.destino = destino;
        this.estado = estado;
        this.usuario = usuario;
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="getters & setters">
    public long getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(long idEstado) {
        this.idEstado = idEstado;
    }

    public PinDestino getDestino() {
        return destino;
    }

    public void setDestino(PinDestino destino) {
        this.destino = destino;
    }

    public PinMiembro getEstado() {
        return estado;
    }

    public void setEstado(PinMiembro estado) {
        this.estado = estado;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getNota() {
        return nota;
    }

    public void setNota(String nota) {
        this.nota = nota;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }
//</editor-fold>

    @Override
    public int hashCode() {
        HashCodeBuilder hcb = new HashCodeBuilder();
        hcb.append(this.idEstado);
        return hcb.toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof PinParte)) {
            return false;
        }
        PinEstado other = (PinEstado) obj;
        EqualsBuilder eb = new EqualsBuilder();
        eb.append(this.idEstado, other.idEstado);
        return eb.isEquals();
    }

    @Override
    public int compareTo(PinEstado o) {
        return fecha.compareTo(o.fecha);
    }
    
    

}
