package org.svq.pol.pinfor.bean;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;
import javax.persistence.Version;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.annotations.Where;
import org.svq.pol.pinfor.bean.auxiliar.PinParteAux;

/**
 *
 * @author jjcampac
 */
@MappedSuperclass
public class PinOrdenServicio {

    public static final Boolean VENCIDA = false;
    public static final Boolean VIGENTE = true;

    @Id
    @SequenceGenerator(name = "seq", sequenceName = "SEQ_PIN")
    @GeneratedValue(generator = "seq")
    @Column(name = "ID_ORDEN_TRABAJO")
    protected long idOrdenTrabajo;

    @Column(name = "REFERENCIA_INTERNA")
    protected Integer referenciaInterna;

    @Column(name = "REFERENCIA_EXTERNA")
    protected String referenciaExterna;

    @JoinColumn(name = "ID_MIEMBRO_ORIGEN", referencedColumnName = "ID_MIEMBRO")
    @OneToOne(fetch = FetchType.EAGER)
    protected PinMiembro origen;

    @JoinColumn(name = "ID_MIEMBRO_UNIDAD", referencedColumnName = "ID_MIEMBRO")
    @ManyToOne(fetch = FetchType.EAGER)
    protected GenMiembro unidad;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "ordenServicio", fetch = FetchType.EAGER, orphanRemoval = true)
    @Where(clause = "tipo = 'PI'")
    protected List<PinParteAux> partes;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "ordenServicio", fetch = FetchType.EAGER, orphanRemoval = true)
    @Where(clause = "tipo = 'IF'")
    protected List<PinParteAux> informes;

    @Column(name = "CONTENIDO")
    protected String contenido;

    @Column(name = "FECHA_REGISTRO")
    protected Timestamp fechaRegistro;

    @Column(name = "FECHA_DESDE")
    protected Timestamp fechaDesde;

    @Column(name = "FECHA_HASTA")
    protected Timestamp fechaHasta;

    @Column(name = "USUARIO")
    protected String usuario;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "ordenServicio", fetch = FetchType.EAGER, orphanRemoval = true)
    protected List<PinOrdenServicioAdjunto> adjuntos;

    @Version
    @Column(name = "VERSION")
    protected int version;

    @Transient
    protected Boolean vigente;

    //<editor-fold defaultstate="collapsed" desc="constructors">
    public PinOrdenServicio() {
        this.adjuntos = new ArrayList<>();
        this.partes = new ArrayList<>();
        this.informes = new ArrayList<>();
    }

    public PinOrdenServicio(String usuario) {
        this();
        this.usuario = usuario;
    }

    public PinOrdenServicio(GenMiembro unidad, Timestamp fechaRegistro, String usuario) {
        this();
        this.unidad = unidad;
        this.fechaRegistro = fechaRegistro;
        this.usuario = usuario;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="getters & setters">
    public long getIdOrdenTrabajo() {
        return idOrdenTrabajo;
    }

    public void setIdOrdenTrabajo(long idOrdenTrabajo) {
        this.idOrdenTrabajo = idOrdenTrabajo;
    }

    public Integer getReferenciaInterna() {
        return referenciaInterna;
    }

    public void setReferenciaInterna(Integer referenciaInterna) {
        this.referenciaInterna = referenciaInterna;
    }

    public String getReferenciaExterna() {
        return referenciaExterna;
    }

    public void setReferenciaExterna(String referenciaExterna) {
        this.referenciaExterna = referenciaExterna;
    }

    public PinMiembro getOrigen() {
        return origen;
    }

    public void setOrigen(PinMiembro origen) {
        this.origen = origen;
    }

    public GenMiembro getUnidad() {
        return unidad;
    }

    public void setUnidad(GenMiembro unidad) {
        this.unidad = unidad;
    }

    public String getContenido() {
        return contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

    public List<PinParteAux> getPartes() {
        return partes;
    }

    public void setPartes(List<PinParteAux> partes) {
        this.partes = partes;
    }

    public List<PinParteAux> getInformes() {
        return informes;
    }

    public void setInformes(List<PinParteAux> informes) {
        this.informes = informes;
    }

    public Boolean getVigente() {
        Date hoy = new Date();

        if (fechaDesde == null || fechaHasta == null) {
            return false;
        } else {
            return fechaDesde.compareTo(hoy) * hoy.compareTo(this.fechaHasta) >= 0;
        }
    }

    public void setVigente(Boolean vigente) {
        this.vigente = vigente;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Timestamp fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Date getFechaDesde() {
        return fechaDesde;
    }

    public void setFechaDesde(Timestamp fechaDesde) {
        this.fechaDesde = fechaDesde;
    }

    public Timestamp getFechaHasta() {
        return fechaHasta;
    }

    public void setFechaHasta(Timestamp fechaHasta) {
        this.fechaHasta = fechaHasta;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public List<PinOrdenServicioAdjunto> getAdjuntos() {
        return adjuntos;
    }

    public void setAdjuntos(List<PinOrdenServicioAdjunto> adjuntos) {
        this.adjuntos = adjuntos;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="hash, equals & compare">
    @Override
    public int hashCode() {
        HashCodeBuilder hcb = new HashCodeBuilder();
        hcb.append(this.idOrdenTrabajo);
        return hcb.toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof PinOrdenServicio)) {
            return false;
        }
        PinOrdenServicio other = (PinOrdenServicio) obj;
        EqualsBuilder eb = new EqualsBuilder();
        eb.append(this.idOrdenTrabajo, other.idOrdenTrabajo);
        return eb.isEquals();
    }
    //</editor-fold>

    
}
