package org.svq.pol.pinfor.bean;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class InfContenidoPerfilPK implements java.io.Serializable {
    private static final long serialVersionUID = -2292118841944139795L;

    @Column(name="ID_PERFIL")
    private long idPerfil;
    @Column(name="ID_APLICACION")
    private long idAplicacion;

    // <editor-fold defaultstate="collapsed" desc="constructores">
    public InfContenidoPerfilPK() {
    }

    public InfContenidoPerfilPK(long idPerfil, long idAplicacion) {
        this.idPerfil = idPerfil;
        this.idAplicacion = idAplicacion;
    }// </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="getters & setters">
    public long getIdPerfil() {
        return this.idPerfil;
    }

    public void setIdPerfil(long idPerfil) {
        this.idPerfil = idPerfil;
    }

    public long getIdAplicacion() {
        return this.idAplicacion;
    }

    public void setIdAplicacion(long idAplicacion) {
        this.idAplicacion = idAplicacion;
    }// </editor-fold>

    @Override
    public boolean equals(Object other) {
        if ((this == other)) {
            return true;
        }
        if ((other == null)) {
            return false;
        }
        if (!(other instanceof InfContenidoPerfilPK)) {
            return false;
        }
        InfContenidoPerfilPK castOther = (InfContenidoPerfilPK) other;

        return (this.getIdPerfil() == castOther.getIdPerfil())
                && (this.getIdAplicacion() == castOther.getIdAplicacion());
    }

    @Override
    public int hashCode() {
        int result = 17;

        result = 37 * result + (int) this.getIdPerfil();
        result = 37 * result + (int) this.getIdAplicacion();
        return result;
    }
}
