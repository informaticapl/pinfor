package org.svq.pol.pinfor.bean;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 *
 * @author jjcampac
 */
@Entity
@Table(name = "PIN_OBJETO_SITUACION")
public class PinObjetoSituacion implements Serializable {

    private static final long serialVersionUID = -7180332028736864345L;

    @Id
    @SequenceGenerator(name = "seq", sequenceName = "SEQ_PIN")
    @GeneratedValue(generator = "seq")
    @Column(name = "ID_SITUACION")
    private Long idSituacion;

    @JoinColumn(name = "ID_OBJETO", referencedColumnName = "ID_OBJETO")
    @ManyToOne
    private PinObjeto objeto;

    @JoinColumn(name = "ID_MIEMBRO_SITUACION", referencedColumnName = "ID_MIEMBRO")
    @ManyToOne(fetch = FetchType.EAGER)
    private PinMiembro situacion;

    @Column(name = "FECHA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;

    @Column(name = "USUARIO")
    private String usuario;

    @Version
    @Column(name = "VERSION")
    private Integer version;

//<editor-fold defaultstate="collapsed" desc="constructors">
    public PinObjetoSituacion() {
        this.idSituacion = -1L;
        this.fecha = Calendar.getInstance().getTime();
    }

    public PinObjetoSituacion(PinMiembro situacion, String usuario) {
        this();
        this.situacion = situacion;
        this.usuario = usuario;
    }    
    
    public PinObjetoSituacion(Long idSituacion) {
        this();
        this.idSituacion = idSituacion;
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="getters & setters">
    public Long getIdSituacion() {
        return idSituacion;
    }
    
    public void setIdSituacion(Long idSituacion) {
        this.idSituacion = idSituacion;
    }
    
    public PinObjeto getObjeto() {
        return objeto;
    }
    
    public void setObjeto(PinObjeto objeto) {
        this.objeto = objeto;
    }
    
    public PinMiembro getSituacion() {
        return situacion;
    }
    
    public void setSituacion(PinMiembro situacion) {
        this.situacion = situacion;
    }
    
    public Date getFecha() {
        return fecha;
    }
    
    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }
    
    public String getUsuario() {
        return usuario;
    }
    
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
    
    public Integer getVersion() {
        return version;
    }
    
    public void setVersion(Integer version) {
        this.version = version;
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="hash, equal & compare">
    @Override
    public int hashCode() {
        HashCodeBuilder hcb = new HashCodeBuilder();
        hcb.append(this.situacion);
        return hcb.toHashCode();
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof PinObjetoSituacion)) {
            return false;
        }
        PinObjetoSituacion other = (PinObjetoSituacion) obj;
        EqualsBuilder eb = new EqualsBuilder();
        eb.append(this.situacion, other.situacion);
        return eb.isEquals();
    }
//</editor-fold>

    
    
    
}
