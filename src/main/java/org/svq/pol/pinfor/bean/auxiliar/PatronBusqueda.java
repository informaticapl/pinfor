package org.svq.pol.pinfor.bean.auxiliar;

import java.io.Serializable;

/**
 *
 * @author jjcamacho
 */
public class PatronBusqueda implements Serializable {
    private static final long serialVersionUID = -5383728686109615342L;

    private Boolean activo;
    private Boolean verFuturo;
    private String apellido1;
    private String apellido2;
    private String indicativo;
    private String usuario;
    private long unidad;
    private long categoria;

    //<editor-fold defaultstate="collapsed" desc="constructors">
    public PatronBusqueda() {
        activo = true;
        verFuturo = false;
        unidad = 0L;
        categoria = 0L;
        apellido1 = "";
        apellido2 = "";
        usuario = "";
        indicativo = "";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="getters & setters">
    public Boolean isActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public String getApellido1() {
        return apellido1;
    }

    public void setApellido1(String apellido1) {
        this.apellido1 = apellido1;
    }

    public String getApellido2() {
        return apellido2;
    }

    public void setApellido2(String apellido2) {
        this.apellido2 = apellido2;
    }

    public String getIndicativo() {
        return indicativo;
    }

    public void setIndicativo(String indicativo) {
        this.indicativo = indicativo;
    }

    public long getCategoria() {
        return categoria;
    }

    public void setCategoria(long categoria) {
        this.categoria = categoria;
    }

    public long getUnidad() {
        return unidad;
    }

    public void setUnidad(long unidad) {
        this.unidad = unidad;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public Boolean isVerFuturo() {
        return verFuturo;
    }

    public void setVerFuturo(Boolean verFuturo) {
        this.verFuturo = verFuturo;
    }
    //</editor-fold>
}
