package org.svq.pol.pinfor.bean.auxiliar;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.svq.pol.pinfor.bean.PinParte;

/**
 *
 * @author jjcampac
 */
public class ParteReport {

    private final PinParte parte;
    private String estado;

    public PinParte getParte() {
        return parte;
    }

    public ParteReport(PinParte parte) {
        this.parte = parte;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public JRDataSource getDestinosDS() {
        return new JRBeanCollectionDataSource(parte.getDestinos());
    }

    public JRDataSource getAdjuntosDS() {
        return new JRBeanCollectionDataSource(parte.getAdjuntos());
    }

    public JRDataSource getPersonasDS() {
        return new JRBeanCollectionDataSource(parte.getPersonas());
    }

    public JRDataSource getObjetosDS() {
        return new JRBeanCollectionDataSource(parte.getObjetos());
    }

    public JRDataSource getHistoricoDS() {
        return new JRBeanCollectionDataSource(parte.getHistorico());
    }
}
