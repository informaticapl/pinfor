package org.svq.pol.pinfor.bean.auxiliar;

import java.io.IOException;
import java.io.Serializable;
import java.text.Collator;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.svq.pol.pinfor.bean.GenMiembro;
import org.svq.pol.pinfor.bean.GenSessionUser;
import org.svq.pol.pinfor.bean.PinActuante;
import org.svq.pol.pinfor.bean.PinDestino;
import org.svq.pol.pinfor.bean.PinEstado;
import org.svq.pol.pinfor.bean.PinHistoricoImpresion;
import org.svq.pol.pinfor.bean.PinMiembro;
import org.svq.pol.pinfor.bean.PinObjeto;
import org.svq.pol.pinfor.bean.PinParte;
import org.svq.pol.pinfor.bean.PinParteAdjunto;
import org.svq.pol.pinfor.bean.PinPersona;
import org.svq.pol.pinfor.dao.PinDAO;
import org.svq.pol.pinfor.utility.Constants;

/**
 *
 * @author jjcampac
 */
@Entity
@Table(name = "PIN_PARTE")
public class PinParteAux extends PinParte implements Serializable {

    private static final long serialVersionUID = -2146708650165291119L;

    @Transient
    private GenSessionUser currentUser;

    @Transient
    private PinMiembro participacion;

    @Transient
    private List<PinFirmante> firmantes;

    //<editor-fold defaultstate="collapsed" desc="constructors">
    public PinParteAux() {
        super();
        this.firmantes = new ArrayList<>();
    }

    public PinParteAux(PinParte parte) {
        this();
    }

    public PinParteAux(PinMiembro participacion, GenSessionUser currentUser) {
        this.participacion = participacion;
        this.currentUser = currentUser;
    }

    public PinParteAux(String tipo) {
        super(tipo);
    }

    public PinParteAux(long idParte, GenSessionUser currentUser) {
        super(idParte, currentUser.getIndicativo(), currentUser.getCategoria());
    }

    @SuppressWarnings("LeakingThisInConstructor")
    public PinParteAux(Date fechaRegistro, GenSessionUser currentUser) {
        super(fechaRegistro, currentUser.getIndicativo(), currentUser.getUnidad(), currentUser.getCategoria());
        this.currentUser = currentUser;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="gestión de estados">
    public Boolean tieneInformeFirmado() {
        Boolean found = false;
        Iterator it = adjuntos.iterator();
        while (!found && it.hasNext()) {
            PinParteAdjunto adjunto = (PinParteAdjunto) it.next();
            found = adjunto.getContieneFirmaE();
        }
        return found;
    }

    public void removeDestinatario(GenMiembro destinatario) {
        this.destinos.removeIf(d -> d.getDestino().equals(destinatario));
    }

    public void addEstado(int estado) {
        PinMiembro mEstado;
        PinEstado pinEstado;
        mEstado = PinDAO.getMiembro("EST", estado);
        PinDestino currentDestino = destinos.stream().filter(d -> d.getDestino().equals(currentUser.getUnidad())).findFirst().get();
        pinEstado = new PinEstado(currentDestino, mEstado, String.format("%s / %s", currentUser.getIndicativo(), currentUser.getId().getUsuario()));
        currentDestino.addEstado(pinEstado);
    }

    public void addOficinaUnidad(int estado) {
        addDestinatario(new PinDestino(currentUser.getUnidad()));
        addEstado(estado);
    }

    private Boolean getEstado(int estado) {
        Boolean result = false;
        PinMiembro estadoActual = getEstadoActual().getEstado();
        if (estadoActual != null) {
            result = estadoActual.getValor() == estado;
        }
        return result;
    }

    private void removeEstado(int estado) {
        this.destinos.removeIf(d -> d.getEstados().stream()
                .anyMatch(e -> d.getDestino().equals(currentUser.getUnidad()) && Objects.equals(e.getEstado().getValor(), estado)));
    }

    /**
     * Devuelve el estado del parte en la unidad a la que pertenece el usuario
     * que hace la consulta
     *
     * @return {@code PinEstado} estado.
     */
    public PinEstado getEstadoActual() {
        // Revisar casos de usos. Posiblemente haya que sustituir por getEstadoReal
        PinEstado result = new PinEstado();
        PinDestino destino = this.destinos.stream()
                .filter(d -> d.getDestino().equals(currentUser.getUnidad()))
                .findFirst()
                .orElse(null);
        if (destino != null) {
            PinEstado estado = destino.getEstados().stream()
                    .sorted(Comparator.comparing(PinEstado::getFecha).reversed())
                    .findFirst()
                    .orElse(null);
            if (estado != null) {
                result = estado;
            }
        }
        return result;
    }

    /**
     * Devuelve el estado del parte en la oficina que lo tramita
     *
     * @return {@code PinEstado} estado.
     */
    public PinEstado getEstadoReal() {

        PinEstado result = new PinEstado();
        PinDestino origen = getDestinos().stream().filter(d -> d.getOrigen().getLista().getCodigo().equals("UNI")).findFirst().orElse(null);
        if (origen != null) {
            PinEstado estado = origen.getEstados().stream().sorted(Comparator.comparing(PinEstado::getFecha).reversed()).findFirst().orElse(null);
            if (estado != null) {
                result = estado;
            }
        }
        return result;
    }

    public Boolean getBorrador() {
        return getEstado(PinParte.EST_BORRADOR);
    }

    public Boolean getListoTramitar() {
        return getEstado(PinParte.EST_LISTO_TRAMITAR);
    }

    public Boolean getTramitado() {
        return getEnviadoUnidad() || getEnviadoRegistro();
    }

    public Boolean getEnviadoUnidad() {
        return getEstado(PinParte.EST_ENVIADO_UNIDAD);
    }

    public Boolean getRecibidoUnidad() {
        return getEstado(PinParte.EST_RECIBIDO_UNIDAD);
    }

    public Boolean getEnviadoRegistro() {
        return getEstado(PinParte.EST_ENVIADO_REGISTRO);
    }

    public Boolean getRecibidoRegistro() {
        return getEstado(PinParte.EST_RECIBIDO_REGISTRO);
    }

    public Boolean getEnviadoPortafirmas() {
        return getEstado(PinParte.EST_ENVIADO_PORTAFIRMAS);
    }

    public Boolean getRecibidoPortafirmas() {
        return getEstado(PinParte.EST_RECIBIDO_PORTAFIRMAS);
    }

    public Boolean getAceptadoUnidad() {
        return getEstado(PinParte.EST_ACEPTADO_UNIDAD);
    }

    public Boolean getRechazadoUnidad() {
        return getEstado(PinParte.EST_RECHAZADO_UNIDAD);
    }

    public Boolean getAceptadoRegistro() {
        return getEstado(PinParte.EST_ACEPTADO_REGISTRO);
    }

    public Boolean getRechazadoRegistro() {
        return getEstado(PinParte.EST_RECHAZADO_REGISTRO);
    }

    public Boolean getDevuelto() {
        return getEstado(PinParte.EST_DEVUELTO);
    }

    public Boolean getArchivado() {
        return getEstado(PinParte.EST_ARCHIVADO);
    }

    public Boolean getDesarchivado() {
        return getEstado(PinParte.EST_DESARCHIVADO);
    }

    public Boolean getRegistradoRies() {
        return getEstado(PinParte.EST_REGISTRADO_RIES);
    }

    public Boolean getRegistradoInvesicres() {
        return getEstado(PinParte.EST_REGISTRADO_INVESICRES);
    }
    //</editor-fold>

    public PinMiembro getParticipacion() {
        return participacion;
    }

    public void setParticipacion(PinMiembro participacion) {
        this.participacion = participacion;
    }

    public String getAutor() {
        String result = Constants.EMPTY_STR;
        /* El autor del parte será el usuario que creó el borrador */
        PinDestino origen = getDestinos().stream().filter(d -> d.getOrigen().getLista().getCodigo().equals("UNI")).findFirst().orElse(null);
        if (origen != null) {
            PinEstado estado = origen.getEstados().stream().filter(e -> e.getEstado().getValor().equals(PinParte.EST_BORRADOR)).findFirst().orElse(null);
            if (estado != null) {
                result = estado.getUsuario();
            }
        }

        return result;
    }

    public List<PinFirmante> getFirmantes() {
        return firmantes;
    }

    public void setFirmantes(List<PinFirmante> firmantes) {
        this.firmantes = firmantes;
    }

    public String getDenunciante() {
        String result = Constants.EMPTY_STR;
        if (!actuantes.isEmpty()) {
            return  ((PinActuante) actuantes.get(0)).getIndicativoJa();
        }
        return result;
    }

    public PinPersona getDenunciado() throws IOException {
        PinPersona result = null;
        Iterator it = personas.iterator();
        boolean found = false;
        while (!found && it.hasNext()) {
            PinPersona persona = (PinPersona) it.next();
            if (persona.getParticipacion().equals(participacion)) {
                found = true;
                result = persona;
            }
        }

        return result;
    }

    public GenSessionUser getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(GenSessionUser currentUser) {
        this.currentUser = currentUser;
    }

    public PinPersona getSolicitante() throws IOException {
        PinPersona result = null;
        Iterator it = personas.iterator();
        boolean found = false;
        while (!found && it.hasNext()) {
            PinPersona persona = (PinPersona) it.next();
            if (persona.getParticipacion().equals(participacion)) {
                found = true;
                result = persona;
            }
        }

        return result;
    }

    public int sortByNumParte(String obj1, String obj2) {
        int o1 = Integer.parseInt(obj1.substring(0, obj1.indexOf('-')).replace("/", ""));
        int o2 = Integer.parseInt(obj2.substring(0, obj2.indexOf('-')).replace("/", ""));
        if (o1 < o2) {
            return -1;
        } else if (o1 == o2) {
            return 0;
        } else {
            return 1;
        }
    }

    public String getTipoFormateado() {
        String result;
        switch (tipo) {
            case INFORME:
                result = "Informe";
                break;
            case PARTE:
                result = "Parte";
                break;
            case ORDENANZA:
                result = "Ordenanza";
                break;
            case ACREDITO:
                result = "Acredito";
                break;
            default:
                result = Constants.EMPTY_STR;
        }

        return result;
    }

    public String getNumParteFormateado() {
        String result = "";
        Calendar cal = Calendar.getInstance(Constants.LOCALE_SPANISH);
        try {
            if (this.numeroParte != null && this.numeroParte != 0) {
                cal.setTime(this.fechaRegistro);
                result = String.format("%d/%d-%s", cal.get(Calendar.YEAR), this.numeroParte, this.tipo);
            }
        } catch (NullPointerException ignore) {
        }
        return result;

    }

    public String getListaActuantes() {
        String result = "";

        result = actuantes.stream()
                .map((actuante) -> actuante.getIndicativo() + ", ")
                .reduce(result, String::concat);

        return result.substring(0, result.lastIndexOf(','));
    }

    public String getListaActuantesConCategoria() {
        String result = "";
        result = actuantes.stream()
                .map((actuante) -> String.format("%s %s, ", actuante.getCategoria().getNombre(), actuante.getIndicativo()))
                .reduce(result, String::concat);

        return result.substring(0, result.lastIndexOf(','));
    }

    public void addActuante(PinActuante actuante) {
        actuante.setParte(this);

        if (!this.actuantes.stream().anyMatch(o -> o.getIndicativo().equals(actuante.getIndicativo()))) {
            this.actuantes.add(actuante);
        }
    }

    public void addDestinatario(PinDestino destinatario) {
        // Para comparar cadenas sin tener en cuenta los acentos ni las mayúscular necesitamos usar una instancia de Collator
        final Collator collator = Collator.getInstance();
        collator.setStrength(Collator.NO_DECOMPOSITION);
        destinatario.setParte(this);
        destinatario.setOrigen(currentUser.getUnidad());

        if (this.destinos.stream().noneMatch(d -> collator.equals(d.getDestino().getNombre(), destinatario.getDestino().getNombre()))) {
            this.destinos.add(destinatario);
        }
    }

    public void addPersona(PinPersona persona) {
        persona.setParte(this);

        if (!this.personas.stream().anyMatch(o -> o.getNumDocumento().equals(persona.getNumDocumento()))) {
            this.personas.add(persona);
        }
    }

    public void addObjeto(PinObjeto objeto) {
        objeto.setParte(this);

        if (!this.objetos.stream().anyMatch(o -> o.getNumObjeto().equals(objeto.getNumObjeto()))) {
            this.objetos.add(objeto);
        }
    }

    public void addToImpressionHistory(PinHistoricoImpresion movimiento) {
        movimiento.setParte(this);
        this.historico.add(movimiento);
    }

    public void addToTarget(PinDestino destino) {
        destino.setParte(this);
        this.destinos.add(destino);
    }

    public String getResumen() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy HH:mm");
        String result = String.format("%s. %s. %s en %s %s. ", sdf.format(fechaSuceso), asunto.getNombre(), subasunto.getNombre(), (lugar.getTipoVia() == null ? "" : lugar.getTipoVia().getNombre()), lugar.getNombreLugar());
        for (PinPersona persona : personas) {
            result += String.format("%s (%s). ", persona.getNombre(), persona.getParticipacion() == null ? "Participación desconocida" : persona.getParticipacion().getNombre());
        }

        for (PinObjeto objeto : objetos) {
            result += String.format("%s (%s). ", objeto.getNota(), objeto.getTipo().getNombre());
        }

        return result;
    }

}
