package org.svq.pol.pinfor.bean;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import javax.persistence.*;

@Entity
@Table(name = "PER_SITUACIONES")
public class PerSituacion implements Serializable, Comparable {
    
    private static final long serialVersionUID = -4420187351124172266L;
      

    @Id
    @SequenceGenerator(name = "seq", sequenceName = "PER_SEQ")
    @GeneratedValue(generator = "seq")
    @Column(name = "ID_SITUACION", updatable = false)
    private long idSituacion;
          
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="ID_PERSONAL", nullable=false, updatable = false)
    private PerPersona persona;
    
    @OneToOne
    @JoinColumn(name="ID_MIEMBRO", nullable=false, updatable = false)
    private GenMiembro miembro;

    @Column(name = "FECHA_APLICACION", updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaAplicacion;
    
    @Column(name = "USR_OPER", updatable = false)
    private String operador;
        
    @Column(name = "FECHA_MOV", updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaMov;
    
    @Version
    @Column(name = "VERSION", updatable = false)
    private Integer version;

    // <editor-fold defaultstate="collapsed" desc="constructores">
    public PerSituacion() {
        this.miembro = new GenMiembro();
    }
    
    public PerSituacion(String nombreMiembro){
        this.miembro = new GenMiembro(nombreMiembro);
    }

    public PerSituacion(GenMiembro miembro){
        this.miembro = miembro;
    }
    
    public PerSituacion(PerPersona persona, String operador, Date fechaMov, GenLista tipo){
        this.persona = persona;
        this.operador = operador;
        this.fechaMov = (Date) fechaMov.clone();
        this.miembro = new GenMiembro(tipo);
    }

    public PerSituacion(long idSituacion, PerPersona persona) {
        this();
        this.idSituacion = idSituacion;
        this.persona = persona;
    }

    public PerSituacion(long idSituacion, PerPersona persona, GenMiembro miembro, Date fechaMov, String operador, Date fechaAplicacion) {
        this();
        this.idSituacion = idSituacion;
        this.persona = persona;
        this.miembro = miembro;
        this.fechaMov = (Date) fechaMov.clone();
        this.operador = operador;
        this.fechaAplicacion = (Date) fechaAplicacion.clone();
    }
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="getters & setters">
    public Date getFechaAplicacion() {
        return (Date) fechaAplicacion.clone();
    }

    public void setFechaAplicacion(Date fechaAplicacion) {
        this.fechaAplicacion = (Date) fechaAplicacion.clone();
    }

    public Date getFechaMov() {
        return (Date) fechaMov.clone();
    }

    public void setFechaMov(Date fechaMov) {
        this.fechaMov = (Date) fechaMov.clone();
    }

    public long getIdSituacion() {
        return idSituacion;
    }

    public void setIdSituacion(long idSituacion) {
        this.idSituacion = idSituacion;
    }

    public GenMiembro getMiembro() {
        return miembro;
    }

    public void setMiembro(GenMiembro miembro) {
        this.miembro = miembro;
    }

    public String getOperador() {
        return operador;
    }

    public void setOperador(String operador) {
        this.operador = operador;
    }

    public PerPersona getPersona() {
        return persona;
    }

    public void setPersona(PerPersona persona) {
        this.persona = persona;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }
      //</editor-fold>
    
    @Override
    public int compareTo(Object obj) {
            //El orden natural de una colección de movimiento en plantilla será el orden descendente por fecha de apliación
            PerSituacion other = (PerSituacion) obj;
            return this.getFechaAplicacion().compareTo(other.getFechaAplicacion()) * -1;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + Objects.hashCode(this.fechaAplicacion);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PerSituacion other = (PerSituacion) obj;
        if (!Objects.equals(this.fechaAplicacion, other.fechaAplicacion)) {
            return false;
        }
        return true;
    }
    
    
}
