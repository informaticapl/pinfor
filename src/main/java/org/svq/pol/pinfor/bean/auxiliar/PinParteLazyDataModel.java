package org.svq.pol.pinfor.bean.auxiliar;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortMeta;
import org.primefaces.model.SortOrder;
import org.svq.pol.pinfor.bean.GenSessionUser;
import org.svq.pol.pinfor.bean.PinActuante;
import org.svq.pol.pinfor.dao.PinDAO;

/**
 *
 * @author jjcampac
 */
public class PinParteLazyDataModel extends LazyDataModel<PinParteAux> implements Serializable {

    private static final long serialVersionUID = 3471610731661348338L;

    private PinParteAux patron;
    private Date fechaDesde;
    private Date fechaHasta;
    private Boolean consultaOficina;
    private Boolean incluidoNovedades;
    private Boolean evaluarFechaImpresion;
    private Integer estado;
//    private String archivado;
//    private String registro;
    private String dni;
    private String persona;
    private PinActuante actuante;
    private GenSessionUser currentUser;
    private List<PinParteAux> result;

    public PinParteLazyDataModel() {

    }

    private PinParteLazyDataModel(Builder builder) {
        this.patron = builder.patron;
        this.fechaDesde = builder.fechaDesde;
        this.fechaHasta = builder.fechaHasta;
        this.consultaOficina = builder.consultaOficina;
        this.incluidoNovedades = builder.incluidoNovedades;
        this.evaluarFechaImpresion = builder.evaluarFechaImpresion;
        this.estado = builder.estado;
//        this.archivado = builder.archivado;
//        this.registro = builder.estafeta;
        this.dni = builder.dni;
        this.persona = builder.persona;
        this.actuante = builder.actuante;
        this.currentUser = builder.currentUser;
        
        int total = PinDAO.searchCountPartes(patron, consultaOficina, fechaDesde, fechaHasta, actuante, dni, persona, estado, incluidoNovedades, evaluarFechaImpresion, currentUser);
        this.setRowCount(total);
    }

    @Override
    public List<PinParteAux> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
        result = PinDAO.searchPartes(patron, consultaOficina, fechaDesde, fechaHasta, actuante, dni, persona, estado, incluidoNovedades, evaluarFechaImpresion, first, pageSize, currentUser);
        return result;
    }

    @Override
    public List<PinParteAux> load(int first, int pageSize, List<SortMeta> multiSortMeta, Map<String, Object> filters) {
        result = PinDAO.searchPartes(patron, consultaOficina, fechaDesde, fechaHasta, actuante, dni, persona, estado, /*archivado, registro, */incluidoNovedades, evaluarFechaImpresion, first, pageSize, currentUser);

        return result;
    }

    @Override
    public Object getRowKey(PinParteAux parte) {
        return parte.getIdParte();
    }

    @Override
    public PinParteAux getRowData(String rowKey) {
        Long rk = Long.valueOf(rowKey);
        PinParteAux resultado = null;
        for (PinParteAux parte : this) {
            if (parte.getIdParte() == rk) {
                resultado = parte;
                break;
            }
        }

        return resultado;
    }

    @Override
    public List<PinParteAux> getWrappedData() {
        return super.getWrappedData();
    }

    public static class Builder {

        private PinParteAux patron;
        private Date fechaDesde;
        private Date fechaHasta;
        private Boolean consultaOficina;
        private Boolean incluidoNovedades;
        private Boolean evaluarFechaImpresion;
        private Integer estado;
//        private String archivado;
//        private String estafeta;
        private String dni;
        private String persona;
        private GenSessionUser currentUser;
        private PinActuante actuante;

        public PinParteLazyDataModel build() {
            return new PinParteLazyDataModel(this);
        }

        public Builder(PinParteAux patron) {
            this.patron = patron;
        }

        public Builder fechaDesde(Date fechaDesde) {
            this.fechaDesde = fechaDesde;
            return this;
        }

        public Builder fechaHasta(Date fechaHasta) {
            this.fechaHasta = fechaHasta;
            return this;
        }

        public Builder consultaOficina(Boolean consultaOficina) {
            this.consultaOficina = consultaOficina;
            return this;
        }

        public Builder incluidoNovedades(Boolean incluidoNovedades) {
            this.incluidoNovedades = incluidoNovedades;
            return this;
        }

        public Builder evaluarFechaImpresion(Boolean evaluarFechaImpresion) {
            this.evaluarFechaImpresion = evaluarFechaImpresion;
            return this;
        }

        public Builder estado(Integer estado) {
            this.estado = estado;
            return this;
        }

//        public Builder archivado(String archivado) {
//            this.archivado = archivado;
//            return this;
//        }
//
//        public Builder estafeta(String estafeta) {
//            this.estafeta = estafeta;
//            return this;
//        }

        public Builder dni(String dni) {
            this.dni = dni;
            return this;
        }

        public Builder persona(String persona) {
            this.persona = persona;
            return this;
        }

        public Builder actuante(PinActuante actuante) {
            this.actuante = actuante;
            return this;
        }
        
        public Builder currentUser(GenSessionUser currentUser) {
            this.currentUser = currentUser;
            return this;
        }
    }
}
