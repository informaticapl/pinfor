package org.svq.pol.pinfor.bean;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author jjcampac
 */

@Entity
@Table(name = "PIN_TICKET")
public class PinTicket implements Serializable {

    private static final long serialVersionUID = -5729192392873475214L; 
    
    @EmbeddedId
    protected PinTicketPK pinTicketPK;
    
    @Column(name = "FECHA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    
//<editor-fold defaultstate="collapsed" desc="constructors">

    public PinTicket() {
    }

    public PinTicket(PinTicketPK gatTicketPK) {
        this.pinTicketPK = gatTicketPK;
    }

    public PinTicket(String usuario, String ticket) {
        this.pinTicketPK = new PinTicketPK(usuario, ticket);
    }

    public PinTicketPK getGatTicketPK() {
        return pinTicketPK;
    }
    
         //</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc="getters & setters">

    public void setGatTicketPK(PinTicketPK pinTicketPK) {
        this.pinTicketPK = pinTicketPK;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }
    
    //</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc="equals && hashcode">
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pinTicketPK != null ? pinTicketPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PinTicket)) {
            return false;
        }
        PinTicket other = (PinTicket) object;
        if ((this.pinTicketPK == null && other.pinTicketPK != null) || (this.pinTicketPK != null && !this.pinTicketPK.equals(other.pinTicketPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.svq.pol.gesate.ws.entity.GatTicket[ gatTicketPK=" + pinTicketPK + " ]";
    }
    
//</editor-fold>
    
}
