package org.svq.pol.pinfor.bean;

import com.google.gson.GsonBuilder;
import java.util.Date;
import org.svq.pol.pinfor.gson.GsonExclusionStrategy;
import org.svq.pol.pinfor.utility.serializer.JsonDateDeserializer;

/**
 *
 * @author jjcampac
 */
public abstract class MyEntity {

    public String getJson() {
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(Date.class, new JsonDateDeserializer());
        
        /* Al establecer una estrategia de exclusiones (ver la clase GsonExclusionStragegy) estamos
        diciendo al GsonBuilder creado qué campos no queremos incluir en el json a obtener. Otra opción es
        añadir la anotación @Expose en cada campo que queramos incluir y decir al GsonBuilder que no
        tenga en cuenta los campos que no tengan esta anotación: builder.excludeFieldsWithoutExposeAnnotation()
        El uso de ExclusionStragegies parece una manera más limpia. */
        
        builder.setExclusionStrategies(new GsonExclusionStrategy());

        return builder.create().toJson(this);
    }

}
