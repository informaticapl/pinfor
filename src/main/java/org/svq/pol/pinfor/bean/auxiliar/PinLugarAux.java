package org.svq.pol.pinfor.bean.auxiliar;

import org.svq.pol.pinfor.bean.PinLugar;

/**
 *
 * @author jjcampac
 */
public class PinLugarAux {
    private PinLugar pinLugar;
    private Float distancia;

    public PinLugar getPinLugar() {
        return pinLugar;
    }

    public void setPinLugar(PinLugar pinLugar) {
        this.pinLugar = pinLugar;
    }

    public Float getDistancia() {
        return distancia;
    }

    public void setDistancia(Float distancia) {
        this.distancia = distancia;
    }
        
}
