package org.svq.pol.pinfor.bean.auxiliar;

import java.io.Serializable;

/**
 *
 * @author jjcamacho
 */
public class Contacto implements Serializable {
    private static final long serialVersionUID = 1642540611921217383L;

    private String email;
    private String nombre;
    private String apellido1;
    private String apellido2;
    private String tlfLargo;
    private String tlfCorto;
    private String indicativo;
    private String unidad;
    private String categoria;

    //<editor-fold defaultstate="collapsed" desc="constructors">
    public Contacto() {
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="getters & setters">
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido1() {
        return apellido1;
    }

    public void setApellido1(String apellido1) {
        this.apellido1 = apellido1;
    }

    public String getApellido2() {
        return apellido2;
    }

    public void setApellido2(String apellido2) {
        this.apellido2 = apellido2;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTlfLargo() {
        return tlfLargo;
    }

    public void setTlfLargo(String tlfLargo) {
        this.tlfLargo = tlfLargo;
    }

    public String getTlfCorto() {
        return tlfCorto;
    }

    public void setTlfCorto(String tlfCorto) {
        this.tlfCorto = tlfCorto;
    }

    public String getIndicativo() {
        return indicativo;
    }

    public void setIndicativo(String indicativo) {
        this.indicativo = indicativo;
    }

    public String getUnidad() {
        return unidad;
    }

    public void setUnidad(String unidad) {
        this.unidad = unidad;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }
    //</editor-fold>
}
