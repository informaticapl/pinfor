package org.svq.pol.pinfor.bean.auxiliar;

/**
 *
 * @author jjcampac
 */
public class PinFirmante {
    private String indicativo;
    private String nombre;
    private String email;

//<editor-fold defaultstate="collapsed" desc="constructors">
    public PinFirmante() {
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="getters & setters">
    public String getIndicativo() {
        return indicativo;
    }
    
    public void setIndicativo(String indicativo) {
        this.indicativo = indicativo;
    }
    
    public String getNombre() {
        return nombre;
    }
    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    public String getEmail() {
        return email;
    }
    
    public void setEmail(String email) {
        this.email = email;
    }
//</editor-fold>
    
    
}
