package org.svq.pol.pinfor.bean;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.svq.pol.pinfor.utility.Constants;

/**
 *
 * @author jjcamacho
 */
@Entity
@Table(name = "GEN_SESIONES")
public class GenSessionUser implements Serializable {

    private static final long serialVersionUID = 2721128165003268976L;

    @EmbeddedId
    @AttributeOverrides({
        @AttributeOverride(name = "usuario", column
                = @Column(name = "USUARIO", nullable = false)),
        @AttributeOverride(name = "ip", column
                = @Column(name = "IP", nullable = false))})
    private GenSesionesId id;
    private String ticket;

    @Transient
    private String dominio;

    @Transient
    private GenMiembro unidad;

    @Transient
    private GenMiembro centroTrabajo;

    @Transient
    private GenMiembro categoria;

    @Transient
    private String indicativo;

    @Transient
    private String indicativoJa;

    @Transient
    private Long accesoActual;

    @Transient
    private List<InfContenidoPerfil> aplicaciones;

    @Transient
    private Boolean consultas;

    @Transient
    private Boolean altas;

    @Transient
    private Boolean modificaciones;

    @Transient
    private Boolean bajas;

    @Transient
    private Boolean administrador;

    @Transient
    private Boolean supervisor;

    @Transient
    private Boolean local;

    @Transient
    private Boolean regional;

    @Transient
    private Boolean global;

    //<editor-fold defaultstate="collapsed" desc="constructors">
    public GenSessionUser() {
        this.id = new GenSesionesId();
        this.accesoActual = 0L;
        this.aplicaciones = new ArrayList<>();
    }

    public GenSessionUser(String dominio) {
        this();
        this.dominio = dominio;
    }

    public GenSessionUser(String dominio, String usuario, String ip, String ticket) throws IOException {
        this(dominio);
        this.id = new GenSesionesId(usuario, ip);
        this.ticket = ticket;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="getters & setters">
    public GenSesionesId getId() {
        return id;
    }

    public void setId(GenSesionesId id) {
        this.id = id;
    }

    public String getDominio() {
        return dominio;
    }

    public void setDominio(String dominio) {
        this.dominio = dominio;
    }

    public String getTicket() {
        return ticket;
    }

    public void setTicket(String ticket) {
        this.ticket = ticket;
    }

    public GenMiembro getUnidad() {
        return unidad;
    }

    public void setUnidad(GenMiembro unidad) {
        this.unidad = unidad;
    }

    public GenMiembro getCentroTrabajo() {
        return centroTrabajo;
    }

    public void setCentroTrabajo(GenMiembro centroTrabajo) {
        this.centroTrabajo = centroTrabajo;
    }

    public GenMiembro getCategoria() {
        return categoria;
    }

    public void setCategoria(GenMiembro categoria) {
        this.categoria = categoria;
    }

    public String getIndicativo() {
        return indicativo;
    }

    public void setIndicativo(String indicativo) {
        this.indicativo = indicativo;
    }

    public String getIndicativoJa() {
        return indicativoJa;
    }

    public void setIndicativoJa(String indicativoJa) {
        this.indicativoJa = indicativoJa;
    }

    public Long getAccesoActual() {
        return accesoActual;
    }

    public void setAccesoActual(Long accesoActual) {
        this.accesoActual = accesoActual;
    }

    public List<InfContenidoPerfil> getAplicaciones() {
        /* Aunque la lista de aplicaciones está definida como un conjunto ordenado, esto
         * es así para que el mismo quede ordenado, pero lo que se devuelve es una lista,
         * que es lo que la vista sabe manejar. */
        return new ArrayList(aplicaciones);
    }

    public void setAplicaciones(List<InfPerfil> perfiles) {
        // En realidad, el conjunto de aplicaciones es un conjunto de InfContenidoPerfil, pero
        // es más intuitivo denominarlos Aplicaciones.
        for (InfPerfil perfil : perfiles) {
            for (InfContenidoPerfil cp : perfil.getContenidoPerfiles()) {
                aplicaciones.add(cp);
            }
        }
    }

    public void setConsultas(Boolean consultas) {
        this.consultas = consultas;
    }

    public void setAltas(Boolean altas) {
        this.altas = altas;
    }

    public void setModificaciones(Boolean modificaciones) {
        this.modificaciones = modificaciones;
    }

    public void setBajas(Boolean bajas) {
        this.bajas = bajas;
    }

    public void setAdministrador(Boolean administrador) {
        this.administrador = administrador;
    }

    public void setSupervisor(Boolean supervisor) {
        this.supervisor = supervisor;
    }

    public void setLocal(Boolean local) {
        this.local = local;
    }

    public void setRegional(Boolean regional) {
        this.regional = regional;
    }

    public void setGlobal(Boolean global) {
        this.global = global;
    }

    public Boolean getConsultas() {
        return (this.accesoActual & Constants.CONSULTA) == Constants.CONSULTA;
    }

    public Boolean getAltas() {
        return (this.accesoActual & Constants.ALTA) == Constants.ALTA;
    }

    public Boolean getModificaciones() {
        return (this.accesoActual & Constants.MODIFICACION) == Constants.MODIFICACION;
    }

    public Boolean getBajas() {
        return (this.accesoActual & Constants.BAJA) == Constants.BAJA;
    }

    public Boolean getAdministrador() {
        return (this.accesoActual & Constants.ADMINISTRADOR) == Constants.ADMINISTRADOR;
    }

    public Boolean getSupervisor() {
        return (this.accesoActual & Constants.SUPERVISOR) == Constants.SUPERVISOR;
    }

    public Boolean getLocal() {
        return (this.accesoActual & Constants.LOCAL) == Constants.LOCAL;
    }

    public Boolean getRegional() {
        return (this.accesoActual & Constants.REGIONAL) == Constants.REGIONAL;
    }

    public Boolean getGlobal() {
        return (this.accesoActual & Constants.GLOBAL) == Constants.GLOBAL;
    }
    //</editor-fold>

    /**
     * Se obtiene si el usuario tiene o no permiso para acceder a la aplicación
     * que recibe como parámetro.
     *
     * @param app Objeto de la clase <code>InfAplicacion</code> para la que se
     * quiere comprobar si el usuario está a autorizado.
     * @return <code>boolean</code> - <tt>True</tt> si el usuario dispone de la
     * aplicación app entre sus permisos, <tt>False</tt> en caso contrario.
     */
    public Boolean getAutorizado(InfAplicacion app) {
        /* El usuario estará autorizado a utilizar la aplicación recibida
         * como parámetro si la misma está en su lista de aplicaciones. En este
         * caso, además se establece el atributo accesoActual con los permisos
         * que corresponde al usuario para esta aplicación.
         */
        accesoActual = 0L;
        for (InfContenidoPerfil cp : aplicaciones) {
            if (app.equals(cp.getAplicacion()) && cp.getTipoAcceso() != null) {
                accesoActual |= cp.getTipoAcceso();
            }
        }
        return accesoActual != 0L;
    }
}
