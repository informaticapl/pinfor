package org.svq.pol.pinfor.backing;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.log4j.Logger;
import org.omnifaces.cdi.ViewScoped;
import org.svq.pol.pinfor.bean.GenMiembro;
import org.svq.pol.pinfor.bean.PinMiembro;
import org.svq.pol.pinfor.dao.PinDAO;
import org.svq.pol.pinfor.utility.Constants;
import org.svq.pol.pinfor.utility.Utilities;

/**
 *
 * @author jjcampac
 */
@ViewScoped
@Named
public class PinDestinatariosBacking extends AbstractBacking implements Serializable {

    private static final long serialVersionUID = 5339711978794975621L;

    private static final Logger LOGGER = Logger.getLogger(PinDestinatariosBacking.class);

    @Inject
    private Utilities utilities;

    private List<PinMiembro> asuntos;
    private List<GenMiembro> destJefatura;
    private List<GenMiembro> destAyuntamiento;
    private List<GenMiembro> destExterno;
    private List<GenMiembro> destinosDisponibles;
    private String origenDestinoDisponible;
    private PinMiembro selectedAsunto, selectedSubasunto;
    private GenMiembro selectedDestinatarioDisponible, selectedDestinatarioAsignado;
    private List<GenMiembro> copiaDestinosAsignados;

    //<editor-fold defaultstate="collapsed" desc="constructors">
    public PinDestinatariosBacking() {

    }

    @PostConstruct
    protected void onConstruct() {
        try {
            selectedAsunto = new PinMiembro();
            selectedSubasunto = null;
            selectedDestinatarioDisponible = null;
            selectedDestinatarioAsignado = null;
            this.origenDestinoDisponible = Constants.DESTINO_JEFATURA;

            this.asuntos = PinDAO.getMiembros("ASU");
            destJefatura = PinDAO.getMiembrosGenericos("DJF");
            destAyuntamiento = PinDAO.getMiembrosGenericos("DAY");
            destExterno = PinDAO.getMiembrosGenericos("DEX");
        } catch (Exception ex) {
            utilities.addFacesMessage(LOGGER, Constants.ERROR, "Se ha producido un error en la inicialización de listas: " + utilities.getExceptionCause(ex));
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="getters & setters">
    public List<PinMiembro> getAsuntos() {
        return asuntos;
    }

    public void setAsuntos(List<PinMiembro> asuntos) {
        this.asuntos = asuntos;
    }

    public PinMiembro getSelectedSubasunto() {
        return selectedSubasunto;
    }

    public void setSelectedSubasunto(PinMiembro selectedSubasunto) {
        this.selectedSubasunto = selectedSubasunto;
    }

    public List<GenMiembro> getDestJefatura() {
        return destJefatura;
    }

    public void setDestJefatura(List<GenMiembro> destJefatura) {
        this.destJefatura = destJefatura;
    }

    public List<GenMiembro> getDestAyuntamiento() {
        return destAyuntamiento;
    }

    public void setDestAyuntamiento(List<GenMiembro> destAyuntamiento) {
        this.destAyuntamiento = destAyuntamiento;
    }

    public List<GenMiembro> getDestExterno() {
        return destExterno;
    }

    public void setDestExterno(List<GenMiembro> destExterno) {
        this.destExterno = destExterno;
    }

    public PinMiembro getSelectedAsunto() {
        return selectedAsunto;
    }

    public void setSelectedAsunto(PinMiembro selectedAsunto) {
        this.selectedAsunto = selectedAsunto;
    }

    public GenMiembro getSelectedDestinatarioDisponible() {
        return selectedDestinatarioDisponible;
    }

    public void setSelectedDestinatarioDisponible(GenMiembro selectedDestinatarioDisponible) {
        this.selectedDestinatarioDisponible = selectedDestinatarioDisponible;
    }

    public GenMiembro getSelectedDestinatarioAsignado() {
        return selectedDestinatarioAsignado;
    }

    public void setSelectedDestinatarioAsignado(GenMiembro selectedDestinatarioAsignado) {
        this.selectedDestinatarioAsignado = selectedDestinatarioAsignado;
    }

    public String getOrigenDestinoDisponible() {
        return origenDestinoDisponible;
    }

    public void setOrigenDestinoDisponible(String origenDestinoDisponible) {
        this.origenDestinoDisponible = origenDestinoDisponible;
    }

    public void setDestinosDisponibles(List<GenMiembro> destinosDisponibles) {
        this.destinosDisponibles = destinosDisponibles;
    }

    public List<GenMiembro> getDestinosDisponibles() {

        switch (this.origenDestinoDisponible) {
            case Constants.DESTINO_JEFATURA:
                this.destinosDisponibles = this.destJefatura;
                break;
            case Constants.DESTINO_AYUNTAMIENTO:
                this.destinosDisponibles = this.destAyuntamiento;
                break;
            case Constants.DESTINO_EXTERNO:
                this.destinosDisponibles = this.destExterno;
                break;
            default:
                this.destinosDisponibles = new ArrayList<>();
        }
        return this.destinosDisponibles;
    }
    //</editor-fold>

    public List<GenMiembro> getListaDestinos() {
        List<GenMiembro> result = new ArrayList<>();
        if (selectedSubasunto != null) {
            result = selectedSubasunto.getDestinos().stream().collect(Collectors.toList());
        } 
        return result;
    }

    public void resetSelectedSubasunto() {
        selectedSubasunto = null;
        copiaDestinosAsignados = null;
    }

    public void resetSelectedDestinatarioDisponible() {
        selectedDestinatarioDisponible = null;
    }

    public void resetSelectedDestinatarioAsignado() {
        selectedDestinatarioAsignado = null;
    }

    public void anadirAsignado() {
        if (selectedSubasunto.addDestino(selectedDestinatarioDisponible)) {
            selectedDestinatarioDisponible = null;
        } else {
            utilities.addFacesMessage(LOGGER, Constants.WARN, "El destino seleccionado no pudo ser añadido a la lista de asignados.");
        }
    }

    public void eliminarAsignado() {
        if (selectedSubasunto.removeDestino(selectedDestinatarioAsignado)) {
            selectedDestinatarioAsignado = null;
        } else {
            utilities.addFacesMessage(LOGGER, Constants.WARN, "El destino asignado no pudo ser eliminado de la lista.");
        }
    }

    public void makeCopyDestinosAsignados() {
        /* Debemos definir una copia de la lista original de destinos asignados por si el usuario decide
        cancelar la operación de modificación, en cuyo caso debemos devolver la lista de destinatarios
        asignados a su estado original.
        */
        copiaDestinosAsignados = selectedSubasunto.getDestinos().stream().collect(Collectors.toList());
    }

    public void modifyDestinos() {
        PinDAO.saveMiembro(selectedSubasunto);
        copiaDestinosAsignados = null;
    }

    public void resetDestinos() {
        // Aquí es donde se devuelve la lista de destinatarios asignados a su valor inicial 
        selectedSubasunto.setDestinos(new HashSet<>(copiaDestinosAsignados));
    }

}
