package org.svq.pol.pinfor.backing;

import javax.inject.Inject;
import org.svq.pol.exception.NoItemSelectedException;
import org.svq.pol.pinfor.bean.GenSessionUser;
import org.svq.pol.pinfor.utility.Utilities;

public abstract class AbstractBacking {

    @Inject
    protected Utilities utilities;

//<editor-fold defaultstate="collapsed" desc="constructores">
    public AbstractBacking() {

    }
//</editor-fold>

    /**
     * Determina si el usuario logado puede efectuar la habilidad descrita en el
     * parámetro.
     *
     * @param ability String que describe la habilidad por la que se pregunta.
     * Puede ser cualquiera de los siguientes valores: create, modify, delete.
     * @return Boolean.
     * @throws org.svq.pol.exception.NoItemSelectedException
     */
    protected boolean userCan(String ability) throws NoItemSelectedException {
        boolean result = false;
        GenSessionUser sessionUser = utilities.getCurrentUser();
        if (sessionUser != null) {
            switch (ability) {
                case "create":
                    result = sessionUser.getAltas();
                    break;
                case "modify":
                    result = sessionUser.getModificaciones();
                    break;
                case "delete":
                    result = sessionUser.getBajas();
                    break;
                default:
                    throw new NoItemSelectedException("Error no esperado");
            }
        }

        return result;
    }

    /**
     * Determina si el usuario cumple un determinado papel, el recibido en el
     * único parámetro del método.
     *
     * @param role String que describe el papel por el que se pregunta. Los
     * valores permitidos son: administrator y supervisor.
     * @return Boolean.
     * @throws org.svq.pol.exception.NoItemSelectedException
     */
    protected boolean userIs(String role) throws NoItemSelectedException {
        boolean result = false;
        GenSessionUser sessionUser = utilities.getCurrentUser();
        if (sessionUser != null) {
            switch (role) {
                case "administrator":
                    result = sessionUser.getAdministrador();
                    break;
                case "supervisor":
                    result = sessionUser.getSupervisor();
                    break;
                default:
                    throw new NoItemSelectedException("Error no esperado");
            }
        }

        return result;
    }

    protected boolean canUseBrowser() {
        return !(getBrowserName().contains("MSIE") || getBrowserName().contains("Trident/"));
    }

    private String getBrowserName() {
        return utilities.getExternalContext().getRequestHeaderMap().get("User-Agent");
    }
}
