package org.svq.pol.pinfor.backing;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.Serializable;
import java.net.URISyntaxException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.inject.Named;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;
import org.apache.log4j.Logger;
import org.omnifaces.cdi.ViewScoped;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.StreamedContent;
import org.svq.pol.exception.PinforException;
import org.svq.pol.http.HttpCaller;
import org.svq.pol.pinfor.bean.GenMiembro;
import org.svq.pol.pinfor.bean.PinDestino;
import org.svq.pol.pinfor.bean.PinHistoricoImpresion;
import org.svq.pol.pinfor.bean.PinMiembro;
import org.svq.pol.pinfor.bean.PinObjeto;
import org.svq.pol.pinfor.bean.PinObjetoSituacion;
import org.svq.pol.pinfor.bean.auxiliar.ParteReport;
import org.svq.pol.pinfor.bean.auxiliar.PinObjetoLazyDataModel;
import org.svq.pol.pinfor.bean.auxiliar.PinParteAux;
import org.svq.pol.pinfor.dao.PinDAO;
import org.svq.pol.pinfor.utility.Constants;
import org.svq.pol.pinfor.utility.Log;

/**
 *
 * @author jjcampac
 */
@ViewScoped
@Named
public class PinObjetoBacking extends AbstractBacking implements Serializable {

    private static final Logger LOGGER = Logger.getLogger(PinObjetoBacking.class);
    private static final long serialVersionUID = -4266795161342720158L;
    
    private PinObjeto selectedObjeto;
    private PinObjetoSituacion selectedObjetoSituacion;
    private PinParteAux selectedParte;
    private PinObjeto patronBusqueda;
    private LazyDataModel<PinObjeto> lazyResultadoBusqueda;
    private List<PinObjeto> selectedObjetoChecked;

    private List<PinMiembro> tiposObjeto;
    private List<PinMiembro> situaciones;
    private PinMiembro selectedSituacion;

    private Date fechaDesde, fechaHasta;
    private List<GenMiembro> unidades;
    private List<PinMiembro> procedencias;
    private String msgAmbito;
    private String idObjeto;

    private HttpCaller httpCaller;
    private String permantentTicket;

    private Integer agno;
    private List<Integer> availableYears;
    private String scanInput;

    private String pdfFileName;
    private static String reportsPath;
    private Boolean esPdf;

    //<editor-fold defaultstate="collapsed" desc="constructors">
    public PinObjetoBacking() {

    }

    @PostConstruct
    protected void onConstruct() {
        try {
            this.procedencias = PinDAO.getMiembros("OOT");
            this.situaciones = PinDAO.getMiembros("SOB");
            this.tiposObjeto = PinDAO.getMiembros("TOB");
            this.selectedObjeto = new PinObjeto(PinDAO.getMiembroById(Long.parseLong(utilities.getProperty("/default.properties", "denunciado"))), utilities.getCurrentUser());
            this.patronBusqueda = new PinObjeto(PinDAO.getMiembroById(Long.parseLong(utilities.getProperty("/default.properties", "denunciado"))), utilities.getCurrentUser());
            this.patronBusqueda.getParte().setUnidad(utilities.getCurrentUser().getUnidad());
            this.selectedObjetoChecked = new ArrayList<>();
            this.idObjeto = Constants.EMPTY_STR;

            this.unidades = PinDAO.getUnidades();

            this.agno = Calendar.getInstance(Constants.LOCALE_SPANISH).get(Calendar.YEAR);
            this.availableYears = new ArrayList<>();
            for (int i = 2019; i <= this.agno; i++) {
                this.availableYears.add(i);
            }

            if (utilities.getCurrentUser().getGlobal()) {
                this.msgAmbito = "Jefatura";
            } else if (utilities.getCurrentUser().getRegional()) {
                this.msgAmbito = "Unidad";
            } else {
                this.msgAmbito = "Usuario";
            }

            this.permantentTicket = utilities.getProperty("/default.properties", "ticket");
        } catch (IOException ex) {
            utilities.addFacesMessage(LOGGER, Constants.WARN, ex.getMessage());
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="getters & setters">
    public Date getHoy() {
        return new Date();
    }

    public Boolean getEsPdf() {
        return esPdf;
    }

    public PinObjetoSituacion getSelectedObjetoSituacion() {
        return selectedObjetoSituacion;
    }

    public void setSelectedObjetoSituacion(PinObjetoSituacion selectedObjetoSituacion) {
        this.selectedObjetoSituacion = selectedObjetoSituacion;
    }

    public List<PinMiembro> getSituaciones() {
        return situaciones;
    }

    public PinMiembro getSelectedSituacion() {
        return selectedSituacion;
    }

    public List<PinMiembro> getTiposObjeto() {
        return tiposObjeto;
    }

    public void setSelectedSituacion(PinMiembro selectedSituacion) {
        this.selectedSituacion = selectedSituacion;
    }

    public String getIdObjeto() {
        return idObjeto;
    }

    public void setIdObjeto(String idObjeto) {
        this.idObjeto = idObjeto;
    }

    public PinParteAux getSelectedParte() {
        return selectedParte;
    }

    public void setSelectedParte(PinParteAux selectedParte) {
        this.selectedParte = selectedParte;
    }

    public List<PinMiembro> getProcedencias() {
        return procedencias;
    }

    public Boolean getCanSave() {
        return selectedObjeto.getIdObjeto() > -1L;
    }

    public Boolean getCanEdit() {
        return selectedObjeto.getIdObjeto() > 0
                && utilities.getCurrentUser().getModificaciones();
    }

    public Boolean getCanAdd() {
        return selectedObjeto != null
                && selectedObjeto.getIdObjeto() > 0
                && utilities.getCurrentUser().getAltas()
                && selectedObjeto.getParte().getUnidad().equals(utilities.getCurrentUser().getUnidad());
    }

    public Boolean getCanDelete() {
        return selectedObjeto.getIdObjeto() > 0
                && utilities.getCurrentUser().getBajas();
    }

    public Boolean getCanPrint() {
        return selectedObjeto != null
                && selectedObjeto.getIdObjeto() > 0
                && utilities.getCurrentUser().getModificaciones()
                && selectedObjeto.getParte().getUnidad().equals(utilities.getCurrentUser().getUnidad());
    }

    public String getPdfFileName() {
        return pdfFileName;
    }

    public void setPdfFileName(String pdfFileName) {
        this.pdfFileName = pdfFileName;
    }

    public static String getReportsPath() {
        return reportsPath;
    }

    public static void setReportsPath(String reportsPath) {
        PinObjetoBacking.reportsPath = reportsPath;
    }

    public Date getFechaDesde() {
        return fechaDesde;
    }

    public void setFechaDesde(Date fechaDesde) {
        this.fechaDesde = fechaDesde;
    }

    public Date getFechaHasta() {
        return fechaHasta;
    }

    public void setFechaHasta(Date fechaHasta) {
        this.fechaHasta = fechaHasta;
    }

    public List<GenMiembro> getUnidades() {
        return unidades;
    }

    public void setUnidades(List<GenMiembro> unidades) {
        this.unidades = unidades;
    }

    public String getMsgAmbito() {
        return msgAmbito;
    }

    public void setMsgAmbito(String msgAmbito) {
        this.msgAmbito = msgAmbito;
    }

    public HttpCaller getHttpCaller() {
        return httpCaller;
    }

    public void setHttpCaller(HttpCaller httpCaller) {
        this.httpCaller = httpCaller;
    }

    public String getPermantentTicket() {
        return permantentTicket;
    }

    public void setPermantentTicket(String permantentTicket) {
        this.permantentTicket = permantentTicket;
    }

    public PinObjeto getSelectedObjeto() {
        return selectedObjeto;
    }

    public void setSelectedObjeto(PinObjeto selectedObjeto) {
        this.selectedObjeto = selectedObjeto;
    }

    public PinObjeto getPatronBusqueda() {
        return patronBusqueda;
    }

    public void setPatronBusqueda(PinObjeto patronBusqueda) {
        this.patronBusqueda = patronBusqueda;
    }

    public List<PinObjeto> getSelectedObjetoChecked() {
        return selectedObjetoChecked;
    }

    public void setSelectedObjetoChecked(List<PinObjeto> selectedObjetoChecked) {
        this.selectedObjetoChecked = selectedObjetoChecked;
    }

    public LazyDataModel<PinObjeto> getLazyResultadoBusqueda() {
        return lazyResultadoBusqueda;
    }

    public void setLazyResultadoBusqueda(LazyDataModel<PinObjeto> lazyResultadoBusqueda) {
        this.lazyResultadoBusqueda = lazyResultadoBusqueda;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="private methods">
    private static void compileJasperFiles() throws IOException, JRException {
        File folder = new File(reportsPath);
        FilenameFilter jasperFilter = (File directory, String fileName) -> directory.equals(folder) && fileName.endsWith(".jrxml");

        File[] xmlFiles = folder.listFiles(jasperFilter);
        for (File file : xmlFiles) {
            String xmlFile = file.getCanonicalPath();
            try {
                Log.log(LOGGER, Constants.INFO, "Procesando el archivo: " + xmlFile);
                String jasperFile = file.getCanonicalPath().replace(".jrxml", ".jasper");
                JasperCompileManager.compileReportToFile(xmlFile, jasperFile);
            } catch (JRException ex) {
                throw new JRException("\nProcesando el archivo: " + xmlFile + "\n\n" + ex.getMessage());
            }
        }

    }

    private void makeReportEtiqueta() throws SQLException, JRException, IOException, URISyntaxException {
        reportsPath = utilities.getExternalContext().getRealPath("/").concat(Constants.REPORTS_PATH);

        File pdfFile = File.createTempFile("etiquetaObjeto", ".pdf");
        pdfFile.deleteOnExit();
        pdfFileName = pdfFile.getAbsolutePath();

        JasperPrint jasperPrint;
        JasperReport jrInforme;

        String jasperFileInforme = reportsPath.concat("etiquetaObjeto.jasper");

        HashMap parameters = new HashMap();

        if (!new File(jasperFileInforme).exists()) {
            compileJasperFiles();
        }
        jrInforme = (JasperReport) JRLoader.loadObject(new BufferedInputStream(new FileInputStream(jasperFileInforme)));

        List<PinObjeto> objetos = new ArrayList<>();
        objetos.add(selectedObjeto);

        JRBeanCollectionDataSource jrDataSource = new JRBeanCollectionDataSource(objetos);
        jasperPrint = JasperFillManager.fillReport(jrInforme, parameters, jrDataSource);
        JasperExportManager.exportReportToPdfFile(jasperPrint, pdfFileName);

    }

    private void makeReportParte() throws SQLException, JRException, IOException, URISyntaxException {
        reportsPath = utilities.getExternalContext().getRealPath("/").concat(Constants.REPORTS_PATH);

        File pdfFile = File.createTempFile("parte", ".pdf");
        pdfFile.deleteOnExit();
        pdfFileName = pdfFile.getAbsolutePath();

        JasperPrint jasperPrint;
        JasperReport jrInforme;

        String jasperFileInforme = selectedParte.getTipo().equals("PI") ? reportsPath.concat("parte.jasper") : reportsPath.concat("informe.jasper");

        HashMap parameters = new HashMap();

        parameters.put("UNIDAD", selectedParte.getUnidad() != null ? selectedParte.getUnidad().getNombre() : "Unidad no definida");

        if (!new File(jasperFileInforme).exists()) {
            compileJasperFiles();
        }
        jrInforme = (JasperReport) JRLoader.loadObject(new BufferedInputStream(new FileInputStream(jasperFileInforme)));

        if (jasperFileInforme.contains("parte")) {
            List<ParteReport> partes = new ArrayList<>();
            partes.add(new ParteReport(selectedParte));

            JRBeanCollectionDataSource jrDataSource = new JRBeanCollectionDataSource(partes);
            jasperPrint = JasperFillManager.fillReport(jrInforme, parameters, jrDataSource);
            JasperExportManager.exportReportToPdfFile(jasperPrint, pdfFileName);
        } else if (jasperFileInforme.contains("informe")) {
            List<PinDestino> destinos = new ArrayList<>();
            for (PinDestino destino : selectedParte.getDestinos()) {
                destinos.add(destino);
            }
            JRBeanCollectionDataSource jrDataSource = new JRBeanCollectionDataSource(destinos);
            jasperPrint = JasperFillManager.fillReport(jrInforme, parameters, jrDataSource);
            JasperExportManager.exportReportToPdfFile(jasperPrint, pdfFileName);
        }

    }

    public Integer getAgno() {
        return agno;
    }

    public void setAgno(Integer agno) {
        this.agno = agno;
    }

    public List<Integer> getAvailableYears() {
        return availableYears;
    }

    public void setAvailableYears(List<Integer> availableYears) {
        this.availableYears = availableYears;
    }

    public String getScanInput() {
        return scanInput;
    }

    public void setScanInput(String scanInput) {
        this.scanInput = scanInput;
    }
    //</editor-fold>

    public void selectRow() {
        if (selectedObjetoChecked.size() == 1) {
            selectedObjeto = selectedObjetoChecked.get(0);
            selectedParte = (PinParteAux) selectedObjeto.getParte();
        } else {
            try {
                selectedObjeto = new PinObjeto(PinDAO.getMiembroById(Long.parseLong(utilities.getProperty("/default.properties", "denunciado"))), utilities.getCurrentUser());
            } catch (IOException ex) {
                utilities.addFacesMessage(LOGGER, Constants.ERROR, utilities.getExceptionCause(ex).toString());
            }
        }
    }

    public void resetSearchFields() {
        try {
            this.patronBusqueda = new PinObjeto(PinDAO.getMiembroById(Long.parseLong(utilities.getProperty("/default.properties", "denunciado"))), utilities.getCurrentUser());
            this.patronBusqueda.getParte().setUnidad(utilities.getCurrentUser().getUnidad());
            this.fechaDesde = null;
            this.fechaHasta = null;
            this.idObjeto = Constants.EMPTY_STR;
        } catch (IOException ex) {
            utilities.addFacesMessage(LOGGER, Constants.ERROR, utilities.getExceptionCause(ex).toString());
        }
    }

    public void abrir() {
        SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyyHHmmss");
        Long id = -1L;
        try {
            if (!scanInput.isEmpty()) {
                String[] parte = scanInput.split(" ");
                id = Long.valueOf(parte[3]);
                scanInput = Constants.EMPTY_STR;
            } else if (!idObjeto.trim().isEmpty()) {
                id = Long.valueOf(idObjeto);
            }

            patronBusqueda.setIdObjeto(id);
            fechaDesde = sdf.parse(String.format("0101%d000000", agno));
            fechaHasta = sdf.parse(String.format("3112%d235959", agno));
            search(true);
            if (lazyResultadoBusqueda.getRowCount() != 0) {

                // Como la carga de la tabla no se hace hasta que se genera la vista, es necesario forzar la misma
                // desde aquí para obtener el primer registro, que podamos referenciar como selectedParte
                DataTable lazyTable = (DataTable) utilities.getFacesContext().getViewRoot().findComponent("fMain:tObjeto");
                lazyTable.loadLazyData();

                this.selectedObjeto = lazyResultadoBusqueda.getWrappedData().get(0);
                if (this.selectedObjetoChecked != null) {
                    this.selectedObjetoChecked.clear();
                }
                this.selectedObjetoChecked.add(selectedObjeto);
                selectRow();
            } else {
                utilities.addFacesMessage(LOGGER, Constants.INFO, "El objeto solicitada aún no está registrado o se encuentra fuera de su ámbito de búsqueda. <br/><br/>ÁMBITO: " + this.msgAmbito);
            }
            resetSearchFields();
        } catch (ParseException ex) {
            utilities.addFacesMessage(LOGGER, Constants.ERROR, "Se ha producido un error en la consulta: " + utilities.getExceptionCause(ex));
        }

    }

    public void search(boolean resetSelectedObjeto) {
        try {
            if (!idObjeto.trim().isEmpty()) {
                this.patronBusqueda.setIdObjeto(Long.parseLong(idObjeto));
            }

            if (this.selectedObjetoChecked != null) {
                this.selectedObjetoChecked.clear();
            }
            if (resetSelectedObjeto) {
                this.selectedObjeto = new PinObjeto(PinDAO.getMiembroById(Long.parseLong(utilities.getProperty("/default.properties", "denunciado"))), utilities.getCurrentUser());
            }

            lazyResultadoBusqueda = new PinObjetoLazyDataModel.Builder(patronBusqueda)
                    .fechaDesde(fechaDesde)
                    .fechaHasta(fechaHasta)
                    .currentUser(utilities.getCurrentUser())
                    .build();

            if (lazyResultadoBusqueda.getRowCount() == 0) {
                utilities.addFacesMessage(LOGGER, Constants.INFO, "No se han encontrado datos coincidentes con la consulta. <br/><br/>ÁMBITO: " + this.msgAmbito);
            }
        } catch (NumberFormatException | IOException ex) {
            utilities.addFacesMessage(LOGGER, Constants.ERROR, "Se ha producido un error en la consulta: " + ex.getMessage());
        }
    }

    public void save() {
        try {
            PinDAO.saveObjeto(selectedObjeto);
            selectedObjeto = null;
        } catch (PinforException ex) {
            utilities.addFacesMessage(LOGGER, Constants.ERROR, "Se ha producido un error al grabar el Objeto: " + utilities.getExceptionCause(ex));
        }
    }

    public void generateEtiquetaReport() {
        PinHistoricoImpresion movimiento = new PinHistoricoImpresion();
        try {
            movimiento.setFecha(new Date());
            movimiento.setUsuario(String.format("%s / %s", utilities.getCurrentUser().getIndicativo(), utilities.getCurrentUser().getId().getUsuario()));
            movimiento.setElemento("Etiqueta objeto " + selectedObjeto.getIdObjeto());
            selectedParte.addToImpressionHistory(movimiento);

            makeReportEtiqueta();
            esPdf = pdfFileName.substring(pdfFileName.lastIndexOf('.'), pdfFileName.length()).equals(".pdf");
            utilities.executeUI("PF('dlgVerEtiqueta').show();");
        } catch (SQLException | NullPointerException | JRException | IOException | URISyntaxException ex) {
            utilities.addFacesMessage(LOGGER, Constants.ERROR, utilities.getExceptionCause(ex) == null ? "Se ha producido un error durante la generación de la etiqueta" : utilities.getExceptionCause(ex).toString());
        }

    }

    public void generateParteReport() {

        if (selectedParte.getAnulado()) {
            utilities.addFacesMessage(LOGGER, Constants.INFO, "El parte está anulado.");
        } else {
            PinHistoricoImpresion movimiento = new PinHistoricoImpresion();
            try {
                // Primero generamos el movimiento para el histórico      
                movimiento.setFecha(new Date());
                movimiento.setUsuario(String.format("%s / %s", utilities.getCurrentUser().getIndicativo(), utilities.getCurrentUser().getId().getUsuario()));
                movimiento.setElemento("Parte");
                selectedParte.addToImpressionHistory(movimiento);

                // Después generamos el informe o el resumen, dependiendo de la seleccion....
                try {

                    makeReportParte();
                    esPdf = pdfFileName.substring(pdfFileName.lastIndexOf('.'), pdfFileName.length()).equals(".pdf");
                    utilities.executeUI("PF('dlgVerParte').show();");
                } catch (SQLException | NullPointerException | JRException | IOException | URISyntaxException ex) {
                    utilities.addFacesMessage(LOGGER, Constants.ERROR, utilities.getExceptionCause(ex) == null ? "Se ha producido un error durante la generación del parte" : utilities.getExceptionCause(ex).toString());
                }

                // Y si todo ha ido bien, persistimos el movimiento en la DB
                PinDAO.saveParte(selectedParte);

            } catch (PinforException ex) {
                selectedParte.getHistorico().remove(movimiento);
                utilities.addFacesMessage(LOGGER, Constants.ERROR, utilities.getExceptionCause(ex).toString());
            }
        }
    }

    public StreamedContent getStream() throws IOException {
        ExternalContext ec = utilities.getExternalContext();
        ec.addResponseHeader("Cache-Control", "no-cache");
        ec.addResponseHeader("Pragma", "no-cache");
        ec.addResponseHeader("Expires", "-1");
        StreamedContent sc = null;
        try {
            if (esPdf) {
                sc = new DefaultStreamedContent(new FileInputStream(pdfFileName), "application/pdf", selectedParte.getNumParteFormateado().replace('/', ' '));
            } else {
                sc = new DefaultStreamedContent(new FileInputStream(pdfFileName), "application/jpeg", selectedParte.getNumParteFormateado().replace('/', ' '));
            }
        } catch (FileNotFoundException ex) {
            System.out.println(ex.getMessage());
        }
        return sc;
    }

    public void resetSelectedOrdenTrabajo() {
        try {
            selectedObjeto = new PinObjeto(PinDAO.getMiembroById(Long.parseLong(utilities.getProperty("/default.properties", "denunciado"))), utilities.getCurrentUser());
            if (selectedObjetoChecked == null) {
                selectedObjetoChecked = new ArrayList<>();
            } else {
                selectedObjetoChecked.clear();
            }
        } catch (IOException ex) {
            utilities.addFacesMessage(LOGGER, Constants.ERROR, utilities.getExceptionCause(ex).toString());
        }
    }

    public void resetSelectedParte() {
        try {
            selectedParte = new PinParteAux(PinDAO.getMiembroById(Long.parseLong(utilities.getProperty("/default.properties", "denunciado"))), utilities.getCurrentUser());
        } catch (IOException ex) {
            utilities.addFacesMessage(LOGGER, Constants.ERROR, utilities.getExceptionCause(ex).toString());
        }
    }

    public void resetSelectedSituacion() {
        selectedSituacion = null;
    }

    public void resetSelectedObjeto() {
        selectedObjeto = null;
    }

    public void addSituacionObjeto() {
        PinObjetoSituacion objSituacion = new PinObjetoSituacion(selectedSituacion, utilities.getCurrentUser().getId().getUsuario());
        selectedObjeto.addToHistoric(objSituacion);
        selectedSituacion = null;
    }

}
