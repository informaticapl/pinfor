package org.svq.pol.pinfor.backing;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.net.URISyntaxException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.inject.Named;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;
import org.apache.log4j.Logger;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.svq.pol.exception.PinforException;
import org.svq.pol.http.HttpCaller;
import org.svq.pol.pinfor.bean.GenMiembro;
import org.svq.pol.pinfor.bean.PinDestino;
import org.svq.pol.pinfor.bean.PinHistoricoImpresion;
import org.svq.pol.pinfor.bean.PinMiembro;
import org.svq.pol.pinfor.bean.PinOrdenServicio;
import org.svq.pol.pinfor.bean.PinOrdenServicioAdjunto;
import org.svq.pol.pinfor.bean.auxiliar.ParteReport;
import org.svq.pol.pinfor.bean.auxiliar.PinOrdenLazyDataModel;
import org.svq.pol.pinfor.bean.auxiliar.PinOrdenServicioAux;
import org.svq.pol.pinfor.bean.auxiliar.PinParteAux;
import org.svq.pol.pinfor.dao.PinDAO;
import org.svq.pol.pinfor.utility.Constants;
import org.svq.pol.pinfor.utility.Log;

/**
 *
 * @author jjcampac
 */
@SessionScoped
@Named
public class PinOrdenServicioBacking extends AbstractBacking implements Serializable {

    private static final Logger LOGGER = Logger.getLogger(PinOrdenServicioBacking.class);
    private static final long serialVersionUID = 2298978741085502190L;

    private PinOrdenServicioAux selectedOrdenServicio;
    private PinOrdenServicioAux patronBusqueda;
    private List<PinOrdenServicioAux> resultadoBusqueda;
    private LazyDataModel<PinOrdenServicioAux> lazyResultadoBusqueda;
    private List<PinOrdenServicioAux> selectedOrdenServicioChecked;

    private PinParteAux selectedParte;

    private Date fechaDesde, fechaHasta;
    private List<GenMiembro> unidades;
    private List<PinMiembro> procedencias;
    private PinOrdenServicioAdjunto selectedAdjunto;
    private String estado;
    private String msgAmbito;
    private String accion;

    private HttpCaller httpCaller;
    private String permantentTicket;

    private Boolean useFileName;
    private UploadedFile uploadedFile;
    private Map<String, File> filesToBeUploaded;
    private Set<String> filesToBeRemoved;
    private File tmpFile;
    private List<PinOrdenServicioAdjunto> filteredAdjuntos;

    private Integer agno;
    private List<Integer> availableYears;
    private String scanInput;

    private String pdfFileName;
    private static String reportsPath;
    private Boolean esPdf;

    private int activeIndex;

    //<editor-fold defaultstate="collapsed" desc="constructors">
    public PinOrdenServicioBacking() {
    }

    @PostConstruct
    protected void onConstruct() {
        this.procedencias = PinDAO.getMiembros("OOT");
        this.selectedOrdenServicio = new PinOrdenServicioAux();
        this.patronBusqueda = new PinOrdenServicioAux();
        this.patronBusqueda.setUnidad(utilities.getCurrentUser().getUnidad());
        this.estado = "vigente";
        this.resultadoBusqueda = new ArrayList<>();
        this.selectedOrdenServicioChecked = new ArrayList<>();
        this.filteredAdjuntos = new ArrayList<>();
        this.activeIndex = -1;

        this.unidades = PinDAO.getUnidades();

        this.agno = Calendar.getInstance(Constants.LOCALE_SPANISH).get(Calendar.YEAR);
        this.availableYears = new ArrayList<>();
        for (int i = 2019; i <= this.agno; i++) {
            this.availableYears.add(i);
        }

        try {
            this.permantentTicket = utilities.getProperty("/default.properties", "ticket");
        } catch (IOException ex) {
            utilities.addFacesMessage(LOGGER, Constants.WARN, ex.getMessage());
        }

        this.useFileName = true;
        this.filesToBeUploaded = new HashMap<>();
        this.filesToBeRemoved = new HashSet<>();
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="getters & setters">
    public Date getHoy() {
        return new Date();
    }

    public boolean isEsPdf() {
        return esPdf;
    }

    public int getActiveIndex() {
        return activeIndex;
    }

    public void setActiveIndex(int activeIndex) {
        this.activeIndex = activeIndex;
    }

    public String getAccion() {
        return accion;
    }

    public PinParteAux getSelectedParte() {
        return selectedParte;
    }

    public void setSelectedParte(PinParteAux selectedParte) {
        this.selectedParte = selectedParte;
    }

    public List<PinOrdenServicioAdjunto> getFilteredAdjuntos() {
        return filteredAdjuntos;
    }

    public void setFilteredAdjuntos(List<PinOrdenServicioAdjunto> filteredAdjuntos) {
        this.filteredAdjuntos = filteredAdjuntos;
    }

    public boolean isUseFileName() {
        return useFileName;
    }

    public void setUseFileName(boolean useFileName) {
        this.useFileName = useFileName;
    }

    public UploadedFile getUploadedFile() {
        return uploadedFile;
    }

    public void setUploadedFile(UploadedFile uploadedFile) {
        this.uploadedFile = uploadedFile;
    }

    public Map<String, File> getFilesToBeUploaded() {
        return filesToBeUploaded;
    }

    public void setFilesToBeUploaded(Map<String, File> filesToBeUploaded) {
        this.filesToBeUploaded = filesToBeUploaded;
    }

    public Set<String> getFilesToBeRemoved() {
        return filesToBeRemoved;
    }

    public void setFilesToBeRemoved(Set<String> filesToBeRemoved) {
        this.filesToBeRemoved = filesToBeRemoved;
    }

    public List<PinMiembro> getProcedencias() {
        return procedencias;
    }

    public Boolean getCanSave() {
        return selectedOrdenServicio.getOrigen() != null && selectedOrdenServicio.getOrigen().getIdMiembro() > -1L
                && selectedOrdenServicio.getReferenciaExterna() != null && !selectedOrdenServicio.getReferenciaExterna().isEmpty()
                && selectedOrdenServicio.getFechaRegistro() != null
                && selectedOrdenServicio.getFechaDesde() != null
                && selectedOrdenServicio.getFechaHasta() != null
                && selectedOrdenServicio.getContenido() != null && !selectedOrdenServicio.getContenido().isEmpty();
    }

    public Boolean getCanEdit() {
        return selectedOrdenServicio.getIdOrdenTrabajo() > 0
                && utilities.getCurrentUser().getModificaciones();
    }

    public Boolean getCanAdd() {
        return utilities.getCurrentUser().getAltas();
    }

    public Boolean getCanDelete() {
        return selectedOrdenServicio.getIdOrdenTrabajo() > 0
                && utilities.getCurrentUser().getBajas()
                && selectedOrdenServicio.getPartes().isEmpty()
                && selectedOrdenServicio.getInformes().isEmpty();
    }

    public Boolean getCanPrint() {
        return selectedParte != null && selectedParte.getIdParte() > 0 && utilities.getCurrentUser().getModificaciones() && !selectedParte.getAnulado();
    }

    public PinOrdenServicioAdjunto getSelectedAdjunto() {
        return selectedAdjunto;
    }

    public void setSelectedAdjunto(PinOrdenServicioAdjunto selectedAdjunto) {
        this.selectedAdjunto = selectedAdjunto;
    }

    public String getPdfFileName() {
        return pdfFileName;
    }

    public void setPdfFileName(String pdfFileName) {
        this.pdfFileName = pdfFileName;
    }

    public static String getReportsPath() {
        return reportsPath;
    }

    public static void setReportsPath(String reportsPath) {
        PinOrdenServicioBacking.reportsPath = reportsPath;
    }

    public Date getFechaDesde() {
        return fechaDesde;
    }

    public void setFechaDesde(Date fechaDesde) {
        this.fechaDesde = fechaDesde;
    }

    public Date getFechaHasta() {
        return fechaHasta;
    }

    public void setFechaHasta(Date fechaHasta) {
        this.fechaHasta = fechaHasta;
    }

    public List<GenMiembro> getUnidades() {
        return unidades;
    }

    public void setUnidades(List<GenMiembro> unidades) {
        this.unidades = unidades;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getMsgAmbito() {
        return msgAmbito;
    }

    public void setMsgAmbito(String msgAmbito) {
        this.msgAmbito = msgAmbito;
    }

    public HttpCaller getHttpCaller() {
        return httpCaller;
    }

    public void setHttpCaller(HttpCaller httpCaller) {
        this.httpCaller = httpCaller;
    }

    public String getPermantentTicket() {
        return permantentTicket;
    }

    public void setPermantentTicket(String permantentTicket) {
        this.permantentTicket = permantentTicket;
    }

    public PinOrdenServicioAux getSelectedOrdenServicio() {
        return selectedOrdenServicio;
    }

    public void setSelectedOrdenServicio(PinOrdenServicioAux selectedOrdenServicio) {
        this.selectedOrdenServicio = selectedOrdenServicio;
    }

    public PinOrdenServicio getPatronBusqueda() {
        return patronBusqueda;
    }

    public void setPatronBusqueda(PinOrdenServicioAux patronBusqueda) {
        this.patronBusqueda = patronBusqueda;
    }

    public List<PinOrdenServicioAux> getResultadoBusqueda() {
        return resultadoBusqueda;
    }

    public void setResultadoBusqueda(List<PinOrdenServicioAux> resultadoBusqueda) {
        this.resultadoBusqueda = resultadoBusqueda;
    }

    public List<PinOrdenServicioAux> getSelectedOrdenServicioChecked() {
        return selectedOrdenServicioChecked;
    }

    public void setSelectedOrdenServicioChecked(List<PinOrdenServicioAux> selectedOrdenServicioChecked) {
        this.selectedOrdenServicioChecked = selectedOrdenServicioChecked;
    }

    public LazyDataModel<PinOrdenServicioAux> getLazyResultadoBusqueda() {
        return lazyResultadoBusqueda;
    }

    public void setLazyResultadoBusqueda(LazyDataModel<PinOrdenServicioAux> lazyResultadoBusqueda) {
        this.lazyResultadoBusqueda = lazyResultadoBusqueda;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="private methods">
    private static void compileJasperFiles() throws IOException, JRException {
        File folder = new File(reportsPath);
        FilenameFilter jasperFilter = (File directory, String fileName) -> directory.equals(folder) && fileName.endsWith(".jrxml");

        File[] xmlFiles = folder.listFiles(jasperFilter);
        for (File file : xmlFiles) {
            String xmlFile = file.getCanonicalPath();
            try {
                Log.log(LOGGER, Constants.INFO, "Procesando el archivo: " + xmlFile);
                String jasperFile = file.getCanonicalPath().replace(".jrxml", ".jasper");
                JasperCompileManager.compileReportToFile(xmlFile, jasperFile);
            } catch (JRException ex) {
                throw new JRException("\nProcesando el archivo: " + xmlFile + "\n\n" + ex.getMessage());
            }
        }

    }

    private void makeReportOrdenTrabajo() throws SQLException, JRException, IOException, URISyntaxException {
        reportsPath = utilities.getExternalContext().getRealPath("/").concat(Constants.REPORTS_PATH);

        File pdfFile = File.createTempFile("ordenServicio", ".pdf");
        pdfFile.deleteOnExit();
        pdfFileName = pdfFile.getAbsolutePath();

        JasperPrint jasperPrint;
        JasperReport jrInforme;

        String jasperFileInforme = reportsPath.concat("orden.jasper");

        HashMap parameters = new HashMap();

        if (!new File(jasperFileInforme).exists()) {
            compileJasperFiles();
        }
        jrInforme = (JasperReport) JRLoader.loadObject(new BufferedInputStream(new FileInputStream(jasperFileInforme)));

        List<PinOrdenServicio> ordenes = new ArrayList<>();
        ordenes.add(selectedOrdenServicio);

        JRBeanCollectionDataSource jrDataSource = new JRBeanCollectionDataSource(ordenes);
        jasperPrint = JasperFillManager.fillReport(jrInforme, parameters, jrDataSource);
        JasperExportManager.exportReportToPdfFile(jasperPrint, pdfFileName);

    }

    private void makeReportParte() throws SQLException, JRException, IOException, URISyntaxException {
        reportsPath = utilities.getExternalContext().getRealPath("/").concat(Constants.REPORTS_PATH);

        File pdfFile = File.createTempFile("informe", ".pdf");
        pdfFile.deleteOnExit();
        pdfFileName = pdfFile.getAbsolutePath();

        JasperPrint jasperPrint;
        JasperReport jrInforme;

        String jasperFileInforme = selectedParte.getTipo().equals("PI") ? reportsPath.concat("parte.jasper") : reportsPath.concat("informe.jasper");

        HashMap parameters = new HashMap();

        parameters.put("UNIDAD", selectedParte.getUnidad() != null ? selectedParte.getUnidad().getNombre() : "Unidad no definida");

        if (!new File(jasperFileInforme).exists()) {
            compileJasperFiles();
        }
        jrInforme = (JasperReport) JRLoader.loadObject(new BufferedInputStream(new FileInputStream(jasperFileInforme)));

        if (jasperFileInforme.contains("parte")) {
            List<ParteReport> partes = new ArrayList<>();
            partes.add(new ParteReport(selectedParte));

            JRBeanCollectionDataSource jrDataSource = new JRBeanCollectionDataSource(partes);
            jasperPrint = JasperFillManager.fillReport(jrInforme, parameters, jrDataSource);
            JasperExportManager.exportReportToPdfFile(jasperPrint, pdfFileName);
        } else if (jasperFileInforme.contains("informe")) {
            List<PinDestino> destinos = new ArrayList<>();
            for (PinDestino destino : selectedParte.getDestinos()) {
                destinos.add(destino);
            }
            JRBeanCollectionDataSource jrDataSource = new JRBeanCollectionDataSource(destinos);
            jasperPrint = JasperFillManager.fillReport(jrInforme, parameters, jrDataSource);
            JasperExportManager.exportReportToPdfFile(jasperPrint, pdfFileName);
        }

    }

    private void comprobarCheck() {
        if (selectedOrdenServicioChecked.size() == 1) {
            selectedOrdenServicio = selectedOrdenServicioChecked.get(0);
        } else {
            selectedOrdenServicio = new PinOrdenServicioAux();
        }
    }

    public Integer getAgno() {
        return agno;
    }

    public void setAgno(Integer agno) {
        this.agno = agno;
    }

    public List<Integer> getAvailableYears() {
        return availableYears;
    }

    public void setAvailableYears(List<Integer> availableYears) {
        this.availableYears = availableYears;
    }

    public String getScanInput() {
        return scanInput;
    }

    public void setScanInput(String scanInput) {
        this.scanInput = scanInput;
    }

    private Set<String> saveUploadedFiles() {
        int pos;
        Set<String> errors = new HashSet<>();
        if (!filesToBeUploaded.isEmpty()) {
            for (Map.Entry<String, File> entry : filesToBeUploaded.entrySet()) {
                String fileName = entry.getKey();
                File file = entry.getValue();
                pos = fileName.lastIndexOf('.');
                for (PinOrdenServicioAdjunto adjunto : this.selectedOrdenServicio.getAdjuntos()) {
                    if (fileName.equals(adjunto.getFileName())) {
                        try {
                            utilities.saveUploadedFile(file, String.valueOf(adjunto.getIdAdjunto()), fileName.substring(pos, fileName.length()), this.selectedOrdenServicio.getFechaRegistro());
                        } catch (IOException ex) {
                            errors.add(fileName);
                        }
                        break;
                    }
                }
            }
        }
        return errors;
    }
    //</editor-fold>

    public void reset() {
        onConstruct();
    }

    public boolean isTramitable() {
        return selectedOrdenServicio.getIdOrdenTrabajo() != 0
                && selectedOrdenServicio.getUnidad().equals(utilities.getCurrentUser().getUnidad())
                && utilities.getCurrentUser().getModificaciones();
    }

    public void selectRow() {
        comprobarCheck();
        this.filteredAdjuntos.clear();
        for (PinOrdenServicioAdjunto docAdjunto : this.selectedOrdenServicio.getAdjuntos()) {
            if (!docAdjunto.getTipo().equals(".jpg") && !docAdjunto.getTipo().equals(".3gp") && !docAdjunto.getTipo().equals(".mp4")) {
                this.filteredAdjuntos.add(docAdjunto);
            }
        }
        if (selectedOrdenServicio.getIdOrdenTrabajo() == 0) {
            this.activeIndex = -1;
            utilities.executeUI("PF('accordion').unselect(0)");
            utilities.executeUI("PF('accordion').unselect(1)");
        } else {
            if (selectedOrdenServicio.getInformes().size() > 0) {
                this.activeIndex = 0;
                utilities.executeUI("PF('accordion').select(0)");
            } else if (selectedOrdenServicio.getPartes().size() > 0) {
                this.activeIndex = 1;
                utilities.executeUI("PF('accordion').select(1)");
            }
        }
    }

    public void resetSearchFields() {
        this.patronBusqueda = new PinOrdenServicioAux();
        this.patronBusqueda.setUnidad(utilities.getCurrentUser().getUnidad());
        this.fechaDesde = null;
        this.fechaHasta = null;
        this.estado = "vigente";
    }

    public void abrir() {
        SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyyHHmmss");
        try {
            if (!scanInput.isEmpty()) {
                String[] parte = scanInput.split(" ");
                agno = Integer.valueOf(parte[0]);
                patronBusqueda.setReferenciaInterna(Integer.valueOf(parte[2]));
                scanInput = Constants.EMPTY_STR;
            }
            fechaDesde = sdf.parse(String.format("0101%d000000", agno));
            fechaHasta = sdf.parse(String.format("3112%d235959", agno));
            search(true);
            if (lazyResultadoBusqueda.getRowCount() != 0) {

                // Como la carga de la tabla no se hace hasta que se genera la vista, es necesario forzar la misma
                // desde aquí para obtener el primer registro, que podamos referenciar como selectedParte
                DataTable lazyTable = (DataTable) utilities.getFacesContext().getViewRoot().findComponent("fMain:tOrdenTrabajo");
                lazyTable.loadLazyData();

                this.selectedOrdenServicio = lazyResultadoBusqueda.getWrappedData().get(0);
                if (this.selectedOrdenServicioChecked != null) {
                    this.selectedOrdenServicioChecked.clear();
                }
                this.selectedOrdenServicioChecked.add(selectedOrdenServicio);
                selectRow();
            } else {
                utilities.addFacesMessage(LOGGER, Constants.INFO, "La orden de servicio solicitada aún no está registrada o se encuentra fuera de su ámbito de búsqueda. <br/><br/>ÁMBITO: " + this.msgAmbito);
            }

            if (utilities.getCurrentUser().getGlobal()) {
                this.msgAmbito = "Jefatura";
            } else if (utilities.getCurrentUser().getRegional()) {
                this.msgAmbito = "Unidad";
            } else {
                this.msgAmbito = "Usuario";
            }
            resetSearchFields();
        } catch (ParseException ex) {
            utilities.addFacesMessage(LOGGER, Constants.ERROR, "Se ha producido un error en la consulta: " + utilities.getExceptionCause(ex));
        }
    }

    public void search(boolean resetSelectedOrden) {
        try {
            if (this.selectedOrdenServicioChecked != null) {
                this.selectedOrdenServicioChecked.clear();
            }
            if (resetSelectedOrden) {
                this.selectedOrdenServicio = new PinOrdenServicioAux();
            }

            lazyResultadoBusqueda = new PinOrdenLazyDataModel.Builder(patronBusqueda)
                    .fechaDesde(fechaDesde)
                    .fechaHasta(fechaHasta)
                    .estado(estado)
                    .currentUser(utilities.getCurrentUser())
                    .build();

            if (utilities.getCurrentUser().getGlobal()) {
                this.msgAmbito = "Jefatura";
            } else if (utilities.getCurrentUser().getRegional()) {
                this.msgAmbito = "Unidad";
            } else {
                this.msgAmbito = "Usuario";
            }
            if (lazyResultadoBusqueda.getRowCount() == 0) {
                utilities.addFacesMessage(LOGGER, Constants.INFO, "No se han encontrado datos coincidentes con la consulta. <br/><br/>ÁMBITO: " + this.msgAmbito);
            }
        } catch (Exception ex) {
            utilities.addFacesMessage(LOGGER, Constants.ERROR, "Se ha producido un error en la consulta: " + ex.getMessage());
        }
    }

    public void generateOrdenTrabajoReport() {
        try {
            makeReportOrdenTrabajo();
            esPdf = pdfFileName.substring(pdfFileName.lastIndexOf('.'), pdfFileName.length()).equals(".pdf");
            utilities.executeUI("PF('dlgVerOrdenTrabajo').show();");
        } catch (SQLException | NullPointerException | JRException | IOException | URISyntaxException ex) {
            utilities.addFacesMessage(LOGGER, Constants.ERROR, utilities.getExceptionCause(ex) == null ? "Se ha producido un error durante la generación de la orden de servicio" : utilities.getExceptionCause(ex).toString());
        }

    }

    public void generateParteReport() {

        if (selectedParte.getAnulado()) {
            utilities.addFacesMessage(LOGGER, Constants.INFO, "El parte está anulado.");
        } else {
            PinHistoricoImpresion movimiento = new PinHistoricoImpresion();
            try {
                // Primero generamos el movimiento para el histórico      
                movimiento.setFecha(new Date());
                movimiento.setUsuario(String.format("%s / %s", utilities.getCurrentUser().getIndicativo(), utilities.getCurrentUser().getId().getUsuario()));
                movimiento.setElemento("Parte");
                selectedParte.addToImpressionHistory(movimiento);

                // Después generamos el informe o el resumen, dependiendo de la seleccion....
                makeReportParte();
                esPdf = pdfFileName.substring(pdfFileName.lastIndexOf('.'), pdfFileName.length()).equals(".pdf");

                utilities.executeUI("PF('dlgVerParte').show();");

                // Y si todo ha ido bien, persistimos el movimiento en la DB
                PinDAO.saveParte(selectedParte);

            } catch (PinforException | SQLException | NullPointerException | JRException | IOException | URISyntaxException ex) {
                selectedParte.getHistorico().remove(movimiento);
                utilities.addFacesMessage(LOGGER, Constants.ERROR, utilities.getExceptionCause(ex).toString());
            }
        }
    }

    public StreamedContent getStream() throws IOException {
        ExternalContext ec = utilities.getExternalContext();
        ec.addResponseHeader("Cache-Control", "no-cache");
        ec.addResponseHeader("Pragma", "no-cache");
        ec.addResponseHeader("Expires", "-1");
        StreamedContent sc = null;
        try {
            if (esPdf) {
                sc = new DefaultStreamedContent(new FileInputStream(pdfFileName), "application/pdf", selectedOrdenServicio.getReferenciaInternaFormateada().replace('/', ' '));
            } else {
                sc = new DefaultStreamedContent(new FileInputStream(pdfFileName), "application/jpeg", selectedOrdenServicio.getReferenciaInternaFormateada().replace('/', ' '));
            }
        } catch (FileNotFoundException ex) {
            System.out.println(ex.getMessage());
        }
        return sc;
    }

    public void resetSelectedOrdenTrabajo() {
        selectedOrdenServicio = new PinOrdenServicioAux(utilities.getCurrentUser().getUnidad(), new Timestamp(Calendar.getInstance(Constants.LOCALE_SPANISH).getTimeInMillis()), utilities.getCurrentUser().getIndicativo());
        if (selectedOrdenServicioChecked == null) {
            selectedOrdenServicioChecked = new ArrayList<>();
        } else {
            selectedOrdenServicioChecked.clear();
        }
        accion = "Nueva";
        //  selectRow();
    }

    public void editar() {
        accion = "Editar";
    }

    public void saveOrdenTrabajo() {
        String error = Constants.EMPTY_STR;
        int pos;
        String fileName, fileType;

        try {
            PinDAO.saveOrdenTrabajo(selectedOrdenServicio);
            for (String name : saveUploadedFiles()) {
                error += name + "\n";

                pos = uploadedFile.getFileName().lastIndexOf('.');
                fileName = name.substring(0, pos);
                fileType = name.substring(pos, name.length());
                this.selectedAdjunto = new PinOrdenServicioAdjunto(fileName, fileType);

                this.selectedOrdenServicio.getAdjuntos().remove(this.selectedAdjunto);
            }

            search(false);
            if (selectedOrdenServicioChecked == null) {
                selectedOrdenServicioChecked = new ArrayList<>();
            } else {
                selectedOrdenServicioChecked.clear();
            }
            selectedOrdenServicioChecked.add(selectedOrdenServicio);
            if (!error.equals(Constants.EMPTY_STR)) {
                utilities.addFacesMessage(LOGGER, Constants.ERROR, "El parte ha sido creado correctamente, pero se ha producido un error al subir los siguientes documentos adjuntos:\n " + error);
            }
        } catch (PinforException ex) {
            utilities.addFacesMessage(LOGGER, Constants.ERROR, "Se ha producido un error al grabar la orden de servicio: " + utilities.getExceptionCause(ex));
        }

    }

    public void deleteOrdenTrabajo() {
        PinDAO.deleteOrdenTrabajo(selectedOrdenServicio);

//        this.selectedOrdenServicio = new PinOrdenServicioAux(utilities.getCurrentUser().getUnidad(), new Timestamp(Calendar.getInstance(Constants.LOCALE_SPANISH).getTimeInMillis()), utilities.getCurrentUser().getIndicativo());
        this.selectedOrdenServicio = null;
        search(true);

    }

    public void resetSelectedAdjunto() {
        this.selectedAdjunto = new PinOrdenServicioAdjunto();
        this.selectedAdjunto.setOrdenTrabajo(this.selectedOrdenServicio);
        utilities.executeUI("PF('dlgAnadirAdjunto').show();");
    }

    public void deleteSelectedAdjunto() {

        // Además de eliminar el documento adjunto de la lista de documentos del parte, necesitamos
        // eliminar el archivo que pudiera estar pendiente de grabar en el disco si acaba de ser subido.
        // Para eso, recorremos el conjunto hasta encontrar uno con igual título que el que tratamos 
        // de eliminar.
        boolean found = false;
        Iterator it = this.filesToBeUploaded.entrySet().iterator();
        Map.Entry<String, File> entry = null;
        while (it.hasNext() && !found) {
            entry = (Map.Entry) it.next();
            if (entry.getKey().equals(String.format("%s%s", this.selectedAdjunto.getTitulo(), this.selectedAdjunto.getTipo()))) {
                found = true;
            }
        }

        // Una vez encontrado, es eliminado.
        if (entry != null) {
            this.filesToBeUploaded.remove(null);
        }

        // También debemos eliminarlo del conjunto de documentos adjuntos del parte 
        // así como el archivo relacionado de su ubicación. Como esta eliminación no se
        // llevará a cabo hasta que se acepten los cambios por parte del usuario, deberemos
        // guardar un conjunto con los archivos removidos para aplicarlos en su caso.
        this.selectedOrdenServicio.getAdjuntos().remove(this.selectedAdjunto);
        if (this.selectedAdjunto.getIdAdjunto() > 0) {
            filesToBeRemoved.add(this.selectedAdjunto.getIdAdjunto() + this.selectedAdjunto.getTipo());
        }
        this.selectedAdjunto = null;
    }

    public void resetSelectedParte() {
        try {
            selectedParte = new PinParteAux(PinDAO.getMiembroById(Long.parseLong(utilities.getProperty("/default.properties", "denunciado"))), utilities.getCurrentUser());
        } catch (IOException ex) {
            utilities.addFacesMessage(LOGGER, Constants.ERROR, utilities.getExceptionCause(ex).toString());
        }
    }

    public void uploadFile(FileUploadEvent event) {
        String fileName, fileType;
        this.uploadedFile = event.getFile();

        List<String> allowedTypes = Arrays.asList(Constants.ALLOWED_TYPES);
        if (uploadedFile != null && uploadedFile.getSize() > 0) {
            int pos = uploadedFile.getFileName().lastIndexOf('.');
            int nameLenght = uploadedFile.getFileName().length();

            if (!allowedTypes.contains(uploadedFile.getFileName().substring(pos + 1, nameLenght))) {
                // Si no es uno de los tipos permitidos, devolvemos un aviso
                utilities.addFacesMessage(LOGGER, Constants.WARN, "El archivo que trata de adjuntar no es uno de los tipos permitidos " + Arrays.toString(Constants.ALLOWED_TYPES));
                uploadedFile = null;
                selectedAdjunto = null;
            } else if (uploadedFile.getSize() > Constants.ALLOWED_SIZE) {
                // Lo mismo hacemos si el tamaño supera el permitido.
                utilities.addFacesMessage(LOGGER, Constants.WARN, "El tamaño del archivo a adjuntar no puede ser mayor a 20MB");
                uploadedFile = null;
                selectedAdjunto = null;
            } else {
                // Si se cumplen las condiciones...

                // Creamos un objeto del tipo Adjunto a partir del nombre y tipo del archivo que se trata de subir....
                fileName = uploadedFile.getFileName().substring(0, pos);
                fileType = uploadedFile.getFileName().substring(pos, uploadedFile.getFileName().length());

                // Y un archivo temporal donde almacenar el fichero que acabamos de subir. Este archivo será eliminado
                // automáticamente por la JVM cuando ésta sea detenida, si es que el usuario no ha guardado los cambios,
                // en cuyo caso, será movido al directorio definitivo.
                try {
                    tmpFile = File.createTempFile(fileName, fileType);
                    tmpFile.deleteOnExit();

                    InputStream iStream = uploadedFile.getInputstream();
                    byte[] buffer = new byte[iStream.available()];
                    iStream.read(buffer);
                    OutputStream oStream = new FileOutputStream(tmpFile);
                    oStream.write(buffer);
                    oStream.close();

                    filesToBeUploaded.put(fileName + fileType, tmpFile);
                    this.selectedAdjunto.setFileName(fileName + fileType);
                    this.selectedAdjunto.setTipo(fileType);
                    if (useFileName) {
                        this.selectedAdjunto.setTitulo(fileName);
                    }
                } catch (IOException ex) {
                    utilities.addFacesMessage(LOGGER, Constants.ERROR, "Se ha producido un error al subir el documento adjunto. Por favor, vuelva a intentarlo.");
                }
            }
        } else {
            utilities.addFacesMessage(LOGGER, Constants.WARN, "No ha seleccionado ningún archivo a adjuntar");
            this.selectedAdjunto = null;
        }
    }

    public void useFileName() {

        if (this.uploadedFile != null && !this.useFileName) {
            if (!this.useFileName) {
                int pos = uploadedFile.getFileName().lastIndexOf('.');
                String fileName = uploadedFile.getFileName().substring(0, pos);
                String fileType = uploadedFile.getFileName().substring(pos, uploadedFile.getFileName().length());
                this.selectedAdjunto.setTitulo(fileName);
                this.selectedAdjunto.setTipo(fileType);
            }
        } else {
            this.selectedAdjunto.setTitulo(Constants.EMPTY_STR);
        }
    }

    public void unselectAdjunto() {
        this.uploadedFile = null;
        for (Map.Entry<String, File> entry : this.filesToBeUploaded.entrySet()) {
            if (entry.getKey().equals(this.selectedAdjunto.getTitulo() + this.selectedAdjunto.getTipo())) {
                entry.getValue().delete();
            }
        }
        this.selectedAdjunto = null;
    }

    public void addAdjunto() {
        PinOrdenServicioAdjunto adjunto = new PinOrdenServicioAdjunto();
        adjunto.setOrdenTrabajo(this.selectedOrdenServicio);
        adjunto.setTitulo("");
        adjunto.setFecha(Calendar.getInstance().getTime());

        this.selectedAdjunto = adjunto;
        // El tipo de adjunto se define a través del tipo del documento subido (pdf, doc, etc)
    }

    public void anadirAdjunto() {
        // Añadimos objeto Adjunto creado al principio a la lista de documentos adjuntos de la orden de servicio.
        if (!selectedOrdenServicio.getAdjuntos().contains(selectedAdjunto)) {
            selectedOrdenServicio.getAdjuntos().add(selectedAdjunto);
            filteredAdjuntos.add(selectedAdjunto);
        } else {
            utilities.addFacesMessage(LOGGER, Constants.WARN, "Ya existe un documento adjunto con el mismo nombre que el que acaba de seleccionar.");
        }
        selectedAdjunto = null;
        uploadedFile = null;
    }

    public void verSelectedAdjunto(boolean seleccionarAdjunto) {
        try {
            // Primero generamos el movimiento para el histórico      

            if (seleccionarAdjunto) {
                if (!selectedOrdenServicio.getAdjuntos().isEmpty()) {
                    selectedAdjunto = selectedOrdenServicio.getAdjuntos().get(0);
                } else {
                    throw new PinforException("La reseña seleccionada no tiene su correspondiente acta de denuncia adjunta.");
                }
            }

            Calendar cal = Calendar.getInstance();
            cal.setTime(selectedAdjunto.getOrdenTrabajo().getFechaRegistro());
            int year = cal.get(Calendar.YEAR);
            File path = new File(utilities.getProperty("/default.properties", "filesPath") + "/" + year);

            pdfFileName = String.format("%s\\%s%s", path, selectedAdjunto.getIdAdjunto(), selectedAdjunto.getTipo());
            File pdfFile = new File(pdfFileName);
            esPdf = pdfFileName.substring(pdfFileName.lastIndexOf('.'), pdfFileName.length()).equals(".pdf");

            if (!pdfFile.exists()) {
                throw new PinforException("No se ha podido localizar el archivo correspondiente al acta solicitada.");
            }
            // Y si todo ha ido bien, persistimos el movimiento en la DB
            PinDAO.saveOrdenTrabajo(selectedOrdenServicio);
            if (esPdf) {
                utilities.executeUI("PF('dlgVerParte').show();");
            } else {
                utilities.executeUI("PF('dlgVerImagen').show();");
            }

        } catch (PinforException ex) {
            utilities.addFacesMessage(LOGGER, Constants.INFO, ex.getMessage());
        } catch (IOException ex) {
            utilities.addFacesMessage(LOGGER, Constants.ERROR, ex.getMessage());
        }
    }

}
