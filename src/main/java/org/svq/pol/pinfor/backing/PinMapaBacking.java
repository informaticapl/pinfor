package org.svq.pol.pinfor.backing;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.Serializable;
import java.net.URISyntaxException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import javax.faces.context.ExternalContext;
import javax.inject.Named;
import javax.persistence.NonUniqueResultException;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;
import org.apache.log4j.Logger;
import org.omnifaces.cdi.ViewScoped;
import org.primefaces.event.map.OverlaySelectEvent;
import org.primefaces.event.map.PointSelectEvent;
import org.primefaces.event.map.StateChangeEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.map.Circle;
import org.primefaces.model.map.DefaultMapModel;
import org.primefaces.model.map.LatLng;
import org.primefaces.model.map.MapModel;
import org.svq.pol.exception.PinforException;
import org.svq.pol.pinfor.bean.PinDestino;
import org.svq.pol.pinfor.bean.PinEstado;
import org.svq.pol.pinfor.bean.PinLugar;
import org.svq.pol.pinfor.bean.auxiliar.ParteReport;
import org.svq.pol.pinfor.bean.auxiliar.PinLugarAux;
import org.svq.pol.pinfor.bean.auxiliar.PinParteAux;
import org.svq.pol.pinfor.dao.PinDAO;
import org.svq.pol.pinfor.utility.Constants;
import org.svq.pol.pinfor.utility.MyMarker;

/**
 *
 * @author jjcampac
 */
@Named
@ViewScoped
public class PinMapaBacking extends AbstractBacking implements Serializable {

    private static final long serialVersionUID = 9074128288568298837L;
    private static final Logger LOGGER = Logger.getLogger(PinMapaBacking.class);

    private static String reportsPath;
    private String pdfFileName;

    private PinParteAux selectedParte;
    private double selectedLat, selectedLng;
    private Integer parteNum, agnoParte, agnoInicio, agnoFin;
    private Integer tipoBusqueda;
    private String origen;
    private Integer distanciaBusqueda, numVecinos;
    private List<Integer> years;
    List<PinLugarAux> searchResult;
    private List<LatLng> heatData;
    private Boolean showPartes, showInformes, showOrdenes;

    private MapModel mapModel;
    private Integer zoom;
    private Boolean verSelectorCapas;
    private String center;
    private static MyMarker marker;
    private String[] parteInfo;
    private final SimpleDateFormat sdf;

//<editor-fold defaultstate="collapsed" desc="constructors">
    public PinMapaBacking() {
        this.selectedParte = null;
        this.selectedLat = 0.0;
        this.tipoBusqueda = Constants.BUSQUEDA_DISTANCIA;
        this.distanciaBusqueda = 150;
        this.numVecinos = 20;
        this.zoom = 13;
        this.agnoParte = Calendar.getInstance(Constants.LOCALE_SPANISH).get(Calendar.YEAR);
        this.parteInfo = new String[6];
        sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
        years = new ArrayList<>();
        for (int i = 2019; i < agnoParte + 1; i++) {
            years.add(i);
        }
        this.agnoFin = Calendar.getInstance(Constants.LOCALE_SPANISH).get(Calendar.YEAR);
        this.agnoInicio = this.agnoFin - 2;
        mapModel = new DefaultMapModel();
        this.center = Constants.DEFAULT_LAT.concat(",").concat(Constants.DEFAULT_LNG);
        searchResult = new ArrayList<>();
        verSelectorCapas = false;
        showPartes = true;
        showInformes = true;
        showOrdenes = true;
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="getters & setters">
    public void setVerSelectorCapas(Boolean verSelectorCapas) {
        this.verSelectorCapas = verSelectorCapas;
    }

    public String getPdfFileName() {
        return pdfFileName;
    }

    public Boolean getVerSelectorCapas() {
        return verSelectorCapas;
    }

    public Boolean getShowPartes() {
        return showPartes;
    }

    public void setShowPartes(Boolean showPartes) {
        this.showPartes = showPartes;
    }

    public Boolean getShowInformes() {
        return showInformes;
    }

    public void setShowInformes(Boolean showInformes) {
        this.showInformes = showInformes;
    }

    public Boolean getShowOrdenes() {
        return showOrdenes;
    }

    public void setShowOrdenes(Boolean showOrdenes) {
        this.showOrdenes = showOrdenes;
    }

    public double getSelectedLat() {
        return selectedLat;
    }

    public void setSelectedLat(double selectedLat) {
        this.selectedLat = selectedLat;
    }

    public double getSelectedLng() {
        return selectedLng;
    }

    public void setSelectedLng(double selectedLng) {
        this.selectedLng = selectedLng;
    }

    public PinParteAux getSelectedParte() {
        return selectedParte;
    }

    public void setSelectedParte(PinParteAux selectedParte) {
        this.selectedParte = selectedParte;
    }

    public Integer getParteNum() {
        return parteNum;
    }

    public void setParteNum(Integer parteNum) {
        this.parteNum = parteNum;
    }

    public Integer getAgnoParte() {
        return agnoParte;
    }

    public void setAgnoParte(Integer agnoParte) {
        this.agnoParte = agnoParte;
    }

    public Integer getAgnoInicio() {
        return agnoInicio;
    }

    public void setAgnoInicio(Integer agnoInicio) {
        this.agnoInicio = agnoInicio;
    }

    public Integer getAgnoFin() {
        return agnoFin;
    }

    public void setAgnoFin(Integer agnoFin) {
        this.agnoFin = agnoFin;
    }

    public Integer getTipoBusqueda() {
        return tipoBusqueda;
    }

    public void setTipoBusqueda(Integer tipoBusqueda) {
        this.tipoBusqueda = tipoBusqueda;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public Integer getNumVecinos() {
        return numVecinos;
    }

    public void setNumVecinos(Integer numVecinos) {
        this.numVecinos = numVecinos;
    }

    public Integer getDistanciaBusqueda() {
        return distanciaBusqueda;
    }

    public void setDistanciaBusqueda(Integer distanciaBusqueda) {
        this.distanciaBusqueda = distanciaBusqueda;
    }

    public List<Integer> getYears() {
        return years;
    }

    public void setYears(List<Integer> years) {
        this.years = years;
    }

    public MapModel getMapModel() {
        return mapModel;
    }

    public void setMapModel(MapModel mapModel) {
        this.mapModel = mapModel;
    }

    public String getCenter() {
        return center;
    }

    public void setCenter(String center) {
        this.center = center;
    }

    public MyMarker getMarker() {
        return marker;
    }

    public void setMarker(MyMarker marker) {
        this.marker = marker;
    }

    public Integer getZoom() {
        return zoom;
    }

    public void setZoom(Integer zoom) {
        this.zoom = zoom;
    }

    public String[] getParteInfo() {
        return parteInfo;
    }

    public void setParteInfo(String[] parteInfo) {
        this.parteInfo = parteInfo;
    }

    public List<PinLugarAux> getSearchResult() {
        return searchResult;
    }

    public void setSearchResult(List<PinLugarAux> searchResult) {
        this.searchResult = searchResult;
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="private methods">
    private int addMarkers() {
        Iterator it = searchResult.iterator();
        int count = 0;
        while (it.hasNext()) {
            Object[] lugarAux = (Object[]) it.next();
            PinLugar lugar = (PinLugar) lugarAux[0];
            Float distancia = (Float) lugarAux[1];
            LatLng latLng = new LatLng(lugar.getLat(), lugar.getLng());
            PinParteAux parteTmp = (PinParteAux) lugar.getParte();
            this.showPartes = true;
            this.showInformes = true;
            this.showOrdenes = true;

            if ((this.origen.equals(Constants.ORIGEN_PUNTO) && (latLng.getLat() != selectedLat && latLng.getLng() != selectedLng))
                    || ((this.origen.equals(Constants.ORIGEN_PARTE) || this.origen.equals(Constants.ORIGEN_INFORME) || this.origen.equals(Constants.ORIGEN_ORDENANZA)) && (!Objects.equals(parteTmp.getIdParte(), selectedParte.getIdParte())))) {
                marker = new MyMarker(latLng);
                count++;
                try {
                    marker.setData(String.format("%s: %s@Fecha: %s@Lugar: %s@Asunto: %s@Estado: %s@Distancia: %.2f metros",
                            parteTmp.getTipoFormateado(), parteTmp.getNumParteFormateado(),
                            sdf.format(parteTmp.getFechaSuceso()),
                            parteTmp.getLugar().getNombreLugar(),
                            parteTmp.getAsunto().getNombre() + " - " + parteTmp.getSubasunto().getNombre(),
                            parteTmp.getEstadoReal().getEstado().getNombre(),
                            distancia)
                    );

                } catch (NullPointerException ignore) {
                    // Ignoramos el error que pudiera lanzar un parteNum que no tuviera algun dato completo
                }

                /* Una vez construido el marcador, comprobamos si ya existe uno igual (la igualdad viene determinada
                por las coordenadas lat y lng. Para poder determinar ésta, el marcador es de la clase MyMarker, que extiende
                a Marker. Si existiera, se añade un offset de 10 metros a las coordenadas del mismo. Las nuevas sustituyen
                a las antiguas y se vuelve a comprobar que tampoco exista ningún marcador con estas nuevas coordenadas (puede
                darse el caso de más de dos lugares con idénticas coordenadas). Miembras se cumpla esta condición, se están
                añadiendo offset a las últimas coordenadas obtenidas. */
                if (mapModel.getMarkers().contains(marker)) {
                    do {
                        LatLng newLatLng = utilities.addMeters(marker.getLatlng(), 10);
                        marker.setLatlng(newLatLng);
                    } while (mapModel.getMarkers().contains(marker));
                }
                switch (parteTmp.getTipo()) {
                    case Constants.ORIGEN_PARTE:
                        marker.setIcon(utilities.getImagesResourcePath() + "marker_parte.png");
                        break;
                    case Constants.ORIGEN_INFORME:
                        marker.setIcon(utilities.getImagesResourcePath() + "marker_informe.png");
                        break;
                    case Constants.ORIGEN_ORDENANZA:
                        marker.setIcon(utilities.getImagesResourcePath() + "marker_ordenanza.png");
                        break;
                }
                mapModel.addOverlay(marker);
            }
        }
        return count;
    }

    private void makeReport(List<ParteReport> data) throws SQLException, JRException, IOException, URISyntaxException, PinforException {
        reportsPath = utilities.getExternalContext().getRealPath("/").concat(Constants.REPORTS_PATH);
        Calendar calIni = Calendar.getInstance(Constants.LOCALE_SPANISH);
        calIni.set(Calendar.DAY_OF_MONTH, 1);
        calIni.set(Calendar.MONTH, 0);
        calIni.set(Calendar.YEAR, this.agnoInicio);
        Calendar calFin = Calendar.getInstance(Constants.LOCALE_SPANISH);
        calFin.set(Calendar.DAY_OF_MONTH, 31);
        calFin.set(Calendar.MONTH, 11);
        calFin.set(Calendar.YEAR, this.agnoFin);

        File pdfFile = File.createTempFile(this.tipoBusqueda == Constants.BUSQUEDA_DISTANCIA ? "Búsqueda por distancia" : "Búsqueda de vecinos cercanos", ".pdf");
        pdfFile.deleteOnExit();
        pdfFileName = pdfFile.getAbsolutePath();

        JasperPrint jasperPrint;
        JasperReport jrAtestado;
        JRBeanCollectionDataSource jrDataSource;

        String jasperFileAtestado = null;
        HashMap parameters = new HashMap();
        parameters.put("FECHA_DESDE", calIni.getTime());
        parameters.put("FECHA_HASTA", calFin.getTime());
        parameters.put("TIPO", this.tipoBusqueda == Constants.BUSQUEDA_DISTANCIA ? "Búsqueda por distancia" : "Búsqueda de vecinos cercanos");
        String org = null;
        switch (this.origen) {
            case Constants.ORIGEN_PARTE:
                org = "Origen Parte Informativo";
                break;
            case Constants.ORIGEN_INFORME:
                org = "Origen Informe";
                break;
            case Constants.ORIGEN_ORDENANZA:
                org = "Origen Ordenanza Municipal";
                break;
            case Constants.ORIGEN_PUNTO:
                org = "Origen Punto sobre el mapa";
                break;
        }
        parameters.put("SUBTIPO", org);
        parameters.put("UNIDAD", utilities.getCurrentUser().getUnidad());

        jasperFileAtestado = reportsPath.concat("listadoAsuntos.jasper");
        jrDataSource = new JRBeanCollectionDataSource((List<ParteReport>) data);

        if (jasperFileAtestado != null) {
            if (!new File(jasperFileAtestado).exists()) {
                compileJasperFiles();
            }
            jrAtestado = (JasperReport) JRLoader.loadObject(new BufferedInputStream(new FileInputStream(jasperFileAtestado)));
            jasperPrint = JasperFillManager.fillReport(jrAtestado, parameters, jrDataSource);
            JasperExportManager.exportReportToPdfFile(jasperPrint, pdfFileName);

            utilities.executeUI("PF('dlgVerParte').show();");
        } else {
            throw new PinforException("Se ha producido un error durante la generación del listado.");
        }

    }

    private static void compileJasperFiles() throws IOException, JRException {
        File folder = new File(reportsPath);
        FilenameFilter jasperFilter = (File directory, String fileName) -> fileName.endsWith(".jrxml");

        File[] xmlFiles = folder.listFiles(jasperFilter);
        for (File file : xmlFiles) {
            String xmlFile = file.getCanonicalPath();
            String jasperFile = file.getCanonicalPath().replace(".jrxml", ".jasper");
            JasperCompileManager.compileReportToFile(xmlFile, jasperFile);
        }
    }

    private List<ParteReport> getPartesFromSearchResult() {
        List<ParteReport> result = new ArrayList<>();

        Iterator it = searchResult.iterator();
        while (it.hasNext()) {
            Object[] lugarAux = (Object[]) it.next();
            PinLugar lugar = (PinLugar) lugarAux[0];
            result.add(new ParteReport(lugar.getParte()));
        }
        return result;
    }
//</editor-fold>

    public void seekData() {
        this.verSelectorCapas = false;
        searchResult.clear();
        mapModel.getMarkers().removeAll(mapModel.getMarkers());
        mapModel.getCircles().removeAll(mapModel.getCircles());

        try {
            switch (tipoBusqueda) {
                case Constants.BUSQUEDA_DISTANCIA:
                    if (origen.equals(Constants.ORIGEN_PUNTO)) {
                        searchResult = PinDAO.getPartesEnRadio(distanciaBusqueda, selectedLat, selectedLng, this.agnoInicio, this.agnoFin);
                    } else {
                        selectedParte = PinDAO.getParte(parteNum, agnoParte, this.origen);
                        if (selectedParte != null && selectedParte.getLugar().getLat() != 0.0) {
                            searchResult = PinDAO.getPartesEnRadio(distanciaBusqueda, selectedParte.getLugar().getLat(), selectedParte.getLugar().getLng(), this.agnoInicio, this.agnoFin);
                        } else {
                            throw new PinforException();
                        }
                    }
                    break;
                case Constants.BUSQUEDA_VECINOS:
                    if (origen.equals(Constants.ORIGEN_PUNTO)) {
                        searchResult = PinDAO.getPartesVecinos(numVecinos, selectedLat, selectedLng, this.agnoInicio, this.agnoFin);
                    } else {
                        selectedParte = PinDAO.getParte(parteNum, agnoParte, this.origen);
                        if (selectedParte != null) {
                            searchResult = PinDAO.getPartesVecinos(numVecinos, selectedParte.getLugar().getLat(), selectedParte.getLugar().getLng(), this.agnoInicio, this.agnoFin);
                        } else {
                            throw new PinforException();
                        }
                    }
                    break;
                default:
            }
            LatLng latLng;
            Circle circle;
            switch (this.origen) {
                case Constants.ORIGEN_PARTE:
                case Constants.ORIGEN_INFORME:
                case Constants.ORIGEN_ORDENANZA:
                    latLng = new LatLng(selectedParte.getLugar().getLat(), selectedParte.getLugar().getLng());
                    marker = new MyMarker(latLng);
                    marker.setData(String.format("%s: %s@Fecha: %s@Lugar: %s@Asunto: %s@Estado: %s",
                            selectedParte.getTipoFormateado(),
                            selectedParte.getNumParteFormateado(),
                            sdf.format(selectedParte.getFechaSuceso()),
                            selectedParte.getLugar().getNombreLugar(),
                            selectedParte.getAsunto().getNombre() + " - " + selectedParte.getSubasunto().getNombre(),
                            selectedParte.getEstadoReal().getEstado().getNombre()));

                    if (selectedParte.getTipo().equals("PI")) {
                        marker.setIcon(utilities.getImagesResourcePath() + "marker_selected_pi.png");
                    } else if (selectedParte.getTipo().equals("IF")) {
                        marker.setIcon(utilities.getImagesResourcePath() + "marker_selected_if.png");
                    } else if (selectedParte.getTipo().equals("OM")) {
                        marker.setIcon(utilities.getImagesResourcePath() + "marker_selected_om.png");
                    }

                    center = String.format(Locale.ENGLISH, "%f,%f", latLng.getLat(), latLng.getLng());
                    mapModel.addOverlay(marker);

                    if (tipoBusqueda == Constants.BUSQUEDA_DISTANCIA) {
                        circle = new Circle(latLng, distanciaBusqueda);
                        circle.setStrokeColor("#00ff00");
                        circle.setFillColor("#00ff00");
                        circle.setStrokeOpacity(0.2);
                        circle.setFillOpacity(0.2);
                        mapModel.addOverlay(circle);
                        if (distanciaBusqueda >= 0 && distanciaBusqueda < 50) {
                            zoom = 21;
                        } else if (distanciaBusqueda >= 50 && distanciaBusqueda < 70) {
                            zoom = 20;
                        } else if (distanciaBusqueda >= 70 && distanciaBusqueda < 110) {
                            zoom = 19;
                        } else if (distanciaBusqueda >= 110 && distanciaBusqueda < 215) {
                            zoom = 18;
                        } else if (distanciaBusqueda >= 215 && distanciaBusqueda < 380) {
                            zoom = 17;
                        } else if (distanciaBusqueda >= 380 && distanciaBusqueda < 900) {
                            zoom = 16;
                        } else if (distanciaBusqueda >= 900 && distanciaBusqueda < 1300) {
                            zoom = 15;
                        } else {
                            zoom = 14;
                        }
                    }

                    break;
                case Constants.ORIGEN_PUNTO:
                    latLng = new LatLng(selectedLat, selectedLng);
                    marker = new MyMarker(latLng);
                    marker.setData("Punto de referencia");

                    center = String.format(Locale.ENGLISH, "%f,%f", latLng.getLat(), latLng.getLng());
                    mapModel.addOverlay(marker);

                    if (tipoBusqueda == Constants.BUSQUEDA_DISTANCIA) {
                        circle = new Circle(latLng, distanciaBusqueda);
                        circle.setStrokeColor("#00ff00");
                        circle.setFillColor("#00ff00");
                        circle.setStrokeOpacity(0.2);
                        circle.setFillOpacity(0.2);
                        mapModel.addOverlay(circle);
                        if (distanciaBusqueda >= 0 && distanciaBusqueda < 50) {
                            zoom = 21;
                        } else if (distanciaBusqueda >= 50 && distanciaBusqueda < 70) {
                            zoom = 20;
                        } else if (distanciaBusqueda >= 70 && distanciaBusqueda < 110) {
                            zoom = 19;
                        } else if (distanciaBusqueda >= 110 && distanciaBusqueda < 215) {
                            zoom = 18;
                        } else if (distanciaBusqueda >= 215 && distanciaBusqueda < 380) {
                            zoom = 17;
                        } else if (distanciaBusqueda >= 380 && distanciaBusqueda < 900) {
                            zoom = 16;
                        } else if (distanciaBusqueda >= 900 && distanciaBusqueda < 1300) {
                            zoom = 15;
                        } else {
                            zoom = 14;
                        }
                    }
                    break;
                default:
            }

            if (!searchResult.isEmpty()) {
                int total = addMarkers();
                if (total > 0) {
                    utilities.executeUI("PF('dlgSeleccionCapas').show();");
                    this.verSelectorCapas = true;
                    utilities.addFacesMessage(LOGGER, Constants.INFO, String.format("Se han encontrado %d documentos.", total));
                } else {
                    utilities.addFacesMessage(LOGGER, Constants.INFO, "No se ha encontrado ningún documento aparte del refenciado que coincida con los términos de la búsqueda.");
                }
            } else {
                utilities.addFacesMessage(LOGGER, Constants.INFO, "No se ha encontrado ningún documento aparte del refenciado que coincida con los términos de la búsqueda.");
            }
        } catch (PinforException ex) {
            utilities.addFacesMessage(LOGGER, Constants.INFO, "El Parte o Informe indicado no existe o los datos referentes al lugar no son válidos.");
        } catch (NonUniqueResultException ex) {
            utilities.addFacesMessage(LOGGER, Constants.WARN, ex.getMessage());
        }
    }

    public void printReport() {
        try {
            List<ParteReport> partes = getPartesFromSearchResult();
            makeReport(partes);
        } catch (SQLException | JRException | URISyntaxException | IOException ex) {
            utilities.addFacesMessage(LOGGER, Constants.ERROR, utilities.getExceptionCause(ex).toString());
        } catch (PinforException ex) {
            utilities.addFacesMessage(LOGGER, Constants.ERROR, ex.getMessage());
        }
    }

    public StreamedContent getStream() throws IOException {
        ExternalContext ec = utilities.getExternalContext();
        String titulo = (this.tipoBusqueda == Constants.BUSQUEDA_DISTANCIA ? "Búsqueda por distancia" : "Búsqueda de vecinos cercanos").concat(".pdf");
        ec.addResponseHeader("Cache-Control", "no-cache");
        ec.addResponseHeader("Pragma", "no-cache");
        ec.addResponseHeader("Expires", "-1");
        ec.addResponseHeader("Title", titulo);
        StreamedContent sc = null;
        try {
            sc = new DefaultStreamedContent(new FileInputStream(pdfFileName), "application/pdf", "Listado ".concat(titulo));
        } catch (FileNotFoundException ex) {
            System.out.println(ex.getMessage());
        }
        return sc;
    }

    public void resetMap() {
        this.parteNum = 0;
        this.agnoParte = Calendar.getInstance(Constants.LOCALE_SPANISH).get(Calendar.YEAR);
        mapModel.getMarkers().removeAll(mapModel.getMarkers());
        mapModel.getCircles().removeAll(mapModel.getCircles());
        this.zoom = 13;
        this.center = Constants.DEFAULT_LAT.concat(",").concat(Constants.DEFAULT_LNG);
    }

    public void onMarkerSelect(OverlaySelectEvent event) {
        if (event.getOverlay() instanceof MyMarker) {
            marker = (MyMarker) event.getOverlay();

            parteInfo = ((String) marker.getData()).split("@");
        }
    }

    public void onStateChange(StateChangeEvent event) {
        // Tras la actualización del mapa (por inserción de un marker u otro evento), se
        // asigna a la variable zoom (que es la que gmap lee para aplicar el zoom) el 
        // valor del último zoom efectuado por el usuario.
        zoom = event.getZoomLevel();
    }

    public void onPointSelect(PointSelectEvent event) {
        if (origen.equals(Constants.ORIGEN_PUNTO)) {
            mapModel.getMarkers().removeAll(mapModel.getMarkers());

            LatLng coord = event.getLatLng();
            selectedLat = coord.getLat();
            selectedLng = coord.getLng();
            marker = new MyMarker(coord);
            marker.setDraggable(true);
            marker.setIcon(utilities.getImagesResourcePath() + "location-marker.png");
            mapModel.addOverlay(marker);
            // center = String.format(Locale.ENGLISH, "%f,%f", selectedLat, selectedLng);
        }
    }

    public void onSelectorCapasChange() {
        if (verSelectorCapas) {
            utilities.executeUI("PF('dlgSeleccionCapas').show();");
        } else {
            utilities.executeUI("PF('dlgSeleccionCapas').hide;");
        }

    }

    public void modificarVisibilidadCapa(int capa) {
        switch (capa) {
            case 1:
                mapModel.getMarkers().stream()
                        .filter(m -> m.getIcon() != null && m.getIcon().endsWith("parte.png"))
                        .forEach((m) -> {
                            m.setVisible(showPartes);
                        });
                break;

            case 2:
                mapModel.getMarkers().stream()
                        .filter(m -> m.getIcon() != null && m.getIcon().endsWith("informe.png"))
                        .forEach((m) -> {
                            m.setVisible(showInformes);
                        });
                break;
            case 3:
                mapModel.getMarkers().stream()
                        .filter(m -> m.getIcon() != null && m.getIcon().endsWith("ordenanza.png"))
                        .forEach((m) -> {
                            m.setVisible(showOrdenes);
                        });
                break;
        }
    }

    public LatLng[] getHeatData() {
        heatData = new ArrayList<>();
        heatData.add(new LatLng(37.782, -122.447));
        heatData.add(new LatLng(37.782, -122.445));
        heatData.add(new LatLng(37.782, -122.443));
        heatData.add(new LatLng(37.782, -122.441));
        heatData.add(new LatLng(37.782, -122.439));
        heatData.add(new LatLng(37.782, -122.437));
        heatData.add(new LatLng(37.782, -122.435));

        return heatData.toArray(new LatLng[heatData.size()]);
    }

    public void doNothing() {
        System.out.println("Llegué!!");
    }
}
