package org.svq.pol.pinfor.backing;

import com.google.gson.Gson;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.parser.PdfTextExtractor;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.net.URISyntaxException;
import java.security.GeneralSecurityException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.inject.Inject;
import javax.inject.Named;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;
import org.apache.log4j.Logger;
import org.omnifaces.cdi.Push;
import org.omnifaces.cdi.PushContext;
import org.omnifaces.cdi.ViewScoped;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.map.OverlaySelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.primefaces.model.map.DefaultMapModel;
import org.primefaces.model.map.LatLng;
import org.primefaces.model.map.MapModel;
import org.primefaces.model.map.Marker;
import org.svq.pol.exception.PinforException;
import org.svq.pol.http.HttpCaller;
import org.svq.pol.pinfor.bean.GenMiembro;
import org.svq.pol.pinfor.bean.PinActuante;
import org.svq.pol.pinfor.bean.PinDestino;
import org.svq.pol.pinfor.bean.PinHistoricoImpresion;
import org.svq.pol.pinfor.bean.PinMiembro;
import org.svq.pol.pinfor.bean.PinObjeto;
import org.svq.pol.pinfor.bean.PinObjetoSituacion;
import org.svq.pol.pinfor.bean.PinOrdenServicio;
import org.svq.pol.pinfor.bean.PinParte;
import org.svq.pol.pinfor.bean.PinParteAdjunto;
import org.svq.pol.pinfor.bean.auxiliar.ParteReport;
import org.svq.pol.pinfor.bean.auxiliar.PinOrdenServicioAux;
import org.svq.pol.pinfor.bean.auxiliar.PinParteAux;
import org.svq.pol.pinfor.bean.auxiliar.PinParteLazyDataModel;
import org.svq.pol.pinfor.dao.PinDAO;
import org.svq.pol.pinfor.utility.Constants;
import org.svq.pol.pinfor.utility.Log;
import org.svq.pol.pinfor.utility.Message;
import org.svq.pol.pinfor.utility.MyMarker;

/**
 *
 * @author jjcampac
 */
@ViewScoped
@Named
public class PinOficinaBacking extends AbstractBacking implements Serializable {

    private static final Logger LOGGER = Logger.getLogger(PinOficinaBacking.class);
    private static final long serialVersionUID = 2298978741085502190L;

    @Inject
    @Push
    private PushContext notificationChannel;

    private PinParteAux selectedParte;
    private PinParteAux patronBusqueda;
    private List<PinParteAux> resultadoBusqueda;
    private LazyDataModel<PinParteAux> lazyResultadoBusqueda;
    private List<PinParteAux> selectedParteChecked;
    private List<PinParteAux> partesNovedades;
    private List<PinOrdenServicioAux> ordenesServicio;
    private boolean showTodasOrdenes;
    Set<Boolean> vigencia;
    private Boolean permitirGrabar, puedeEnviarPortafirmas;

    private PinActuante selectedActuante, actuante;
    private PinDestino selectedDestinatario;
    private PinMiembro selectedDestino;
    private GenMiembro selectedDestinatarioDisponible;
    private PinDestino selectedDestinatarioAsignado;
    private List<GenMiembro> destJefatura;
    private List<GenMiembro> destAyuntamiento;
    private List<GenMiembro> destExterno;
    private List<GenMiembro> destinosDisponibles;
    private String origenDestinoDisponible;
    private PinObjeto selectedObjeto;
    private PinObjetoSituacion selectedObjetoSituacion;
    private PinMiembro selectedSituacion;
    private Date fechaDesde, fechaHasta, fechaImpresion;
    private List<GenMiembro> unidades;
    private List<PinMiembro> asuntos;
    private List<PinMiembro> destinos;
    private List<PinMiembro> estados;
    private List<PinMiembro> situaciones;
    private PinParteAdjunto selectedAdjunto;
    private String accion;
    private Integer estado;
    private String dni;
    private String persona;
    private Boolean puedeTramitar;
    private Boolean incluidoNovedades;
    private String msgAmbito;
    private StreamedContent zipFile;
    private List<PinParteAdjunto> filteredAdjuntos;
    private Map<String, File> filesToBeUploaded;
    private Set<String> filesToBeRemoved;
    private UploadedFile uploadedFile;
    private Boolean useFileName;
    private File tmpFile;
    private Boolean archivado;

    private List<PinDestino> copiaDestinatariosAsignados;

    private String center;
    private double selectedLat, selectedLng;
    private MapModel mapModel;
    private static MyMarker marker;
    private int zoom;

    private int idx;
    private String scanInput;

    private Integer agno;
    private List<Integer> availableYears;
    private String aviso;

    private HttpCaller httpCaller;
    private String permantentTicket;

    private String pdfFileName;
    private static String reportsPath;
    private Boolean esPdf;

    //<editor-fold defaultstate="collapsed" desc="constructors">
    public PinOficinaBacking() {

    }

    @PostConstruct
    protected void onConstruct() {
        try {
            vigencia = new HashSet<>();
            vigencia.add(PinOrdenServicio.VIGENTE);

            this.asuntos = PinDAO.getMiembros("ASU");
            this.unidades = PinDAO.getUnidades();
            this.destinos = PinDAO.getMiembros("DES");
            this.estados = PinDAO.getMiembros("EST");
            this.estados = this.estados.stream().sorted(Comparator.comparing(PinMiembro::getNombre)).collect(Collectors.toList());
            this.situaciones = PinDAO.getMiembros("SOB");
            destJefatura = PinDAO.getMiembrosGenericos("DJF");
            destAyuntamiento = PinDAO.getMiembrosGenericos("DAY");
            destExterno = PinDAO.getMiembrosGenericos("DEX");
            this.ordenesServicio = PinDAO.getOrdenesTrabajo(vigencia, null, utilities.getCurrentUser().getUnidad());
            this.selectedParte = new PinParteAux(PinDAO.getMiembroById(Long.parseLong(utilities.getProperty("/default.properties", "denunciado"))), utilities.getCurrentUser());
            this.patronBusqueda = new PinParteAux(PinDAO.getMiembroById(Long.parseLong(utilities.getProperty("/default.properties", "denunciado"))), utilities.getCurrentUser());
            this.patronBusqueda.setUnidad(utilities.getCurrentUser().getUnidad());
            this.estado = PinParte.EST_LISTO_TRAMITAR;
            this.accion = "";
            this.resultadoBusqueda = new ArrayList<>();
            this.selectedParteChecked = new ArrayList<>();
            this.partesNovedades = new ArrayList<>();
            this.incluidoNovedades = false;
            this.selectedAdjunto = null;
            this.useFileName = true;
            this.showTodasOrdenes = false;
            this.permitirGrabar = false;
            this.puedeEnviarPortafirmas = false;
            selectedDestinatarioDisponible = null;
            selectedDestinatarioAsignado = null;
            this.origenDestinoDisponible = Constants.DESTINO_JEFATURA;

            this.filteredAdjuntos = new ArrayList<>();
            this.filesToBeUploaded = new HashMap<>();
            this.filesToBeRemoved = new HashSet<>();

            this.agno = Calendar.getInstance(Constants.LOCALE_SPANISH).get(Calendar.YEAR);
            this.availableYears = new ArrayList<>();
            for (int i = 2019; i <= this.agno; i++) {
                this.availableYears.add(i);
            }

            this.mapModel = new DefaultMapModel();
            this.center = Constants.DEFAULT_LAT.concat(",").concat(Constants.DEFAULT_LNG);
            this.zoom = 13;

            this.actuante = new PinActuante();
            this.dni = Constants.EMPTY_STR;
            this.persona = Constants.EMPTY_STR;
            this.scanInput = Constants.EMPTY_STR;

            if (utilities.getCurrentUser().getGlobal()) {
                this.msgAmbito = "Jefatura";
            } else if (utilities.getCurrentUser().getRegional()) {
                this.msgAmbito = "Unidad";
            } else {
                this.msgAmbito = "Usuario";
            }

            this.permantentTicket = utilities.getProperty("/default.properties", "ticket");
            search(true, true, true);
        } catch (IOException ex) {
            utilities.addFacesMessage(LOGGER, Constants.ERROR, ex.getMessage());
        }

    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="getters & setters">
    public Date getHoy() {
        return new Date();
    }

    public Boolean getArchivado() {
        return selectedParte.getArchivado();
    }

    public void setArchivado(Boolean archivado) {
        this.archivado = archivado;
        this.accion = selectedParte.getArchivado() ? Constants.EMPTY_STR : Constants.ARCHIVAR;
        puedeEnviarPortafirmas = false;

        try {
            PinMiembro situacionEstafeta = PinDAO.getMiembroById(Integer.valueOf(utilities.getProperty("/default.properties", "situacionEstafeta")));
            for (PinObjeto objeto : selectedParte.getObjetos()) {
                PinObjetoSituacion sit = new PinObjetoSituacion(situacionEstafeta, utilities.getCurrentUser().getId().getUsuario());
                if (objeto.getSituaciones().contains(sit)) {
                    objeto.getSituaciones().remove(sit);
                }
            }
            selectedParte.getDestinos().clear();
        } catch (IOException ex) {
            utilities.addFacesMessage(LOGGER, Constants.ERROR, "Se ha producido un error al grabar el Parte: " + utilities.getExceptionCause(ex));
        }
    }

    public String getAviso() {
        return aviso;
    }

    public GenMiembro getSelectedDestinatarioDisponible() {
        return selectedDestinatarioDisponible;
    }

    public void setSelectedDestinatarioDisponible(GenMiembro selectedDestinatarioDisponible) {
        this.selectedDestinatarioDisponible = selectedDestinatarioDisponible;
    }

    public PinDestino getSelectedDestinatarioAsignado() {
        return selectedDestinatarioAsignado;
    }

    public void setSelectedDestinatarioAsignado(PinDestino selectedDestinatarioAsignado) {
        this.selectedDestinatarioAsignado = selectedDestinatarioAsignado;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public List<GenMiembro> getDestinosDisponibles() {
        switch (this.origenDestinoDisponible) {
            case Constants.DESTINO_JEFATURA:
                this.destinosDisponibles = this.destJefatura;
                break;
            case Constants.DESTINO_AYUNTAMIENTO:
                this.destinosDisponibles = this.destAyuntamiento;
                break;
            case Constants.DESTINO_EXTERNO:
                this.destinosDisponibles = this.destExterno;
                break;
            default:
                this.destinosDisponibles = new ArrayList<>();
        }
        return this.destinosDisponibles;
    }

    public void setDestinosDisponibles(List<GenMiembro> destinosDisponibles) {
        this.destinosDisponibles = destinosDisponibles;
    }

    public String getOrigenDestinoDisponible() {
        return origenDestinoDisponible;
    }

    public void setOrigenDestinoDisponible(String origenDestinoDisponible) {
        this.origenDestinoDisponible = origenDestinoDisponible;
    }

    public PushContext getNotificationChannel() {
        return notificationChannel;
    }

    public Boolean getPuedeEnviarPortafirmas() {
        return puedeEnviarPortafirmas;
    }

    public boolean isShowTodasOrdenes() {
        return showTodasOrdenes;
    }

    public void setShowTodasOrdenes(boolean showTodasOrdenes) {
        this.showTodasOrdenes = showTodasOrdenes;
    }

    public String getPersona() {
        return persona;
    }

    public void setPersona(String persona) {
        this.persona = persona;
    }

    public List<PinOrdenServicioAux> getOrdenesServicio() {
        return ordenesServicio;
    }

    public Boolean getIncluidoNovedades() {
        return incluidoNovedades;
    }

    public void setIncluidoNovedades(Boolean incluidoNovedades) {
        this.incluidoNovedades = incluidoNovedades;
    }

    public boolean isEsPdf() {
        return esPdf;
    }

    public String getScanInput() {
        return scanInput;
    }

    public void setScanInput(String scanInput) {
        if (!scanInput.isEmpty()) {
            this.scanInput = scanInput;

        }
    }

    public PinParteAdjunto getSelectedAdjunto() {
        return selectedAdjunto;
    }

    public void setSelectedAdjunto(PinParteAdjunto selectedAdjunto) {
        this.selectedAdjunto = selectedAdjunto;
    }

    public boolean isUseFileName() {
        return useFileName;
    }

    public void setUseFileName(boolean useFileName) {
        this.useFileName = useFileName;
    }

    public UploadedFile getUploadedFile() {
        return uploadedFile;
    }

    public void setUploadedFile(UploadedFile uploadedFile) {
        this.uploadedFile = uploadedFile;
    }

    public PinObjeto getSelectedObjeto() {
        return selectedObjeto;
    }

    public void setSelectedObjeto(PinObjeto selectedObjeto) {
        this.selectedObjeto = selectedObjeto;
    }

    public List<PinMiembro> getSituaciones() {
        return situaciones;
    }

    public PinObjetoSituacion getSelectedObjetoSituacion() {
        return selectedObjetoSituacion;
    }

    public void setSelectedObjetoSituacion(PinObjetoSituacion selectedObjetoSituacion) {
        this.selectedObjetoSituacion = selectedObjetoSituacion;
    }

    public PinMiembro getSelectedSituacion() {
        return selectedSituacion;
    }

    public void setSelectedSituacion(PinMiembro selectedSituacion) {
        this.selectedSituacion = selectedSituacion;
    }

    public boolean isPuedeTramitar() {
        return puedeTramitar;
    }

    public String getAccion() {
        return accion;
    }

    public void setAccion(String accion) {
        this.accion = accion;
    }

    public List<PinMiembro> getAsuntos() {
        return asuntos;
    }

    public List<PinParteAdjunto> getFilteredAdjuntos() {
        return filteredAdjuntos;
    }

    public void setFilteredAdjuntos(List<PinParteAdjunto> filteredAdjuntos) {
        this.filteredAdjuntos = filteredAdjuntos;
    }

    public PinDestino getSelectedDestinatario() {
        return selectedDestinatario;
    }

    public void setSelectedDestinatario(PinDestino selectedDestinatario) {
        this.selectedDestinatario = selectedDestinatario;
    }

    public PinMiembro getSelectedDestino() {
        return selectedDestino;
    }

    public void setSelectedDestino(PinMiembro selectedDestino) {
        this.selectedDestino = selectedDestino;
    }

    public List<PinMiembro> getDestinos() {
        return destinos;
    }

    public List<PinMiembro> getEstados() {
        return estados;
    }

    public StreamedContent getZipFile() {
        try {
            File tmpFile = File.createTempFile("tmp-parte-" + selectedParte.getNumeroParte() + "-", ".zip");
            FileOutputStream fos = new FileOutputStream(tmpFile);
            ZipOutputStream zos = new ZipOutputStream(fos);
            Calendar cal = Calendar.getInstance();
            cal.setTime(selectedParte.getFechaRegistro());
            String agno = String.valueOf(cal.get(Calendar.YEAR));

            for (PinParteAdjunto docAdjunto : selectedParte.getAdjuntos()) {

                File file = new File(Constants.FILES_PATH + agno + "/" + docAdjunto.getIdAdjunto() + docAdjunto.getTipo());
                FileInputStream fis = new FileInputStream(file);
                ZipEntry zipEntry = new ZipEntry(file.getName());
                zos.putNextEntry(zipEntry);

                byte[] bytes = new byte[1024];
                int lenght;
                while ((lenght = fis.read(bytes)) >= 0) {
                    zos.write(bytes, 0, lenght);
                }
                zos.closeEntry();
                fis.close();
            }

            zos.close();
            fos.close();

            InputStream stream = (InputStream) new FileInputStream(tmpFile);
            zipFile = new DefaultStreamedContent(stream, "application/x-compressed", "Parte " + selectedParte.getNumParteFormateado() + ".zip");
            tmpFile.delete();
        } catch (IOException ex) {
            utilities.addFacesMessage(LOGGER, Constants.ERROR, "Se ha producido un error durante la compresión de los archivos: " + utilities.getExceptionCause(ex));
        }

        return zipFile;
    }

    public void setZipFile(StreamedContent zipFile) {
        this.zipFile = zipFile;
    }

    public Boolean getCanSave() {
        boolean result = !accion.equals(Constants.EMPTY_STR);
        if (accion.equals(Constants.REMITIR_REGISTRO_JEFATURA)) {
            result = !selectedParte.getDestinos().isEmpty();
        }
        if (accion.equals(Constants.ENVIAR_PORTA_FIRMAS)) {
            result = !selectedParte.getFirmantes().isEmpty();
        }
        if (accion.equals(Constants.ARCHIVAR)) {
            result = true;
        }

        return result && puedeTramitar;
    }

    public Boolean getCanEdit() {
        return selectedParte.getIdParte() > 0
                && utilities.getCurrentUser().getModificaciones()
                && !selectedParte.getAnulado()
                && !selectedParte.getListoTramitar();
    }

    public Boolean getCanAdd() {
        return utilities.getCurrentUser().getAltas();
    }

    public Boolean getCanDelete() {
        return selectedParte.getIdParte() > 0
                && utilities.getCurrentUser().getBajas()
                && !selectedParte.getAnulado()
                && !selectedParte.getListoTramitar();
    }

    public Boolean getCanPrint() {
        return selectedParte.getIdParte() > 0 && utilities.getCurrentUser().getModificaciones() && !selectedParte.getAnulado();
    }

    public String getPdfFileName() {
        return pdfFileName;
    }

    public void setPdfFileName(String pdfFileName) {
        this.pdfFileName = pdfFileName;
    }

    public static String getReportsPath() {
        return reportsPath;
    }

    public static void setReportsPath(String reportsPath) {
        PinOficinaBacking.reportsPath = reportsPath;
    }

    public PinActuante getSelectedActuante() {
        return selectedActuante;
    }

    public void setSelectedActuante(PinActuante selectedActuante) {
        this.selectedActuante = selectedActuante;
    }

    public PinActuante getActuante() {
        return actuante;
    }

    public void setActuante(PinActuante actuante) {
        this.actuante = actuante;
    }

    public Date getFechaDesde() {
        return fechaDesde;
    }

    public void setFechaDesde(Date fechaDesde) {
        this.fechaDesde = fechaDesde;
    }

    public Date getFechaHasta() {
        return fechaHasta;
    }

    public void setFechaHasta(Date fechaHasta) {
        this.fechaHasta = fechaHasta;
    }

    public Date getFechaImpresion() {
        return fechaImpresion;
    }

    public void setFechaImpresion(Date fechaImpresion) {
        this.fechaImpresion = fechaImpresion;
    }

    public List<GenMiembro> getUnidades() {
        return unidades;
    }

    public void setUnidades(List<GenMiembro> unidades) {
        this.unidades = unidades;
    }

    public Integer getEstado() {
        return estado;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }

    public String getMsgAmbito() {
        return msgAmbito;
    }

    public void setMsgAmbito(String msgAmbito) {
        this.msgAmbito = msgAmbito;
    }

    public HttpCaller getHttpCaller() {
        return httpCaller;
    }

    public void setHttpCaller(HttpCaller httpCaller) {
        this.httpCaller = httpCaller;
    }

    public String getPermantentTicket() {
        return permantentTicket;
    }

    public void setPermantentTicket(String permantentTicket) {
        this.permantentTicket = permantentTicket;
    }

    public PinParteAux getSelectedParte() {
        return selectedParte;
    }

    public void setSelectedParte(PinParteAux selectedParte) {
        this.selectedParte = selectedParte;
    }

    public PinParteAux getPatronBusqueda() {
        return patronBusqueda;
    }

    public void setPatronBusqueda(PinParteAux patronBusqueda) {
        this.patronBusqueda = patronBusqueda;
    }

    public List<PinParteAux> getResultadoBusqueda() {
        return resultadoBusqueda;
    }

    public void setResultadoBusqueda(List<PinParteAux> resultadoBusqueda) {
        this.resultadoBusqueda = resultadoBusqueda;
    }

    public LazyDataModel<PinParteAux> getLazyResultadoBusqueda() {
        return lazyResultadoBusqueda;
    }

    public void setLazyResultadoBusqueda(LazyDataModel<PinParteAux> lazyResultadoBusqueda) {
        this.lazyResultadoBusqueda = lazyResultadoBusqueda;
    }

    public List<PinParteAux> getSelectedParteChecked() {
        return selectedParteChecked;
    }

    public void setSelectedParteChecked(List<PinParteAux> selectedParteChecked) {
        this.selectedParteChecked = selectedParteChecked;
    }

    public List<PinParteAux> getPartesNovedades() {
        return partesNovedades;
    }

    public String getCenter() {
        return center;
    }

    public void setCenter(String center) {
        this.center = center;
    }

    public double getSelectedLat() {
        return selectedLat;
    }

    public void setSelectedLat(double selectedLat) {
        this.selectedLat = selectedLat;
    }

    public double getSelectedLng() {
        return selectedLng;
    }

    public void setSelectedLng(double selectedLng) {
        this.selectedLng = selectedLng;
    }

    public MapModel getMapModel() {
        return mapModel;
    }

    public void setMapModel(MapModel mapModel) {
        this.mapModel = mapModel;
    }

    public static MyMarker getMarker() {
        return marker;
    }

    public static void setMarker(MyMarker marker) {
        PinOficinaBacking.marker = marker;
    }

    public int getZoom() {
        return zoom;
    }

    public void setZoom(int zoom) {
        this.zoom = zoom;
    }

    public Integer getAgno() {
        return agno;
    }

    public void setAgno(Integer agno) {
        this.agno = agno;
    }

    public List<Integer> getAvailableYears() {
        return availableYears;
    }

    public void setAvailableYears(List<Integer> availableYears) {
        this.availableYears = availableYears;
    }

    public Map<String, File> getFilesToBeUploaded() {
        return filesToBeUploaded;
    }

    public void setFilesToBeUploaded(Map<String, File> filesToBeUploaded) {
        this.filesToBeUploaded = filesToBeUploaded;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="private methods">
    private void resetMap() {
        this.center = Constants.DEFAULT_LAT.concat(",").concat(Constants.DEFAULT_LNG);
        this.zoom = 13;
        this.mapModel.getMarkers().clear();
    }

    private static void compileJasperFiles() throws IOException, JRException {
        File folder = new File(reportsPath);
        FilenameFilter jasperFilter = (File directory, String fileName) -> directory.equals(folder) && fileName.endsWith(".jrxml");

        File[] xmlFiles = folder.listFiles(jasperFilter);
        for (File file : xmlFiles) {
            String xmlFile = file.getCanonicalPath();
            try {
                Log.log(LOGGER, Constants.INFO, "Procesando el archivo: " + xmlFile);
                String jasperFile = file.getCanonicalPath().replace(".jrxml", ".jasper");
                JasperCompileManager.compileReportToFile(xmlFile, jasperFile);
            } catch (JRException ex) {
                throw new JRException("\nProcesando el archivo: " + xmlFile + "\n\n" + ex.getMessage());
            }
        }

    }

    private void makeReportParte() throws SQLException, JRException, IOException, URISyntaxException {
        reportsPath = utilities.getExternalContext().getRealPath("/").concat(Constants.REPORTS_PATH);

        File pdfFile = File.createTempFile("informe", ".pdf");
        pdfFile.deleteOnExit();
        pdfFileName = pdfFile.getAbsolutePath();

        JasperPrint jasperPrint;
        JasperReport jrInforme;

        String jasperFileInforme = Constants.EMPTY_STR;
        switch (selectedParte.getTipo()) {
            case PinParte.PARTE:
                jasperFileInforme = reportsPath.concat("parte.jasper");
                break;
            case PinParte.INFORME:
                jasperFileInforme = reportsPath.concat("informe.jasper");
                break;
            case PinParte.ACREDITO:
                jasperFileInforme = reportsPath.concat("acredito.jasper");
                break;
        }

        HashMap parameters = new HashMap();

        parameters.put("UNIDAD", selectedParte.getUnidad() != null ? selectedParte.getUnidad().getNombre() : "Unidad no definida");

        if (!new File(jasperFileInforme).exists()) {
            compileJasperFiles();
        }
        jrInforme = (JasperReport) JRLoader.loadObject(new BufferedInputStream(new FileInputStream(jasperFileInforme)));

        List<ParteReport> partes = new ArrayList<>();
        partes.add(new ParteReport(selectedParte));

        JRBeanCollectionDataSource jrDataSource = new JRBeanCollectionDataSource(partes);
        jasperPrint = JasperFillManager.fillReport(jrInforme, parameters, jrDataSource);
        JasperExportManager.exportReportToPdfFile(jasperPrint, pdfFileName);

    }

    private void makeReportNovedades() throws SQLException, JRException, IOException, URISyntaxException {
        reportsPath = utilities.getExternalContext().getRealPath("/").concat(Constants.REPORTS_PATH);
        List<JasperPrint> printList = new ArrayList<>();
        JRBeanCollectionDataSource jrDataSource;

        File pdfFile = File.createTempFile("novedades", ".pdf");
        pdfFile.deleteOnExit();
        pdfFileName = pdfFile.getAbsolutePath();

        JasperReport jrInforme;
        String jasperFileInforme = reportsPath.concat("novedades.jasper");

        HashMap parameters = new HashMap();

        if (!new File(jasperFileInforme).exists()) {
            compileJasperFiles();
        }
        jrInforme = (JasperReport) JRLoader.loadObject(new BufferedInputStream(new FileInputStream(jasperFileInforme)));

        int totalArchivados = 0;
        for (PinParteAux parte : partesNovedades) {
            if (parte.getArchivado()) {
                totalArchivados++;
            }
        }

        parameters.put("UNIDAD", utilities.getCurrentUser().getUnidad());
        parameters.put("FECHA_NOVEDADES", fechaImpresion);
        parameters.put("TOTAL_ARCHIVADOS", totalArchivados);
        parameters.put("TOTAL_REGISTROS", partesNovedades.size());

        // Se genera la relación de novedades marcada como copia 1 y que será la que se envíe a Estafeta si procede,
        // Tanto esta copia como la de oficina se añaden a una lista (printList) que será la que se defina en el 
        // pdfExporter como entrada para generar un único PDF con ambos reportes.
        parameters.put("COPIA", 1);
        jrDataSource = new JRBeanCollectionDataSource(partesNovedades);
        JasperPrint jpRelacionNovedades = JasperFillManager.fillReport(jrInforme, parameters, jrDataSource);
        jpRelacionNovedades.setName("Relación Novedades");
        printList.add(jpRelacionNovedades);

        // Se vuelve a generar la relación de novedades, pero marcada como copia 2, que será evaluada durante la 
        // construcción del reporte añadiendo la reseña de los partes marcados como archivo. Además, será marcada
        // como COPIA OFICINA
        parameters.put("COPIA", 2);
        jrDataSource = new JRBeanCollectionDataSource(partesNovedades);
        JasperPrint jpCopiaOficina = JasperFillManager.fillReport(jrInforme, parameters, jrDataSource);
        jpCopiaOficina.setName("Copia Oficina");
        printList.add(jpCopiaOficina);

        JRPdfExporter pdfExporter = new JRPdfExporter();
        FileOutputStream output = new FileOutputStream(pdfFile);
        pdfExporter.setExporterInput(SimpleExporterInput.getInstance(printList));
        pdfExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(output));
        SimplePdfExporterConfiguration configuration = new SimplePdfExporterConfiguration();
        configuration.setCreatingBatchModeBookmarks(true);
        pdfExporter.setConfiguration(configuration);
        pdfExporter.exportReport();

    }

    private void comprobarCheck() {
        if (selectedParteChecked.size() == 1) {
            selectedParte = selectedParteChecked.get(0);
        } else {
            try {
                selectedParte = new PinParteAux(PinDAO.getMiembroById(Long.parseLong(utilities.getProperty("/default.properties", "denunciado"))), utilities.getCurrentUser());
            } catch (IOException ex) {
                utilities.addFacesMessage(LOGGER, Constants.ERROR, utilities.getExceptionCause(ex).toString());
            }
        }
    }

    private boolean checkFechasImpresion() {
        boolean ok = true;
        aviso = "Para generar o ver el documento <strong>Relación de Novedades</strong> deben seleccionarse únicamente partes o informes"
                + " con la fecha de impresión en blanco, si se quiere generar un nuevo documento, o con la misma fecha si se quiere recuperar"
                + " un documento previamente impreso.";

        Iterator it = selectedParteChecked.iterator();
        Date fecha = selectedParteChecked.get(0).getFechaImpresion();
        while (ok && it.hasNext()) {
            PinParte parte = (PinParte) it.next();
            if ((parte.getFechaImpresion() == null) != (fecha == null)) { //XOR
                ok = false;
            } else {
                if (parte.getFechaImpresion() != null) {
                    if (!parte.getFechaImpresion().equals(fecha)) {
                        ok = false;
                    }
                }
            }
        }

        return ok;
    }

    private boolean puedeImprimirNovedades() {
        boolean ok = true;
        Iterator it = selectedParteChecked.iterator();
        while (ok && it.hasNext()) {
            PinParteAux parte = (PinParteAux) it.next();
            if (!parte.getUnidad().equals(utilities.getCurrentUser().getUnidad())) {
                ok = false;
                aviso = "Para generar o ver el documento <strong>Relación de Novedades</strong> deben seleccionarse únicamente partes o informes"
                        + " pertenencientes a su Unidad.";
            } else if (parte.getAnulado()) {
                ok = false;
                aviso = "Para generar o ver el documento <strong>Relación de Novedades</strong> deben seleccionarse únicamente partes o informes"
                        + " que no estén anulados.";
            } else if (parte.getEnviadoRegistro()) {
                ok = false;
                aviso = "Para generar <strong>Relación de Novedades</strong> deben seleccionarse únicamente partes o informes"
                        + " que no estén remitidos a sus destinos.";
            } else {
//                if (envioEstafeta != null) {
//                    Calendar cal = Calendar.getInstance(Constants.LOCALE_SPANISH);
//                    Calendar now = Calendar.getInstance(Constants.LOCALE_SPANISH);
//                    cal.setTime(envioEstafeta.getFecha());
//                    if (parte.getEstado(utilities.getCurrentUser().getUnidad(), PinParte.EST_LISTO_TRAMITAR)) {
//                        if (cal.get(Calendar.YEAR) != now.get(Calendar.YEAR) || cal.get(Calendar.MONTH) != now.get(Calendar.MONTH) || cal.get(Calendar.DAY_OF_MONTH) != now.get(Calendar.DAY_OF_MONTH)) {
//                            ok = false;
//                            aviso = "Para generar <strong>Relación de Novedades</strong> deben seleccionarse únicamente partes o informes"
//                                    + " que hayan sido remitidos a Estafeta durante el día de hoy.";
//                        }
//                    } else if (!parte.getArchivado(utilities.getCurrentUser().getUnidad())) {
//                        ok = false;
//                        aviso = "Para generar <strong>Relación de Novedades</strong> deben seleccionarse únicamente partes o informes"
//                                + " que hayan sido remitidos a Estafeta o Archivados.";
//                    }
//                }
            }
        }
        return ok;
    }

    private Set<String> saveUploadedFiles() {
        int pos;
        Set<String> errors = new HashSet<>();
        if (!filesToBeUploaded.isEmpty()) {
            for (Map.Entry<String, File> entry : filesToBeUploaded.entrySet()) {
                String fileName = entry.getKey();
                File file = entry.getValue();
                pos = fileName.lastIndexOf('.');
                for (PinParteAdjunto adjunto : this.selectedParte.getAdjuntos()) {
                    if (fileName.equals(adjunto.getFileName())) {
                        try {
                            utilities.saveUploadedFile(file, String.valueOf(adjunto.getIdAdjunto()), fileName.substring(pos, fileName.length()), this.selectedParte.getFechaRegistro());
                        } catch (IOException ex) {
                            errors.add(fileName);
                        }
                        break;
                    }
                }
            }
        }
        return errors;
    }

    /* El método que detecta si un documento está firmado o no es bastante chapuza
     ya únicamente se limita a comprobar si en todas las páginas aparece algo
     el texto presente en todos las cajetines de firma. */
    private boolean isSigned(File file) throws IOException, GeneralSecurityException {
        boolean found = true;
        PdfReader pdfReader = new PdfReader(file.getAbsolutePath());
        int pages = pdfReader.getNumberOfPages();

        while (pages > 0 && found) {
            String text = PdfTextExtractor.getTextFromPage(pdfReader, pages);
            found = text.contains("Código") && text.contains("Verificación") && text.contains("Firmado");
            pages--;
        }

        pdfReader.close();
        return found;
    }

    /**
     * El método envía un mensaje push a todos los usuarios conectados
     * pertenecientes a la Unidad cuyo id se aporta.
     *
     * @param idUnidad ID de la Unidad destinataria del mensaje.
     * @param message Contenido del mensaje a enviar.
     */
    private void pushMessage(Long idUnidad, String message) {
        final Gson gson = new Gson();
        Message msg = new Message(idUnidad, message);
        notificationChannel.send(gson.toJson(msg));
    }
    //</editor-fold>

    public boolean isTramitable() {
        return selectedParte.getIdParte() > 0
                && selectedParte.getUnidad().equals(utilities.getCurrentUser().getUnidad())
                && selectedParte.getListoTramitar()
                && !selectedParte.getAnulado()
                && !selectedParte.getEnviadoRegistro();
    }

    public void selectRow() {
        comprobarCheck();
        resetMap();
        if (selectedParte.getIdParte() != 0L && selectedParte.getLugar().getLat() != 0.0) {
            Double lng = selectedParte.getLugar().getLng();
            Double lat = selectedParte.getLugar().getLat();
            this.center = lat.toString().concat(",").concat(lng.toString());
            this.zoom = 18;
            LatLng coord = new LatLng(lat, lng);
            this.mapModel.addOverlay(new Marker(coord, selectedParte.getNumParteFormateado()));

            this.filteredAdjuntos.clear();
            selectedParte.getAdjuntos().stream().filter((docAdjunto) -> (!docAdjunto.getTipo().equals(".jpg") && !docAdjunto.getTipo().equals(".3gp") && !docAdjunto.getTipo().equals(".mp4"))).forEachOrdered((docAdjunto) -> {
                this.filteredAdjuntos.add(docAdjunto);
            });
        }
    }

    public void onMarkerSelect(OverlaySelectEvent event) {
        marker = (MyMarker) event.getOverlay();
    }

    public void resetSearchFields() {
        try {
            this.patronBusqueda = new PinParteAux(PinDAO.getMiembroById(Long.parseLong(utilities.getProperty("/default.properties", "denunciado"))), utilities.getCurrentUser());
            this.patronBusqueda.setUnidad(utilities.getCurrentUser().getUnidad());
            this.fechaDesde = null;
            this.fechaHasta = null;
            this.actuante = new PinActuante();
            this.estado = PinParte.EST_LISTO_TRAMITAR;
            this.dni = Constants.EMPTY_STR;
            this.persona = Constants.EMPTY_STR;
        } catch (IOException ex) {
            utilities.addFacesMessage(LOGGER, Constants.ERROR, utilities.getExceptionCause(ex).toString());
        }
    }

    public void abrir() {

        SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyyHHmmss");
        try {
            if (!scanInput.isEmpty()) {
                String[] parte = scanInput.split(" ");
                if (parte.length < 3) {
                    throw new PinforException("El código de barras escaneado no corresponde a un Parte o Informe.");
                } else {
                    agno = Integer.valueOf(parte[0]);
                    patronBusqueda.setTipo(parte[1]);
                    patronBusqueda.setNumeroParte(Integer.valueOf(parte[2]));
                    scanInput = Constants.EMPTY_STR;
                }
            }
            fechaDesde = sdf.parse(String.format("0101%d000000", agno));
            fechaHasta = sdf.parse(String.format("3112%d235959", agno));
            this.estado = null;
            search(true, true, true);
            if (lazyResultadoBusqueda.getRowCount() != 0) {

                // Como la carga de la tabla no se hace hasta que se genera la vista, es necesario forzar la misma
                // desde aquí para obtener el primer registro, que podamos referenciar como selectedParte
                DataTable lazyTable = (DataTable) utilities.getFacesContext().getViewRoot().findComponent("fMain:tParte");
                lazyTable.loadLazyData();

                this.selectedParte = lazyResultadoBusqueda.getWrappedData().get(0);
                if (this.selectedParteChecked != null) {
                    this.selectedParteChecked.clear();
                }
                this.selectedParteChecked.add(selectedParte);
                selectRow();
            } else {
                utilities.addFacesMessage(LOGGER, Constants.INFO, "El documento solicitado aún no está registrado o se encuentra fuera de su ámbito de búsqueda. <br/><br/>ÁMBITO: " + this.msgAmbito);
            }

            resetPatronBusqueda();

        } catch (ParseException ex) {
            utilities.addFacesMessage(LOGGER, Constants.ERROR, "Se ha producido un error en la consulta: " + utilities.getExceptionCause(ex));
        } catch (PinforException ex) {
            utilities.addFacesMessage(LOGGER, Constants.WARN, ex.getMessage());
        }
    }

    public void abrirNovedades() {
        Calendar fecha = Calendar.getInstance(Constants.LOCALE_SPANISH);
        fecha.setTimeInMillis(Long.valueOf(scanInput));
        try {

            if (!scanInput.isEmpty()) {

                if (scanInput.length() < 13) {
                    throw new PinforException("El código de barras escaneado no corresponde a una Relación de Novedades.");
                }
            }

            patronBusqueda = new PinParteAux(PinDAO.getMiembroById(Long.parseLong(utilities.getProperty("/default.properties", "denunciado"))), utilities.getCurrentUser());
            patronBusqueda.setFechaImpresion(fecha.getTime());
            searchNovedades(true, true, true);
            if (lazyResultadoBusqueda.getRowCount() != 0) {

                // Como la carga de la tabla no se hace hasta que se genera la vista, es necesario forzar la misma
                // desde aquí para obtener el primer registro, que podamos referenciar como selectedParte
                DataTable lazyTable = (DataTable) utilities.getFacesContext().getViewRoot().findComponent("fMain:tParte");
                lazyTable.loadLazyData();

                this.selectedParte = lazyResultadoBusqueda.getWrappedData().get(0);
                if (this.selectedParteChecked != null) {
                    this.selectedParteChecked.clear();
                }
                this.selectedParteChecked.add(selectedParte);
                selectRow();
            } else {
                utilities.addFacesMessage(LOGGER, Constants.INFO, "El documento solicitado aún no está registrado o se encuentra fuera de su ámbito de búsqueda. <br/><br/>ÁMBITO: " + this.msgAmbito);
            }

            resetPatronBusqueda();
        } catch (PinforException ex) {
            utilities.addFacesMessage(LOGGER, Constants.WARN, ex.getMessage());
        } catch (IOException ex) {
            utilities.addFacesMessage(LOGGER, Constants.ERROR, utilities.getExceptionCause(ex).toString());
        }

    }

    public void search(boolean resetMap, boolean resetSelectedParte, boolean evaluarFechaImpresion) {
        try {
            this.selectedParteChecked.clear();
            if (resetSelectedParte) {
                this.selectedParte = new PinParteAux(PinDAO.getMiembroById(Long.parseLong(utilities.getProperty("/default.properties", "denunciado"))), utilities.getCurrentUser());
            }

            lazyResultadoBusqueda = new PinParteLazyDataModel.Builder(patronBusqueda)
                    .fechaDesde(fechaDesde)
                    .fechaHasta(fechaHasta)
                    .evaluarFechaImpresion(evaluarFechaImpresion)
                    .incluidoNovedades(incluidoNovedades)
                    .consultaOficina(true)
                    .estado(estado)
                    .actuante(actuante)
                    .dni(dni)
                    .persona(persona)
                    .currentUser(utilities.getCurrentUser())
                    .build();

            if (resetMap) {
                resetMap();
            }

            if (lazyResultadoBusqueda.getRowCount() == 0) {
                utilities.addFacesMessage(LOGGER, Constants.INFO, "No se han encontrado datos coincidentes con la consulta. <br/><br/>ÁMBITO: " + this.msgAmbito);
            }
        } catch (IOException | NumberFormatException ex) {
            utilities.addFacesMessage(LOGGER, Constants.ERROR, "Se ha producido un error en la consulta: " + ex.getMessage());
        }
    }

    public void searchNovedades(boolean resetMap, boolean resetSelectedParte, boolean evaluarFechaImpresion) {
        try {
            this.selectedParteChecked.clear();
            if (resetSelectedParte) {
                this.selectedParte = new PinParteAux(PinDAO.getMiembroById(Long.parseLong(utilities.getProperty("/default.properties", "denunciado"))), utilities.getCurrentUser());
            }

            patronBusqueda.setFechaImpresion(new Date(Long.parseLong(scanInput)));

            lazyResultadoBusqueda = new PinParteLazyDataModel.Builder(patronBusqueda)
                    .actuante(actuante)
                    .evaluarFechaImpresion(evaluarFechaImpresion)
                    .incluidoNovedades(true)
                    .dni(dni)
                    .persona(persona)
                    .currentUser(utilities.getCurrentUser())
                    .build();

            if (resetMap) {
                resetMap();
            }

            if (lazyResultadoBusqueda.getRowCount() == 0) {
                utilities.addFacesMessage(LOGGER, Constants.INFO, "No se han encontrado datos coincidentes con la consulta. <br/><br/>ÁMBITO: " + this.msgAmbito);
            }

        } catch (NumberFormatException | IOException ex) {
            utilities.addFacesMessage(LOGGER, Constants.ERROR, "Se ha producido un error en la consulta: " + ex.getMessage());
        }
    }

    public void generateReport() {
        if (selectedParte.getAnulado()) {
            utilities.addFacesMessage(LOGGER, Constants.INFO, "El parte está anulado.");
        } else {
            PinHistoricoImpresion movimiento = new PinHistoricoImpresion();
            try {
                // Primero generamos el movimiento para el histórico      
                movimiento.setFecha(new Date());
                movimiento.setUsuario(String.format("%s / %s", utilities.getCurrentUser().getIndicativo(), utilities.getCurrentUser().getId().getUsuario()));
                movimiento.setElemento("Parte");
                selectedParte.addToImpressionHistory(movimiento);

                // Después generamos el informe o el resumen, dependiendo de la seleccion....
                makeReportParte();
                esPdf = pdfFileName.substring(pdfFileName.lastIndexOf('.'), pdfFileName.length()).equals(".pdf");
                utilities.executeUI("PF('dlgVerParte').show();");

                // Y si todo ha ido bien, persistimos el movimiento en la DB
                PinDAO.saveParte(selectedParte);

            } catch (SQLException | NullPointerException | JRException | IOException | URISyntaxException | PinforException ex) {
                selectedParte.getHistorico().remove(movimiento);
                utilities.addFacesMessage(LOGGER, Constants.ERROR, utilities.getExceptionCause(ex).toString());
            }
        }
    }

    public void generateReportNovedades(boolean save) throws PinforException {

        PinHistoricoImpresion movimiento = new PinHistoricoImpresion();
        try {

            // Después generamos el informe o el resumen, dependiendo de la seleccion....
            makeReportNovedades();
            esPdf = pdfFileName.substring(pdfFileName.lastIndexOf('.'), pdfFileName.length()).equals(".pdf");

            // Y si todo ha ido bien y el parámetro save es true, persistimos el movimiento en la DB
            if (save) {
                for (PinParteAux parte : partesNovedades) {
                    // Primero generamos el movimiento para el histórico      
                    movimiento.setFecha(new Date());
                    movimiento.setUsuario(String.format("%s / %s", utilities.getCurrentUser().getIndicativo(), utilities.getCurrentUser().getId().getUsuario()));
                    movimiento.setElemento(String.format("Novedades %s", new SimpleDateFormat("dd-MM-yy HH:mm").format(fechaImpresion)));
                    parte.addToImpressionHistory(movimiento);
                    parte.setFechaImpresion(fechaImpresion);

                    PinDAO.saveParte(parte);
                }
            }
            utilities.executeUI("PF('dlgVerParte').show();");

        } catch (SQLException | NullPointerException | JRException | IOException | URISyntaxException ex) {
            selectedParte.getHistorico().remove(movimiento);
            utilities.addFacesMessage(LOGGER, Constants.ERROR, utilities.getExceptionCause(ex).toString());
        }
    }

    public StreamedContent getStream() throws IOException {
        ExternalContext ec = utilities.getExternalContext();
        ec.addResponseHeader("Cache-Control", "no-cache");
        ec.addResponseHeader("Pragma", "no-cache");
        ec.addResponseHeader("Expires", "-1");
        StreamedContent sc = null;
        try {
            if (esPdf) {
                sc = new DefaultStreamedContent(new FileInputStream(pdfFileName), "application/pdf", selectedParte.getNumParteFormateado().replace('/', ' '));
            } else {
                sc = new DefaultStreamedContent(new FileInputStream(pdfFileName), "application/jpeg", selectedParte.getNumParteFormateado().replace('/', ' '));
            }
        } catch (FileNotFoundException ex) {
            System.out.println(ex.getMessage());
        }
        return sc;
    }

    public void resetSelectedDestino() {
        this.selectedDestino = null;
    }

    public void deleteSelectedDestino() {
        this.selectedParte.getDestinos().remove(this.selectedDestinatario);
        this.selectedDestino = null;
        this.selectedDestinatario = null;
    }

    public void resetSelectedAdjunto() {

        this.selectedAdjunto = new PinParteAdjunto();
        this.selectedAdjunto.setParte(selectedParte);
        utilities.executeUI("PF('dlgAnadirAdjunto').show();");
    }

    public void deleteSelectedAdjunto() {

        // Además de eliminar el documento adjunto de la lista de documentos del parte, necesitamos
        // eliminar el archivo que pudiera estar pendiente de grabar en el disco si acaba de ser subido.
        // Para eso, recorremos el conjunto hasta encontrar uno con igual título que el que tratamos 
        // de eliminar.
        boolean found = false;
        Iterator it = this.filesToBeUploaded.entrySet().iterator();
        Map.Entry<String, File> entry = null;
        while (it.hasNext() && !found) {
            entry = (Map.Entry) it.next();
            if (entry.getKey().equals(String.format("%s%s", this.selectedAdjunto.getTitulo(), this.selectedAdjunto.getTipo()))) {
                found = true;
            }
        }

        // Una vez encontrado, es eliminado.
        if (entry != null) {
            this.filesToBeUploaded.remove(null);
        }

        // También debemos eliminarlo del conjunto de documentos adjuntos del parte 
        // así como el archivo relacionado de su ubicación. Como esta eliminación no se
        // llevará a cabo hasta que se acepten los cambios por parte del usuario, deberemos
        // guardar un conjunto con los archivos removidos para aplicarlos en su caso.
        this.selectedParte.getAdjuntos().remove(this.selectedAdjunto);
        if (this.selectedAdjunto.getIdAdjunto() > 0) {
            filesToBeRemoved.add(this.selectedAdjunto.getIdAdjunto() + this.selectedAdjunto.getTipo());
        }
        this.selectedAdjunto = null;
    }

    public void uploadFile(FileUploadEvent event) {
        String fileName, fileType;
        this.uploadedFile = event.getFile();

        List<String> allowedTypes;
        String tiposPermitidos;

        allowedTypes = Arrays.asList(Constants.ALLOWED_TYPES);
        tiposPermitidos = Arrays.toString(Constants.ALLOWED_TYPES);
        if (uploadedFile != null && uploadedFile.getSize() > 0) {
            int pos = uploadedFile.getFileName().lastIndexOf('.');
            int nameLenght = uploadedFile.getFileName().length();

            if (!allowedTypes.contains(uploadedFile.getFileName().substring(pos + 1, nameLenght))) {
                // Si no es uno de los tipos permitidos, devolvemos un aviso
                utilities.addFacesMessage(LOGGER, Constants.WARN, "El archivo que trata de adjuntar no es uno de los tipos permitidos " + tiposPermitidos);
                uploadedFile = null;
                selectedAdjunto = null;
            } else if (uploadedFile.getSize() > Constants.ALLOWED_SIZE) {
                // Lo mismo hacemos si el tamaño supera el permitido.
                utilities.addFacesMessage(LOGGER, Constants.WARN, "El tamaño del archivo a adjuntar no puede ser mayor a 20MB");
                uploadedFile = null;
                selectedAdjunto = null;
            } else {
                // Si se cumplen las condiciones...

                // Creamos un objeto del tipo Adjunto a partir del nombre y tipo del archivo que se trata de subir....
                fileName = uploadedFile.getFileName().substring(0, pos);
                fileType = uploadedFile.getFileName().substring(pos, uploadedFile.getFileName().length());

                // Y un archivo temporal donde almacenar el fichero que acabamos de subir. Este archivo será eliminado
                // automáticamente por la JVM cuando ésta sea detenida, si es que el usuario no ha guardado los cambios,
                // en cuyo caso, será movido al directorio definitivo.
                try {
                    tmpFile = File.createTempFile(fileName, fileType);
                    tmpFile.deleteOnExit();

                    InputStream iStream = uploadedFile.getInputstream();
                    byte[] buffer = new byte[iStream.available()];
                    iStream.read(buffer);
                    OutputStream oStream = new FileOutputStream(tmpFile);
                    oStream.write(buffer);
                    oStream.close();

                    if (!isSigned(tmpFile)) {
                        throw new PinforException("El documento seleccionado no contiene una firma digital válida.");
                    }

                    filesToBeUploaded.put(fileName + fileType, tmpFile);
                    this.selectedAdjunto = new PinParteAdjunto();
                    this.selectedAdjunto.setParte(selectedParte);
                    this.selectedAdjunto.setFileName(fileName + fileType);
                    this.selectedAdjunto.setTipo(fileType);
                    this.selectedAdjunto.setContieneFirmaE(true);
                    if (useFileName) {
                        this.selectedAdjunto.setTitulo(fileName);
                    }
                } catch (GeneralSecurityException ex) {
                    utilities.addFacesMessage(LOGGER, Constants.ERROR, "Se ha producido un error al comprobar la firma del documento. Por favor, vuelva a intentarlo.");
                } catch (IOException ex) {
                    utilities.addFacesMessage(LOGGER, Constants.ERROR, String.format("Se ha producido un error al subir el documento adjunto.</br>%s</br/Por favor, vuelva a intentarlo.", ex.getMessage()));
                } catch (PinforException ex) {
                    this.uploadedFile = null;
                    this.selectedAdjunto = new PinParteAdjunto();
                    utilities.addFacesMessage(LOGGER, Constants.WARN, ex.getMessage());
                }
            }
        } else {
            utilities.addFacesMessage(LOGGER, Constants.WARN, "No ha seleccionado ningún archivo a adjuntar");
            this.selectedAdjunto = null;
        }
    }

    public void verSelectedAdjunto(boolean seleccionarAdjunto) {

        PinHistoricoImpresion movimiento = new PinHistoricoImpresion();
        try {
            // Primero generamos el movimiento para el histórico      

            if (seleccionarAdjunto) {
                if (!selectedParte.getAdjuntos().isEmpty()) {
                    selectedAdjunto = selectedParte.getAdjuntos().get(0);
                } else {
                    throw new PinforException("La reseña seleccionada no tiene su correspondiente acta de denuncia adjunta.");
                }
            }

            movimiento.setFecha(new Date());
            movimiento.setUsuario(String.format("%s / %s", utilities.getCurrentUser().getIndicativo(), utilities.getCurrentUser().getId().getUsuario()));
            movimiento.setElemento("Adjunto: " + selectedAdjunto.getTitulo().substring(0, selectedAdjunto.getTitulo().length() > 150 ? 150 : selectedAdjunto.getTitulo().length()));
            selectedParte.addToImpressionHistory(movimiento);

            Calendar cal = Calendar.getInstance();
            cal.setTime(selectedAdjunto.getParte().getFechaRegistro());
            int year = cal.get(Calendar.YEAR);
            File path = new File(utilities.getProperty("/default.properties", "filesPath") + "/" + year);

            pdfFileName = String.format("%s\\%s%s", path, selectedAdjunto.getIdAdjunto(), selectedAdjunto.getTipo());
            File pdfFile = new File(pdfFileName);
            esPdf = pdfFileName.substring(pdfFileName.lastIndexOf('.'), pdfFileName.length()).equals(".pdf");

            if (!pdfFile.exists()) {
                throw new PinforException("No se ha podido localizar el archivo correspondiente al acta solicitada.");
            }
            // Y si todo ha ido bien, persistimos el movimiento en la DB
            PinDAO.saveParte(selectedParte);
            if (esPdf) {
                utilities.executeUI("PF('dlgVerParte').show();");
            } else {
                utilities.executeUI("PF('dlgVerImagen').show();");
            }

        } catch (PinforException ex) {
            selectedParte.getHistorico().remove(movimiento);
            utilities.addFacesMessage(LOGGER, Constants.INFO, ex.getMessage());
        } catch (IOException ex) {
            selectedParte.getHistorico().remove(movimiento);
            utilities.addFacesMessage(LOGGER, Constants.ERROR, ex.getMessage());
        }
    }

    public void addDestinatario() {
//        this.selectedDestinatario = new PinDestino(this.selectedDestino);
//
//        if (!this.selectedParte.getDestinos().stream().anyMatch(o -> o.getDestino().getIdMiembro() == selectedDestino.getIdMiembro())) {
//            this.selectedParte.addDestinatario(selectedDestinatario);
//        } else {
//            utilities.addFacesMessage(LOGGER, Constants.INFO, "El destinatario seleccionado ya está incluido en la lista de destinos.");
//        }
//        this.selectedDestinatario = null;
//        this.selectedDestino = null;
    }

    public void resetSelectedParte() {
        idx = 0;
        puedeEnviarPortafirmas = false;
    }

    public void resetDestinatarios() {
        this.selectedParte.getDestinos().removeIf(d -> d.getEstados().isEmpty());
    }

    public void resetSelectedDestinatarioDisponible() {
        selectedDestinatarioDisponible = null;
    }

    public void resetSelectedDestinatarioAsignado() {
        selectedDestinatarioAsignado = null;
    }

    public void anadirAsignado() {

        PinDestino destinatario = new PinDestino(selectedDestinatarioDisponible);
        selectedParte.addDestinatario(destinatario);
        selectedDestinatarioDisponible = null;
    }

    public void eliminarAsignado() {
        if (selectedDestinatarioAsignado.getDestino().equals(selectedParte.getUnidad())) {
            utilities.addFacesMessage(LOGGER, Constants.INFO, "No puede eliminar a la oficina de su propia Unidad de la lista de destinatarios.");
        } else {
            selectedParte.getDestinos().remove(selectedDestinatarioAsignado);
            selectedDestinatarioAsignado = null;
        }
    }

    public void makeCopyDestinosAsignados() {
        /* Debemos definir una copia de la lista original de destinos asignados por si el usuario decide
        cancelar la operación de modificación, en cuyo caso debemos devolver la lista de destinatarios
        asignados a su estado original. */
        copiaDestinatariosAsignados = selectedParte.getDestinos().stream().collect(Collectors.toList());
    }

    public void modifyDestinos() {
        //   PinDAO.saveMiembro(selectedParte.getSubasunto());
        copiaDestinatariosAsignados = null;
    }

    public void resetDestinos() {
        // Aquí es donde se devuelve la lista de destinatarios asignados a su valor inicial 
        selectedParte.setDestinos(copiaDestinatariosAsignados);
        copiaDestinatariosAsignados = null;
    }

    public void tramitar() {
        idx = 0;
        selectedParte = selectedParteChecked.get(idx);
        if (selectedParte.getArchivado()) {
            accion = Constants.ARCHIVAR;
        } else {
            accion = Constants.EMPTY_STR;
        }
        puedeTramitar = selectedParte.getIdParte() > 0
                && selectedParte.getListoTramitar()
                && !selectedParte.getAnulado()
                && !selectedParte.getEnviadoRegistro();
    }

    public boolean puedeVer() {
        return selectedParte.getOrdenServicio() == null || selectedParte.getOrdenServicio().getIdOrdenTrabajo() < 0;
    }

    public void next(boolean save) {
        this.accion = Constants.EMPTY_STR;
        this.puedeEnviarPortafirmas = false;
        try {
            if (save) {
                PinDAO.saveParte(selectedParte);
            }
            if (++idx < selectedParteChecked.size()) {
                selectedParte = selectedParteChecked.get(idx);
                if (selectedParte.getArchivado()) {
                    accion = Constants.ARCHIVAR;
                } else {
                    accion = Constants.EMPTY_STR;
                }
                puedeTramitar = selectedParte.getIdParte() > 0
                        && selectedParte.getListoTramitar()
                        && !selectedParte.getAnulado()
                        && !selectedParte.getEnviadoRegistro();
            } else {
                idx = 0;
                utilities.executeUI("PF('dlgTramitarParte').hide();");
            }
        } catch (PinforException ex) {
            utilities.addFacesMessage(LOGGER, Constants.ERROR, "Se ha producido un error al grabar el Parte: " + utilities.getExceptionCause(ex));
        }
    }

    public void save() {
        try {
            String error = Constants.EMPTY_STR;
            String fileName, fileType;
            int pos;
            PinDAO.saveParte(selectedParte);
            for (String name : saveUploadedFiles()) {
                error += name + "\n";

                pos = uploadedFile.getFileName().lastIndexOf('.');
                fileName = name.substring(0, pos);
                fileType = name.substring(pos, name.length());
                this.selectedAdjunto = new PinParteAdjunto(fileName, fileType);
                this.selectedAdjunto.setParte(selectedParte);

                this.selectedParte.getAdjuntos().remove(this.selectedAdjunto);
            }
            filesToBeUploaded.clear();
            for (String f : filesToBeRemoved) {
                try {
                    // Que el ID sea positivo indica que el docAdjunto ya ha sido grabado, así como su archivo
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(this.selectedParte.getFechaRegistro());
                    int year = cal.get(Calendar.YEAR);
                    File path = new File(utilities.getProperty("/default.properties", "filesPath") + "/" + year);
                    File file = new File(path.getAbsolutePath(), f);
                    file.delete();
                } catch (IOException ex) {
                    utilities.addFacesMessage(LOGGER, Constants.ERROR, String.format("Se ha producido un error al eliminar el archivo asociado al documento adjunto %s.", this.selectedAdjunto.getTitulo()));
                }
            }
            filesToBeRemoved.clear();
            this.permitirGrabar = false;
            if (!error.equals(Constants.EMPTY_STR)) {
                utilities.addFacesMessage(LOGGER, Constants.ERROR, "El parte ha sido creado correctamente, pero se ha producido un error al subir los siguientes documentos adjuntos:\n " + error);
            }
        } catch (PinforException ex) {
            utilities.addFacesMessage(LOGGER, Constants.ERROR, "Se ha producido un error al grabar el Parte: " + utilities.getExceptionCause(ex));
        }
    }

    public void actualizarAccion() {
        puedeEnviarPortafirmas = false;
        switch (accion) {
            case Constants.DEVOLVER_REMITENTE:
                selectedParte.removeDestinatario(utilities.getCurrentUser().getUnidad());
                try {
                    PinMiembro situacionEstafeta = PinDAO.getMiembroById(Integer.valueOf(utilities.getProperty("/default.properties", "situacionEstafeta")));
                    for (PinObjeto objeto : selectedParte.getObjetos()) {
                        PinObjetoSituacion sit = new PinObjetoSituacion(situacionEstafeta, utilities.getCurrentUser().getId().getUsuario());
                        if (objeto.getSituaciones().contains(sit)) {
                            objeto.getSituaciones().remove(sit);
                        }
                    }
                } catch (IOException ex) {
                    utilities.addFacesMessage(LOGGER, Constants.ERROR, "Se ha producido un error al grabar el Parte: " + utilities.getExceptionCause(ex));
                }
                break;

            case Constants.REMITIR_UNIDAD_DESTINO:
                //TODO: Remitir a unidad de destino
                break;

            case Constants.REMITIR_REGISTRO_JEFATURA:
                if (selectedParte.getTipo().equals(PinParte.INFORME) && !selectedParte.tieneInformeFirmado()) {
                    this.accion = Constants.EMPTY_STR;
                    utilities.addFacesMessage(LOGGER, Constants.WARN, "El informe que trata de enviar a Registro de Jefatura debe contener una copia firmada electrónicamente y no se ha detectado ninguna entre sus documentos adjuntos.</br>"
                            + "Por favor, imprima el informe, remítalo al mando de la Unidad a través de la aplicación Portafirmas y adjunte la copia firmada marcándola como 'Contiene firma electrónica'");
                } else {
                    selectedParte.addEstado(PinParte.EST_ENVIADO_REGISTRO);

                    try {
                        PinMiembro situacionEstafeta = PinDAO.getMiembroById(Integer.valueOf(utilities.getProperty("/default.properties", "situacionEstafeta")));
                        for (PinObjeto objeto : selectedParte.getObjetos()) {
                            PinObjetoSituacion sit = new PinObjetoSituacion(situacionEstafeta, utilities.getCurrentUser().getId().getUsuario());
                            if (objeto.enOficina(PinDAO.getMiembroById(Integer.valueOf(utilities.getProperty("/default.properties", "situacionInicialObjeto")))) && !objeto.getSituaciones().contains(sit)) {
                                objeto.addToHistoric(sit);
                            }
                        }
                    } catch (IOException ex) {
                        utilities.addFacesMessage(LOGGER, Constants.ERROR, "Se ha producido un error al grabar el Parte: " + utilities.getExceptionCause(ex));
                    }
                }
                break;

            case Constants.ENVIAR_PORTA_FIRMAS:
                puedeEnviarPortafirmas = true;
                //TODO: Enviar a PortaFirmas
                break;

            case Constants.CONVERTIR_EN_INFORME:
                //TODO: Convertir en Informe
                break;
        }
    }

    public void addSituacionObjeto() {
        PinObjetoSituacion objSituacion = new PinObjetoSituacion(selectedSituacion, utilities.getCurrentUser().getId().getUsuario());
        selectedObjeto.addToHistoric(objSituacion);
        selectedSituacion = null;
    }

    public void resetSelectedSituacion() {
        selectedSituacion = null;
    }

    public void resetSelectedObjeto() {
        selectedObjeto = null;
    }

    public void printObjectTag(boolean todos) {
        try {
            makeReportEtiqueta(todos);
            utilities.executeUI("PF('dlgVerEtiqueta').show();");
        } catch (SQLException | NullPointerException | JRException | IOException | URISyntaxException ex) {
            utilities.addFacesMessage(LOGGER, Constants.ERROR, utilities.getExceptionCause(ex) == null ? "Se ha producido un error durante la generación de la etiqueta" : utilities.getExceptionCause(ex).toString());
        } catch (PinforException ex) {
            utilities.addFacesMessage(LOGGER, Constants.INFO, ex.getMessage());
        }
    }

    private void makeReportEtiqueta(boolean todos) throws SQLException, JRException, IOException, URISyntaxException, PinforException {
        reportsPath = utilities.getExternalContext().getRealPath("/").concat(Constants.REPORTS_PATH);

        File pdfFile = File.createTempFile("etiquetaObjeto", ".pdf");
        pdfFile.deleteOnExit();
        pdfFileName = pdfFile.getAbsolutePath();

        JasperPrint jasperPrint;
        JasperReport jrInforme;

        String jasperFileInforme = reportsPath.concat("etiquetaObjeto.jasper");

        HashMap parameters = new HashMap();

        if (!new File(jasperFileInforme).exists()) {
            compileJasperFiles();
        }
        jrInforme = (JasperReport) JRLoader.loadObject(new BufferedInputStream(new FileInputStream(jasperFileInforme)));

        List<PinObjeto> ordenes = new ArrayList<>();
        if (todos) {
            List<PinObjeto> objetos = selectedParte.getObjetos();
            for (PinObjeto objeto : objetos) {
                if (objeto.enOficina(PinDAO.getMiembroById(Integer.valueOf(utilities.getProperty("/default.properties", "situacionInicialObjeto"))))) {
                    ordenes.add(objeto);
                }
            }
            if (ordenes.isEmpty()) {
                throw new PinforException("Ninguno de los objetos entregados permanecen en la Oficina.");
            }
        } else {
            ordenes.add(selectedObjeto);
        }

        JRBeanCollectionDataSource jrDataSource = new JRBeanCollectionDataSource(ordenes);
        jasperPrint = JasperFillManager.fillReport(jrInforme, parameters, jrDataSource);
        JasperExportManager.exportReportToPdfFile(jasperPrint, pdfFileName);

    }

    public void resetPatronBusqueda() {
        resetSearchFields();
        scanInput = Constants.EMPTY_STR;
        patronBusqueda.setTipo("PI");
    }

    public void novedades() throws PinforException {
        // Si pasa la validación de fechas de impresión (todas son iguales o todas son nulas), 
        // pasamos a comprobar en qué caso nos encontramos, generando la relación de novedades o
        // recuperando la existente...
        if (checkFechasImpresion()) {
            // En este punto, como todas las fechas de impresión son la misma, nos vale con 
            // tomar la del primer elemento para tomar una decisión...
            if (selectedParteChecked.get(0).getFechaImpresion() == null) {
                // Nos aseguramos que los partes seleccionados para incluir en la Relación de Novedades
                // no estén anulados, pertenezcan a la Unidad del usuario, no estén en la Estafeta
                // y no estén remitidos a sus destinos.
                if (puedeImprimirNovedades()) {
                    partesNovedades.clear();
                    partesNovedades.addAll(selectedParteChecked);

                    // Filtra el conjunto de partes a incluir en las novedades a partir de todos aquellos que no estén archivados para el destino
                    // que corresponde a la propia Oficina de la Unidad tramitadora. 
                    // Después lo ordena según su orden natural (idParte); orden que corresponde con el de la fechaRegistro. 
                    partesNovedades.stream()
                            .filter(pn -> pn.getDestinos().stream().anyMatch(d -> d.getDestino().equals(pn.getUnidad()) && !d.getArchivado()))
                            .sorted();

                    utilities.executeUI("PF('dlgNovedades').show();");
                } else {
                    utilities.executeUI("PF('dlgNovedadesWarning').show();");
                }
            } else {
                // Si la fecha de impresión no es nula es porque los partes seleccionados (puede ser sólo uno)
                // ya se han incluido en una Relación de Novedades, en cuyo caso se procede a generar directamente
                // la Relación en la que se incluyó originalmente. 
                // No obstante, como puede que el parte no forme parte de una consulta donde se hayan mostrado todos
                // los partes que componían la misma relación de novedades, se vuelve a efectuar una consulta de
                // de la relación a partir de la fecha de impresión del parte seleccionado.
                try {
                    Calendar fecha = Calendar.getInstance(Constants.LOCALE_SPANISH);
                    fecha.setTimeInMillis(selectedParteChecked.get(0).getFechaImpresion().getTime());
                    fechaImpresion = fecha.getTime();
                    PinParteAux patron = new PinParteAux(PinDAO.getMiembroById(Long.parseLong(utilities.getProperty("/default.properties", "denunciado"))), utilities.getCurrentUser());
                    patron.setUnidad(utilities.getCurrentUser().getUnidad());
                    patron.setFechaImpresion(fecha.getTime());

                    List<PinParteAux> relacionNovedades = PinDAO.searchPartes(patron, true, null, null, new PinActuante(), Constants.EMPTY_STR, Constants.EMPTY_STR, null, true, true, 0, 0, utilities.getCurrentUser());
                    partesNovedades.clear();
                    partesNovedades.addAll(relacionNovedades);

                    // Filtra el conjunto de partes a incluir en las novedades a partir de todos aquellos que no estén archivados para el destino
                    // que corresponde a la propia Oficina de la Unidad tramitadora. 
                    // Después lo ordena según su orden natural (idParte); orden que corresponde con el de la fechaRegistro. 
                    partesNovedades.stream()
                            .filter(pn -> pn.getDestinos().stream().anyMatch(d -> d.getDestino().equals(pn.getUnidad()) && !d.getArchivado()))
                            .sorted();

                    generateReportNovedades(false);
                } catch (IOException ex) {
                    utilities.addFacesMessage(LOGGER, Constants.ERROR, utilities.getExceptionCause(ex).toString());
                }
            }
        }

    }

    public void resetFechaImpresion() {
        this.fechaImpresion = null;
    }

    public String getTipoInforme() {
        String result = "Documento";

        if (selectedParte.getTipo().equals("PI")) {
            result = "Parte";
        } else if (selectedParte.getTipo().equals("IF")) {
            result = "Informe";
        }
        return result;
    }

    public void useFileName() {

        if (this.uploadedFile != null && !this.useFileName) {
            if (!this.useFileName) {
                int pos = uploadedFile.getFileName().lastIndexOf('.');
                String fileName = uploadedFile.getFileName().substring(0, pos);
                String fileType = uploadedFile.getFileName().substring(pos, uploadedFile.getFileName().length());
                this.selectedAdjunto.setTitulo(fileName);
                this.selectedAdjunto.setTipo(fileType);
            }
        } else {
            this.selectedAdjunto.setTitulo(Constants.EMPTY_STR);
        }
    }

    public void unselectAdjunto() {
        this.uploadedFile = null;
        for (Map.Entry<String, File> entry : this.filesToBeUploaded.entrySet()) {
            if (entry.getKey().equals(this.selectedAdjunto.getTitulo() + this.selectedAdjunto.getTipo())) {
                entry.getValue().delete();
            }
        }
        this.selectedAdjunto = null;
    }

    public void anadirAdjunto() {
        // Añadimos el objeto Adjunto creado al principio a la lista de documentos adjuntos del parte.
        if (!selectedParte.getAdjuntos().contains(selectedAdjunto)) {
            selectedParte.getAdjuntos().add(selectedAdjunto);
            filteredAdjuntos.add(selectedAdjunto);
        } else {
            utilities.addFacesMessage(LOGGER, Constants.WARN, "Ya existe un documento adjunto con el mismo nombre que el que acaba de seleccionar.");
        }
        selectedAdjunto = null;
        uploadedFile = null;
    }

    public boolean isHayCambios() {
        return !filesToBeUploaded.isEmpty() || !filesToBeRemoved.isEmpty() || this.permitirGrabar;
    }

    public void quitarCambios() {
        this.permitirGrabar = false;
        filesToBeUploaded.entrySet().forEach((Map.Entry<String, File> entry) -> {
            selectedParte.getAdjuntos().removeIf((adjunto) -> adjunto.getFileName().equals(entry.getKey()));
        });
        filesToBeUploaded.clear();
        filesToBeRemoved.clear();
    }

    public void updateOrdenesServicio() {
        // this.permitirGrabar = true;
        if (showTodasOrdenes) {
            vigencia.add(PinOrdenServicio.VENCIDA);
        } else {
            vigencia.remove(PinOrdenServicio.VENCIDA);
        }
        this.ordenesServicio = PinDAO.getOrdenesTrabajo(vigencia, selectedParte.getOrdenServicio(), utilities.getCurrentUser().getUnidad());
    }

    public Boolean getPuedeAnadirDestinatarios() {
        return selectedParte.getListoTramitar();
    }

    public void resetSelectedDestinatario() {
        this.selectedDestinatario = null;
    }

    public void setModificado() {
        this.permitirGrabar = true;
    }
}
