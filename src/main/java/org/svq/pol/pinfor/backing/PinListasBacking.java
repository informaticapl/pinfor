package org.svq.pol.pinfor.backing;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import org.apache.log4j.Logger;
import org.hibernate.StaleObjectStateException;
import org.svq.pol.exception.PinforException;
import org.svq.pol.pinfor.bean.PinLista;
import org.svq.pol.pinfor.bean.PinMiembro;
import org.svq.pol.pinfor.dao.PinDAO;
import org.svq.pol.pinfor.utility.Constants;

/**
 *
 * @author jjcampac
 */
@ApplicationScoped
@Named
public class PinListasBacking extends AbstractBacking implements Serializable {

    private static final long serialVersionUID = 5339711978794979621L;

    private static final Logger LOGGER = Logger.getLogger(PinListasBacking.class);

    private static List<PinLista> listas;
    private Map<String, List<PinMiembro>> situaciones;
    private PinMiembro selectedMiembro;

    private PinLista selectedLista;
    private int indexSelectedMiembro;

    static {
        listas = new ArrayList<>();
    }

    //<editor-fold defaultstate="collapsed" desc="constructors">
    public PinListasBacking() {

    }

    @PostConstruct
    protected void onConstruct() {
        try {
            /*
            * Las listas a mostrar en los distintos desplegables se enuentran
            * en una misma tabla (bueno, en realidad en dos Listas y Miembros).
            * Para cargar todas las listas creadas en el constructor se obtiene
            * una lista con todos estos tipos a traves del metodo getListas especificas de la clase DAO
            * correspondiente para esta aplicacion.
             */
            selectedLista = new PinLista();

            selectedMiembro = null;
            situaciones = new HashMap<>();
            listas = PinDAO.getListasEspecificas();
        } catch (Exception ex) {
            utilities.addFacesMessage(LOGGER, Constants.ERROR, "Se ha producido un error en la inicialización de listas: " + utilities.getExceptionCause(ex));
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="getters & setters">
    public int getIndexSelectedMiembro() {
        return indexSelectedMiembro;
    }

    public void setIndexSelectedMiembro(int indexSelectedMiembro) {
        this.indexSelectedMiembro = indexSelectedMiembro;
    }

    public List<PinLista> getListas() {
        return listas;
    }

    public static synchronized void setListas(List<PinLista> listas) {
        PinListasBacking.listas = listas;
    }

    public Map<String, List<PinMiembro>> getSituaciones() {
        return situaciones;
    }

    public void setSituaciones(Map<String, List<PinMiembro>> situaciones) {
        this.situaciones = situaciones;
    }

    public PinLista getSelectedLista() {
        return selectedLista;
    }

    public void setSelectedLista(PinLista selectedLista) {
        this.selectedLista = selectedLista;
    }

    public PinMiembro getSelectedMiembro() {
        return selectedMiembro;
    }

    public void setSelectedMiembro(PinMiembro selectedMiembro) {
        this.selectedMiembro = selectedMiembro;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="gestión de listas">
    /**
     * Recarga el contenido de las lista desde la base de datos.
     */
    public void refreshListas() {
        int idx = listas.indexOf(selectedLista);
        listas = null;
        onConstruct();
        resetSelectedMiembro();
        selectedLista = listas.get(idx);
    }

    public void reorderSelectedLista() {
        try {
            List<PinMiembro> miembros = selectedLista.getMiembros();
            int orden = 1;
            for (PinMiembro miembro : miembros) {
                miembro.setOrden(orden);
                orden++;
            }
            PinDAO.saveLista(selectedLista);
            utilities.addFacesMessage(LOGGER, Constants.INFO, "La reordenación de la lista se ha guardado correctamente.");
        } catch (Exception ex) {
            utilities.addFacesMessage(LOGGER, Constants.ERROR, "Se ha producido un error durante la grabación de la lista: " + utilities.getExceptionCause(ex));
        }
    }

    public static PinLista getListaByCodigo(String codigo) {
        PinLista result = null;
        boolean found = false;
        Iterator it = listas.iterator();
        while (!found && it.hasNext()) {
            PinLista l = (PinLista) it.next();
            if (l.getIdLista().equals(codigo)) {
                result = l;
                found = true;
            }
        }
        return result;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="gestion de miembros">
    public void resetSelectedLista() {
        selectedLista = new PinLista();
    }

    public void newMiembro() {
        this.selectedMiembro = new PinMiembro();
    }

    public void setMiembro() {
        // Ver código javascript definido en default.js y cargado en onload de la vista listas.xhtml
        indexSelectedMiembro = Integer.parseInt(utilities.getFacesContext().getExternalContext().getRequestParameterMap().get("selectedIndex"));
        selectedMiembro = selectedLista.getMiembros().get(indexSelectedMiembro);
    }

    public void resetSelectedMiembro() {
        selectedMiembro = null;
    }

    public static PinMiembro getMiembroById(long id) {
        if (listas.isEmpty()) {
            listas = PinDAO.getListasEspecificas();
        }
        Iterator itLista = listas.iterator();
        boolean found = false;
        PinMiembro result = null;
        while (itLista.hasNext() && !found) {
            PinLista lista = (PinLista) itLista.next();
            Iterator it = lista.getMiembros().iterator();
            while (it.hasNext() && !found) {
                PinMiembro miembro = (PinMiembro) it.next();
                if (miembro.getIdMiembro() == id) {
                    result = miembro;
                    found = true;
                }
            }
        }
        return result;
    }

    public void addMiembro() {

        try {
            selectedMiembro.setOrden(selectedLista.getMiembros().size() + 1);
            selectedMiembro.setLista(selectedLista);
            selectedLista.addMiembro(selectedMiembro);
            PinDAO.saveLista(selectedLista);
            utilities.addFacesMessage(LOGGER, Constants.INFO, "El miembro " + selectedMiembro.getNombre() + " de la lista " + selectedLista.getDescripcion() + " se insertó correctamente");
            refreshListas();
            //Cerramos el dialog-box solamente si todo ha ido bien.
            utilities.executeUI("PF('dlgAddMiembro').hide()");
        } catch (StaleObjectStateException ex) {
            selectedLista.getMiembros().remove(selectedMiembro);
            utilities.addFacesMessage(LOGGER, Constants.WARN, "Los datos que trata de guardar ya han sido modificados por otro usuario. Por favor, pulse el botón 'Refrescar' para obtener información actualizada.");
        } catch (Exception ex) {
            selectedLista.getMiembros().remove(selectedMiembro);
            utilities.addFacesMessage(LOGGER, Constants.WARN, "El elemento no pudo ser añadido a la lista. Por favor, vuelva a intentarlo.");
        }
    }

    public void modifyMiembro() {
        try {
            PinDAO.saveLista(selectedLista);
            utilities.addFacesMessage(LOGGER, Constants.INFO, "El elemento seleccionado ha sido modificado a: " + selectedMiembro.getNombre() + " correctamente.");
            resetSelectedMiembro();
        } catch (StaleObjectStateException ex) {
            utilities.addFacesMessage(LOGGER, Constants.WARN, "Los datos que trata de guardar ya han sido modificados por otro usuario. Por favor, pulse el botón 'Refrescar' para obtener información actualizada.");
        } catch (Exception ex) {
            utilities.addFacesMessage(LOGGER, Constants.WARN, "El elemento no pudo ser modificado. Por favor, vuelva a intentarlo. " + (ex.getMessage() == null ? "" : ex.getMessage()));
        }
    }

    public void deleteMiembro() {
        try {
            selectedLista.getMiembros().remove(selectedMiembro);
            PinDAO.deleteMiembro(selectedMiembro);
            utilities.addFacesMessage(LOGGER, Constants.INFO, "El elemento " + selectedMiembro.getNombre() + " ha sido eliminado correctamente.");
            resetSelectedMiembro();
        } catch (StaleObjectStateException ex) {
            utilities.addFacesMessage(LOGGER, Constants.WARN, "Los datos que trata de borrar ya han sido modificados por otro usuario. Por favor, pulse el botón 'Refrescar' para obtener información actualizada.");
        } catch (PinforException ex) {
            utilities.addFacesMessage(LOGGER, Constants.WARN, "El elemento no pudo ser borrado de la lista. Asegúrese que no está siendo usado y vuelva a intentarlo. " + ex.getMessage());
        }
    }
    //</editor-fold>

}
