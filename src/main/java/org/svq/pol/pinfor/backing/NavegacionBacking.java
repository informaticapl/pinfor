package org.svq.pol.pinfor.backing;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.util.logging.Level;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.spi.CDI;
import javax.faces.context.ExternalContext;
import javax.inject.Named;
import org.apache.log4j.Logger;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.svq.pol.exception.NoItemSelectedException;
import org.svq.pol.pinfor.utility.Constants;

/**
 *
 * @author jjcamacho
 */
@SessionScoped
@Named
public class NavegacionBacking extends AbstractBacking implements Serializable {

    private static final long serialVersionUID = -6922236661901615945L;
    private static final Logger LOGGER = Logger.getLogger(NavegacionBacking.class);

    private String pagina;
    private String editMenuItemSelected;
    private boolean canDelete;
    private boolean canModify;
    private boolean canCreate;
    private boolean administrator;
    private boolean supervisor;
    private int sessionTime;
    private String version, buildTime;

    private static String helpFilesPath;
    private String pdfFileName;
    String titulo;

    //<editor-fold defaultstate="collapsed" desc="constructors">
    public NavegacionBacking() {

    }

    @PostConstruct
    public void onConstruct() {
        try {
            /*
             * Por defecto es falso, ya que la presentación de la vista se hace sin
             * ningún policía seleccionado. Cuando se seleccione alguno (método
             * plantillaBacking.onSelectNode()) el campo volverá a actualizarse con
             * el valor que le corresponda.
             */
            canDelete = false;
            canModify = false;
            canCreate = this.userCan("create");
            administrator = this.userIs("administrator");
            supervisor = this.userIs("supervisor");

            pagina = "/WEB-INF/formularios/home.xhtml";
            sessionTime = Integer.valueOf(utilities.getProperty("/default.properties", "sessionTime"));

            version = "v".concat(utilities.getProperty("/default.properties", "version"));
            buildTime = utilities.getProperty("/default.properties", "buildTime");

            if (!canUseBrowser()) {
                utilities.getFacesContext().getApplication().getNavigationHandler().handleNavigation(utilities.getFacesContext(), null, "/warning?faces-redirect=true");
            }
        } catch (NoItemSelectedException | IOException ex) {
            utilities.addFacesMessage(LOGGER, Constants.ERROR, ex.getMessage());
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="getters & setters">   
    public String getPagina() {
        return pagina;
    }

    public void setPagina(String pagina) {
        this.pagina = pagina;
    }

    public boolean isCanCreate() {
        return canCreate;
    }

    public void setCanCreate(boolean canCreate) {
        this.canCreate = canCreate;
    }

    public boolean isCanDelete() {
        return canDelete;
    }

    public void setCanDelete(boolean canDelete) {
        this.canDelete = canDelete;
    }

    public boolean isCanModify() {
        return canModify;
    }

    public void setCanModify(boolean canModify) {
        this.canModify = canModify;
    }

    public String getEditMenuItemSelected() {
        return editMenuItemSelected;
    }

    public void setEditMenuItemSelected(String editMenuItemSelected) {
        this.editMenuItemSelected = editMenuItemSelected;
    }

    public boolean isAdministrator() {
        return administrator;
    }

    public void setAdministrator(boolean administrator) {
        this.administrator = administrator;
    }

    public boolean isSupervisor() {
        return supervisor;
    }

    public void setSupervisor(boolean supervisor) {
        this.supervisor = supervisor;
    }

    public int getSessionTime() {
        return sessionTime * 1000;
    }

    public String getPdfFileName() {
        return pdfFileName;
    }

    public String getBuildTime() {
        return buildTime;
    }

    public String getVersion() {
        return version;
    }
    //</editor-fold>

    public void closeSession() {
        try {
            utilities.cancelUser(null);
        } catch (IOException ex) {
            utilities.addFacesMessage(LOGGER, Constants.ERROR, utilities.getExceptionCause(ex).toString());
        }
    }

    public void keepSessionAlive() {
    }

    public StreamedContent getStream() throws IOException {
        ExternalContext ec = utilities.getExternalContext();
        ec.addResponseHeader("Cache-Control", "no-cache");
        ec.addResponseHeader("Pragma", "no-cache");
        ec.addResponseHeader("Expires", "-1");
        ec.addResponseHeader("Title", titulo);
        StreamedContent sc = null;
        try {
            sc = new DefaultStreamedContent(new FileInputStream(pdfFileName), "application/pdf", titulo);
        } catch (FileNotFoundException ex) {
            System.out.println(ex.getMessage());
        }
        return sc;
    }

    public void showAyuda(int tipo) {
        File pdfFile = null;
        helpFilesPath = utilities.getExternalContext().getRealPath("/").concat(Constants.HELP_PATH);
        switch (tipo) {
            case 1:
                this.titulo = "Manual básico".concat(".pdf");
                pdfFile = new File(helpFilesPath.concat("manual_basico.pdf"));
                break;
            case 2:
                this.titulo = "Manual oficina".concat(".pdf");
                pdfFile = new File(helpFilesPath.concat("manual_oficina.pdf"));
                break;
            case 3:
                this.titulo = "Manual estafeta".concat(".pdf");
                pdfFile = new File(helpFilesPath.concat("manual_estafeta.pdf"));
                break;
        }
        if (pdfFile != null) {
            pdfFileName = pdfFile.getAbsolutePath();
            utilities.executeUI("PF('dlgVerAyuda').show();");
        } else {

        }
    }

}
