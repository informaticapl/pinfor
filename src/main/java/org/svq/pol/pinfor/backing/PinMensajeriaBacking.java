package org.svq.pol.pinfor.backing;

import com.google.gson.Gson;
import java.io.Serializable;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.log4j.Logger;
import org.omnifaces.cdi.Push;
import org.omnifaces.cdi.PushContext;
import org.omnifaces.cdi.ViewScoped;
import org.svq.pol.pinfor.utility.Constants;
import org.svq.pol.pinfor.utility.Message;

/**
 *
 * @author jjcamacho
 */
@Named
@ViewScoped
public class PinMensajeriaBacking extends AbstractBacking implements Serializable {

    private static final Logger LOGGER = Logger.getLogger(PinMensajeriaBacking.class);
    private static final long serialVersionUID = 621049503466834769L;

    private String mensaje;
    
    @Inject @Push
    private PushContext notificationChannel;

    //<editor-fold defaultstate="collapsed" desc="constructors">
    public PinMensajeriaBacking() {
        this.mensaje = Constants.EMPTY_STR;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="getters & setters">
    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public PushContext getNotificationChannel() {
        return notificationChannel;
    }
    //</editor-fold>

    public void send() {
        final Gson gson = new Gson();
        Message message = new Message(null, mensaje);
        notificationChannel.send(gson.toJson(message));

        clean();

    }

    public void clean() {
        this.mensaje = Constants.EMPTY_STR;
    }
}
