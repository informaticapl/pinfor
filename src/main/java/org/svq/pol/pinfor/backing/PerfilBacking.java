package org.svq.pol.pinfor.backing;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import javax.inject.Named;
import org.apache.commons.lang.SerializationUtils;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.omnifaces.cdi.ViewScoped;
import org.svq.pol.pinfor.bean.InfAplicacion;
import org.svq.pol.pinfor.bean.InfContenidoPerfil;
import org.svq.pol.pinfor.bean.InfContenidoPerfilPK;
import org.svq.pol.pinfor.bean.InfPerfil;
import org.svq.pol.pinfor.dao.PinDAO;
import org.svq.pol.pinfor.utility.Constants;
import org.svq.pol.pinfor.utility.comparator.AplicacionNombreComparator;

/**
 *
 * @author jjcamacho
 */
@ViewScoped
@Named
public class PerfilBacking extends AbstractBacking implements Serializable {

    private static final long serialVersionUID = 3961006119975097698L;
    private static final Logger LOGGER = Logger.getLogger(PerfilBacking.class);

    private InfPerfil selectedPerfil;
    private InfPerfil nuevoPerfil;
    private InfContenidoPerfil selectedContenido;
    private InfContenidoPerfil nuevoContenido;
    private InfPerfil patronBusqueda;
    private InfAplicacion nuevaAplicacion;
    private List<InfPerfil> resultadoBusqueda;
    private List<InfAplicacion> listaAplicaciones;

    //<editor-fold defaultstate="collapsed" desc="constructors">
    public PerfilBacking() {
        patronBusqueda = new InfPerfil();
        resultadoBusqueda = new ArrayList<>();
        nuevoPerfil = new InfPerfil();
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="getters & setters">
    public InfAplicacion getNuevaAplicacion() {
        return nuevaAplicacion;
    }

    public void setNuevaAplicacion(InfAplicacion nuevaAplicacion) {
        this.nuevaAplicacion = nuevaAplicacion;
    }

    public InfPerfil getSelectedPerfil() {
        return selectedPerfil;
    }

    public void setSelectedPerfil(InfPerfil selectedPerfil) {
        this.selectedPerfil = selectedPerfil;
    }

    public InfContenidoPerfil getSelectedContenido() {
        return selectedContenido;
    }

    public void setSelectedContenido(InfContenidoPerfil selectedContenido) {
        this.selectedContenido = selectedContenido;
    }

    public InfPerfil getNuevoPerfil() {
        return nuevoPerfil;
    }

    public void setNuevoPerfil(InfPerfil nuevoPerfil) {
        this.nuevoPerfil = nuevoPerfil;
    }

    public InfPerfil getPatronBusqueda() {
        return patronBusqueda;
    }

    public void setPatronBusqueda(InfPerfil patronBusqueda) {
        this.patronBusqueda = patronBusqueda;
    }

    public List<InfPerfil> getResultadoBusqueda() {
        return resultadoBusqueda;
    }

    public void setResultadoBusqueda(List<InfPerfil> resultadoBusqueda) {
        this.resultadoBusqueda = resultadoBusqueda;
    }

    public List<InfAplicacion> getListaAplicaciones() {
        if (listaAplicaciones == null) {
            try {
                listaAplicaciones = PinDAO.getAllApplications();
                Collections.sort(listaAplicaciones, new AplicacionNombreComparator());
            } catch (HibernateException ex) {
                utilities.addFacesMessage(LOGGER, Constants.ERROR, "Se ha producido un error al obtener la lista de aplicaciones: " + utilities.getExceptionCause(ex));
            } catch (Exception ex) {
                utilities.addFacesMessage(LOGGER, Constants.ERROR, "Se ha producido un error al obtener la lista de aplicaciones: " + utilities.getExceptionCause(ex));
            }
        }

        return listaAplicaciones;
    }

    public void setListaAplicaciones(List<InfAplicacion> listaAplicaciones) {
        this.listaAplicaciones = listaAplicaciones;
    }

    public InfContenidoPerfil getNuevoContenido() {
        return nuevoContenido;
    }

    public void setNuevoContenido(InfContenidoPerfil nuevoContenido) {
        this.nuevoContenido = nuevoContenido;
    }
    //</editor-fold>

    public void search() {
        try {
            selectedPerfil = null;
            resultadoBusqueda = PinDAO.searchPerfil(patronBusqueda);
        } catch (Exception ex) {
            utilities.addFacesMessage(LOGGER, Constants.ERROR, "Se ha producido un error en la consulta: " + utilities.getExceptionCause(ex));
        }
    }

    //<editor-fold defaultstate="collapsed" desc="CRUD perfil">
    public void createProfile() {
        nuevoPerfil = new InfPerfil();
    }

    public void addProfile() {
        /* Una vez grabado el perfil, se vuelve a lanzar la búsqueda, lo que permitirá listar el nuevo objeto
         * en el resultado si es que coincide con el patrón de búsqueda.
         */
        try {
            PinDAO.savePerfil(nuevoPerfil);
            search();
            utilities.executeUI("PF('dlgAnadirPerfil').hide()");
        } catch (Exception ex) {
            utilities.addFacesMessage(LOGGER, Constants.ERROR, "Se ha producido un error al grabar el perfil: " + utilities.getExceptionCause(ex));
        }
    }

    public void editProfile() {
        /* Creamos un objeto nuevoPerfil que es copia del perfil seleccionado y que será sobre el
         * que se hagan las modificaciones. Así, si el usuario decide descartar los
         * cambios, con desechar este nuevo objeto es suficiente. */
        nuevoPerfil = (InfPerfil) SerializationUtils.clone(selectedPerfil);
    }

    public void modifyProfile() {
        /* Si el usuario acepta las modificaciones, llamamos al método DAO update pasándole tanto el perfil actualmente
         * seleccionado como el nuevo que contiene los cambios. Esto se debe a que deberemos recorrer todo el conjunto
         * de contenidoPerfil del perfil original para ver si alguno de los elementos ha sido eliminado y proceder a
         * su borrado de la BD manualmente. 
         * Después se vuelve a lanzar la consulta original. */
        try {
            PinDAO.updatePerfil(nuevoPerfil);
            search();
            selectedContenido = null;
            utilities.executeUI("PF('dlgEditarPerfil').hide()");
        } catch (Exception ex) {
            utilities.addFacesMessage(LOGGER, Constants.ERROR, "Se ha producido un error al grabar los cambios: " + utilities.getExceptionCause(ex));
        }
    }

    public void deleteProfile() {
        try {
            PinDAO.deletePerfil(selectedPerfil);
            search();
            utilities.executeUI("PF('dlgBorrarPerfil').hide()");
        } catch (Exception ex) {
            utilities.addFacesMessage(LOGGER, Constants.ERROR, "Se ha producido un error al eliminar el perfil: " + utilities.getExceptionCause(ex));
        }
    }

    public void resetSelectedProfile() {
        selectedPerfil = new InfPerfil();
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="CRUD contenido perfil">
    public void createProfileContent() {
        nuevoContenido = new InfContenidoPerfil();
    }

    public void addProfileContent() {
        try {
            // Primero comprobamos que la aplicación aún no exista en el perfil....
            boolean found = false;
            Iterator it = nuevoPerfil.getContenidoPerfiles().iterator();
            while (!found && it.hasNext()) {
                found = ((InfContenidoPerfil) it.next()).getAplicacion().equals(nuevoContenido.getAplicacion());
            }

            //...y dependiendo de esto se incluirá en el mismo o se informará al usuario de que no puede incluir una aplicación ya existente.
            if (!found) {
                long idPerfil = nuevoPerfil.getIdPerfil();
                long idAplicacion = nuevoContenido.getAplicacion().getIdAplicacion();
                nuevoContenido.setId(new InfContenidoPerfilPK(idPerfil, idAplicacion));
                nuevoContenido.setPerfil(nuevoPerfil);
                nuevoPerfil.getContenidoPerfiles().add(nuevoContenido);
                nuevoContenido = new InfContenidoPerfil();
                utilities.executeUI("PF('dlgAnadirContenidoPerfil').hide()");
            } else {
                utilities.addFacesMessage(LOGGER, Constants.WARN, "La aplicación que trata de añadir ya se encuentra en el perfil.");
            }
        } catch (Exception ex) {
            utilities.addFacesMessage(LOGGER, Constants.ERROR, "Se ha producido un error al añadir la aplicación: " + utilities.getExceptionCause(ex));
        }
    }

    public void modifyProfileContent() {

        try {
            utilities.executeUI("PF('dlgEditarContenidoPerfil').hide()");
        } catch (Exception ex) {
            utilities.addFacesMessage(LOGGER, Constants.ERROR, "Se ha producido un error al grabar los cambios: " + utilities.getExceptionCause(ex));
        }
    }

    public void deleteProfileContent() {
        try {
            selectedContenido.setPerfil(null);
            nuevoPerfil.getContenidoPerfiles().remove(selectedContenido);
            utilities.executeUI("PF('dlgBorrarContenidoPerfil').hide()");
        } catch (Exception ex) {
            utilities.addFacesMessage(LOGGER, Constants.ERROR, "Se ha producido un error al eliminar el perfil: " + utilities.getExceptionCause(ex));
        }
    }

    public void resetSelectedContenido() {
        selectedContenido = null;
    }
    //</editor-fold>

    public void resetSelectedAplicacion() {
        nuevaAplicacion = new InfAplicacion();
    }
}
