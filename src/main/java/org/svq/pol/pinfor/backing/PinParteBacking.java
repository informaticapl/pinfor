package org.svq.pol.pinfor.backing;

import com.google.gson.reflect.TypeToken;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.net.URISyntaxException;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.inject.Named;
import javax.xml.crypto.MarshalException;
import javax.xml.crypto.dsig.XMLSignatureException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;
import org.apache.log4j.Logger;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.map.MarkerDragEvent;
import org.primefaces.event.map.OverlaySelectEvent;
import org.primefaces.event.map.PointSelectEvent;
import org.primefaces.event.map.StateChangeEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.primefaces.model.map.DefaultMapModel;
import org.primefaces.model.map.LatLng;
import org.primefaces.model.map.MapModel;
import org.primefaces.model.map.Marker;
import org.svq.pol.exception.PinforException;
import org.svq.pol.http.HttpCaller;
import org.svq.pol.json.GesateRestResponse;
import org.svq.pol.pinfor.bean.GenMiembro;
import org.svq.pol.pinfor.bean.PerPersona;
import org.svq.pol.pinfor.bean.PinActuante;
import org.svq.pol.pinfor.bean.PinDestino;
import org.svq.pol.pinfor.bean.PinHistoricoImpresion;
import org.svq.pol.pinfor.bean.PinLugar;
import org.svq.pol.pinfor.bean.PinMiembro;
import org.svq.pol.pinfor.bean.PinObjeto;
import org.svq.pol.pinfor.bean.PinObjetoSituacion;
import org.svq.pol.pinfor.bean.PinOrdenServicio;
import org.svq.pol.pinfor.bean.PinParte;
import org.svq.pol.pinfor.bean.PinParteAdjunto;
import org.svq.pol.pinfor.bean.PinPersona;
import org.svq.pol.pinfor.bean.auxiliar.ParteReport;
import org.svq.pol.pinfor.bean.auxiliar.PinOrdenServicioAux;
import org.svq.pol.pinfor.bean.auxiliar.PinParteAux;
import org.svq.pol.pinfor.bean.auxiliar.PinParteLazyDataModel;
import org.svq.pol.pinfor.dao.PinDAO;
import org.svq.pol.pinfor.utility.Constants;
import org.svq.pol.pinfor.utility.Log;
import org.svq.pol.pinfor.utility.MyMarker;
import org.xml.sax.SAXException;

/**
 *
 * @author jjcamacho
 */
@SessionScoped
@Named
public class PinParteBacking extends AbstractBacking implements Serializable {

    private static final Logger LOGGER = Logger.getLogger(PinParteBacking.class);
    private static final long serialVersionUID = 3574496630916541352L;

    private PinParteAux selectedParte;
    private PinParteAux selectedSubparte;
    private PinOrdenServicioAux selectedOrdenTrabajo;
    private PinParteAux patronBusqueda;
    private String vigencia;
    private List<PinParteAux> resultadoBusqueda;
    private LazyDataModel<PinParteAux> lazyResultadoBusqueda;
    private List<PinParteAux> selectedParteChecked;
    private PinActuante selectedActuante, actuante;
    private PinDestino selectedDestinatario;
    private PinMiembro selectedDestino;
    private List<PinMiembro> asuntos;
    private List<PinMiembro> subasuntos;
    private List<PinMiembro> tiposObjetos;
    private List<PinParteAdjunto> filteredAdjuntos;
    private List<PinMiembro> destinos;
    private List<PinMiembro> estados;
    private List<PinMiembro> participaciones;
    private PinParteAdjunto selectedAdjunto;
    private StreamedContent zipFile;
    private PerPersona selectedPersona;
    private PinPersona selectedResenaPersona;
    private PinObjeto selectedResenaObjeto;
    private String msgAmbito;
    private Boolean firstLoad, nuevoElemento;

    private Date fechaDesde, fechaHasta;
    private String accion, paginaActual;

    private Integer agno;
    private List<Integer> availableYears;

    private List<PinLugar> vias;
    private PinLugar selectedVia;
    private String via, numeroVia;

    private List<GenMiembro> unidades;
    private List<PinOrdenServicioAux> ordenesTrabajo;
    Set<Boolean> vigente;

    private String center;
    private double selectedLat, selectedLng;
    private MapModel mapModel;
    private static MyMarker marker;
    private int zoom;

    private HttpCaller httpCaller;
    private String permantentTicket;

    private Boolean useFileName;
    private UploadedFile uploadedFile;
    private File tmpFile;
    private Map<String, File> filesToBeUploaded;
    private Set<String> filesToBeRemoved;
    private Boolean esPdf;
    private String selectedImagePath;

    private int maxHour, maxMinute;
    private Integer estado;
    private Integer tipoNumeroLugar;
    private String dni;
    private String persona;

    private String scanInput;

    private String pdfFileName;
    private static String reportsPath;
    private String msgRegistro;

    private PinMiembro selectedAsunto, selectedSubasunto;
    private List<GenMiembro> destJefatura;
    private List<GenMiembro> destAyuntamiento;
    private List<GenMiembro> destExterno;
    private List<GenMiembro> destinosDisponibles;
    private String origenDestinoDisponible;
    private GenMiembro selectedDestinatarioDisponible;
    private PinDestino selectedDestinatarioAsignado;
    private List<PinDestino> copiaDestinatariosAsignados;

    //<editor-fold defaultstate="collapsed" desc="constructors">
    public PinParteBacking() {

    }

    @PostConstruct
    protected void onConstruct() {

        try {
            vigente = new HashSet<>();
            vigente.add(PinOrdenServicio.VIGENTE);
            selectedSubasunto = null;
            selectedDestinatarioDisponible = null;
            selectedDestinatarioAsignado = null;
            this.origenDestinoDisponible = Constants.DESTINO_JEFATURA;

            this.asuntos = PinDAO.getMiembros("ASU");
            this.ordenesTrabajo = PinDAO.getOrdenesTrabajo(vigente, null, utilities.getCurrentUser().getUnidad());
            this.tiposObjetos = PinDAO.getMiembros("TOB");
            this.destinos = PinDAO.getMiembros("DES");
            this.estados = PinDAO.getMiembros("EST");
            //this.estados = this.estados.stream().sorted((e1,e2) -> e1.getNombre().compareTo(e2.getNombre())).collect(Collectors.toList());
            this.estados = this.estados.stream().sorted(Comparator.comparing(PinMiembro::getNombre)).collect(Collectors.toList());
            this.participaciones = PinDAO.getMiembros("PAR");
            destJefatura = PinDAO.getMiembrosGenericos("DJF");
            destAyuntamiento = PinDAO.getMiembrosGenericos("DAY");
            destExterno = PinDAO.getMiembrosGenericos("DEX");
            if (paginaActual != null && (paginaActual.equals("parte") || paginaActual.equals("informe"))) {
                this.selectedParte = new PinParteAux(Calendar.getInstance(Constants.LOCALE_SPANISH).getTime(), utilities.getCurrentUser());
            } else {
                this.selectedParte = new PinParteAux(PinDAO.getMiembroById(Long.parseLong(utilities.getProperty("/default.properties", "denunciado"))), utilities.getCurrentUser());
            }

            this.selectedSubparte = new PinParteAux(PinDAO.getMiembroById(Long.parseLong(utilities.getProperty("/default.properties", "denunciado"))), utilities.getCurrentUser());

            this.agno = Calendar.getInstance(Constants.LOCALE_SPANISH).get(Calendar.YEAR);
            this.availableYears = new ArrayList<>();
            for (int i = 2019; i <= this.agno; i++) {
                this.availableYears.add(i);
            }

            if (utilities.getCurrentUser().getGlobal()) {
                this.msgAmbito = "Jefatura";
            } else if (utilities.getCurrentUser().getRegional()) {
                this.msgAmbito = "Unidad";
            } else {
                this.msgAmbito = "Usuario";
            }

            this.firstLoad = true;
            this.selectedParteChecked = new ArrayList<>();
            this.patronBusqueda = new PinParteAux(PinDAO.getMiembroById(Long.parseLong(utilities.getProperty("/default.properties", "denunciado"))), utilities.getCurrentUser());
            this.patronBusqueda.setUnidad(utilities.getCurrentUser().getUnidad());

            this.resultadoBusqueda = new ArrayList<>();
            this.filteredAdjuntos = new ArrayList<>();
            this.subasuntos = new ArrayList<>();
            this.unidades = PinDAO.getUnidades();
            this.actuante = new PinActuante();
            this.vigencia = "todas";
            this.mapModel = new DefaultMapModel();
            this.center = Constants.DEFAULT_LAT.concat(",").concat(Constants.DEFAULT_LNG);
            this.zoom = 13;
            this.useFileName = true;
            this.filesToBeUploaded = new HashMap<>();
            this.filesToBeRemoved = new HashSet<>();
            this.estado = PinParte.EST_BORRADOR;
            this.selectedVia = null;
            this.dni = Constants.EMPTY_STR;
            this.persona = Constants.EMPTY_STR;

            this.permantentTicket = utilities.getProperty("/default.properties", "ticket");
        } catch (IOException | NumberFormatException ex) {
            utilities.addFacesMessage(LOGGER, Constants.WARN, ex.getMessage());
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="getters & setters">
    public Integer getAgno() {
        return agno;
    }

    public void setAgno(Integer agno) {
        this.agno = agno;
    }

    public String getMsgRegistro() {
        return msgRegistro;
    }

    public Boolean getEsPdf() {
        return esPdf;
    }

    public String getSelectedImagePath() {
        return selectedImagePath;
    }

    public String getAccion() {
        return accion;
    }

    public List<PinMiembro> getParticipaciones() {
        return participaciones;
    }

    public List<Integer> getAvailableYears() {
        return availableYears;
    }

    public void setAvailableYears(List<Integer> availableYears) {
        this.availableYears = availableYears;
    }

    public PinDestino getSelectedDestinatario() {
        return selectedDestinatario;
    }

    public void setSelectedDestinatario(PinDestino selectedDestinatario) {
        this.selectedDestinatario = selectedDestinatario;
    }

    public PinOrdenServicioAux getSelectedOrdenTrabajo() {
        return selectedOrdenTrabajo;
    }

    public void setSelectedOrdenTrabajo(PinOrdenServicioAux selectedOrdenTrabajo) {
        this.selectedOrdenTrabajo = selectedOrdenTrabajo;
    }

    public String getTipoInforme() {
        String result = "Documento";

        if (paginaActual != null) {
            switch (paginaActual) {
                case "parte":
                    result = "Parte";
                    break;
                case "informe":
                    result = "Informe";
                    break;
                case "ordenanza":
                    result = "Ordenanza";
                    break;
                case "acredito":
                    result = "Acredito";
                    break;
                default:
                    if (selectedParte.getTipo().equals("PI")) {
                        result = "Parte";
                    } else if (selectedParte.getTipo().equals("IF")) {
                        result = "Informe";
                    } else if (selectedParte.getTipo().equals("OM")) {
                        result = "Ordenanza Municipal";
                    } else if (selectedParte.getTipo().equals("AC")) {
                        result = "Acredito";
                    }
            }
        }
        return result;
    }

    public Integer getTipoNumeroLugar() {
        return tipoNumeroLugar;
    }

    public void setTipoNumeroLugar(Integer tipoNumeroLugar) {
        this.tipoNumeroLugar = tipoNumeroLugar;
    }

    public String getNumeroVia() {
        return numeroVia;
    }

    public void setNumeroVia(String numeroVia) {
        this.numeroVia = numeroVia;
    }

    public Boolean getFirstLoad() {
        return firstLoad;
    }

    public void setFirstLoad(Boolean firstLoad) {
        this.firstLoad = firstLoad;
    }

    public PinActuante getSelectedActuante() {
        return selectedActuante;
    }

    public void setSelectedActuante(PinActuante selectedActuante) {
        this.selectedActuante = selectedActuante;
    }

    public PinMiembro getSelectedDestino() {
        return selectedDestino;
    }

    public void setSelectedDestino(PinMiembro selectedDestino) {
        this.selectedDestino = selectedDestino;
    }

    public PinActuante getActuante() {
        return actuante;
    }

    public void setActuante(PinActuante actuante) {
        this.actuante = actuante;
    }

    public PinParteAux getSelectedParte() {
        return selectedParte;
    }

    public void setSelectedParte(PinParteAux selectedParte) {
        this.selectedParte = selectedParte;
    }

    public PinParteAux getSelectedSubparte() {
        return selectedSubparte;
    }

    public void setSelectedSubparte(PinParteAux selectedSubparte) {
        this.selectedSubparte = selectedSubparte;
    }

    public PinParteAux getPatronBusqueda() {
        return patronBusqueda;
    }

    public void setPatronBusqueda(PinParteAux patronBusqueda) {
        this.patronBusqueda = patronBusqueda;
    }

    public List<PinParteAux> getResultadoBusqueda() {
        return resultadoBusqueda;
    }

    public void setResultadoBusqueda(List<PinParteAux> resultadoBusqueda) {
        this.resultadoBusqueda = resultadoBusqueda;
    }

    public LazyDataModel<PinParteAux> getLazyResultadoBusqueda() {
        return lazyResultadoBusqueda;
    }

    public void setLazyResultadoBusqueda(LazyDataModel<PinParteAux> lazyResultadoBusqueda) {
        this.lazyResultadoBusqueda = lazyResultadoBusqueda;
    }

    public List<PinParteAux> getSelectedParteChecked() {
        return selectedParteChecked;
    }

    public void setSelectedParteChecked(List<PinParteAux> selectedParteChecked) {
        this.selectedParteChecked = selectedParteChecked;
    }

    public String getUrlBase() {
        return utilities.getUrlBase();
    }

    public Boolean getCanSave() {
        return selectedParte.getAsunto() != null && selectedParte.getAsunto().getIdMiembro() > -1L
                && selectedParte.getSubasunto() != null && selectedParte.getSubasunto().getIdMiembro() > -1L
                && selectedParte.getLugar().getNombreLugar() != null && !selectedParte.getLugar().getNombreLugar().equals(Constants.EMPTY_STR)
                && selectedParte.getFechaSuceso() != null
                && !selectedParte.getActuantes().isEmpty()
                && selectedParte.getInforme() != null && !selectedParte.getInforme().isEmpty();
    }

    public Boolean getCanSaveOrdenanza() {
        return selectedParte.getAsunto() != null && selectedParte.getAsunto().getIdMiembro() > -1L
                && selectedParte.getSubasunto() != null && selectedParte.getSubasunto().getIdMiembro() > -1L
                && selectedParte.getLugar().getNombreLugar() != null && !selectedParte.getLugar().getNombreLugar().equals(Constants.EMPTY_STR)
                && selectedParte.getFechaSuceso() != null
                && selectedParte.getInforme() != null && !selectedParte.getInforme().trim().isEmpty()
                && selectedParte.getReferenciaOrigen() != null && !selectedParte.getReferenciaOrigen().trim().isEmpty()
                && !selectedParte.getActuantes().isEmpty()
                && !selectedParte.getPersonas().isEmpty()
                && !selectedParte.getAdjuntos().isEmpty();
    }

    public Boolean getCanSaveAcredito() {
        return selectedParte.getAsunto() != null && selectedParte.getAsunto().getIdMiembro() > -1L
                && selectedParte.getSubasunto() != null && selectedParte.getSubasunto().getIdMiembro() > -1L
                && selectedParte.getLugar().getNombreLugar() != null && !selectedParte.getLugar().getNombreLugar().equals(Constants.EMPTY_STR)
                && selectedParte.getFechaSuceso() != null
                && selectedParte.getInforme() != null && !selectedParte.getInforme().trim().isEmpty()
                && !selectedParte.getActuantes().isEmpty()
                && !selectedParte.getPersonas().isEmpty()
                && !selectedParte.getAdjuntos().isEmpty();
    }

    public Boolean getCanEdit() {
        return selectedParte.getIdParte() > 0
                && utilities.getCurrentUser().getModificaciones()
                && !selectedParte.getAnulado()
                && !selectedParte.getListoTramitar();
    }

    public Boolean getCanEditOrdenanza() {
        return selectedParte.getIdParte() > 0
                && utilities.getCurrentUser().getModificaciones()
                && !selectedParte.getAnulado()
                && !selectedParte.getEnviadoRegistro();
    }

    public Boolean getCanAdd() {
        return utilities.getCurrentUser().getAltas();
    }

    public Boolean getCanDelete() {
        return selectedParte.getIdParte() > 0
                && utilities.getCurrentUser().getBajas()
                && !selectedParte.getAnulado()
                && !selectedParte.getListoTramitar();
    }

    public Boolean getCanDeleteAcredito() {
        return selectedParte.getIdParte() > 0
                && utilities.getCurrentUser().getBajas()
                && !selectedParte.getAnulado()
                && !selectedParte.getEnviadoRegistro();
    }

    public Boolean getCanPrint() {
        return selectedParte.getIdParte() > 0 && utilities.getCurrentUser().getModificaciones() && !selectedParte.getAnulado();
    }

    public String getVigencia() {
        return vigencia;
    }

    public void setVigencia(String vigencia) {
        this.vigencia = vigencia;
    }

    public List<GenMiembro> getUnidades() {
        return unidades;
    }

    public void setUnidades(List<GenMiembro> unidades) {
        this.unidades = unidades;
    }

    public Date getHoy() {
        return new Date();
    }

    public List<PinMiembro> getAsuntos() {
        return asuntos;
    }

    public List<PinMiembro> getSubasuntos() {
        return subasuntos;
    }

    public List<PinOrdenServicioAux> getOrdenesTrabajo() {
        return ordenesTrabajo;
    }

    public List<PinMiembro> getDestinos() {
        return destinos;
    }

    public List<PinMiembro> getEstados() {
        return estados;
    }

    public List<PinMiembro> getTiposObjetos() {
        return tiposObjetos;
    }

    public List<PinLugar> getVias() {
        return vias;
    }

    public void setVias(List<PinLugar> vias) {
        this.vias = vias;
    }

    public PinLugar getSelectedVia() {
        return selectedVia;
    }

    public void setSelectedVia(PinLugar selectedVia) {
        this.selectedVia = selectedVia;
    }

    public String getVia() {
        return via;
    }

    public void setVia(String via) {
        this.via = via;
    }

    public String getCenter() {
        return center;
    }

    public void setCenter(String center) {
        this.center = center;
    }

    public double getSelectedLat() {
        return selectedLat;
    }

    public double getSelectedLng() {
        return selectedLng;
    }

    public MapModel getMapModel() {
        return mapModel;
    }

    public void setMapModel(MapModel mapModel) {
        this.mapModel = mapModel;
    }

    public static MyMarker getMarker() {
        return marker;
    }

    public static void setMarker(MyMarker marker) {
        PinParteBacking.marker = marker;
    }

    public List<PinParteAdjunto> getFilteredAdjuntos() {
        return filteredAdjuntos;
    }

    public void setFilteredAdjuntos(List<PinParteAdjunto> filteredAdjuntos) {
        this.filteredAdjuntos = filteredAdjuntos;
    }

    public PinParteAdjunto getSelectedAdjunto() {
        return selectedAdjunto;
    }

    public void setSelectedAdjunto(PinParteAdjunto selectedAdjunto) {
        this.selectedAdjunto = selectedAdjunto;
    }

    public PerPersona getSelectedPersona() {
        return selectedPersona;
    }

    public void setSelectedPersona(PerPersona selectedPersona) {
        this.selectedPersona = selectedPersona;
    }

    public PinPersona getSelectedResenaPersona() {
        return selectedResenaPersona;
    }

    public void setSelectedResenaPersona(PinPersona selectedResenaPersona) {
        this.selectedResenaPersona = selectedResenaPersona;
    }

    public PinObjeto getSelectedResenaObjeto() {
        return selectedResenaObjeto;
    }

    public void setSelectedResenaObjeto(PinObjeto selectedResenaObjeto) {
        this.selectedResenaObjeto = selectedResenaObjeto;
    }

    public Date getFechaDesde() {
        return fechaDesde;
    }

    public void setFechaDesde(Date fechaDesde) {
        this.fechaDesde = fechaDesde;
    }

    public Date getFechaHasta() {
        return fechaHasta;
    }

    public void setFechaHasta(Date fechaHasta) {
        this.fechaHasta = fechaHasta;
    }

    public int getZoom() {
        return zoom;
    }

    public void setZoom(int zoom) {
        this.zoom = zoom;
    }

    public String getMsgAmbito() {
        return msgAmbito;
    }

    public Boolean getUseFileName() {
        return useFileName;
    }

    public void setUseFileName(Boolean useFileName) {
        this.useFileName = useFileName;
    }

    public UploadedFile getUploadedFile() {
        return uploadedFile;
    }

    public void setUploadedFile(UploadedFile uploadedFile) {
        this.uploadedFile = uploadedFile;
    }

    public Map<String, File> getFilesToBeUploaded() {
        return filesToBeUploaded;
    }

    public void setFilesToBeUploaded(Map<String, File> filesToBeUploaded) {
        this.filesToBeUploaded = filesToBeUploaded;
    }

    public int getMaxHour() {
        return maxHour;
    }

    public int getMaxMinute() {
        return maxMinute;
    }

    public String getPdfFileName() {
        return pdfFileName;
    }

    public void setPdfFileName(String pdfFileName) {
        this.pdfFileName = pdfFileName;
    }

    public static String getReportsPath() {
        return reportsPath;
    }

    public static void setReportsPath(String reportsPath) {
        PinParteBacking.reportsPath = reportsPath;
    }

    public Integer getEstado() {
        return estado;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }

    public String getPersona() {
        return persona;
    }

    public void setPersona(String persona) {
        this.persona = persona;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getScanInput() {
        return scanInput;
    }

    public void setScanInput(String scanInput) {
        this.scanInput = scanInput;
    }

    public List<GenMiembro> getDestinosDisponibles() {
        switch (this.origenDestinoDisponible) {
            case Constants.DESTINO_JEFATURA:
                this.destinosDisponibles = this.destJefatura;
                break;
            case Constants.DESTINO_AYUNTAMIENTO:
                this.destinosDisponibles = this.destAyuntamiento;
                break;
            case Constants.DESTINO_EXTERNO:
                this.destinosDisponibles = this.destExterno;
                break;
            default:
                this.destinosDisponibles = new ArrayList<>();
        }
        return this.destinosDisponibles;
    }

    public void setDestinosDisponibles(List<GenMiembro> destinosDisponibles) {
        this.destinosDisponibles = destinosDisponibles;
    }

    public String getOrigenDestinoDisponible() {
        return origenDestinoDisponible;
    }

    public void setOrigenDestinoDisponible(String origenDestinoDisponible) {
        this.origenDestinoDisponible = origenDestinoDisponible;
    }

    public GenMiembro getSelectedDestinatarioDisponible() {
        return selectedDestinatarioDisponible;
    }

    public void setSelectedDestinatarioDisponible(GenMiembro selectedDestinatarioDisponible) {
        this.selectedDestinatarioDisponible = selectedDestinatarioDisponible;
    }

    public PinDestino getSelectedDestinatarioAsignado() {
        return selectedDestinatarioAsignado;
    }

    public void setSelectedDestinatarioAsignado(PinDestino selectedDestinatarioAsignado) {
        this.selectedDestinatarioAsignado = selectedDestinatarioAsignado;
    }

    public PinMiembro getSelectedAsunto() {
        return selectedAsunto;
    }

    public void setSelectedAsunto(PinMiembro selectedAsunto) {
        this.selectedAsunto = selectedAsunto;
    }

    public PinMiembro getSelectedSubasunto() {
        return selectedSubasunto;
    }

    public void setSelectedSubasunto(PinMiembro selectedSubasunto) {
        this.selectedSubasunto = selectedSubasunto;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="private methods">
    private void resetMap() {
        this.center = Constants.DEFAULT_LAT.concat(",").concat(Constants.DEFAULT_LNG);
        this.zoom = 13;
        this.mapModel.getMarkers().clear();
    }

    private GesateRestResponse<?> enviarPeticionCalle(String nombreVia) throws InvocationTargetException, IllegalAccessException,
            IllegalArgumentException, IOException, ClassNotFoundException, InstantiationException, FileNotFoundException,
            NoSuchAlgorithmException, CertificateException, UnrecoverableKeyException, ParserConfigurationException,
            SAXException, TransformerException, TransformerConfigurationException, KeyStoreException,
            MarshalException, XMLSignatureException, InvalidAlgorithmParameterException, UnsupportedOperationException, KeyManagementException, PinforException {

        String urlSuffix = "atestadofcm";
        GesateRestResponse gesateResponse;

        String url = String.format("%s/%s/buscardireccion/%s/%s/%s", Constants.WEBSERVICE_GAT_URI, urlSuffix, nombreVia, utilities.getCurrentUser().getId().getUsuario(), this.permantentTicket);
        this.httpCaller = new HttpCaller();
        gesateResponse = httpCaller.doGet(url, new TypeToken<GesateRestResponse<PinLugar>>() {
        });
        if (gesateResponse.getStatus() != Constants.RESULT_OK) {
            throw new PinforException(gesateResponse.getMessage());
        }

        return gesateResponse;
    }

    private Set<String> saveUploadedFiles() {
        int pos;
        Set<String> errors = new HashSet<>();
        if (!filesToBeUploaded.isEmpty()) {
            for (Map.Entry<String, File> entry : filesToBeUploaded.entrySet()) {
                String fileName = entry.getKey();
                File file = entry.getValue();
                pos = fileName.lastIndexOf('.');
                for (PinParteAdjunto adjunto : this.selectedParte.getAdjuntos()) {
                    if (fileName.equals(adjunto.getFileName())) {
                        try {
                            utilities.saveUploadedFile(file, String.valueOf(adjunto.getIdAdjunto()), fileName.substring(pos, fileName.length()), this.selectedParte.getFechaRegistro());
                        } catch (IOException ex) {
                            errors.add(fileName);
                        }
                        break;
                    }
                }
            }
        }
        return errors;
    }

    private static void compileJasperFiles() throws IOException, JRException {
        File folder = new File(reportsPath);
        FilenameFilter jasperFilter = (File directory, String fileName) -> directory.equals(folder) && fileName.endsWith(".jrxml");

        File[] xmlFiles = folder.listFiles(jasperFilter);
        for (File file : xmlFiles) {
            String xmlFile = file.getCanonicalPath();
            try {
                Log.log(LOGGER, Constants.INFO, "Procesando el archivo: " + xmlFile);
                String jasperFile = file.getCanonicalPath().replace(".jrxml", ".jasper");
                JasperCompileManager.compileReportToFile(xmlFile, jasperFile);
            } catch (JRException ex) {
                throw new JRException("\nProcesando el archivo: " + xmlFile + "\n\n" + ex.getMessage());
            }
        }

    }

    private void makeReportParte() throws SQLException, JRException, IOException, URISyntaxException {
        reportsPath = utilities.getExternalContext().getRealPath("/").concat(Constants.REPORTS_PATH);

        File pdfFile = File.createTempFile("informe", ".pdf");
        pdfFile.deleteOnExit();
        pdfFileName = pdfFile.getAbsolutePath();

        JasperPrint jasperPrint;
        JasperReport jrInforme;

        String jasperFileInforme = Constants.EMPTY_STR;
        switch (selectedParte.getTipo()) {
            case PinParte.PARTE:
                jasperFileInforme = reportsPath.concat("parte.jasper");
                break;
            case PinParte.INFORME:
                jasperFileInforme = reportsPath.concat("informe.jasper");
                break;
            case PinParte.ACREDITO:
                jasperFileInforme = reportsPath.concat("acredito.jasper");
                break;
        }

        HashMap parameters = new HashMap();

        parameters.put("UNIDAD", selectedParte.getUnidad() != null ? selectedParte.getUnidad().getNombre() : "Unidad no definida");

        if (!new File(jasperFileInforme).exists()) {
            compileJasperFiles();
        }
        jrInforme = (JasperReport) JRLoader.loadObject(new BufferedInputStream(new FileInputStream(jasperFileInforme)));

        if (jasperFileInforme.contains("parte") || jasperFileInforme.contains("acredito")) {
            List<ParteReport> partes = new ArrayList<>();
            partes.add(new ParteReport(selectedParte));

            JRBeanCollectionDataSource jrDataSource = new JRBeanCollectionDataSource(partes);
            jasperPrint = JasperFillManager.fillReport(jrInforme, parameters, jrDataSource);
            JasperExportManager.exportReportToPdfFile(jasperPrint, pdfFileName);
        } else if (jasperFileInforme.contains("informe")) {
            List<PinDestino> dest = new ArrayList<>();
            selectedParte.getDestinos().forEach((destino) -> {
                dest.add(destino);
            });
            JRBeanCollectionDataSource jrDataSource = new JRBeanCollectionDataSource(dest);
            jasperPrint = JasperFillManager.fillReport(jrInforme, parameters, jrDataSource);
            JasperExportManager.exportReportToPdfFile(jasperPrint, pdfFileName);
        }
    }

    private void makeReportOrdenTrabajo() throws SQLException, JRException, IOException, URISyntaxException {
        reportsPath = utilities.getExternalContext().getRealPath("/").concat(Constants.REPORTS_PATH);

        File pdfFile = File.createTempFile("ordenServicio", ".pdf");
        pdfFile.deleteOnExit();
        pdfFileName = pdfFile.getAbsolutePath();

        JasperPrint jasperPrint;
        JasperReport jrInforme;

        String jasperFileInforme = reportsPath.concat("orden.jasper");

        HashMap parameters = new HashMap();

        parameters.put("UNIDAD", selectedParte.getOrdenServicio().getUnidad() != null ? selectedParte.getOrdenServicio().getUnidad().getNombre() : "Unidad no definida");

        if (!new File(jasperFileInforme).exists()) {
            compileJasperFiles();
        }
        jrInforme = (JasperReport) JRLoader.loadObject(new BufferedInputStream(new FileInputStream(jasperFileInforme)));

        List<PinOrdenServicio> ordenes = new ArrayList<>();
        ordenes.add(selectedParte.getOrdenServicio());

        JRBeanCollectionDataSource jrDataSource = new JRBeanCollectionDataSource(ordenes);
        jasperPrint = JasperFillManager.fillReport(jrInforme, parameters, jrDataSource);
        JasperExportManager.exportReportToPdfFile(jasperPrint, pdfFileName);

    }

    private void comprobarCheck() {

        if (selectedParteChecked.size() == 1) {
            selectedParte = selectedParteChecked.get(0);
        } else {
            try {
                selectedParte = new PinParteAux(PinDAO.getMiembroById(Long.parseLong(utilities.getProperty("/default.properties", "denunciado"))), utilities.getCurrentUser());
                this.selectedParte.addOficinaUnidad(PinParte.EST_BORRADOR);
            } catch (IOException ex) {
                utilities.addFacesMessage(LOGGER, Constants.ERROR, utilities.getExceptionCause(ex).toString());
            }
        }
    }
    //</editor-fold>

    public void abrir() {
        SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyyHHmmss");
        try {
            if (!scanInput.isEmpty()) {
                String[] parte = scanInput.split(" ");
                if (parte.length < 3) {
                    throw new PinforException("El código de barras escaneado no corresponde a un Parte o Informe.");
                } else {
                    agno = Integer.valueOf(parte[0]);
                    patronBusqueda.setTipo(parte[1]);
                    patronBusqueda.setNumeroParte(Integer.valueOf(parte[2]));
                    scanInput = Constants.EMPTY_STR;
                }
            }
            // Para poder abrir el registro por su número, independientemente de la fecha y el estado del mismo (borrador u enviado a oficina).
            fechaDesde = sdf.parse(String.format("0101%d000000", agno));
            fechaHasta = sdf.parse(String.format("3112%d235959", agno));
            this.estado = null;
            search(true, true, false);
            if (lazyResultadoBusqueda.getRowCount() != 0) {

                // Como la carga de la tabla no se hace hasta que se genera la vista, es necesario forzar la misma
                // desde aquí para obtener el primer registro, que podamos referenciar como selectedParte
                DataTable lazyTable;
                if (paginaActual != null && paginaActual.equals("ordenanza")) {
                    lazyTable = (DataTable) utilities.getFacesContext().getViewRoot().findComponent("fMain:tOrdenanza");
                } else if (paginaActual != null && paginaActual.equals("acredito")) {
                    lazyTable = (DataTable) utilities.getFacesContext().getViewRoot().findComponent("fMain:tAcredito");
                } else {
                    lazyTable = (DataTable) utilities.getFacesContext().getViewRoot().findComponent("fMain:tParte");
                }

                lazyTable.loadLazyData();

                this.selectedParte = lazyResultadoBusqueda.getWrappedData().get(0);
                if (this.selectedParteChecked != null) {
                    this.selectedParteChecked.clear();
                }
                this.selectedParteChecked.add(selectedParte);
                selectRow();
            }

            resetPatronBusqueda();
        } catch (ParseException ex) {
            utilities.addFacesMessage(LOGGER, Constants.ERROR, "Se ha producido un error en la consulta: " + utilities.getExceptionCause(ex));
        } catch (PinforException ex) {
            utilities.addFacesMessage(LOGGER, Constants.WARN, ex.getMessage());
        }
    }

    public void search(Boolean resetMap, boolean resetSelectedParte, boolean evaluarFechaImpresion) {
        try {
            this.selectedParteChecked.clear();
            if (resetSelectedParte) {
                this.selectedParte = new PinParteAux(PinDAO.getMiembroById(Long.parseLong(utilities.getProperty("/default.properties", "denunciado"))), utilities.getCurrentUser());
                this.selectedParte.addOficinaUnidad(PinParte.EST_BORRADOR);
            }
            if (paginaActual != null && paginaActual.equals("ordenanza")) {
                patronBusqueda.setTipo(PinParte.ORDENANZA);
                this.estado = null;
            } else if (paginaActual != null && paginaActual.equals("acredito")) {
                patronBusqueda.setTipo(PinParte.ACREDITO);
                this.estado = null;
            } else if (patronBusqueda.getTipo().equals(PinParte.ORDENANZA)) {
                patronBusqueda.setTipo(Constants.EMPTY_STR);
            }

            lazyResultadoBusqueda = new PinParteLazyDataModel.Builder(patronBusqueda)
                    .fechaDesde(fechaDesde)
                    .fechaHasta(fechaHasta)
                    .consultaOficina(false)
                    .evaluarFechaImpresion(evaluarFechaImpresion)
                    .estado(estado)
                    .actuante(actuante)
                    .dni(dni)
                    .persona(persona)
                    .currentUser(utilities.getCurrentUser())
                    .build();

            if (resetMap) {
                resetMap();
            }

            if (lazyResultadoBusqueda.getRowCount() == 0) {
                utilities.addFacesMessage(LOGGER, Constants.INFO, "No se han encontrado datos coincidentes con la consulta dentro de su ámbito de búsqueda. <br/><br/>"
                        + "Pruebe a redefinir los términos de la búsqueda y/o marcar la casilla Mostrar anulados<br/><br/>"
                        + "ÁMBITO: " + this.msgAmbito);
            }
        } catch (IOException | NumberFormatException ex) {
            utilities.addFacesMessage(LOGGER, Constants.ERROR, "Se ha producido un error en la consulta: " + utilities.getExceptionCause(ex));
        }
    }

    public void buscarVia() {
        try {
            this.vias = (List<PinLugar>) enviarPeticionCalle(this.via).getEntities();
        } catch (InvocationTargetException | IllegalAccessException | IllegalArgumentException | IOException | ClassNotFoundException | InstantiationException | NoSuchAlgorithmException | CertificateException | UnrecoverableKeyException | ParserConfigurationException | SAXException | TransformerException | KeyStoreException | MarshalException | XMLSignatureException | InvalidAlgorithmParameterException | UnsupportedOperationException | KeyManagementException ex) {
            utilities.addFacesMessage(LOGGER, Constants.ERROR, ex.getMessage());
        } catch (PinforException ex) {
            utilities.addFacesMessage(LOGGER, Constants.INFO, ex.getMessage());
        }
    }

    public void cargarDestinatariosPredefinidos() {
        this.selectedParte.getDestinos().clear();

        this.selectedParte.getSubasunto().getDestinos().forEach((destinatario) -> {
            this.selectedParte.addDestinatario(new PinDestino(destinatario));
        });
    }

    public void saveBorrador(Boolean searchAfterSave) {
        /* Una vez grabada el PinParteAux se vuelve a lanzar la búsqueda, lo que permitirá listar el nuevo objeto
         * en el resultado si es que coincide con el patrón de búsqueda. */
        try {
            String error = Constants.EMPTY_STR;
            String fileName, fileType;
            int pos;

            if (paginaActual.equals("parte")) {
                selectedParte.setTipo(PinParte.PARTE);
            }
            if (paginaActual.equals("informe")) {
                selectedParte.setTipo(PinParte.INFORME);
            }

            PinDAO.saveParte(selectedParte);
            for (String name : saveUploadedFiles()) {
                error += name + "\n";

                pos = name.lastIndexOf('.');
                fileName = name.substring(0, pos);
                fileType = name.substring(pos, name.length());
                this.selectedAdjunto = new PinParteAdjunto(fileName, fileType);

                this.selectedParte.getAdjuntos().remove(this.selectedAdjunto);
            }
            if (searchAfterSave) {
                search(false, false, false);
            }
            filesToBeUploaded.clear();
            filesToBeRemoved.forEach((String f) -> {
                try {
                    // Que el ID sea positivo indica que el docAdjunto ya ha sido grabado, así como su archivo
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(this.selectedParte.getFechaRegistro());
                    int year = cal.get(Calendar.YEAR);
                    File path = new File(utilities.getProperty("/default.properties", "filesPath") + "/" + year);
                    File file = new File(path.getAbsolutePath(), f);
                    file.delete();
                } catch (IOException ex) {
                    utilities.addFacesMessage(LOGGER, Constants.ERROR, String.format("Se ha producido un error al eliminar el archivo asociado al documento adjunto %s.", this.selectedAdjunto.getTitulo()));
                }
            });
            filesToBeRemoved.clear();
            selectedParteChecked.clear();
            selectedParteChecked.add(selectedParte);
            if (!error.equals(Constants.EMPTY_STR)) {
                utilities.addFacesMessage(LOGGER, Constants.ERROR, "El parte ha sido creado correctamente, pero se ha producido un error al subir los siguientes documentos adjuntos:\n " + error);
            }
        } catch (PinforException ex) {
            utilities.addFacesMessage(LOGGER, Constants.ERROR, "Se ha producido un error al grabar el Parte: " + utilities.getExceptionCause(ex));
        }
    }

    public void saveParteyEnviarOficina() {
        /* Antes de grabar el parte se marca como Terminado, lo que equivale a 'Enviar a la Oficina', ya que 
         * serán los terminados los que puedan ser rescatados por la Oficina para su tramitación.
         * Una vez grabada el PinParteAux se vuelve a lanzar la búsqueda, lo que permitirá listar el nuevo objeto
         * en el resultado si es que coincide con el patrón de búsqueda. */
        try {
            String error = Constants.EMPTY_STR;
            String fileName, fileType;
            int pos;

            selectedParte.addEstado(PinParte.EST_LISTO_TRAMITAR);

            // Si es un parte referente a una orden de servicio, al mismo tiempo que se envía a la oficina, se marca como archivado.
            if (selectedParte.getTipo().equals("PI") && selectedParte.getOrdenServicio() != null) {
                selectedParte.addEstado(PinParte.EST_ARCHIVADO);
            }
            if (paginaActual.equals("parte")) {
                selectedParte.setTipo(PinParte.PARTE);
            }
            if (paginaActual.equals("informe")) {
                selectedParte.setTipo(PinParte.INFORME);
            }

            PinDAO.saveParte(selectedParte);
            for (String name : saveUploadedFiles()) {
                error += name + "\n";

                pos = uploadedFile.getFileName().lastIndexOf('.');
                fileName = name.substring(0, pos);
                fileType = name.substring(pos, name.length());
                this.selectedAdjunto = new PinParteAdjunto(fileName, fileType);

                this.selectedParte.getAdjuntos().remove(this.selectedAdjunto);
            }
            if (paginaActual.equals("parte") || paginaActual.equals("informe")) {
                this.selectedParte = new PinParteAux(Calendar.getInstance(Constants.LOCALE_SPANISH).getTime(), utilities.getCurrentUser());
            } else {
                this.selectedParte = new PinParteAux(PinDAO.getMiembroById(Long.parseLong(utilities.getProperty("/default.properties", "denunciado"))), utilities.getCurrentUser());
            }
            this.selectedParte.addOficinaUnidad(PinParte.EST_BORRADOR);

            search(true, true, false);

            filesToBeUploaded.clear();
            utilities.executeUI("PF('dlgEditarParte').hide();");
            selectedParteChecked.clear();
            selectedParteChecked.add(selectedParte);
            if (!error.equals(Constants.EMPTY_STR)) {
                utilities.addFacesMessage(LOGGER, Constants.ERROR, "El parte ha sido creado correctamente, pero se ha producido un error al subir los siguientes documentos adjuntos:\n " + error);
            }
        } catch (PinforException | IOException ex) {
            utilities.addFacesMessage(LOGGER, Constants.ERROR, "Se ha producido un error al grabar el Parte: " + utilities.getExceptionCause(ex));
        }
    }

    public void saveParteyNuevo() {
        try {
            String error = Constants.EMPTY_STR;
            String fileName, fileType;
            int pos;

            selectedParte.addEstado(PinParte.EST_LISTO_TRAMITAR);

            if (paginaActual.equals("parte")) {
                selectedParte.setTipo(PinParte.PARTE);
            }
            if (paginaActual.equals("informe")) {
                selectedParte.setTipo(PinParte.INFORME);
            }

            // Si es un parte referente a una orden de servicio, al mismo tiempo que se envía a la oficina, se marca como archivado.
            if (selectedParte.getTipo().equals("PI") && selectedParte.getOrdenServicio() != null) {
                selectedParte.addEstado(PinParte.EST_ARCHIVADO);
            }
            PinDAO.saveParte(selectedParte);
            utilities.addFacesMessage(LOGGER, Constants.INFO, "El parte ha sido registrado con el número " + selectedParte.getNumParteFormateado());
            for (String name : saveUploadedFiles()) {
                error += name + "\n";

                pos = uploadedFile.getFileName().lastIndexOf('.');
                fileName = name.substring(0, pos);
                fileType = name.substring(pos, name.length());
                this.selectedAdjunto = new PinParteAdjunto(fileName, fileType);

                this.selectedParte.getAdjuntos().remove(this.selectedAdjunto);
            }

            msgRegistro = String.format("El %s ha sido grabado con el número </br></br> <span style='font-size:large;color:blue'><strong>%s</strong></span>.",
                    selectedParte.getTipo().equals(PinParte.INFORME) ? "Informe" : "Parte",
                    selectedParte.getNumParteFormateado());
            resetNuevoParte();

            if (!error.equals(Constants.EMPTY_STR)) {
                utilities.addFacesMessage(LOGGER, Constants.ERROR, "El parte ha sido creado correctamente, pero se ha producido un error al subir los siguientes documentos adjuntos:\n " + error);
            }

        } catch (PinforException ex) {
            utilities.addFacesMessage(LOGGER, Constants.ERROR, "Se ha producido un error al grabar el Parte: " + utilities.getExceptionCause(ex));
        }
    }

    public void anularParte() {
        try {
            if (selectedParte.getListoTramitar()) {
                throw new PinforException("Un parte marcado como enviado a la Oficina no puede ser anulado.");
            }
            selectedParte.setAnulado(true);
            PinDAO.saveParte(selectedParte);
            search(true, true, false);
            utilities.executeUI("PF('dlgBorrarParte').hide();");
        } catch (PinforException ex) {
            utilities.addFacesMessage(LOGGER, Constants.WARN, ex.getMessage());
        }
    }

    public void anularAcredito() {
        try {
            if (selectedParte.getEnviadoRegistro()) {
                throw new PinforException("Un acredito que ya ha sido remitido a su solicitante no puede ser anulado.");
            }
            selectedParte.setAnulado(true);
            PinDAO.saveParte(selectedParte);
            search(false, true, false);
            utilities.executeUI("PF('dlgBorrarAcredito').hide();");
        } catch (PinforException ex) {
            utilities.addFacesMessage(LOGGER, Constants.WARN, ex.getMessage());
        }
    }

    public void resetSelectedParte() {
        try {
            selectedParte = new PinParteAux(PinDAO.getMiembroById(Long.parseLong(utilities.getProperty("/default.properties", "denunciado"))), utilities.getCurrentUser());
            this.selectedParte.addOficinaUnidad(PinParte.EST_BORRADOR);
        } catch (IOException ex) {
            utilities.addFacesMessage(LOGGER, Constants.ERROR, utilities.getExceptionCause(ex).toString());
        }
    }

    public void resetSelectedSubparte() {
        try {
            selectedSubparte = new PinParteAux(PinDAO.getMiembroById(Long.parseLong(utilities.getProperty("/default.properties", "denunciado"))), utilities.getCurrentUser());

        } catch (IOException ex) {
            utilities.addFacesMessage(LOGGER, Constants.ERROR, utilities.getExceptionCause(ex).toString());
        }
    }

    public void resetSelectedActuante() {
        this.selectedActuante = new PinActuante();
        this.selectedActuante.setParte(selectedParte);
    }

    public void resetSelectedDestino() {
        this.selectedDestino = null;
    }

    public void resetSelectedDestinatario() {
        this.selectedDestinatario = null;
    }

    public void deleteSelectedDestino() {
        this.selectedParte.getDestinos().remove(this.selectedDestinatario);
        this.selectedDestino = null;
        this.selectedDestinatario = new PinDestino();
    }

    public void resetSelectedAdjunto() {
        if (paginaActual.equals("ordenanza") && !selectedParte.getAdjuntos().isEmpty()) {
            utilities.addFacesMessage(LOGGER, Constants.INFO, "Cada reseña de ordenanza sólo puede llevar adjunta una imagen del acta de denuncia");
        } else {
            this.selectedAdjunto = new PinParteAdjunto();
            this.selectedAdjunto.setParte(selectedParte);
            this.uploadedFile = null;
            utilities.executeUI("PF('dlgAnadirAdjunto').show();");
        }
    }

    public void resetSelectedResenaPersona(Integer participacion) {
        this.nuevoElemento = true;

        if (participacion != null) {
            try {
                PinMiembro part = null;
                switch (participacion) {
                    case PinPersona.PARTICIPACION_DENUNCIANTE:
                        part = PinDAO.getMiembroById(Long.parseLong(utilities.getProperty("default.properties", "denunciante")));
                        this.selectedParte.setParticipacion(part);
                        break;
                    case PinPersona.PARTICIPACION_DENUNCIADO:
                        part = PinDAO.getMiembroById(Long.parseLong(utilities.getProperty("default.properties", "denunciado")));
                        this.selectedParte.setParticipacion(part);

                        break;
                    case PinPersona.PARTICIPACION_SOLICITANTE:
                        part = PinDAO.getMiembroById(Long.parseLong(utilities.getProperty("default.properties", "solicitante")));
                        this.selectedParte.setParticipacion(part);

                        break;
                }
                this.selectedResenaPersona = this.selectedParte.getSolicitante() != null ? this.selectedParte.getSolicitante() : new PinPersona();
                this.selectedResenaPersona.setParticipacion(part);
            } catch (IOException ex) {
                utilities.addFacesMessage(LOGGER, Constants.WARN, "Se ha producido un error al definir la participación de la persona que trata de reseñar.");
            }
        }
        this.selectedResenaPersona.setParte(selectedParte);

        utilities.executeUI("PF('dlgAnadirPersona').show();");

    }

    public void editandoElemento() {
        this.nuevoElemento = false;
    }

    public void resetSelectedResenaObjeto() {
        try {
            this.selectedResenaObjeto = new PinObjeto(PinDAO.getMiembroById(Long.parseLong(utilities.getProperty("/default.properties", "denunciado"))), utilities.getCurrentUser());
            this.nuevoElemento = true;

            PinMiembro sit = PinDAO.getMiembroById(Integer.valueOf(utilities.getProperty("/default.properties", "situacionInicialObjeto")));
            PinObjetoSituacion situacion = new PinObjetoSituacion(sit, utilities.getCurrentUser().getId().getUsuario());
            this.selectedResenaObjeto.addToHistoric(situacion);
            this.selectedResenaObjeto.setParte(selectedParte);
            utilities.executeUI("PF('dlgAnadirObjeto').show();");
        } catch (IOException ex) {
            utilities.addFacesMessage(LOGGER, Constants.ERROR, "Se ha producido un error al crear el objeto inicial.");
        }

    }

    public void deleteSelectedAdjunto() {

        // Además de eliminar el documento adjunto de la lista de documentos del parte, necesitamos
        // eliminar el archivo que pudiera estar pendiente de grabar en el disco si acaba de ser subido.
        // Para eso, recorremos el conjunto hasta encontrar uno con igual título que el que tratamos 
        // de eliminar.
        boolean found = false;
        Iterator it = this.filesToBeUploaded.entrySet().iterator();
        Map.Entry<String, File> entry = null;
        while (it.hasNext() && !found) {
            entry = (Map.Entry) it.next();
            if (entry.getKey().equals(String.format("%s%s", this.selectedAdjunto.getTitulo(), this.selectedAdjunto.getTipo()))) {
                found = true;
            }
        }

        // Una vez encontrado, es eliminado.
        if (entry != null) {
            this.filesToBeUploaded.remove(null);
        }

        // También debemos eliminarlo del conjunto de documentos adjuntos del parte 
        // así como el archivo relacionado de su ubicación. Como esta eliminación no se
        // llevará a cabo hasta que se acepten los cambios por parte del usuario, deberemos
        // guardar un conjunto con los archivos removidos para aplicarlos en su caso.
        this.selectedParte.getAdjuntos().remove(this.selectedAdjunto);
        if (this.selectedAdjunto.getIdAdjunto() > 0) {
            filesToBeRemoved.add(this.selectedAdjunto.getIdAdjunto() + this.selectedAdjunto.getTipo());
        }
        this.selectedAdjunto = null;
    }

    public void deleteSelectedResenaPersona() {
        this.selectedParte.getPersonas().remove(this.selectedResenaPersona);

        this.selectedResenaPersona = null;
    }

    public void deleteSelectedResenaObjeto() {
        this.selectedParte.getObjetos().remove(this.selectedResenaObjeto);

        this.selectedResenaObjeto = null;
    }

    public void checkParteEnviado() {
        if (selectedParte.getIdParte() > 0 && !selectedParte.getListoTramitar()) {
            utilities.executeUI("PF('dlgEnviarParte').show();");
        } else {
            resetNuevoParte();
        }
    }

    public void resetNuevoParte() {
        try {
            if (paginaActual.equals("parte") || paginaActual.equals("informe")) {
                this.selectedParte = new PinParteAux(Calendar.getInstance(Constants.LOCALE_SPANISH).getTime(), utilities.getCurrentUser());
            } else {
                this.selectedParte = new PinParteAux(PinDAO.getMiembroById(Long.parseLong(utilities.getProperty("/default.properties", "denunciado"))), utilities.getCurrentUser());
            }
            this.selectedParte.addOficinaUnidad(PinParte.EST_BORRADOR);

            this.selectedActuante = null;
            this.selectedPersona = null;
            this.selectedAdjunto = null;
            this.uploadedFile = null;
            this.selectedParteChecked.clear();
            this.filesToBeUploaded.entrySet().forEach((Map.Entry<String, File> entry) -> {
                entry.getValue().delete();
            });
            this.filesToBeUploaded.clear();
            resetVia();
            mapModel.getMarkers().forEach((Marker m) -> {
                m.setDraggable(true);
            });
            utilities.refresh();
        } catch (IOException ex) {
            utilities.addFacesMessage(LOGGER, Constants.ERROR, utilities.getExceptionCause(ex).toString());
        }
    }

    public void resetVia() {
        this.via = Constants.EMPTY_STR;
        if (this.vias != null) {
            this.vias.clear();
        }
        zoom = 13;
        this.selectedVia = new PinLugar();
        this.tipoNumeroLugar = -1;
        this.mapModel.getMarkers().removeAll(this.mapModel.getMarkers());
    }

    public void resetSearchFields() {
        try {
            this.patronBusqueda = new PinParteAux(PinDAO.getMiembroById(Long.parseLong(utilities.getProperty("/default.properties", "denunciado"))), utilities.getCurrentUser());
            this.patronBusqueda.setUnidad(utilities.getCurrentUser().getUnidad());
            switch (this.paginaActual) {
                case "parte":
                    this.patronBusqueda.setTipo(PinParte.PARTE);
                    break;
                case "informe":
                    this.patronBusqueda.setTipo(PinParte.INFORME);
                    break;
                case "ordenanza":
                    this.patronBusqueda.setTipo(PinParte.ORDENANZA);
                    break;
                default:
                    this.patronBusqueda.setTipo(Constants.EMPTY_STR);
            }

            this.fechaDesde = null;
            this.fechaHasta = null;
            this.actuante = new PinActuante();
            this.dni = Constants.EMPTY_STR;
            this.persona = Constants.EMPTY_STR;
        } catch (IOException ex) {
            utilities.addFacesMessage(LOGGER, Constants.ERROR, utilities.getExceptionCause(ex).toString());
        }
    }

    public void deleteSelectedActuante() {
        this.selectedParte.getActuantes().remove(this.selectedActuante);
        this.selectedActuante = null;
        this.selectedPersona = null;
    }

    public void addActuante() {
        this.selectedActuante = new PinActuante(this.selectedPersona.getPlantillaList().get(0).getCategoria(), this.selectedPersona.getPlantillaList().get(0).getIndicativo());

        if (!this.selectedParte.getActuantes().stream().anyMatch(o -> o.getIndicativo().equals(selectedActuante.getIndicativo()))) {
            this.selectedParte.addActuante(selectedActuante);
        } else {
            utilities.addFacesMessage(LOGGER, Constants.INFO, "El indicativo seleccionado ya está incluido en la lista de actuantes.");
        }
        this.selectedPersona = null;
        this.selectedActuante = null;
    }

    public void resetSelectedPersona() {
        this.selectedPersona = null;
        this.selectedActuante = null;
    }

    public void selectRow() {
        comprobarCheck();
        resetMap();

        if (selectedParte.getTipo().equals(PinParte.ACREDITO)) {
            this.subasuntos = PinDAO.getMiembros("ACR");
        }
        if (selectedParte.getIdParte() != 0L && selectedParte.getLugar().getLat() != 0.0) {
            Double lng = selectedParte.getLugar().getLng();
            Double lat = selectedParte.getLugar().getLat();
            this.center = lat.toString().concat(",").concat(lng.toString());
            this.zoom = 18;
            LatLng coord = new LatLng(lat, lng);
            this.mapModel.addOverlay(new Marker(coord, selectedParte.getNumParteFormateado()));

            this.filteredAdjuntos.clear();
            selectedParte.getAdjuntos().stream()
                    .filter((docAdjunto) -> (!docAdjunto.getTipo().equals(".jpg") && !docAdjunto.getTipo().equals(".3gp") && !docAdjunto.getTipo().equals(".mp4")))
                    .forEachOrdered((docAdjunto) -> {
                        this.filteredAdjuntos.add(docAdjunto);
                    });
        }

    }

    public void onMarkerSelect(OverlaySelectEvent event) {
        marker = (MyMarker) event.getOverlay();
    }

    public void onPointSelect(PointSelectEvent event) {

        mapModel.getMarkers().removeAll(mapModel.getMarkers());

        LatLng coord = event.getLatLng();
        selectedLat = coord.getLat();
        selectedLng = coord.getLng();
        marker = new MyMarker(coord);
        marker.setDraggable(true);
        marker.setIcon(utilities.getImagesResourcePath() + "location-red.png");
        mapModel.addOverlay(marker);
        center = String.format(Locale.ENGLISH, "%f,%f", selectedLat, selectedLng);
    }

    public void onMarkerDrag(MarkerDragEvent event) {
        marker = (MyMarker) event.getMarker();
        selectedVia.setLat(marker.getLatlng().getLat());
        selectedVia.setLng(marker.getLatlng().getLng());
        LatLng latLng = new LatLng(selectedVia.getLat(), selectedVia.getLng());
        center = String.format(Constants.LOCALE_SPANISH, "%f,%f", latLng.getLat(), latLng.getLng());
    }

    public void onStateChange(StateChangeEvent event) {
        // Tras la actualización del mapa (por inserción de un marker u otro evento), se
        // asigna a la variable zoom (que es la que gmap lee para aplicar el zoom) el 
        // valor del último zoom efectuado por el usuario.
        zoom = event.getZoomLevel();
    }

    public StreamedContent getZipFile() {

        try {
            tmpFile = File.createTempFile("tmp-parte-" + selectedParte.getNumeroParte() + "-", ".zip");
            FileOutputStream fos = new FileOutputStream(tmpFile);
            ZipOutputStream zos = new ZipOutputStream(fos);
            Calendar cal = Calendar.getInstance();
            cal.setTime(selectedParte.getFechaRegistro());
            String anno = String.valueOf(cal.get(Calendar.YEAR));

            for (PinParteAdjunto docAdjunto : selectedParte.getAdjuntos()) {

                File file = new File(Constants.FILES_PATH + anno + "/" + docAdjunto.getIdAdjunto() + docAdjunto.getTipo());
                FileInputStream fis = new FileInputStream(file);
                ZipEntry zipEntry = new ZipEntry(file.getName());
                zos.putNextEntry(zipEntry);

                byte[] bytes = new byte[1024];
                int lenght;
                while ((lenght = fis.read(bytes)) >= 0) {
                    zos.write(bytes, 0, lenght);
                }
                zos.closeEntry();
                fis.close();
            }

            zos.close();
            fos.close();

            InputStream stream = (InputStream) new FileInputStream(tmpFile);
            zipFile = new DefaultStreamedContent(stream, "application/x-compressed", "Parte " + selectedParte.getNumParteFormateado().replace('/', ' ') + ".zip");
            tmpFile.delete();
        } catch (IOException ex) {
            utilities.addFacesMessage(LOGGER, Constants.ERROR, "Se ha producido un error durante la compresión de los archivos: " + utilities.getExceptionCause(ex));
        }

        return zipFile;
    }

    public List<PerPersona> searchPersona(String patron) {
        int detalle = Constants.BASICO | Constants.PLANTILLA;

        return PinDAO.searchPersonaByIndicativo(patron, detalle);
    }

    public void resetTipoVia() {
        switch (this.tipoNumeroLugar) {
            case 0:
            case 1:
                this.numeroVia = "";
                break;
        }
    }

    public void indicarNumero() {
        if (!this.via.trim().isEmpty() && selectedVia != null) {
            String[] partesVia = via.split(",");
            if (partesVia.length > 1) {
                this.numeroVia = partesVia[1];
                this.tipoNumeroLugar = 1;
            } else {
                this.tipoNumeroLugar = 0;
                this.numeroVia = "";
            }
        } else {
            this.tipoNumeroLugar = -1;
            this.numeroVia = "";
        }
    }

    public void asignarVia(Boolean insertarNumero) {
        if (insertarNumero) {
            switch (this.tipoNumeroLugar) {
                case 0:
                    this.numeroVia = "s/n";
                    break;
                case 1:
                    this.numeroVia = "dte. núm. " + this.numeroVia;
                    break;
                case 2:
                    this.numeroVia = "fte. núm. " + this.numeroVia;
                    break;
                default:
                    this.numeroVia = "";
            }
            selectedVia.setNombreLugar(selectedVia.getNombreLugar() + ", " + numeroVia);
            this.selectedParte.setLugar(selectedVia);
            this.tipoNumeroLugar = -1;
            this.numeroVia = "";
        } else {
            this.selectedParte.setLugar(selectedVia);
        }

        LatLng latLng = new LatLng(selectedVia.getLat(), selectedVia.getLng());
        marker = new MyMarker(latLng);
        marker.setIcon(utilities.getImagesResourcePath() + "location-marker.png");
        marker.setDraggable(true);
        center = String.format(Constants.LOCALE_SPANISH, "%f,%f", latLng.getLat(), latLng.getLng());
        mapModel.getMarkers().removeAll(mapModel.getMarkers());
        mapModel.addOverlay(marker);
        zoom = 18;

    }

    public void makeDraggable() {
        mapModel.getMarkers().stream().forEachOrdered((mkr) -> {
            mkr.setIcon(utilities.getImagesResourcePath() + "location-marker.png");
            mkr.setDraggable(true);
        });
    }

    public void unselectAdjunto() {
        this.uploadedFile = null;
        this.filesToBeUploaded.entrySet().stream().filter((entry) -> (entry.getKey().equals(this.selectedAdjunto.getTitulo() + this.selectedAdjunto.getTipo()))).forEachOrdered((entry) -> {
            entry.getValue().delete();
        });
        this.selectedAdjunto = null;
    }

    public void unselectResenaPersona() {
        this.selectedResenaPersona = null;
    }

    public void unselectResenaObjeto() {
        this.selectedResenaObjeto = null;
    }

    public void addAdjunto() {
        PinParteAdjunto adjunto = new PinParteAdjunto();
        adjunto.setParte(selectedParte);
        adjunto.setTitulo("");
        adjunto.setFecha(Calendar.getInstance().getTime());

        this.selectedAdjunto = adjunto;
        // El tipo de adjunto se define a través del tipo del documento subido (pdf, doc, etc)
    }

    public void anadirAdjunto() {
        // Añadimos el objeto Adjunto creado al principio a la lista de documentos adjuntos del parte.
        if (!selectedParte.getAdjuntos().contains(selectedAdjunto)) {
            selectedParte.getAdjuntos().add(selectedAdjunto);
            filteredAdjuntos.add(selectedAdjunto);
        } else {
            utilities.addFacesMessage(LOGGER, Constants.WARN, "Ya existe un documento adjunto con el mismo nombre que el que acaba de seleccionar.");
        }
        selectedAdjunto = null;
        uploadedFile = null;
    }

    public void anadirResenaPersona() {
        if (nuevoElemento) {
            if (!selectedParte.getPersonas().contains(selectedResenaPersona)) {
                selectedResenaPersona.setParte(selectedParte);
                selectedParte.addPersona(selectedResenaPersona);
            } else {
                utilities.addFacesMessage(LOGGER, Constants.WARN, "Ya existe un persona reseñada con el mismo nombre que el que acaba de seleccionar.");
            }
        }
        selectedResenaPersona = null;
    }

    public void anadirSolicitanteAcredito() {
        if (selectedParte.getPersonas().isEmpty()) {
            selectedParte.getPersonas().add(selectedResenaPersona);
        } else {
            selectedParte.getPersonas().set(0, selectedResenaPersona);
        }
    }

    public void anadirDocumentoSolicitudAcredito() {
        if (selectedParte.getAdjuntos().isEmpty()) {
            selectedAdjunto.setParte(selectedParte);
            selectedParte.getAdjuntos().add(selectedAdjunto);
        } else {
            selectedParte.getAdjuntos().set(0, selectedAdjunto);
        }
        resetSelectedAdjunto();
    }

    public void anadirFirmanteAcredito() {
        this.selectedActuante = new PinActuante(this.selectedPersona.getPlantillaList().get(0).getCategoria(), this.selectedPersona.getPlantillaList().get(0).getIndicativo());
        this.selectedActuante.setParte(selectedParte);
        if (selectedParte.getActuantes().isEmpty()) {
            selectedParte.getActuantes().add(selectedActuante);
        } else {
            selectedParte.getActuantes().set(0, selectedActuante);
        }
        resetSelectedPersona();
    }

    public void anadirResenaObjeto() {
        if (nuevoElemento) {
            if (!selectedParte.getObjetos().contains(selectedResenaObjeto)) {
                selectedResenaObjeto.setParte(selectedParte);
                selectedParte.getObjetos().add(selectedResenaObjeto);
            } else {
                utilities.addFacesMessage(LOGGER, Constants.WARN, "Ya existe un objeto reseñado con el mismo nombre que el que acaba de seleccionar.");
            }
        }
        selectedResenaObjeto = null;
    }

    public void uploadFile(FileUploadEvent event) {
        String fileName, fileType;
        this.uploadedFile = event.getFile();

        List<String> allowedTypes;
        String tiposPermitidos;
        if (paginaActual.equals("ordenanza")) {
            allowedTypes = Arrays.asList(Constants.ALLOWED_TYPES_ORDENANZA);
            tiposPermitidos = Arrays.toString(Constants.ALLOWED_TYPES_ORDENANZA);
        } else {
            allowedTypes = Arrays.asList(Constants.ALLOWED_TYPES);
            tiposPermitidos = Arrays.toString(Constants.ALLOWED_TYPES);
        }
        if (uploadedFile != null && uploadedFile.getSize() > 0) {
            int pos = uploadedFile.getFileName().lastIndexOf('.');
            int nameLenght = uploadedFile.getFileName().length();

            if (!allowedTypes.contains(uploadedFile.getFileName().substring(pos + 1, nameLenght))) {
                // Si no es uno de los tipos permitidos, devolvemos un aviso
                utilities.addFacesMessage(LOGGER, Constants.WARN, "El archivo que trata de adjuntar no es uno de los tipos permitidos " + tiposPermitidos);
                uploadedFile = null;
                selectedAdjunto = null;
            } else if (uploadedFile.getSize() > Constants.ALLOWED_SIZE) {
                // Lo mismo hacemos si el tamaño supera el permitido.
                utilities.addFacesMessage(LOGGER, Constants.WARN, "El tamaño del archivo a adjuntar no puede ser mayor a 20MB");
                uploadedFile = null;
                selectedAdjunto = null;
            } else {
                // Si se cumplen las condiciones...

                // Creamos un objeto del tipo Adjunto a partir del nombre y tipo del archivo que se trata de subir....
                fileName = uploadedFile.getFileName().substring(0, pos);
                fileType = uploadedFile.getFileName().substring(pos, uploadedFile.getFileName().length());

                // Y un archivo temporal donde almacenar el fichero que acabamos de subir. Este archivo será eliminado
                // automáticamente por la JVM cuando ésta sea detenida, si es que el usuario no ha guardado los cambios,
                // en cuyo caso, será movido al directorio definitivo.
                try {
                    tmpFile = File.createTempFile(fileName, fileType);
                    tmpFile.deleteOnExit();

                    InputStream iStream = uploadedFile.getInputstream();
                    byte[] buffer = new byte[iStream.available()];
                    iStream.read(buffer);
                    OutputStream oStream = new FileOutputStream(tmpFile);
                    oStream.write(buffer);
                    oStream.close();

                    filesToBeUploaded.put(fileName + fileType, tmpFile);
                    this.selectedAdjunto.setFileName(fileName + fileType);
                    this.selectedAdjunto.setTipo(fileType);
                    if (useFileName) {
                        this.selectedAdjunto.setTitulo(fileName);
                    }
                } catch (IOException ex) {
                    utilities.addFacesMessage(LOGGER, Constants.ERROR, "Se ha producido un error al subir el documento adjunto. Por favor, vuelva a intentarlo.");
                }
            }
        } else {
            utilities.addFacesMessage(LOGGER, Constants.WARN, "No ha seleccionado ningún archivo a adjuntar");
            this.selectedAdjunto = null;
        }
    }

    public void useFileName() {

        if (this.uploadedFile != null && !this.useFileName) {
            if (!this.useFileName) {
                int pos = uploadedFile.getFileName().lastIndexOf('.');
                String fileName = uploadedFile.getFileName().substring(0, pos);
                String fileType = uploadedFile.getFileName().substring(pos, uploadedFile.getFileName().length());
                this.selectedAdjunto.setTitulo(fileName);
                this.selectedAdjunto.setTipo(fileType);
            }
        } else {
            this.selectedAdjunto.setTitulo(Constants.EMPTY_STR);
        }
    }

    public void generateReport() {
        if (selectedParte.getAnulado()) {
            utilities.addFacesMessage(LOGGER, Constants.INFO, "El parte está anulado.");
        } else {
            PinHistoricoImpresion movimiento = new PinHistoricoImpresion();
            try {
                // Primero generamos el movimiento para el histórico      
                movimiento.setFecha(new Date());
                movimiento.setUsuario(String.format("%s / %s", utilities.getCurrentUser().getIndicativo(), utilities.getCurrentUser().getId().getUsuario()));
                movimiento.setElemento("Parte");
                selectedParte.addToImpressionHistory(movimiento);

                // Después generamos el informe o el resumen, dependiendo de la seleccion....
                try {
                    makeReportParte();
                    esPdf = pdfFileName.substring(pdfFileName.lastIndexOf('.'), pdfFileName.length()).equals(".pdf");
                    utilities.executeUI("PF('dlgVerParte').show();");
                } catch (SQLException | NullPointerException | JRException | IOException | URISyntaxException ex) {
                    utilities.addFacesMessage(LOGGER, Constants.ERROR, utilities.getExceptionCause(ex) == null ? "Se ha producido un error durante la generación del parte" : utilities.getExceptionCause(ex).toString());
                }

                // Y si todo ha ido bien, persistimos el movimiento en la DB
                PinDAO.saveParte(selectedParte);

            } catch (PinforException ex) {
                selectedParte.getHistorico().remove(movimiento);
                utilities.addFacesMessage(LOGGER, Constants.ERROR, utilities.getExceptionCause(ex).toString());
            }
        }
    }

    public void generateOrdenTrabajoReport() {
        try {
            makeReportOrdenTrabajo();
            esPdf = pdfFileName.substring(pdfFileName.lastIndexOf('.'), pdfFileName.length()).equals(".pdf");
            utilities.executeUI("PF('dlgVerOrdenTrabajo').show();");
        } catch (SQLException | NullPointerException | JRException | IOException | URISyntaxException ex) {
            utilities.addFacesMessage(LOGGER, Constants.ERROR, utilities.getExceptionCause(ex) == null ? "Se ha producido un error durante la generación de la orden de servicio" : utilities.getExceptionCause(ex).toString());
        }

    }

    public StreamedContent getStream() throws IOException {
        ExternalContext ec = utilities.getExternalContext();
        ec.addResponseHeader("Cache-Control", "no-cache");
        ec.addResponseHeader("Pragma", "no-cache");
        ec.addResponseHeader("Expires", "-1");
        StreamedContent sc = null;
        try {
            if (esPdf) {
                sc = new DefaultStreamedContent(new FileInputStream(pdfFileName), "application/pdf", selectedParte.getNumParteFormateado().replace('/', ' '));
            } else {
                sc = new DefaultStreamedContent(new FileInputStream(pdfFileName), "application/jpeg", selectedParte.getNumParteFormateado().replace('/', ' '));
            }
        } catch (FileNotFoundException ex) {
            System.out.println(ex.getMessage());
        }
        return sc;
    }

    public Boolean getPuedeVer() {
        return selectedParte.getOrdenServicio() == null || selectedParte.getOrdenServicio().getIdOrdenTrabajo() < 0;
    }

    public void resetPatronBusqueda() {
        resetSearchFields();
        this.patronBusqueda.setTipo(PinParte.PARTE);
    }

    public void crearOrdenanza() {
        this.accion = "Crear";
        this.selectedParte = new PinParteAux("OM");
        this.selectedParte.setFechaRegistro(Calendar.getInstance(Constants.LOCALE_SPANISH).getTime());
        this.selectedParte.setUnidad(utilities.getCurrentUser().getUnidad());
        this.selectedParte.addOficinaUnidad(PinParte.EST_LISTO_TRAMITAR);
        //this.selectedParte.setArchivado(true);
        makeDraggable();
    }

    public void editarOrdenanza() {
        this.accion = "Editar";
        makeDraggable();
    }

    public void crearAcredito() {

        this.accion = "Crear";
        this.selectedParteChecked.clear();
        this.subasuntos = PinDAO.getMiembros("ACR");

        PinLugar oficina = new PinLugar();
        oficina.setNombreLugar(String.format("Oficina de %s", utilities.getCurrentUser().getUnidad().getNombre()));
        this.selectedParte = new PinParteAux(Calendar.getInstance(Constants.LOCALE_SPANISH).getTime(), utilities.getCurrentUser());
        this.selectedParte.setTipo("AC");
        this.selectedParte.addOficinaUnidad(PinParte.EST_LISTO_TRAMITAR);
        this.selectedParte.setUnidad(utilities.getCurrentUser().getUnidad());
        this.selectedParte.addEstado(PinParte.EST_ARCHIVADO);
        this.selectedParte.setLugar(oficina);
        this.selectedParte.setAsunto(PinDAO.getMiembroCabeceraDeSublista("ACR"));

    }

    public void editarAcredito() {
        this.accion = "Editar";
    }

    public void setPaginaActual(String paginaActual) {
        try {
            this.paginaActual = paginaActual;
            this.patronBusqueda = new PinParteAux(PinDAO.getMiembroById(Long.parseLong(utilities.getProperty("/default.properties", "denunciado"))), utilities.getCurrentUser());
            this.lazyResultadoBusqueda = null;
            if (paginaActual.equals("parte") || paginaActual.equals("informe")) {
                this.selectedParte = new PinParteAux(Calendar.getInstance(Constants.LOCALE_SPANISH).getTime(), utilities.getCurrentUser());
            } else {
                this.selectedParte = new PinParteAux(PinDAO.getMiembroById(Long.parseLong(utilities.getProperty("/default.properties", "denunciado"))), utilities.getCurrentUser());
            }
            this.selectedParte.addOficinaUnidad(PinParte.EST_BORRADOR);
        } catch (IOException ex) {
            utilities.addFacesMessage(LOGGER, Constants.ERROR, utilities.getExceptionCause(ex).toString());
        }
    }

    public void verSelectedAdjunto(Boolean seleccionarAdjunto) {

        PinHistoricoImpresion movimiento = new PinHistoricoImpresion();
        try {
            // Primero generamos el movimiento para el histórico      

            if (seleccionarAdjunto) {
                if (!selectedParte.getAdjuntos().isEmpty()) {
                    selectedAdjunto = selectedParte.getAdjuntos().get(0);
                } else {
                    throw new PinforException("La reseña seleccionada no tiene su correspondiente acta de denuncia adjunta.");
                }
            }

            movimiento.setFecha(new Date());
            movimiento.setUsuario(String.format("%s / %s", utilities.getCurrentUser().getIndicativo(), utilities.getCurrentUser().getId().getUsuario()));
            movimiento.setElemento("Adjunto: " + selectedAdjunto.getTitulo());
            selectedParte.addToImpressionHistory(movimiento);

            Calendar cal = Calendar.getInstance();
            cal.setTime(selectedAdjunto.getParte().getFechaRegistro());
            int year = cal.get(Calendar.YEAR);
            File path = new File(utilities.getProperty("/default.properties", "filesPath") + "/" + year);

            pdfFileName = String.format("%s\\%s%s", path, selectedAdjunto.getIdAdjunto(), selectedAdjunto.getTipo());
            File pdfFile = new File(pdfFileName);
            esPdf = pdfFileName.substring(pdfFileName.lastIndexOf('.'), pdfFileName.length()).equals(".pdf");

            if (!pdfFile.exists()) {
                throw new PinforException("No se ha podido localizar el archivo correspondiente al acta solicitada.");
            }
            // Y si todo ha ido bien, persistimos el movimiento en la DB
            PinDAO.saveParte(selectedParte);
            if (esPdf) {
                utilities.executeUI("PF('dlgVerParte').show();");
            } else {
                utilities.executeUI("PF('dlgVerImagen').show();");
            }

        } catch (PinforException ex) {
            selectedParte.getHistorico().remove(movimiento);
            utilities.addFacesMessage(LOGGER, Constants.INFO, ex.getMessage());
        } catch (IOException ex) {
            selectedParte.getHistorico().remove(movimiento);
            utilities.addFacesMessage(LOGGER, Constants.ERROR, ex.getMessage());
        }
    }

    public void recargarDestinatarios() {
        this.selectedParte.getDestinos().removeIf(d -> d.getEstados().isEmpty());
        if (this.selectedParte.getSubasunto() != null) {
            this.selectedParte.getSubasunto().getDestinos().forEach((destinatario) -> {
                this.selectedParte.addDestinatario(new PinDestino(destinatario));
            });
        }
//        this.selectedParte.addOficinaUnidad(PinParte.EST_BORRADOR);
    }

    public void resetDestinatarios() {
        this.selectedParte.getDestinos().removeIf(d -> d.getEstados().isEmpty());
    }

    public void resetSelectedDestinatarioDisponible() {
        selectedDestinatarioDisponible = null;
    }

    public void resetSelectedDestinatarioAsignado() {
        selectedDestinatarioAsignado = null;
    }

    public void anadirAsignado() {
        // Debemos obtener un idDestino temporar para el nuevo destinatario.
        PinDestino destinatario = new PinDestino(getTmpIdDestino(), selectedDestinatarioDisponible);
        selectedParte.addDestinatario(destinatario);
        selectedDestinatarioDisponible = null;
    }

    public void eliminarAsignado() {
        if (selectedDestinatarioAsignado.getDestino().equals(selectedParte.getUnidad())) {
            utilities.addFacesMessage(LOGGER, Constants.INFO, "No puede eliminar a la oficina de su propia Unidad de la lista de destinatarios.");
        } else {
            selectedParte.getDestinos().remove(selectedDestinatarioAsignado);
            selectedDestinatarioAsignado = null;
        }
    }

    public void makeCopyDestinosAsignados() {
        /* Debemos definir una copia de la lista original de destinos asignados por si el usuario decide
        cancelar la operación de modificación, en cuyo caso debemos devolver la lista de destinatarios
        asignados a su estado original. */
        copiaDestinatariosAsignados = selectedParte.getDestinos().stream().collect(Collectors.toList());
    }

    public void modifyDestinos() {
        //   PinDAO.saveMiembro(selectedParte.getSubasunto());
        copiaDestinatariosAsignados = null;
    }

    public void resetDestinos() {
        // Aquí es donde se devuelve la lista de destinatarios asignados a su valor inicial 
        selectedParte.setDestinos(copiaDestinatariosAsignados);
        copiaDestinatariosAsignados = null;
    }

    public void selectCentroTrabajo() {
        selectedVia = new PinLugar();
        selectedVia.setTipoVia(utilities.getCurrentUser().getCentroTrabajo().getUbicacion().getTipoVia());
        selectedVia.setNombreLugar(utilities.getCurrentUser().getCentroTrabajo().getUbicacion().getNombreLugar());
        selectedVia.setLat(utilities.getCurrentUser().getCentroTrabajo().getUbicacion().getLat());
        selectedVia.setLng(utilities.getCurrentUser().getCentroTrabajo().getUbicacion().getLng());
        selectedVia.setSrid(utilities.getCurrentUser().getCentroTrabajo().getUbicacion().getSrid());

        this.selectedParte.setLugar(selectedVia);

        LatLng latLng = new LatLng(selectedVia.getLat(), selectedVia.getLng());
        marker = new MyMarker(latLng);
        marker.setIcon(utilities.getImagesResourcePath() + "location-marker.png");
        marker.setDraggable(true);
        center = String.format(Constants.LOCALE_SPANISH, "%f,%f", latLng.getLat(), latLng.getLng());
        mapModel.getMarkers().removeAll(mapModel.getMarkers());
        mapModel.addOverlay(marker);
        zoom = 18;
    }

    public String getPersonaPhoto() {

        String result = Constants.PHOTO_PATH.concat("empty_profile.jpg");
        if (selectedPersona != null) {
            String url = Constants.PHOTO_PATH.concat(String.valueOf(selectedPersona.getIdPersona()).concat(".jpg"));
            if (utilities.resourceExists(url)) {
                result = url;
            }
        }
        return result;
    }

    private long getTmpIdDestino() {
        long result = 0;
        for (PinDestino destino : selectedParte.getDestinos()) {
            if (destino.getIdDestino() < result) {
                result = destino.getIdDestino();
            }
        }
        return --result;
    }
}
