package org.svq.pol.pinfor.backing;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.Serializable;
import java.net.URISyntaxException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;
import org.apache.log4j.Logger;
import org.omnifaces.cdi.ViewScoped;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.svq.pol.exception.PinforException;
import org.svq.pol.pinfor.bean.auxiliar.ParteReport;
import org.svq.pol.pinfor.bean.auxiliar.PinParteAux;
import org.svq.pol.pinfor.dao.PinDAO;
import org.svq.pol.pinfor.utility.Constants;

/**
 *
 * @author jjcampac
 */
@ViewScoped
@Named
public class PinListadosBacking extends AbstractBacking implements Serializable {

    private static final Logger LOGGER = Logger.getLogger(PinListadosBacking.class);
    private static final long serialVersionUID = 7114494981277029292L;

    private static final String[] LISTA_ESTADOS_TRAMITACION = new String[]{"Todos", "Borradores", "Archivado", "Pendientes envío a Estafeta", "Pendientes tramitación por Estafeta", "Entregados en destino"};

    private static String reportsPath;

    private String pdfFileName;
    private Date fechaDesde, fechaHasta, hoy;
    private boolean incluirBorradores;
    private String tipo, subtipo;
    private List<String> subtipos;
    private boolean mostrarDatos, group;

//<editor-fold defaultstate="collapsed" desc="constructors">
    public PinListadosBacking() {
        this.hoy = Calendar.getInstance(Constants.LOCALE_SPANISH).getTime();
        this.tipo = "-1";
        this.mostrarDatos = false;
        this.group = true;
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="getters & setters">
    public String getPdfFileName() {
        return pdfFileName;
    }

    public void setPdfFileName(String pdfFileName) {
        this.pdfFileName = pdfFileName;
    }

    public Date getFechaDesde() {
        return fechaDesde;
    }

    public void setFechaDesde(Date fechaDesde) {
        this.fechaDesde = fechaDesde;
    }

    public Date getFechaHasta() {
        return fechaHasta;
    }

    public void setFechaHasta(Date fechaHasta) {
        this.fechaHasta = fechaHasta;
    }

    public Date getHoy() {
        return hoy;
    }

    public void setHoy(Date hoy) {
        this.hoy = hoy;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getSubtipo() {
        return subtipo;
    }

    public void setSubtipo(String subtipo) {
        this.subtipo = subtipo;
    }

    public List<String> getSubtipos() {
        return subtipos;
    }

    public void setSubtipos(List<String> subtipos) {
        this.subtipos = subtipos;
    }

    public boolean isMostrarDatos() {
        return mostrarDatos;
    }

    public void setMostrarDatos(boolean mostrarDatos) {
        this.mostrarDatos = mostrarDatos;
    }

    public boolean isGroup() {
        return group;
    }

    public void setGroup(boolean group) {
        this.group = group;
        this.subtipo = null;
    }

    public boolean isIncluirBorradores() {
        return incluirBorradores;
    }

    public void setIncluirBorradores(boolean incluirBorradores) {
        this.incluirBorradores = incluirBorradores;
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="private methods">
    private <T> List<T> seekReportData() throws IOException {
        List<T> result = new ArrayList<>();
        List<T> data;
        switch (this.tipo) {
            case Constants.LISTADO_ASUNTOS:
                data = (List<T>) PinDAO.listadoPorAsuntos(this.subtipo, this.fechaDesde, this.fechaHasta, this.incluirBorradores);
                for (T parte : data) {
                    result.add((T) new ParteReport((PinParteAux) parte));
                }
                break;

            case Constants.LISTADO_CATEGORIAS:
                data = (List<T>) PinDAO.listadoPorCategorias(this.subtipo, this.fechaDesde, this.fechaHasta, this.incluirBorradores);
                for (T actuante : data) {
                    result.add(actuante);
                }
                break;

            case Constants.LISTADO_DESTINATARIOS:
                data = (List<T>) PinDAO.listadoPorDestinatarios(this.subtipo, this.fechaDesde, this.fechaHasta, this.incluirBorradores);
                for (T destino : data) {
                    result.add(destino);
                }
                break;

            case Constants.LISTADO_ESTADOS_TRAMITACION:
                data = (List<T>) PinDAO.listadoPorEstadoTramitacion(this.subtipo, this.fechaDesde, this.fechaHasta);
                for (T parte : data) {
                    result.add((T) new ParteReport((PinParteAux) parte));
                }
                break;

            default:
                result = new ArrayList<>();
        }

        return result;
    }

    private <T> void makeReport(List<T> data) throws SQLException, JRException, IOException, URISyntaxException, PinforException {
        reportsPath = utilities.getExternalContext().getRealPath("/").concat(Constants.REPORTS_PATH);

        File pdfFile = File.createTempFile(this.tipo, ".pdf");
        pdfFile.deleteOnExit();
        pdfFileName = pdfFile.getAbsolutePath();

        JasperPrint jasperPrint;
        JasperReport jrAtestado;
        JRBeanCollectionDataSource jrDataSource = null;

        String jasperFileAtestado = null;
        HashMap parameters = new HashMap();
        parameters.put("FECHA_DESDE", this.fechaDesde);
        parameters.put("FECHA_HASTA", this.fechaHasta);
        parameters.put("TIPO", this.tipo);
        parameters.put("SUBTIPO", this.subtipo.equals("Todos") ? null : this.subtipo);
        parameters.put("UNIDAD", utilities.getCurrentUser().getUnidad());

        switch (this.tipo) {
            case Constants.LISTADO_ASUNTOS:
                jasperFileAtestado = reportsPath.concat("listadoAsuntos.jasper");
                jrDataSource = new JRBeanCollectionDataSource((List<ParteReport>) data);
                break;

            case Constants.LISTADO_CATEGORIAS:
                jasperFileAtestado = reportsPath.concat("listadoCategorias.jasper");
                jrDataSource = new JRBeanCollectionDataSource((List<ParteReport>) data);
                break;

            case Constants.LISTADO_DESTINATARIOS:
                jasperFileAtestado = reportsPath.concat("listadoDestinatarios.jasper");
                jrDataSource = new JRBeanCollectionDataSource((List<ParteReport>) data);
                break;

            case Constants.LISTADO_ESTADOS_TRAMITACION:
                jasperFileAtestado = reportsPath.concat("listadoEstadoTramitacion.jasper");
                jrDataSource = new JRBeanCollectionDataSource((List<ParteReport>) data);
                break;
        }

        if (jasperFileAtestado != null) {
            if (!new File(jasperFileAtestado).exists()) {
                compileJasperFiles();
            }
            jrAtestado = (JasperReport) JRLoader.loadObject(new BufferedInputStream(new FileInputStream(jasperFileAtestado)));
            jasperPrint = JasperFillManager.fillReport(jrAtestado, parameters, jrDataSource);
            JasperExportManager.exportReportToPdfFile(jasperPrint, pdfFileName);
        } else {
            throw new PinforException("Se ha producido un error durante la generación del listado.");
        }

    }

    private static void compileJasperFiles() throws IOException, JRException {
        File folder = new File(reportsPath);
        FilenameFilter jasperFilter = (File directory, String fileName) -> fileName.endsWith(".jrxml");

        File[] xmlFiles = folder.listFiles(jasperFilter);
        for (File file : xmlFiles) {
            String xmlFile = file.getCanonicalPath();
            String jasperFile = file.getCanonicalPath().replace(".jrxml", ".jasper");
            JasperCompileManager.compileReportToFile(xmlFile, jasperFile);
        }
    }
//</editor-fold>



    public StreamedContent getStream() throws IOException {
        FacesContext fc = utilities.getFacesContext();
        ExternalContext ec = fc.getExternalContext();
        ec.addResponseHeader("Cache-Control", "no-cache");
        ec.addResponseHeader("Pragma", "no-cache");
        ec.addResponseHeader("Expires", "-1");
        ec.addResponseHeader("Title", this.tipo.concat(".pdf"));
        StreamedContent sc = null;
        try {
            sc = new DefaultStreamedContent(new FileInputStream(pdfFileName), "application/pdf", "Listado ".concat(this.tipo.concat(".pdf")));
        } catch (FileNotFoundException ex) {
            System.out.println(ex.getMessage());
        }
        return sc;
    }

    public void onSelectTipo() {
        this.subtipos = new ArrayList<>();
        switch (this.tipo) {
            case Constants.LISTADO_ASUNTOS:
                List<String> asuntos = new ArrayList<>();
                asuntos.add("Todos");
                PinDAO.getMiembros("ASU").forEach((asunto) -> {
                    asuntos.add(asunto.getNombre());
                });
                this.subtipos = asuntos;
                this.subtipo = "-1";
                break;

            case Constants.LISTADO_CATEGORIAS:
                List<String> categorias = new ArrayList<>();
                categorias.add("Todos");
                PinDAO.getMiembrosByLista("CAT").forEach((categoria) -> {
                    categorias.add(categoria.getNombre());
                });
                this.subtipos = categorias;
                this.subtipo = "-1";
                break;

            case Constants.LISTADO_DESTINATARIOS:
                List<String> destinatarios = new ArrayList<>();
                destinatarios.add("Todos");
                PinDAO.getMiembros("DES").forEach((destinatario) -> {
                    destinatarios.add(destinatario.getNombre());
                });
                this.subtipos = destinatarios;
                this.subtipo = "-1";
                break;

            case Constants.LISTADO_ESTADOS_TRAMITACION:
                this.subtipos = Arrays.asList(LISTA_ESTADOS_TRAMITACION);
                this.subtipo = "-1";
                break;
            default:
                this.tipo = "-1";
                this.subtipo = "-1";
        }
    }

    public <T> void generateReport() {
        /* Puesto que este método se utiliza para la generación de distintos tipos de informes, 
         cada uno con un tipo de dato diferente, necesitamos usar tipos genéricos para definirlo.
         Lo mismo sucede con los métodos llamados (seekReportData que se encarga de buscar los 
         datos y makeReport, que se encarga de confeccionar el informe con los datos recibidos.*/
        try {
            List<T> listado = seekReportData();
            makeReport(listado);
        } catch (SQLException | JRException | URISyntaxException | IOException ex) {
            utilities.addFacesMessage(LOGGER, Constants.ERROR, utilities.getExceptionCause(ex).toString());
        } catch (PinforException ex) {
            utilities.addFacesMessage(LOGGER, Constants.ERROR, ex.getMessage());
        }
    }

}
