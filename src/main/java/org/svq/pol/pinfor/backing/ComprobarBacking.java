package org.svq.pol.pinfor.backing;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Named;
import org.apache.log4j.Logger;
import org.omnifaces.cdi.ViewScoped;
import org.svq.pol.pinfor.bean.InfContenidoPerfil;
import org.svq.pol.pinfor.bean.InfPerfil;
import org.svq.pol.pinfor.bean.PerPersona;
import org.svq.pol.pinfor.bean.auxiliar.PatronBusqueda;
import org.svq.pol.pinfor.dao.PinDAO;
import org.svq.pol.pinfor.utility.Constants;

/**
 *
 * @author jjcamacho
 */
@ViewScoped
@Named
public class ComprobarBacking extends AbstractBacking implements Serializable {

    private static final long serialVersionUID = 5302530028086168718L;
    private static final Logger LOGGER = Logger.getLogger(ComprobarBacking.class);
    
    private PerPersona usuario;
    private List<InfContenidoPerfil> contenido;
    private PatronBusqueda patronBusqueda;

    //<editor-fold defaultstate="collapsed" desc="constructors">
    public ComprobarBacking() {
        usuario = null;
        contenido = new ArrayList<>();
        patronBusqueda = new PatronBusqueda();
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="getters & setters">
    public PerPersona getUsuario() {
        return usuario;
    }

    public void setUsuario(PerPersona usuario) {
        this.usuario = usuario;
    }

    public List<InfContenidoPerfil> getContenido() {
        return contenido;
    }

    public void setContenido(List<InfContenidoPerfil> contenido) {
        this.contenido = contenido;
    }

    public PatronBusqueda getPatronBusqueda() {
        return patronBusqueda;
    }

    public void setPatronBusqueda(PatronBusqueda patronBusqueda) {
        this.patronBusqueda = patronBusqueda;
    }
    //</editor-fold>

    public void search() {
        try {
            usuario = new PerPersona();
            int detalle = Constants.BASICO | Constants.INFO | Constants.PLANTILLA | Constants.UNIDAD;
            contenido.clear();
            patronBusqueda.setUsuario(patronBusqueda.getUsuario().replace("%", ""));
            patronBusqueda.setIndicativo(patronBusqueda.getIndicativo().replace("%", ""));

            if (patronBusqueda.getUsuario().trim().isEmpty() && patronBusqueda.getIndicativo().trim().isEmpty()) {
                utilities.addFacesMessage(LOGGER, Constants.INFO, "Debe indicar un criterio de búsqueda.");
            } else {
                usuario = PinDAO.getUser(patronBusqueda, detalle);
                if (usuario == null) {
                    utilities.addFacesMessage(LOGGER, Constants.INFO,  "No se han encontrado resultados que coincidan con el patrón de búsqueda");
                } else {
                    for (InfPerfil per : usuario.getPerfiles()) {
                        for (InfContenidoPerfil cont : per.getContenidoPerfiles()) {
                            contenido.add(cont);
                        }
                    }
                }
                utilities.executeUI("PF('dlgBuscarUsuario').hide();");
            }
        } catch (Exception ex) {
            utilities.addFacesMessage(LOGGER, Constants.ERROR,  "Se ha producido un error en la consulta: " + utilities.getExceptionCause(ex));
        }
    }
}
