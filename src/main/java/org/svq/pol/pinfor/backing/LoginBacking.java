package org.svq.pol.pinfor.backing;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.event.ComponentSystemEvent;
import javax.inject.Inject;
import javax.inject.Named;
import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.directory.InitialDirContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.svq.pol.exception.PinforException;
import org.svq.pol.exception.UserNotAllowedException;
import org.svq.pol.pinfor.bean.GenSessionUser;
import org.svq.pol.pinfor.bean.InfAplicacion;
import org.svq.pol.pinfor.bean.PerPersona;
import org.svq.pol.pinfor.dao.PinDAO;
import org.svq.pol.pinfor.utility.Constants;
import org.svq.pol.pinfor.utility.HibernateUtil;
import org.svq.pol.pinfor.utility.Log;
import org.svq.pol.pinfor.utility.Utilities;

/**
 *
 * @author jjcampac
 */
@SessionScoped
@Named
public class LoginBacking implements Serializable {

    private static final long serialVersionUID = 6844981579495786711L;
    private static final Logger LOGGER = Logger.getLogger(LoginBacking.class);

    @Inject
    private Utilities utilities;

    private GenSessionUser sessionUser;
    private String password;

    //<editor-fold defaultstate="collapsed" desc="constructores">
    public LoginBacking() {

    }

    @PostConstruct
    private void onConstruct() {
        try {
            sessionUser = new GenSessionUser(utilities.getProperty("/default.properties", "dominio"));
        } catch (IOException ex) {
            Log.log(LOGGER, Constants.FATAL, "Excepcion inicializando LoginBacking: " + ex.getMessage());
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="getters & setters">
    public GenSessionUser getSessionUser() {
        return sessionUser;
    }

    public void setSessionUser(GenSessionUser sessionUser) {
        this.sessionUser = sessionUser;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    //</editor-fold>

    /**
     * Efectúa el proceso de logado ante el directorio activo definido en el
     * archivo de configuración y con los datos recibidos de un formuario de
     * identificación.
     *
     * @return String - Cadena nula si se ha producido un fallo en la
     * identificación o el texto <code>success</code> si la misma ha sido
     * correcta. Éste pude usarse en la navegación definida en faces-config.xml
     */
    public String login() {
        /*
         * Se define y crea el conjunto al que se añadiran los parametros
         * correspondientes a la conexión LDAP, que no es más que un objeto de
         * la clase InitialDirContext que implementa la interfaz DirContext.
         */
        String resultado = null;
        Hashtable<String, String> env = new Hashtable<>();
        String ldapServer;
        try {
            ldapServer = utilities.getProperty("/default.properties", "ldapserver");
            env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
            env.put(Context.PROVIDER_URL, ldapServer);
            env.put(Context.SECURITY_AUTHENTICATION, "simple");
            env.put(Context.SECURITY_PRINCIPAL, sessionUser.getDominio() + "\\" + sessionUser.getId().getUsuario());
            env.put(Context.SECURITY_CREDENTIALS, getPassword());
            /*
             * Al instanciar un objeto del tipo InitialDirContext se efectúa la
             * validación de los datos del usuario contra el directorio activo.
             * Si los datos (domino \ usuario y contraseña) son validos, se
             * producirá la creación del objeto (aunque finalmente no se usará).
             * En caso contrario, se lanzará una excepción que se recogerá y
             * pondrá en la cola de mensajes de Faces.
             */
            new InitialDirContext(env);

            /*
             * Si la ejecución del método llega hasta aquí será señal que la 
             * identificación LDAP  se ha llevado a cabo correctamente (si no, se hubiera
             * lanzado una excepción del tipo NamingException). Ya solo queda
             * establecer el usuario en la sesión y devolver la cadena "success"
             * para ser valorada por el NavigationHandler.  
             * Se establece  la variable global usuario para que pueda ser accesible 
             * desde otras aplicaciones del sistema. */
            utilities.setCurrentUser(sessionUser);
            utilities.setCookies(sessionUser, false);
            resultado = "success";
        } catch (FileNotFoundException ex) {
            utilities.addFacesMessage(LOGGER, Constants.FATAL, "No se encontró el archivo de propiedades de la aplicación");
        } catch (IOException ex) {
            utilities.addFacesMessage(LOGGER, Constants.FATAL, "No se encontró el archivo de propiedades de la aplicación");
        } catch (NamingException ex) {
            utilities.addFacesMessage(LOGGER, Constants.WARN, "Usuario y/o contraseña incorrectos");
        } catch (UserNotAllowedException | NoSuchAlgorithmException ex) {
            utilities.addFacesMessage(LOGGER, Constants.ERROR, ex.getMessage());
        }
        return resultado;
    }

    public void logout() {
        try {
            utilities.setCookies(sessionUser, true);
            utilities.cancelUser(sessionUser);
        } catch (FileNotFoundException ex) {
            utilities.addFacesMessage(LOGGER, Constants.ERROR, ex.getMessage());
        } catch (IOException ex) {
            utilities.addFacesMessage(LOGGER, Constants.ERROR, ex.getMessage());
        }
    }

    /**
     * El método efectúa una invalidación de la sesión activa y fuerza la
     * redirección a la página del portal.
     */
    public void home() {
        HttpServletRequest request = utilities.getRequest();
        String servidor = request.getServerName();
        String puerto = Constants.PORTAL_PUERTO;
        try {
            utilities.getExternalContext().redirect("http://".concat(servidor).concat(":").concat(puerto).concat("/portal/"));
        } catch (IOException ex) {
            utilities.addFacesMessage(LOGGER, Constants.ERROR, "No se pudo efectuar la redirección por " + utilities.getExceptionCause(ex));
        }
    }

}
