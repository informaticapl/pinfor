package org.svq.pol.pinfor.gson;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;

/**
 *
 * @author jjcampac
 */
public class GsonExclusionStrategy implements ExclusionStrategy {

    @Override
    public boolean shouldSkipField(FieldAttributes field) {
        boolean result;
        switch (field.getName()) {
            case "codigoEt":
            case "fechaEnvioDgt":
            case "usuarioEnvioDgt":
            case "numeroAltaDgt":
            case "enviadoDgt":
            case "cerradoDgt":
            case "noDgt":
            case "hayCambiosPendientesDgt":
                result = true;
                break;
            default:
                result = false;
        }
        return result;
    }

    @Override
    public boolean shouldSkipClass(Class<?> arg0) {
        return false;
    }

}
